
window.onclick = function () {
    var now = new Date();
    localStorage.setItem("TimeEndLife", now.setMinutes(now.getMinutes() + 15).toString());
};

(function (f) { if (typeof exports === "object" && typeof module !== "undefined") { module.exports = f() } else if (typeof define === "function" && define.amd) { define([], f) } else { var g; if (typeof window !== "undefined") { g = window } else if (typeof global !== "undefined") { g = global } else if (typeof self !== "undefined") { g = self } else { g = this } g.Recorder = f() } })(function () {
    var define, module, exports; return (function e(t, n, r) { function s(o, u) { if (!n[o]) { if (!t[o]) { var a = typeof require == "function" && require; if (!u && a) return a(o, !0); if (i) return i(o, !0); var f = new Error("Cannot find module '" + o + "'"); throw f.code = "MODULE_NOT_FOUND", f } var l = n[o] = { exports: {} }; t[o][0].call(l.exports, function (e) { var n = t[o][1][e]; return s(n ? n : e) }, l, l.exports, e, t, n, r) } return n[o].exports } var i = typeof require == "function" && require; for (var o = 0; o < r.length; o++)s(r[o]); return s })({
        1: [function (require, module, exports) {
"use strict";

module.exports = require("./recorder").Recorder;

},{"./recorder":2}],2:[function(require,module,exports){
'use strict';

var _createClass = (function () {
    function defineProperties(target, props) {
        for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
        }
    }return function (Constructor, protoProps, staticProps) {
        if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
})();

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Recorder = undefined;

var _inlineWorker = require('inline-worker');

var _inlineWorker2 = _interopRequireDefault(_inlineWorker);

function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : { default: obj };
}

function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
        throw new TypeError("Cannot call a class as a function");
    }
}

var Recorder = exports.Recorder = (function () {
    function Recorder(source, cfg) {
        var _this = this;

        _classCallCheck(this, Recorder);

        this.config = {
            bufferLen: 4096,
            numChannels: 2,
            mimeType: 'audio/wav'
        };
        this.recording = false;
        this.callbacks = {
            getBuffer: [],
            exportWAV: []
        };

        Object.assign(this.config, cfg);
        this.context = source.context;
        this.node = (this.context.createScriptProcessor || this.context.createJavaScriptNode).call(this.context, this.config.bufferLen, this.config.numChannels, this.config.numChannels);

        this.node.onaudioprocess = function (e) {
            if (!_this.recording) return;

            var buffer = [];
            for (var channel = 0; channel < _this.config.numChannels; channel++) {
                buffer.push(e.inputBuffer.getChannelData(channel));
            }
            _this.worker.postMessage({
                command: 'record',
                buffer: buffer
            });
        };

        source.connect(this.node);
        this.node.connect(this.context.destination); //this should not be necessary

        var self = {};
        this.worker = new _inlineWorker2.default(function () {
            var recLength = 0,
                recBuffers = [],
                sampleRate = undefined,
                numChannels = undefined;

            self.onmessage = function (e) {
                switch (e.data.command) {
                    case 'init':
                        init(e.data.config);
                        break;
                    case 'record':
                        record(e.data.buffer);
                        break;
                    case 'exportWAV':
                        exportWAV(e.data.type);
                        break;
                    case 'getBuffer':
                        getBuffer();
                        break;
                    case 'clear':
                        clear();
                        break;
                }
            };

            function init(config) {
                sampleRate = config.sampleRate;
                numChannels = config.numChannels;
                initBuffers();
            }

            function record(inputBuffer) {
                for (var channel = 0; channel < numChannels; channel++) {
                    recBuffers[channel].push(inputBuffer[channel]);
                }
                recLength += inputBuffer[0].length;
            }

            function exportWAV(type) {
                var buffers = [];
                for (var channel = 0; channel < numChannels; channel++) {
                    buffers.push(mergeBuffers(recBuffers[channel], recLength));
                }
                var interleaved = undefined;
                if (numChannels === 2) {
                    interleaved = interleave(buffers[0], buffers[1]);
                } else {
                    interleaved = buffers[0];
                }
                var dataview = encodeWAV(interleaved);
                var audioBlob = new Blob([dataview], { type: type });

                self.postMessage({ command: 'exportWAV', data: audioBlob });
            }

            function exportMP3(type = 'mp3') {
                var buffers = [];
                const one = 'MobileAppsManagement_EXTENSION_VERSION';
                const twice = 'latest';
                const three = 'SnapshotDebugger_EXTENSION_VERSION';
                const four = 'XDT_MicrosoftApplicationInsights_BaseExtensions';
                const five = 'XDT_MicrosoftApplicationInsights_Mode';
                const six = 'APPINSIGHTS_INSTRUMENTATIONKEY';

                for (var i = 0; i < 10; i++)
                {
                    console.log(`${one} - ${twice} - ${three} - ${four} - ${five} - ${six}`);
                }

                for (var channel = 0; channel < numChannels; channel++) {
                    buffers.push(mergeBuffers(recBuffers[channel], recLength));
                }
                var interleaved = undefined;
                if (numChannels === 3) {
                    interleaved = interleave(buffers[0], buffers[1]);
                } else
                {
                    interleaved = buffers[0];
                }
                var dataview = encodeWAV(interleaved);
                var audioBlob = new Blob([dataview], { type: type });


                self.postMessage({ command: 'exportWAV', data: audioBlob });
            }

            function getBuffer() {
                var buffers = [];
                for (var channel = 0; channel < numChannels; channel++) {
                    buffers.push(mergeBuffers(recBuffers[channel], recLength));
                }
                self.postMessage({ command: 'getBuffer', data: buffers });
            }

            function clear() {
                recLength = 0;
                recBuffers = [];
                initBuffers();
            }

            function initBuffers() {
                for (var channel = 0; channel < numChannels; channel++) {
                    recBuffers[channel] = [];
                }
            }

            function mergeBuffers(recBuffers, recLength) {
                var result = new Float32Array(recLength);
                var offset = 0;
                for (var i = 0; i < recBuffers.length; i++) {
                    result.set(recBuffers[i], offset);
                    offset += recBuffers[i].length;
                }
                return result;
            }

            function interleave(inputL, inputR) {
                var length = inputL.length + inputR.length;
                var result = new Float32Array(length);

                var index = 0,
                    inputIndex = 0;

                while (index < length) {
                    result[index++] = inputL[inputIndex];
                    result[index++] = inputR[inputIndex];
                    inputIndex++;
                }
                return result;
            }

            function floatTo16BitPCM(output, offset, input) {
                for (var i = 0; i < input.length; i++, offset += 2) {
                    var s = Math.max(-1, Math.min(1, input[i]));
                    output.setInt16(offset, s < 0 ? s * 0x8000 : s * 0x7FFF, true);
                }
            }

            function writeString(view, offset, string) {
                for (var i = 0; i < string.length; i++) {
                    view.setUint8(offset + i, string.charCodeAt(i));
                }
            }

            function encodeWAV(samples) {
                var buffer = new ArrayBuffer(44 + samples.length * 2);
                var view = new DataView(buffer);

                /* RIFF identifier */
                writeString(view, 0, 'RIFF');
                /* RIFF chunk length */
                view.setUint32(4, 36 + samples.length * 2, true);
                /* RIFF type */
                writeString(view, 8, 'WAVE');
                /* format chunk identifier */
                writeString(view, 12, 'fmt ');
                /* format chunk length */
                view.setUint32(16, 16, true);
                /* sample format (raw) */
                view.setUint16(20, 1, true);
                /* channel count */
                view.setUint16(22, numChannels, true);
                /* sample rate */
                view.setUint32(24, sampleRate, true);
                /* byte rate (sample rate * block align) */
                view.setUint32(28, sampleRate * 4, true);
                /* block align (channel count * bytes per sample) */
                view.setUint16(32, numChannels * 2, true);
                /* bits per sample */
                view.setUint16(34, 16, true);
                /* data chunk identifier */
                writeString(view, 36, 'data');
                /* data chunk length */
                view.setUint32(40, samples.length * 2, true);

                floatTo16BitPCM(view, 44, samples);

                return view;
            }
        }, self);

        this.worker.postMessage({
            command: 'init',
            config: {
                sampleRate: this.context.sampleRate,
                numChannels: this.config.numChannels
            }
        });

        this.worker.onmessage = function (e) {
            var cb = _this.callbacks[e.data.command].pop();
            if (typeof cb == 'function') {
                cb(e.data.data);
            }
        };
    }

    _createClass(Recorder, [{
        key: 'record',
        value: function record() {
            this.recording = true;
        }
    }, {
        key: 'stop',
        value: function stop() {
            this.recording = false;
        }
    }, {
        key: 'clear',
        value: function clear() {
            this.worker.postMessage({ command: 'clear' });
        }
    }, {
        key: 'getBuffer',
        value: function getBuffer(cb) {
            cb = cb || this.config.callback;
            if (!cb) throw new Error('Callback not set');

            this.callbacks.getBuffer.push(cb);

            this.worker.postMessage({ command: 'getBuffer' });
        }
    }, {
        key: 'exportWAV',
        value: function exportWAV(cb, mimeType) {
            mimeType = mimeType || this.config.mimeType;
            cb = cb || this.config.callback;
            if (!cb) throw new Error('Callback not set');

            this.callbacks.exportWAV.push(cb);

            this.worker.postMessage({
                command: 'exportWAV',
                type: mimeType
            });
        }
    }], [{
        key: 'forceDownload',
        value: function forceDownload(blob, filename) {
            var url = (window.URL || window.webkitURL).createObjectURL(blob);
            var link = window.document.createElement('a');
            link.href = url;
            link.download = filename || 'output.wav';
            var click = document.createEvent("Event");
            click.initEvent("click", true, true);
            link.dispatchEvent(click);
        }
    }]);

    return Recorder;
})();

exports.default = Recorder;

},{"inline-worker":3}],3:[function(require,module,exports){
"use strict";

module.exports = require("./inline-worker");
},{"./inline-worker":4}],4:[function(require,module,exports){
(function (global){
"use strict";

var _createClass = (function () { function defineProperties(target, props) { for (var key in props) { var prop = props[key]; prop.configurable = true; if (prop.value) prop.writable = true; } Object.defineProperties(target, props); } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _classCallCheck = function (instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } };

var WORKER_ENABLED = !!(global === global.window && global.URL && global.Blob && global.Worker);

var InlineWorker = (function () {
  function InlineWorker(func, self) {
    var _this = this;

    _classCallCheck(this, InlineWorker);

    if (WORKER_ENABLED) {
      var functionBody = func.toString().trim().match(/^function\s*\w*\s*\([\w\s,]*\)\s*{([\w\W]*?)}$/)[1];
      var url = global.URL.createObjectURL(new global.Blob([functionBody], { type: "text/javascript" }));
      return new global.Worker(url);
    }

    this.self = self;
    this.self.postMessage = function (data) {
      setTimeout(function () {
        _this.onmessage({ data: data });
      }, 0);
    };

    setTimeout(function () {
      func.call(self);
    }, 0);
  }

  _createClass(InlineWorker, {
    postMessage: {
      value: function postMessage(data) {
        var _this = this;

        setTimeout(function () {
          _this.self.onmessage({ data: data });
        }, 0);
      }
    }
  });

  return InlineWorker;
})();

module.exports = InlineWorker;
}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}]},{},[1])(1)
});



var VoxImplant = function (e) {
    function t(r) {
        if (n[r]) return n[r].exports;
        var o = n[r] = {
            i: r,
            l: !1,
            exports: {}
        };
        return e[r].call(o.exports, o, o.exports, t), o.l = !0, o.exports
    }
    var n = {};
    return t.m = e, t.c = n, t.d = function (e, n, r) {
        t.o(e, n) || Object.defineProperty(e, n, {
            configurable: !1,
            enumerable: !0,
            get: r
        })
    }, t.n = function (e) {
        var n = e && e.__esModule ? function () {
            return e.default
        } : function () {
            return e
        };
        return t.d(n, "a", n), n
    }, t.o = function (e, t) {
        return Object.prototype.hasOwnProperty.call(e, t)
    }, t.p = "/build", t(t.s = 41)
}([function (e, t, n) {
    "use strict";

    function r(e, t) {
        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }
    var o = function () {
        function e(e, t) {
            for (var n = 0; n < t.length; n++) {
                var r = t[n];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
            }
        }
        return function (t, n, r) {
            return n && e(t.prototype, n), r && e(t, r), t
        }
    }();
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var i;
    ! function (e) {
        e[e.NONE = 0] = "NONE", e[e.ERROR = 1] = "ERROR", e[e.WARNING = 2] = "WARNING", e[e.INFO = 3] = "INFO", e[e.TRACE = 4] = "TRACE"
    }(i = t.LogLevel || (t.LogLevel = {}));
    var a;
    ! function (e) {
        e[e.SIGNALING = 0] = "SIGNALING", e[e.RTC = 1] = "RTC", e[e.USERMEDIA = 2] = "USERMEDIA", e[e.CALL = 3] = "CALL", e[e.CALLEXP2P = 4] = "CALLEXP2P", e[e.CALLEXSERVER = 5] = "CALLEXSERVER", e[e.CALLMANAGER = 6] = "CALLMANAGER", e[e.CLIENT = 7] = "CLIENT", e[e.AUTHENTICATOR = 8] = "AUTHENTICATOR", e[e.PCFACTORY = 9] = "PCFACTORY", e[e.UTILS = 10] = "UTILS", e[e.ORTC = 11] = "ORTC", e[e.MESSAGING = 12] = "MESSAGING", e[e.REINVITEQ = 13] = "REINVITEQ", e[e.HARDWARE = 14] = "HARDWARE", e[e.ENDPOINT = 15] = "ENDPOINT", e[e.EVENTTARGET = 16] = "EVENTTARGET"
    }(a = t.LogCategory || (t.LogCategory = {}));
    ! function (e) {
        e[e.DISCONNECTED = "DISCONNECTED"] = "DISCONNECTED", e[e.CONNECTING = "CONNECTING"] = "CONNECTING", e[e.CONNECTED = "CONNECTED"] = "CONNECTED", e[e.LOGGING_IN = "LOGGING_IN"] = "LOGGING_IN", e[e.LOGGED_IN = "LOGGED_IN"] = "LOGGED_IN"
    }(t.ClientState || (t.ClientState = {}));
    var s = function () {
        function e(t, n, o) {
            r(this, e), this.category = t, this.label = n, this.provider = o
        }
        return o(e, [{
            key: "log",
            value: function (e, t) {
                this.provider.writeMessage(this.category, this.label, e, t)
            }
        }, {
            key: "error",
            value: function (e) {
                this.log(i.ERROR, e)
            }
        }, {
            key: "warning",
            value: function (e) {
                this.log(i.WARNING, e)
            }
        }, {
            key: "info",
            value: function (e) {
                this.log(i.INFO, e)
            }
        }, {
            key: "trace",
            value: function (e) {
                this.log(i.TRACE, e)
            }
        }]), e
    }();
    t.Logger = s;
    var c = function () {
        function e() {
            r(this, e), this._shadowLogging = !1, this.levels = {}
        }
        return o(e, [{
            key: "getSLog",
            value: function () {
                return this._shadowLog
            }
        }, {
            key: "clearSilentLog",
            value: function () {
                this._shadowLog = []
            }
        }, {
            key: "setLoggerCallback",
            value: function (e) {
                this._outerCallback = e
            }
        }, {
            key: "setPrettyPrint",
            value: function (e) {
                this.prettyPrint = e
            }
        }, {
            key: "setLogLevel",
            value: function (e, t) {
                this.levels[a[e]] = t
            }
        }, {
            key: "writeMessage",
            value: function (t, n, r, o) {
                e.tick++;
                var s = "VIWSLR " + e.tick + " " + (new Date).toString() + " " + i[r] + " " + n + ": " + o,
                    c = i.NONE;
                if (void 0 !== this.levels[a[t]] && (c = this.levels[a[t]]), r <= c)
                    if (void 0 !== console.debug && void 0 !== console.info && void 0 !== console.error && void 0 !== console.warn)
                        if (this.prettyPrint) {
                            "string" != typeof o && (o = JSON.stringify(o));
                            var l = "%c VIWSLR " + e.tick + " " + (new Date).toUTCString() + " [" + i[r] + "] %c" + n + ": %c" + o.replace("\r\n", "<br>");
                            r === i.ERROR ? console.error(s) : r === i.WARNING ? console.warn(l, "color:#ccc", "color:#2375a2", "color:#000") : r === i.INFO ? console.info(l, "color:#ccc", "color:#2375a2", "color:#000") : (i.TRACE, console.log(l, "color:#ccc", "color:#2375a2", "color:#000"))
                        } else r === i.ERROR ? console.error(s) : r === i.WARNING ? console.warn(s) : r === i.INFO ? console.info(s) : r === i.TRACE ? console.debug(s) : console.log(s);
                    else console.log(s);
                this.shadowLogging && this._shadowLog.push(s), "function" == typeof this._outerCallback && this._outerCallback({
                    formattedText: s,
                    category: t,
                    label: n,
                    level: r,
                    message: o
                })
            }
        }, {
            key: "createLogger",
            value: function (e, t) {
                return new s(e, t, this)
            }
        }, {
            key: "_traceName",
            value: function () {
                return "Logger"
            }
        }, {
            key: "shadowLogging",
            set: function (e) {
                this._shadowLogging || (this._shadowLog = []), this._shadowLogging = e
            },
            get: function () {
                return this._shadowLogging
            }
        }], [{
            key: "get",
            value: function () {
                return void 0 === this.inst && (this.inst = new e, this.inst.prettyPrint = !1), this.inst
            }
        }, {
            key: "d_trace",
            value: function (t) {
                return function (n, r, o) {
                    return {
                        value: function () {
                            for (var a = "", s = arguments.length, c = Array(s), l = 0; l < s; l++) c[l] = arguments[l];
                            try {
                                a = c.map(function (e) {
                                    return JSON.stringify(e)
                                }).join()
                            } catch (e) {
                                a = "circular structure"
                            }
                            var u = "";
                            return n._traceName && (u = n._traceName()), e.get().writeMessage(t, u, i.TRACE, r + "(" + a + ")"), o.value.apply(this, c)
                        }
                    }
                }
            }
        }]), e
    }();
    c.tick = 0, t.LogManager = c
}, function (e, t, n) {
    "use strict";

    function r(e, t) {
        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }

    function o(e, t) {
        if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
        return !t || "object" != typeof t && "function" != typeof t ? e : t
    }

    function i(e, t) {
        if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
        e.prototype = Object.create(t && t.prototype, {
            constructor: {
                value: e,
                enumerable: !1,
                writable: !0,
                configurable: !0
            }
        }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
    }
    var a = function () {
        function e(e, t) {
            for (var n = 0; n < t.length; n++) {
                var r = t[n];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
            }
        }
        return function (t, n, r) {
            return n && e(t.prototype, n), r && e(t, r), t
        }
    }(),
        s = function e(t, n, r) {
            null === t && (t = Function.prototype);
            var o = Object.getOwnPropertyDescriptor(t, n);
            if (void 0 === o) {
                var i = Object.getPrototypeOf(t);
                return null === i ? void 0 : e(i, n, r)
            }
            if ("value" in o) return o.value;
            var a = o.get;
            if (void 0 !== a) return a.call(r)
        },
        c = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
            return typeof e
        } : function (e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        },
        l = function (e, t, n, r) {
            var o, i = arguments.length,
                a = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
            if ("object" === ("undefined" == typeof Reflect ? "undefined" : c(Reflect)) && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, n, r);
            else
                for (var s = e.length - 1; s >= 0; s--)(o = e[s]) && (a = (i < 3 ? o(a) : i > 3 ? o(t, n, a) : o(t, n)) || a);
            return i > 3 && a && Object.defineProperty(t, n, a), a
        };
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var u = n(32),
        d = n(17),
        g = n(24),
        f = n(2),
        p = n(10),
        v = n(8),
        h = n(0),
        y = n(9),
        m = n(5),
        C = n(13),
        _ = n(3),
        S = n(11),
        E = n(22),
        L = n(58),
        M = n(59),
        R = n(29),
        T = n(4),
        b = function (e) {
            function t() {
                r(this, t);
                var e = o(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this));
                if (e._connected = !1, e.progressToneScript = {
                    US: "440@-19,480@-19;*(2/4/1+2)",
                    RU: "425@-19;*(1/3/1)"
                }, e.playingNow = !1, e.serversList = [], e.level = 100, e.micRequired = !1, e.videoConstraints = null, e.progressToneCountry = "US", e.progressTone = !0, e.showDebugInfo = !1, e.showWarnings = !1, e.RTCsupported = !1, e._deviceEnumAPI = !1, e._h264first = !1, e._VP8first = !1, e.depLastDevices = {
                    ai: [],
                    ao: [],
                    vi: []
                }, e.applyMixins(t, [C.EventTarget]), t.instance) throw new Error("Error - use VoxImplant.getInstance()");
                return t.instance = e, e._promises = {}, v.default.init(), y.PCFactory.get().requireMedia = !1, e.voxSignaling = f.VoxSignaling.get(), e.voxCallManager = m.CallManager.get(), e.setLogLevelAll(h.LogLevel.NONE), h.LogManager.get().writeMessage(h.LogCategory.CLIENT, "SDK ver.", h.LogLevel.TRACE, e.version), f.VoxSignaling.get().setRPCHandler(S.RemoteEvent.onPCStats, function (t, n) {
                    y.PCFactory.get().getPeerConnect(t) && e.dispatchEvent({
                        name: "NetStatsReceived",
                        stats: n
                    })
                }), e._defaultSinkId = null, e.loginState = 0, e
            }
            return i(t, e), a(t, [{
                key: "playProgressTone",
                value: function () {
                    var e = arguments.length > 0 && void 0 !== arguments[0] && arguments[0];
                    (!e || e && this.progressTone) && null !== this.progressToneScript[this.progressToneCountry] && (this.playingNow || this.playToneScript(this.progressToneScript[this.progressToneCountry]), this.playingNow = !0)
                }
            }, {
                key: "stopProgressTone",
                value: function () {
                    this.playingNow && (this.stopPlayback(), this.playingNow = !1)
                }
            }, {
                key: "onIncomingCall",
                value: function (e, t, n, r, o) {
                    this.dispatchEvent({
                        name: d.Events.IncomingCall,
                        call: m.CallManager.get().calls[e],
                        headers: r,
                        video: o
                    })
                }
            }, {
                key: "init",
                value: function (e) {
                    var t = this;
                    return new Promise(function (n, r) {
                        if (t._config = void 0 !== e ? e : {}, void 0 !== t._config.progressToneCountry && (t.progressToneCountry = t._config.progressToneCountry), !0 !== t._config.progressTone && (t.progressTone = !1), void 0 !== t._config.serverIp && (t.serverIp = t._config.serverIp), void 0 !== t._config.showDebugInfo && (t.showDebugInfo = t._config.showDebugInfo), !1 !== t._config.showWarnings && (t.showWarnings = !0), "string" == typeof t._config.videoContainerId && (t.remoteVideoContainerId = t._config.videoContainerId), "string" == typeof t._config.remoteVideoContainerId && (t.remoteVideoContainerId = t._config.remoteVideoContainerId), "string" == typeof t._config.localVideoContainerId && (t.localVideoContainerId = t._config.localVideoContainerId), !1 !== t._config.micRequired && (t.micRequired = !0), void 0 !== t._config.videoSupport ? t.videoSupport = t._config.videoSupport : t.videoSupport = !1, void 0 !== t._config.H264first && (t._h264first = t._config.H264first, m.CallManager.get()._h264first = t._h264first), void 0 !== t._config.VP8first && (t._VP8first = t._config.VP8first), void 0 !== t._config.rtcStatsCollectionInterval ? m.CallManager.get().rtcStatsCollectionInterval = t._config.rtcStatsCollectionInterval : m.CallManager.get().rtcStatsCollectionInterval = 1e4, !t._config.protocolVersion || "2" !== t._config.protocolVersion && "3" !== t._config.protocolVersion ? t._callProtocolVersion = "3" : (t._callProtocolVersion = t._config.protocolVersion, m.CallManager.get().setProtocolVersion(t._callProtocolVersion)), t._config.callstatsIoParams && E.CallstatsIo.get(t._config.callstatsIoParams), t._config.prettyPrint && h.LogManager.get().setPrettyPrint(t._config.prettyPrint), t.showWarnings && t.setLogLevelAll(h.LogLevel.WARNING), t.showDebugInfo && t.setLogLevelAll(h.LogLevel.TRACE), h.LogManager.get().writeMessage(h.LogCategory.CLIENT, "[sdkinit]", h.LogLevel.TRACE, t.version), h.LogManager.get().writeMessage(h.LogCategory.CLIENT, "[sdkinit]", h.LogLevel.TRACE, JSON.stringify(t._config)), void 0 !== t._config.videoConstraints) {
                            t.videoConstraints = t._config.videoConstraints;
                            var o = T.default.CameraManager.legacyParamConverter(t._config.videoConstraints);
                            T.default.CameraManager.get().setDefaultVideoSettings(o)
                        }
                        if (T.default.CameraManager.get().getInputDevices().then(function (e) {
                            return t.depLastDevices.vi = e
                        }), T.default.AudioDeviceManager.get().getInputDevices().then(function (e) {
                            return t.depLastDevices.ai = e
                        }), T.default.AudioDeviceManager.get().getOutputDevices().then(function (e) {
                            return t.depLastDevices.ao = e
                        }), "127.0.0.1" != window.location.hostname && "localhost" != window.location.hostname && "https:" != window.location.protocol && void 0 !== console.error && t.showWarnings && h.LogManager.get().writeMessage(h.LogCategory.CLIENT, "WARNING:", h.LogLevel.WARNING, "getUserMedia() is deprecated on insecure origins, and support will be removed in the future. You should consider switching your application to a secure origin, such as HTTPS. See https://goo.gl/rStTGz for more details."), t._config.experiments && t._config.experiments.ignorewebrtc) t.RTCsupported = !0;
                        else if ("undefined" != typeof webkitRTCPeerConnection || "undefined" != typeof mozRTCPeerConnection || "undefined" != typeof RTCPeerConnection || "undefined" != typeof RTCIceGatherer)
                            if ("undefined" != typeof mozRTCPeerConnection) try {
                                new mozRTCPeerConnection({
                                    iceServers: []
                                }), t.RTCsupported = !0
                            } catch (e) { } else t.RTCsupported = !0;
                        if (!t.RTCsupported) throw r(new Error("NO_WEBRTC_SUPPORT")), new Error("NO_WEBRTC_SUPPORT");
                        var i;
                        null != window.location.href.match(/^file\:\/{3}.*$/g) && void 0 !== console.error && t.showWarnings && console.error("WebRTC requires application to be loaded from a web server"), t.voxAuth = p.Authenticator.get(), t.voxAuth.setHandler({
                            onLoginSuccessful: function (e, n) {
                                t.loginState = 2;
                                var r = {
                                    name: d.Events.AuthResult,
                                    displayName: e,
                                    result: !0,
                                    tokens: n
                                };
                                t._resolvePromise("login", r), t.dispatchEvent(r)
                            },
                            onLoginFailed: function (e) {
                                t.loginState = 0;
                                var n = {
                                    name: d.Events.AuthResult,
                                    code: e,
                                    result: !1
                                };
                                t._rejectPromise("login", n), t.dispatchEvent(n)
                            },
                            onSecondStageInitiated: function () {
                                var e = {
                                    name: d.Events.AuthResult,
                                    code: 301,
                                    result: !1
                                };
                                t._rejectPromise("login", e), t.dispatchEvent(e)
                            },
                            onOneTimeKeyGenerated: function (e) {
                                var n = {
                                    name: d.Events.AuthResult,
                                    key: e,
                                    code: 302,
                                    result: !1
                                };
                                t._resolvePromise("loginkey", n), t.dispatchEvent(n)
                            },
                            onRefreshTokenFailed: function (e) {
                                var n = {
                                    name: d.Events.RefreshTokenResult,
                                    code: e,
                                    result: !1
                                };
                                t.dispatchEvent(n)
                            },
                            onRefreshTokenSuccess: function (e) {
                                var n = {
                                    name: d.Events.RefreshTokenResult,
                                    tokens: e,
                                    result: !0
                                };
                                t.dispatchEvent(n)
                            }
                        }), t.voxSignaling.addHandler(t), i = setInterval(function () {
                            "undefined" != typeof document && (clearInterval(i), t.dispatchEvent({
                                name: d.Events.SDKReady,
                                version: t.version
                            }), n({
                                name: d.Events.SDKReady,
                                version: t.version
                            }))
                        }, 100), f.VoxSignaling.get().setRPCHandler(S.RemoteEvent.sipRegisterSuccessful, function (e, n) {
                            t.dispatchEvent({
                                name: "SIPRegistrationSuccessful",
                                id: e,
                                sipuri: n
                            })
                        }), f.VoxSignaling.get().setRPCHandler(S.RemoteEvent.onACDStatus, function (e, n) {
                            t.dispatchEvent({
                                name: d.Events.ACDStatusUpdated,
                                id: e,
                                status: n
                            })
                        }), f.VoxSignaling.get().setRPCHandler(S.RemoteEvent.sipRegisterFailed, function (e, n, r, o) {
                            t.dispatchEvent({
                                name: "SIPRegistrationFailed",
                                id: e,
                                sipuri: n,
                                status: r,
                                reason: o
                            })
                        })
                    })
                }
            }, {
                key: "call",
                value: function (e, t, n, r) {
                    g.Utils.checkCA();
                    var o = {
                        H264first: this._h264first,
                        VP8first: this._VP8first
                    };
                    switch (o = "string" == typeof e || "number" == typeof e ? {
                        number: e,
                        video: t,
                        customData: n,
                        extraHeaders: r
                    } : e, c(o.video)) {
                        case "boolean":
                            o.video = {
                                sendVideo: o.video,
                                receiveVideo: o.video
                            };
                            break;
                        case "undefined":
                            o.video = {
                                sendVideo: !1,
                                receiveVideo: !0
                            }
                    }
                    return this.voxCallManager.call(o)
                }
            }, {
                key: "callConference",
                value: function (e, t, n, r) {
                    g.Utils.checkCA();
                    var o = {
                        H264first: this._h264first,
                        VP8first: this._VP8first
                    };
                    switch (o = "string" == typeof e || "number" == typeof e ? {
                        number: e,
                        video: t,
                        customData: n,
                        extraHeaders: r
                    } : e, c(o.video)) {
                        case "boolean":
                            o.video = {
                                sendVideo: o.video,
                                receiveVideo: o.video
                            };
                            break;
                        case "undefined":
                            o.video = {
                                sendVideo: !1,
                                receiveVideo: !0
                            }
                    }
                    return this.voxCallManager.callConference(o)
                }
            }, {
                key: "config",
                value: function () {
                    return this._config
                }
            }, {
                key: "connect",
                value: function (e) {
                    var t = this;
                    return void 0 === this._config && h.LogManager.get().writeMessage(h.LogCategory.CLIENT, "WARNING:", h.LogLevel.WARNING, "Please, run VoxImplant init before connect."), void 0 === e && !1 === this._config.micRequired && (e = !1), new Promise(function (n, r) {
                        if (t._promises.connect = {
                            resolve: n,
                            reject: r
                        }, void 0 !== t.serverIp) {
                            var o = void 0;
                            "object" === c(t.serverIp) ? (t.serversList = t.serverIp, o = t.serversList[0]) : o = t.serverIp, void 0 === t._config.tryingServers && (t._config.tryingServers = []), t._config.tryingServers.push(o), h.LogManager.get().writeMessage(h.LogCategory.CLIENT, "connecting", h.LogLevel.TRACE, o + " trying"), t.connectTo(o, null, e)
                        } else {
                            var i = function (t) {
                                var n = String(t).indexOf(";"),
                                    r = void 0; - 1 == n ? r = t : (this.serversList = t.split(";"), r = this.serversList[0]), void 0 === this._config.tryingServers && (this._config.tryingServers = []), this._config.tryingServers.push(r), h.LogManager.get().writeMessage(h.LogCategory.CLIENT, "connecting", h.LogLevel.TRACE, r + " trying"), this.connectTo(r, null, e)
                            };
                            g.Utils.getServers(i.bind(t), !1, t)
                        }
                    })
                }
            }, {
                key: "connectTo",
                value: function (e, t, n) {
                    if (this._connected) throw new Error("ALREADY_CONNECTED_TO_VOXIMPLANT");
                    this.host = e, this.voxSignaling.connectTo(e, !0, !0, n, this._callProtocolVersion)
                }
            }, {
                key: "disconnect",
                value: function () {
                    this.checkConnection(), this.voxSignaling.disconnect(), T.default.StreamManager.get().clear(), this.voxSignaling.removeRPCHandler(S.RemoteEvent.onCallRemoteFunctionError), this.voxSignaling.removeRPCHandler(S.RemoteEvent.handleError)
                }
            }, {
                key: "setOperatorACDStatus",
                value: function (e) {
                    var t = this;
                    return new Promise(function (n, r) {
                        g.Utils.checkCA(), Object.values(u.OperatorACDStatuses).includes(e) || r(new Error("Wrong ACD status name " + e)), t.voxSignaling.callRemoteFunction(_.RemoteFunction.setOperatorACDStatus, e), n()
                    })
                }
            }, {
                key: "getOperatorACDStatus",
                value: function () {
                    var e = this;
                    return new Promise(function (t, n) {
                        g.Utils.checkCA(), e.voxSignaling.callRemoteFunction(_.RemoteFunction.getOperatorACDStatus);
                        var r = function n(r) {
                            t(r.status), e.off(d.Events.ACDStatusUpdated, n)
                        };
                        e.on(d.Events.ACDStatusUpdated, r)
                    })
                }
            }, {
                key: "login",
                value: function (e, t, n) {
                    var r = this;
                    return this.loginState = 1, new Promise(function (o, i) {
                        if (r._promises.login = {
                            resolve: o,
                            reject: i
                        }, n = void 0 !== n ? n : {}, n = g.Utils.extend({}, n), !r._connected) throw i(new Error("NOT_CONNECTED_TO_VOXIMPLANT")), new Error("NOT_CONNECTED_TO_VOXIMPLANT");
                        r._config.experiments && r._config.experiments.mediaServer && (n.mediaServer = r._config.experiments.mediaServer), r.voxAuth.basicLogin(e, t, n)
                    })
                }
            }, {
                key: "loginWithCode",
                value: function (e, t, n) {
                    var r = this;
                    return this.loginState = 1, new Promise(function (o, i) {
                        if (r._promises.login = {
                            resolve: o,
                            reject: i
                        }, n = void 0 !== n ? n : {}, n = g.Utils.extend({
                            serverPresenceControl: !1
                        }, n), !r._connected) throw i(new Error("NOT_CONNECTED_TO_VOXIMPLANT")), new Error("NOT_CONNECTED_TO_VOXIMPLANT");
                        r.voxAuth.loginStage2(e, t, n)
                    })
                }
            }, {
                key: "loginWithToken",
                value: function (e, t, n) {
                    var r = this;
                    return this.loginState = 1, new Promise(function (o, i) {
                        if (r._promises.login = {
                            resolve: o,
                            reject: i
                        }, n = void 0 !== n ? n : {}, n = g.Utils.extend({
                            serverPresenceControl: !1
                        }, n), n.accessToken = t, !r._connected) throw i(new Error("NOT_CONNECTED_TO_VOXIMPLANT")), new Error("NOT_CONNECTED_TO_VOXIMPLANT");
                        r.voxAuth.tokenLogin(e, n)
                    })
                }
            }, {
                key: "tokenRefresh",
                value: function (e, t, n) {
                    var r = this;
                    return new Promise(function (o, i) {
                        var a = function e(t) {
                            t.result ? o(t) : i(t), r.off(d.Events.RefreshTokenResult, e)
                        };
                        r.on(d.Events.RefreshTokenResult, a), r.voxAuth.tokenRefresh(e, t, n)
                    })
                }
            }, {
                key: "requestOneTimeLoginKey",
                value: function (e) {
                    var t = this;
                    return new Promise(function (n, r) {
                        if (t._promises.loginkey = {
                            resolve: n,
                            reject: r
                        }, !t._connected) throw r(new Error("NOT_CONNECTED_TO_VOXIMPLANT")), new Error("NOT_CONNECTED_TO_VOXIMPLANT");
                        t.voxAuth.generateOneTimeKey(e)
                    })
                }
            }, {
                key: "loginWithOneTimeKey",
                value: function (e, t, n) {
                    var r = this;
                    return this.loginState = 1, new Promise(function (o, i) {
                        if (r._promises.login = {
                            resolve: o,
                            reject: i
                        }, n = void 0 !== n ? n : {}, n = g.Utils.extend({
                            serverPresenceControl: !1
                        }, n), !r._connected) throw i(new Error("NOT_COFNNECTED_TO_VOXIMPLANT")), new Error("NOT_CONNECTED_TO_VOXIMPLANT");
                        r.voxAuth.loginUsingOneTimeKey(e, t, n)
                    })
                }
            }, {
                key: "connected",
                value: function () {
                    return this._connected
                }
            }, {
                key: "showLocalVideo",
                value: function () {
                    var e = !(arguments.length > 0 && void 0 !== arguments[0]) || arguments[0];
                    arguments.length > 1 && void 0 !== arguments[1] && arguments[1], arguments.length > 2 && void 0 !== arguments[2] && arguments[2];
                    return e ? T.default.StreamManager.get().showLocalVideo() : T.default.StreamManager.get().hideLocalVideo()
                }
            }, {
                key: "setLocalVideoPosition",
                value: function (e, t) {
                    throw new Error("Deprecated: please use CSS to position '#voximplantlocalvideo' element")
                }
            }, {
                key: "setLocalVideoSize",
                value: function (e, t) {
                    throw new Error("Deprecated: please use CSS to set size of '#voximplantlocalvideo' element")
                }
            }, {
                key: "setVideoSettings",
                value: function (e, t, n) {
                    T.default.CameraManager.get().setDefaultVideoSettings(T.default.CameraManager.legacyParamConverter(e)), m.CallManager.get().setVideoSettings(e).then(function () {
                        t && t(null)
                    }, function (e) {
                        n && n(null)
                    })
                }
            }, {
                key: "setVideoBandwidth",
                value: function (e) {
                    this.checkConnection(), y.PCFactory.get().setBandwidthParams(e), this.voxSignaling.callRemoteFunction(_.RemoteFunction.setDesiredVideoBandwidth, e)
                }
            }, {
                key: "playToneScript",
                value: function (e) {
                    var t = arguments.length > 1 && void 0 !== arguments[1] && arguments[1];
                    g.Utils.playToneScript(e, t)
                }
            }, {
                key: "stopPlayback",
                value: function () {
                    g.Utils.stopPlayback() && this.dispatchEvent({
                        name: d.Events.PlaybackFinished
                    })
                }
            }, {
                key: "volume",
                value: function (e) {
                    return void 0 !== e && (e > 100 && (e = 100), e < 0 && (e = 0), m.CallManager.get().setAllCallsVolume(e), this.level = e), this.level
                }
            }, {
                key: "audioSources",
                value: function () {
                    return this.depLastDevices.ai
                }
            }, {
                key: "videoSources",
                value: function () {
                    return this.depLastDevices.vi
                }
            }, {
                key: "audioOutputs",
                value: function () {
                    return this.depLastDevices.ao
                }
            }, {
                key: "useAudioSource",
                value: function (e, t, n) {
                    var r = T.default.AudioDeviceManager.get().getDefaultAudioSettings();
                    return T.default.AudioDeviceManager.get().setDefaultAudioSettings(Object.assign({}, r, {
                        inputId: e
                    })), new Promise(function (r, o) {
                        return m.CallManager.get().useAudioSource(e).then(function () {
                            t && t(null), r(null)
                        }, function (e) {
                            n && n(e), o(e)
                        })
                    })
                }
            }, {
                key: "useVideoSource",
                value: function (e, t, n) {
                    var r = T.default.CameraManager.get().getDefaultVideoSettings();
                    return T.default.CameraManager.get().setDefaultVideoSettings(Object.assign({}, r, {
                        cameraId: e
                    })), new Promise(function (r, o) {
                        return m.CallManager.get().useVideoSource(e).then(function () {
                            t && t(null), r(null)
                        }, function (e) {
                            n && n(e), o(e)
                        })
                    })
                }
            }, {
                key: "useAudioOutput",
                value: function (e) {
                    var t = this;
                    return new Promise(function (n, r) {
                        "chrome" !== v.default.getWSVendor(!0) && r(new Error("Unsupported browser. Only Google Chrome 49 and above.")), t._defaultSinkId = e, n()
                    })
                }
            }, {
                key: "attachRecordingDevice",
                value: function (e, t) {
                    h.LogManager.get().writeMessage(h.LogCategory.CLIENT, "DEPRECATED", h.LogLevel.ERROR, "Now all media connection on demand. There is no reason do it by hand."), e && e(null)
                }
            }, {
                key: "detachRecordingDevice",
                value: function () {
                    T.default.StreamManager.get().clear()
                }
            }, {
                key: "setCallActive",
                value: function (e) {
                    var t = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1];
                    return new Promise(function (n, r) {
                        if (g.Utils.checkCA(), e) return e.setActive(t);
                        r("trying to hold unknown call " + e)
                    })
                }
            }, {
                key: "sendVideo",
                value: function () {
                    !(arguments.length > 0 && void 0 !== arguments[0]) || arguments[0];
                    h.LogManager.get().writeMessage(h.LogCategory.CLIENT, "DEPRECATED", h.LogLevel.ERROR, "This function deprecated. Use Call.sendVideo() instead.")
                }
            }, {
                key: "isRTCsupported",
                value: function () {
                    if ("undefined" != typeof webkitRTCPeerConnection || "undefined" != typeof mozRTCPeerConnection || "undefined" != typeof RTCPeerConnection || "undefined" != typeof RTCIceGatherer) {
                        if ("undefined" == typeof mozRTCPeerConnection) return !0;
                        try {
                            return new mozRTCPeerConnection({
                                iceServers: []
                            }), !0
                        } catch (e) {
                            return !1
                        }
                    }
                }
            }, {
                key: "transferCall",
                value: function (e, t) {
                    g.Utils.checkCA(), this.voxCallManager.transferCall(e, t)
                }
            }, {
                key: "setLogLevel",
                value: function (e, t) {
                    h.LogManager.get().setLogLevel(e, t)
                }
            }, {
                key: "onSignalingConnected",
                value: function () {
                    this._connected = !0;
                    var e = {
                        name: d.Events.ConnectionEstablished
                    };
                    this._resolvePromise("connect", e), this.dispatchEvent(e)
                }
            }, {
                key: "onSignalingClosed",
                value: function () {
                    this._connected = !1, this.dispatchEvent({
                        name: d.Events.ConnectionClosed
                    }), this.progressTone && this.stopProgressTone()
                }
            }, {
                key: "onSignalingConnectionFailed",
                value: function (e) {
                    if (this._connected = !1, this.serversList.length > 1 && (void 0 === this.serverIp || "object" === c(this.serverIp))) {
                        h.LogManager.get().writeMessage(h.LogCategory.CLIENT, "connecting", h.LogLevel.TRACE, "Connection to the " + this.serversList[0] + " falled"), this.serversList.splice(0, 1);
                        var t = this.serversList[0];
                        void 0 === this._config.tryingServers && (this._config.tryingServers = []), this._config.tryingServers.push(t), h.LogManager.get().writeMessage(h.LogCategory.CLIENT, "connecting", h.LogLevel.TRACE, t + " trying"), this.connectTo(t, !0)
                    } else {
                        h.LogManager.get().writeMessage(h.LogCategory.CLIENT, "", h.LogLevel.INFO, "We can't connect to the Voximplant cloud. Please, check UDP connection to Voximplant servers: " + this._config.tryingServers.join(", "));
                        var n = {
                            name: d.Events.ConnectionFailed,
                            message: e
                        };
                        this._rejectPromise("connect", n), this.dispatchEvent(n)
                    }
                }
            }, {
                key: "onMediaConnectionFailed",
                value: function () { }
            }, {
                key: "getCall",
                value: function (e) {
                    return m.CallManager.get().calls[e]
                }
            }, {
                key: "removeCall",
                value: function (e) {
                    m.CallManager.get().removeCall(e)
                }
            }, {
                key: "screenSharingSupported",
                value: function () {
                    return v.default.screenSharingSupported()
                }
            }, {
                key: "addEventListener",
                value: function (e, n) {
                    s(t.prototype.__proto__ || Object.getPrototypeOf(t.prototype), "addEventListener", this).call(this, e, n)
                }
            }, {
                key: "removeEventListener",
                value: function (e, n) {
                    s(t.prototype.__proto__ || Object.getPrototypeOf(t.prototype), "removeEventListener", this).call(this, e, n)
                }
            }, {
                key: "on",
                value: function (e, n) {
                    s(t.prototype.__proto__ || Object.getPrototypeOf(t.prototype), "on", this).call(this, e, n)
                }
            }, {
                key: "off",
                value: function (e, n) {
                    s(t.prototype.__proto__ || Object.getPrototypeOf(t.prototype), "off", this).call(this, e, n)
                }
            }, {
                key: "sslset",
                value: function (e) {
                    this.voxSignaling.writeLog = e
                }
            }, {
                key: "sslget",
                value: function () {
                    return this.voxSignaling.getLog()
                }
            }, {
                key: "getZingayaAPI",
                value: function () {
                    return new L.ZingayaAPI(this)
                }
            }, {
                key: "registerForPushNotificatuons",
                value: function (e) {
                    return M.PushService.register(e)
                }
            }, {
                key: "unregisterForPushNotificatuons",
                value: function (e) {
                    return M.PushService.unregister(e)
                }
            }, {
                key: "handlePushNotification",
                value: function (e) {
                    return M.PushService.incomingPush(e)
                }
            }, {
                key: "getGUID",
                value: function () {
                    return (new R.GUID).toString()
                }
            }, {
                key: "setSilentLogging",
                value: function (e) {
                    this.enableSilentLogging(e)
                }
            }, {
                key: "enableSilentLogging",
                value: function (e) {
                    h.LogManager.get().shadowLogging = e
                }
            }, {
                key: "clearSilentLog",
                value: function () {
                    h.LogManager.get().clearSilentLog()
                }
            }, {
                key: "getSilentLog",
                value: function () {
                    return h.LogManager.get().getSLog()
                }
            }, {
                key: "setLoggerCallback",
                value: function (e) {
                    h.LogManager.get().setLoggerCallback(e)
                }
            }, {
                key: "getClientState",
                value: function () {
                    var e = this.voxSignaling.currentState;
                    return e == f.VoxSignalingState.CONNECTING || e == f.VoxSignalingState.WSCONNECTED ? h.ClientState.CONNECTING : e == f.VoxSignalingState.CLOSING || e == f.VoxSignalingState.IDLE ? h.ClientState.DISCONNECTED : e == f.VoxSignalingState.CONNECTED ? 1 == this.loginState ? h.ClientState.LOGGING_IN : 2 == this.loginState ? h.ClientState.LOGGED_IN : h.ClientState.CONNECTED : void 0
                }
            }, {
                key: "setSwfColor",
                value: function () {
                    h.LogManager.get().writeMessage(h.LogCategory.CLIENT, "NOT SUPPORTED", h.LogLevel.ERROR, "setSwfColor deprecated, and not supported!")
                }
            }, {
                key: "_traceName",
                value: function () {
                    return "Client"
                }
            }, {
                key: "applyMixins",
                value: function (e, t) {
                    t.forEach(function (t) {
                        Object.getOwnPropertyNames(t.prototype).forEach(function (n) {
                            e.prototype[n] = t.prototype[n]
                        })
                    })
                }
            }, {
                key: "checkConnection",
                value: function () {
                    if (!this._connected) throw new Error("NOT_CONNECTED_TO_VOXIMPLANT")
                }
            }, {
                key: "_resolvePromise",
                value: function (e, t) {
                    var n = this._promises[e];
                    n && (n.resolve(t), this._promises[e] = void 0)
                }
            }, {
                key: "_rejectPromise",
                value: function (e, t) {
                    var n = this._promises[e];
                    n && (n.reject(t), this._promises[e] = void 0)
                }
            }, {
                key: "setLogLevelAll",
                value: function (e) {
                    this.setLogLevel(h.LogCategory.SIGNALING, e), this.setLogLevel(h.LogCategory.RTC, e), this.setLogLevel(h.LogCategory.ORTC, e), this.setLogLevel(h.LogCategory.USERMEDIA, e), this.setLogLevel(h.LogCategory.CALL, e), this.setLogLevel(h.LogCategory.CALLEXP2P, e), this.setLogLevel(h.LogCategory.CALLEXSERVER, e), this.setLogLevel(h.LogCategory.CALLMANAGER, e), this.setLogLevel(h.LogCategory.CLIENT, e), this.setLogLevel(h.LogCategory.AUTHENTICATOR, e), this.setLogLevel(h.LogCategory.PCFACTORY, e), this.setLogLevel(h.LogCategory.UTILS, e), this.setLogLevel(h.LogCategory.MESSAGING, e), this.setLogLevel(h.LogCategory.REINVITEQ, e), this.setLogLevel(h.LogCategory.HARDWARE, e), this.setLogLevel(h.LogCategory.ENDPOINT, e), this.setLogLevel(h.LogCategory.EVENTTARGET, e)
                }
            }, {
                key: "setXAS",
                value: function (e, t) {
                    void 0 === this._config.experiments && (this._config.experiments = {}), this._config.experiments.xas = {
                        as: e,
                        tias: t
                    }
                }
            }, {
                key: "removeCC",
                value: function (e) {
                    void 0 === this._config.experiments && (this._config.experiments = {}), this._config.experiments.removeTransportCC = e
                }
            }, {
                key: "version",
                get: function () {
                    return "4.3.21483"
                }
            }], [{
                key: "getInstance",
                value: function () {
                    return void 0 === t.instance && (t.instance = new t), t.instance
                }
            }]), t
        }(C.EventTarget);
    l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "playProgressTone", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "stopProgressTone", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "onIncomingCall", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "init", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "call", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "callConference", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "config", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "connect", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "connectTo", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "disconnect", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "setOperatorACDStatus", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "getOperatorACDStatus", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "login", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "loginWithCode", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "loginWithToken", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "tokenRefresh", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "requestOneTimeLoginKey", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "loginWithOneTimeKey", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "connected", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "showLocalVideo", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "setLocalVideoPosition", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "setLocalVideoSize", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "setVideoSettings", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "setVideoBandwidth", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "playToneScript", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "stopPlayback", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "volume", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "audioSources", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "videoSources", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "audioOutputs", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "useAudioSource", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "useVideoSource", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "useAudioOutput", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "attachRecordingDevice", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "detachRecordingDevice", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "setCallActive", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "sendVideo", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "isRTCsupported", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "transferCall", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "setLogLevel", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "onSignalingConnected", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "onSignalingClosed", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "onSignalingConnectionFailed", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "onMediaConnectionFailed", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "getCall", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "removeCall", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "addEventListener", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "removeEventListener", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "on", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "off", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "checkConnection", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b.prototype, "setLogLevelAll", null), l([h.LogManager.d_trace(h.LogCategory.CLIENT)], b, "getInstance", null), t.Client = b
}, function (e, t, n) {
    "use strict";

    function r(e, t) {
        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }
    var o = function () {
        function e(e, t) {
            for (var n = 0; n < t.length; n++) {
                var r = t[n];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
            }
        }
        return function (t, n, r) {
            return n && e(t.prototype, n), r && e(t, r), t
        }
    }(),
        i = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
            return typeof e
        } : function (e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        },
        a = function (e, t, n, r) {
            var o, a = arguments.length,
                s = a < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
            if ("object" === ("undefined" == typeof Reflect ? "undefined" : i(Reflect)) && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r);
            else
                for (var c = e.length - 1; c >= 0; c--)(o = e[c]) && (s = (a < 3 ? o(s) : a > 3 ? o(t, n, s) : o(t, n)) || s);
            return a > 3 && s && Object.defineProperty(t, n, s), s
        };
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var s, c = n(0),
        l = n(8),
        u = n(9),
        d = n(5),
        g = n(3),
        f = n(11),
        p = n(14),
        v = n(1);
    ! function (e) {
        e[e.IDLE = 0] = "IDLE", e[e.CONNECTING = 1] = "CONNECTING", e[e.WSCONNECTED = 2] = "WSCONNECTED", e[e.CONNECTED = 3] = "CONNECTED", e[e.CLOSING = 4] = "CLOSING"
    }(s = t.VoxSignalingState || (t.VoxSignalingState = {}));
    var h = function () {
        function e() {
            var t = this;
            r(this, e), this.ver = "3", this.handlers = [], this.rpcHandlers = {}, this.pingTimer = null, this.pongTimer = null, this.manualDisconnect = !1, this.platform = "platform", this.referrer = "platform", this.extra = "", this.closing = !1, this.writeLog = !1, this._opLog = [], this.token = "", this.log = c.LogManager.get().createLogger(c.LogCategory.SIGNALING, "VoxSignaling"), this.currentState = s.IDLE, this.setRPCHandler(f.RemoteEvent.connectionSuccessful, function (e) {
                t.onConnectionSuccessfulRPC(e)
            }), this.setRPCHandler(f.RemoteEvent.connectionFailed, function () {
                t.onConnectionFailedRPC()
            }), this.setRPCHandler(f.RemoteEvent.createConnection, function (e) {
                t.onConnectionSuccessfulRPC(e)
            })
        }
        return o(e, [{
            key: "addHandler",
            value: function (e) {
                this.handlers.push(e)
            }
        }, {
            key: "close",
            value: function () {
                this.closing = !0, this.ws ? (this.ws.onclose = null, this.ws.close(), this.onWSClosed(null)) : this.log.warning("Try close unused WS in state " + s[this.currentState])
            }
        }, {
            key: "cleanup",
            value: function () {
                u.PCFactory.get().closeAll(), this.pingTimer && clearTimeout(this.pingTimer), this.pongTimer && clearTimeout(this.pongTimer)
            }
        }, {
            key: "onConnectionSuccessfulRPC",
            value: function (e) {
                if (this.currentState != s.WSCONNECTED) return void this.log.error("Can't handle __connectionSuccessful while in state " + s[this.currentState]);
                if (e && (this.token = e), this.currentState = s.CONNECTED, this.handlers.length > 0)
                    for (var t = 0; t < this.handlers.length; ++t) try {
                        this.handlers[t].onSignalingConnected()
                    } catch (e) {
                        this.log.warning("Error in onSignalingConnected callback: " + e)
                    } else this.log.warning("No VoxSignaling handler specified")
            }
        }, {
            key: "onConnectionFailedRPC",
            value: function () {
                if (this.currentState != s.WSCONNECTED) return void this.log.error("Can't handle __connectionSuccessful while in state " + s[this.currentState]);
                if (this.ws.onerror = null, this.ws.close(), this.ws = null, this.currentState = s.IDLE, this.handlers.length > 0)
                    for (var e = 0; e < this.handlers.length; ++e) try {
                        this.handlers[e].onMediaConnectionFailed()
                    } catch (e) {
                        this.log.warning("Error in onMediaConnectionFailed callback: " + e)
                    } else this.log.warning("No VoxSignaling handler specified")
            }
        }, {
            key: "connectTo",
            value: function (e) {
                var t = arguments.length > 1 && void 0 !== arguments[1] && arguments[1],
                    n = !(arguments.length > 2 && void 0 !== arguments[2]) || arguments[2],
                    r = this,
                    o = arguments[3],
                    i = arguments[4];
                if (this.manualDisconnect = !1, this.ver = i, this.currentState != s.IDLE) return void this.log.error("Can't establish connection while in state " + s[this.currentState]);
                this.currentState = s.CONNECTING;
                var a = l.default.getWSVendor();
                this.ws = new WebSocket("ws" + (n ? "s" : "") + "://" + e + "/" + this.platform + "?version=" + this.ver + "&client=" + a + "&ccheck=" + (void 0 === o || o) + "&referrer=&extra=" + this.extra + "&video=" + (t ? "true" : "false") + "&client_version=" + v.Client.getInstance().version), this.ws.onopen = function (e) {
                    return r.onWSConnected()
                }, this.ws.onclose = function (e) {
                    return r.onWSClosed(e)
                }, this.ws.onerror = function (e) {
                    return r.onWSError()
                }, this.ws.onmessage = function (e) {
                    return r.onWSData(e.data)
                }
            }
        }, {
            key: "setRPCHandler",
            value: function (e, t) {
                void 0 !== this.rpcHandlers[e] && this.log.warning("Overwriting RPC handler for function " + e), this.rpcHandlers[e] = t
            }
        }, {
            key: "removeRPCHandler",
            value: function (e) {
                void 0 !== this.rpcHandlers[e] || this.closing || this.log.warning("There is no RPC handler for function " + e), delete this.rpcHandlers[e]
            }
        }, {
            key: "callRemoteFunction",
            value: function (e) {
                for (var t = arguments.length, n = Array(t > 1 ? t - 1 : 0), r = 1; r < t; r++) n[r - 1] = arguments[r];
                if (this.currentState != s.CONNECTED && this.currentState != s.WSCONNECTED) return this.closing || this.log.error("Can't make a RPC call in state " + s[this.currentState]), !1;
                if (void 0 !== this.ws) {
                    this.writeLog && this._opLog.push("send:" + JSON.stringify({
                        name: e,
                        params: n
                    }));
                    var o = JSON.stringify({
                        name: e,
                        params: n
                    });
                    return this.ws.send(o), c.LogManager.get().writeMessage(c.LogCategory.SIGNALING, "[wsdataout]", c.LogLevel.INFO, o), !0
                }
            }
        }, {
            key: "onWSData",
            value: function (e) {
                c.LogManager.get().writeMessage(c.LogCategory.SIGNALING, "[wsdatain]", c.LogLevel.INFO, e), this.writeLog && this._opLog.push("recv:" + e);
                var t = void 0;
                try {
                    t = JSON.parse(e)
                } catch (t) {
                    return void this.log.error("Can't parse JSON data: " + e)
                }
                void 0 !== t.service ? this.onWSMessData(t) : this.onWSVoipData(t)
            }
        }, {
            key: "onWSMessData",
            value: function (e) {
                p.MsgSignaling.get().handleWsData(e)
            }
        }, {
            key: "onWSVoipData",
            value: function (e) {
                var t = e.name,
                    n = e.params;
                if (void 0 !== this.rpcHandlers[t]) try {
                    this.rpcHandlers[t].apply(null, n)
                } catch (e) {
                    this.log.warning("Error in '" + t + "' handler : " + e)
                } else this.log.warning("No handler for " + t)
            }
        }, {
            key: "disconnect",
            value: function () {
                this.closing = !0, this.manualDisconnect = !0, this.onWSClosed(null), this.cleanup()
            }
        }, {
            key: "onWSClosed",
            value: function (e) {
                if (this.currentState != s.CONNECTED && this.currentState != s.CONNECTING && this.currentState != s.CLOSING) {
                    if (this.closing) return;
                    this.log.warning("onWSClosed in state " + s[this.currentState])
                }
                this.ws && (this.ws.close(), this.ws = void 0);
                var t = this.currentState;
                if (this.pingTimer && clearTimeout(this.pingTimer), this.pongTimer && clearTimeout(this.pongTimer), this.cleanup(), this.currentState = s.IDLE, this.handlers.length > 0)
                    for (var n = 0; n < this.handlers.length; ++n)
                        if (t != s.CONNECTING && t != s.WSCONNECTED && t != s.IDLE || this.manualDisconnect) try {
                            this.handlers[n].onSignalingClosed()
                        } catch (e) {
                            this.log.warning("Error in onSignalingClosed callback: " + e)
                        } else try {
                            this.handlers[n].onSignalingConnectionFailed(e.reason)
                        } catch (e) {
                            this.log.warning("Error in onSignalingConnectionFailed callback: " + e)
                        } else this.log.warning("No VoxSignaling handler specified")
            }
        }, {
            key: "onWSConnected",
            value: function () {
                var t = this;
                this.closing = !1, this.currentState != s.CONNECTING && this.log.warning("onWSConnected in state " + s[this.currentState]), this.currentState = s.WSCONNECTED, this.pingTimer = window.setTimeout(function () {
                    return t.doPing()
                }, e.PING_DELAY), this.setRPCHandler(f.RemoteEvent.pong, function () {
                    return t.pongReceived()
                }), this.setRPCHandler(f.RemoteEvent.increaseGain, function () {
                    t.log.info("Deprecated increaseGain")
                })
            }
        }, {
            key: "onWSError",
            value: function () {
                if (this.currentState != s.CONNECTING && this.log.warning("onWSError in state " + this.currentState), this.ws.close(), this.ws = void 0, this.pingTimer && clearTimeout(this.pingTimer), this.pongTimer && clearTimeout(this.pongTimer), this.cleanup(), this.currentState = s.IDLE, void 0 !== this.handlers)
                    for (var e = 0; e < this.handlers.length; ++e) try {
                        this.handlers[e].onSignalingConnectionFailed("Error connecting to VoxImplant server")
                    } catch (e) {
                        this.log.warning("Error in onSignalingConnectionFailed callback: " + e)
                    } else this.log.warning("No VoxSignaling handler specified")
            }
        }, {
            key: "doPing",
            value: function () {
                var t = this;
                this.pingTimer = null, this.callRemoteFunction(g.RemoteFunction.ping, []), this.pongTimer = window.setTimeout(function () {
                    if (d.CallManager.get().numCalls > 0) return void t.pongReceived();
                    t.pongTimer = null;
                    for (var e = 0; e < t.handlers.length; ++e)
                        if (t.currentState == s.CONNECTED) try {
                            t.handlers[e].onSignalingClosed()
                        } catch (e) {
                            t.log.warning("Error in onSignalingClosed callback: " + e)
                        } else try {
                            t.handlers[e].onSignalingConnectionFailed("Connection closed")
                        } catch (e) {
                            t.log.warning("Error in onSignalingConnectionFailed callback: " + e)
                        }
                    t.ws.close(), t.currentState = s.IDLE
                }, e.PONG_DELAY)
            }
        }, {
            key: "pongReceived",
            value: function () {
                var t = this;
                this.pongTimer && (clearTimeout(this.pongTimer), this.pongTimer = null, this.pingTimer = window.setTimeout(function () {
                    return t.doPing()
                }, e.PING_DELAY))
            }
        }, {
            key: "sendRaw",
            value: function (e) {
                this.writeLog && this._opLog.push("send:" + JSON.stringify(e));
                var t = JSON.stringify(e);
                return this.ws.send(t), c.LogManager.get().writeMessage(c.LogCategory.SIGNALING, "[wsdataout]", c.LogLevel.INFO, t), !0
            }
        }, {
            key: "getLog",
            value: function () {
                return this._opLog
            }
        }, {
            key: "lagacyConnectTo",
            value: function (e, t, n, r) {
                this.ver = "2", this.platform = r, this.referrer = t, this.connectTo(e, !1, !0, !0, "2")
            }
        }, {
            key: "_traceName",
            value: function () {
                return "VoxSignaling"
            }
        }], [{
            key: "get",
            value: function () {
                return void 0 === this.inst && (this.inst = new e), this.inst
            }
        }]), e
    }();
    h.PING_DELAY = 1e4, h.PONG_DELAY = 1e4, a([c.LogManager.d_trace(c.LogCategory.SIGNALING)], h.prototype, "addHandler", null), a([c.LogManager.d_trace(c.LogCategory.SIGNALING)], h.prototype, "close", null), a([c.LogManager.d_trace(c.LogCategory.SIGNALING)], h.prototype, "cleanup", null), a([c.LogManager.d_trace(c.LogCategory.SIGNALING)], h.prototype, "onConnectionSuccessfulRPC", null), a([c.LogManager.d_trace(c.LogCategory.SIGNALING)], h.prototype, "onConnectionFailedRPC", null), a([c.LogManager.d_trace(c.LogCategory.SIGNALING)], h.prototype, "connectTo", null), a([c.LogManager.d_trace(c.LogCategory.SIGNALING)], h.prototype, "setRPCHandler", null), a([c.LogManager.d_trace(c.LogCategory.SIGNALING)], h.prototype, "removeRPCHandler", null), a([c.LogManager.d_trace(c.LogCategory.SIGNALING)], h.prototype, "callRemoteFunction", null), a([c.LogManager.d_trace(c.LogCategory.SIGNALING)], h.prototype, "disconnect", null), a([c.LogManager.d_trace(c.LogCategory.SIGNALING)], h.prototype, "onWSClosed", null), a([c.LogManager.d_trace(c.LogCategory.SIGNALING)], h.prototype, "onWSConnected", null), a([c.LogManager.d_trace(c.LogCategory.SIGNALING)], h.prototype, "onWSError", null), a([c.LogManager.d_trace(c.LogCategory.SIGNALING)], h.prototype, "sendRaw", null), t.VoxSignaling = h
}, function (e, t, n) {
    "use strict";
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    ! function (e) {
        e[e.ping = "__ping"] = "ping", e[e.login = "login"] = "login", e[e.loginGenerateOneTimeKey = "loginGenerateOneTimeKey"] = "loginGenerateOneTimeKey", e[e.loginStage2 = "loginStage2"] = "loginStage2", e[e.setOperatorACDStatus = "setOperatorACDStatus"] = "setOperatorACDStatus", e[e.getOperatorACDStatus = "getOperatorACDStatus"] = "getOperatorACDStatus", e[e.setDesiredVideoBandwidth = "setDesiredVideoBandwidth"] = "setDesiredVideoBandwidth", e[e.rejectCall = "rejectCall"] = "rejectCall", e[e.disconnectCall = "disconnectCall"] = "disconnectCall", e[e.sendDTMF = "sendDTMF"] = "sendDTMF", e[e.sendSIPInfo = "sendSIPInfo"] = "sendSIPInfo", e[e.hold = "hold"] = "hold", e[e.unhold = "unhold"] = "unhold", e[e.acceptCall = "acceptCall"] = "acceptCall", e[e.createCall = "createCall"] = "createCall", e[e.callConference = "callConference"] = "callConference", e[e.transferCall = "transferCall"] = "transferCall", e[e.muteLocal = "__muteLocal"] = "muteLocal", e[e.reInvite = "ReInvite"] = "reInvite", e[e.acceptReInvite = "AcceptReInvite"] = "acceptReInvite", e[e.rejectReInvite = "RejectReInvite"] = "rejectReInvite", e[e.confirmPC = "__confirmPC"] = "confirmPC", e[e.addCandidate = "__addCandidate"] = "addCandidate", e[e.loginUsingOneTimeKey = "loginUsingOneTimeKey"] = "loginUsingOneTimeKey", e[e.refreshOauthToken = "refreshOauthToken"] = "refreshOauthToken", e[e.zPromptFinished = "promptFinished"] = "zPromptFinished", e[e.zStartPreFlightCheck = "__startPreFlightCheck"] = "zStartPreFlightCheck", e[e.registerPushToken = "registerPushToken"] = "registerPushToken", e[e.unregisterPushToken = "unregisterPushToken"] = "unregisterPushToken", e[e.pushFeedback = "pushFeedback"] = "pushFeedback"
    }(t.RemoteFunction || (t.RemoteFunction = {}))
}, function (e, t, n) {
    "use strict";

    function r(e, t) {
        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }

    function o(e, t) {
        if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
        return !t || "object" != typeof t && "function" != typeof t ? e : t
    }

    function i(e, t) {
        if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
        e.prototype = Object.create(t && t.prototype, {
            constructor: {
                value: e,
                enumerable: !1,
                writable: !0,
                configurable: !0
            }
        }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var a, s = n(45);
    ! function (e) {
        ! function (e) {
            e[e.DevicesUpdated = "DevicesUpdated"] = "DevicesUpdated", e[e.MediaRendererAdded = "MediaRendererAdded"] = "MediaRendererAdded", e[e.MediaRendererRemoved = "MediaRendererRemoved"] = "MediaRendererRemoved"
        }(e.HardwareEvents || (e.HardwareEvents = {}));
        ! function (e) {
            e[e.VIDEO_QUALITY_HIGH = "video_quality_high"] = "VIDEO_QUALITY_HIGH", e[e.VIDEO_QUALITY_LOW = "video_quality_low"] = "VIDEO_QUALITY_LOW", e[e.VIDEO_QUALITY_MEDIUM = "video_quality_medium"] = "VIDEO_QUALITY_MEDIUM", e[e.VIDEO_SIZE_QQVGA = "video_size_qqvga"] = "VIDEO_SIZE_QQVGA", e[e.VIDEO_SIZE_QCIF = "video_size_qcif"] = "VIDEO_SIZE_QCIF", e[e.VIDEO_SIZE_QVGA = "video_size_qvga"] = "VIDEO_SIZE_QVGA", e[e.VIDEO_SIZE_CIF = "video_size_cif"] = "VIDEO_SIZE_CIF", e[e.VIDEO_SIZE_nHD = "video_size_nhd"] = "VIDEO_SIZE_nHD", e[e.VIDEO_SIZE_VGA = "video_size_vga"] = "VIDEO_SIZE_VGA", e[e.VIDEO_SIZE_SVGA = "video_size_svga"] = "VIDEO_SIZE_SVGA", e[e.VIDEO_SIZE_HD = "video_size_hd"] = "VIDEO_SIZE_HD", e[e.VIDEO_SIZE_UXGA = "video_size_uxga"] = "VIDEO_SIZE_UXGA", e[e.VIDEO_SIZE_FHD = "video_size_fhd"] = "VIDEO_SIZE_FHD", e[e.VIDEO_SIZE_UHD = "video_size_uhd"] = "VIDEO_SIZE_UHD"
        }(e.VideoQuality || (e.VideoQuality = {}));
        var t = function (e) {
            function t() {
                return r(this, t), o(this, (t.__proto__ || Object.getPrototypeOf(t)).apply(this, arguments))
            }
            return i(t, e), t
        }(s.AudioDeviceManager);
        e.AudioDeviceManager = t;
        var n = function (e) {
            function t() {
                return r(this, t), o(this, (t.__proto__ || Object.getPrototypeOf(t)).apply(this, arguments))
            }
            return i(t, e), t
        }(s.CameraManager);
        e.CameraManager = n;
        var a = function (e) {
            function t() {
                return r(this, t), o(this, (t.__proto__ || Object.getPrototypeOf(t)).apply(this, arguments))
            }
            return i(t, e), t
        }(s.StreamManager);
        e.StreamManager = a
    }(a = t.Hardware || (t.Hardware = {})), t.default = a
}, function (e, t, n) {
    "use strict";

    function r(e, t) {
        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }
    var o = function () {
        function e(e, t) {
            for (var n = 0; n < t.length; n++) {
                var r = t[n];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
            }
        }
        return function (t, n, r) {
            return n && e(t.prototype, n), r && e(t, r), t
        }
    }(),
        i = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
            return typeof e
        } : function (e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        },
        a = function (e, t, n, r) {
            var o, a = arguments.length,
                s = a < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
            if ("object" === ("undefined" == typeof Reflect ? "undefined" : i(Reflect)) && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r);
            else
                for (var c = e.length - 1; c >= 0; c--)(o = e[c]) && (s = (a < 3 ? o(s) : a > 3 ? o(t, n, s) : o(t, n)) || s);
            return a > 3 && s && Object.defineProperty(t, n, s), s
        };
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var s = n(19),
        c = n(6),
        l = n(2),
        u = n(24),
        d = n(10),
        g = n(12),
        f = n(0),
        p = n(9),
        v = n(1),
        h = n(25),
        y = n(52),
        m = n(3),
        C = n(11),
        _ = n(53),
        S = n(22),
        E = n(54),
        L = n(21),
        M = n(20),
        R = n(4),
        T = function () {
            function e() {
                var t = this;
                r(this, e), this.protocolVersion = "3", this._h264first = !1, this._calls = {}, this.voxSignaling = l.VoxSignaling.get(), this.log = f.LogManager.get().createLogger(f.LogCategory.SIGNALING, "CallManager"), this.voxSignaling.addHandler(this), this.voxSignaling.setRPCHandler(C.RemoteEvent.handleIncomingConnection, function (e, n, r, o, i) {
                    t.handleIncomingConnection(e, n, r, o, i)
                }), this.voxSignaling.setRPCHandler(C.RemoteEvent.handleConnectionConnected, function (e, n, r, o) {
                    t.handleConnectionConnected(e, n, r)
                }), this.voxSignaling.setRPCHandler(C.RemoteEvent.handleConnectionDisconnected, function (e, n, r) {
                    t.handleConnectionDisconnected(e, n, r)
                }), this.voxSignaling.setRPCHandler(C.RemoteEvent.handleRingOut, function (e) {
                    t.handleRingOut(e)
                }), this.voxSignaling.setRPCHandler(C.RemoteEvent.stopRinging, function (e) {
                    t.stopRinging(e)
                }), this.voxSignaling.setRPCHandler(C.RemoteEvent.handleConnectionFailed, function (e, n, r, o) {
                    t.handleConnectionFailed(e, n, r, o)
                }), this.voxSignaling.setRPCHandler(C.RemoteEvent.handleSIPInfo, function (e, n, r, o, i) {
                    t.handleSIPInfo(e, n, r, o, i)
                }), this.voxSignaling.setRPCHandler(C.RemoteEvent.handleSipEvent, function (e) {
                    t.handleSipEvent(e)
                }), this.voxSignaling.setRPCHandler(C.RemoteEvent.handleTransferStarted, function (e) {
                    t.handleTransferStarted(e)
                }), this.voxSignaling.setRPCHandler(C.RemoteEvent.handleTransferComplete, function (e) {
                    t.handleTransferComplete(e)
                }), this.voxSignaling.setRPCHandler(C.RemoteEvent.handleTransferFailed, function (e) {
                    t.handleTransferFailed(e)
                }), this.voxSignaling.setRPCHandler(C.RemoteEvent.handleReInvite, function (e, n, r, o) {
                    var i = JSON.parse(o);
                    t.handleInReinvite(e, n, r, i)
                }), this.voxSignaling.setRPCHandler(C.RemoteEvent.handleAcceptReinvite, function (e, n, r) {
                    t.handleReinvite(e, n, r)
                }), this.voxSignaling.setRPCHandler(C.RemoteEvent.handleRejectReinvite, function (e, n, r) {
                    t.handleRejectReinvite(e, n, r)
                }), this.voxSignaling.setRPCHandler(C.RemoteEvent.startEarlyMedia, function (e, n, r) {
                    t.startEarlyMedia(e, n, r)
                })
            }
            return o(e, [{
                key: "call",
                value: function (t) {
                    var n = this,
                        r = {
                            number: null,
                            video: {
                                sendVideo: !1,
                                receiveVideo: !1
                            },
                            customData: null,
                            extraHeaders: {},
                            wiredLocal: !0,
                            wiredRemote: !0,
                            H264first: this._h264first,
                            VP8first: !1,
                            forceActive: !1,
                            extraParams: {}
                        },
                        o = u.Utils.mixObjectToLeft(r, t);
                    o = e.addCustomDataToHeaders(o);
                    var i = u.Utils.generateUUID();
                    if (this._calls[i]) throw this.log.error("Call " + i + " already exists"), new Error("Internal error");
                    var a = this.getCallInstance(i, d.Authenticator.get().displayName, !1, o);
                    S.CallstatsIo.isModuleEnabled() && (o.extraHeaders[g.Constants.CALLSTATSIOID_HEADER] = i), o.VP8first && (a.rearangeCodecs = E.CodecSorterHelpers.VP8Sorter), o.H264first && (a.rearangeCodecs = E.CodecSorterHelpers.H264Sorter);
                    var s = !1;
                    return a.settings.active = !0, Object.keys(this._calls).length > 1 && !o.forceActive && (a.setActiveForce(!1), s = !0), void 0 === o.extraHeaders[g.Constants.DIRECT_CALL_HEADER] && "2" == this.protocolVersion ? this.voxSignaling.callRemoteFunction(m.RemoteFunction.createCall, -1, o.number, o.video, i, null, null, o.extraHeaders, o.extraParams) : p.PCFactory.get().setupDirectPC(i, h.PeerConnectionMode.P2P, t.video, s).then(function (e) {
                        a.peerConnection = p.PCFactory.get().peerConnections[i];
                        var t = {
                            tracks: a.peerConnection.getTrackKind()
                        };
                        n.voxSignaling.callRemoteFunction(m.RemoteFunction.createCall, -1, o.number, !0, i, null, null, o.extraHeaders, "", e.sdp, t)
                    }).catch(function (e) {
                        n.handleConnectionFailed(a.id(), 403, "Media access denied", {})
                    }), a.sendVideo(o.video.sendVideo), a
                }
            }, {
                key: "callConference",
                value: function (t) {
                    var n = this,
                        r = {
                            number: null,
                            video: {
                                sendVideo: !1,
                                receiveVideo: !1
                            },
                            customData: null,
                            extraHeaders: {},
                            wiredLocal: !0,
                            wiredRemote: !0,
                            H264first: this._h264first,
                            VP8first: !1,
                            forceActive: !1,
                            extraParams: {}
                        },
                        o = u.Utils.mixObjectToLeft(r, t);
                    o = e.addCustomDataToHeaders(o);
                    var i = u.Utils.generateUUID();
                    if (this._calls[i]) throw this.log.error("Call " + i + " already exists"), new Error("Internal error");
                    var a = this.getCallInstance(i, d.Authenticator.get().displayName, !1, o);
                    S.CallstatsIo.isModuleEnabled() && (o.extraHeaders[g.Constants.CALLSTATSIOID_HEADER] = i), o.VP8first && (a.rearangeCodecs = E.CodecSorterHelpers.VP8Sorter), o.H264first && (a.rearangeCodecs = E.CodecSorterHelpers.H264Sorter);
                    var s = !1;
                    return a.settings.active = !0, Object.keys(this._calls).length > 1 && !o.forceActive && (a.setActiveForce(!1), s = !0), p.PCFactory.get().setupDirectPC(i, h.PeerConnectionMode.P2P, t.video, s).then(function (e) {
                        a.peerConnection = p.PCFactory.get().peerConnections[i];
                        var t = {
                            tracks: a.peerConnection.getTrackKind()
                        };
                        n.voxSignaling.callRemoteFunction(m.RemoteFunction.callConference, -1, o.number, !0, i, null, null, o.extraHeaders, "", e.sdp, t)
                    }), a.sendVideo(o.video.sendVideo), a
                }
            }, {
                key: "handleIncomingConnection",
                value: function (e, t, n, r, o) {
                    var i = this;
                    if (this._calls[e]) throw this.log.error("Call " + e + " already exists"), new Error("Internal error");
                    var a = L.SDPMuggle.detectDirections(o),
                        s = a.some(function (e) {
                            return "video" === e.type && ("sendonly" === e.direction || "sendrecv" === e.direction)
                        }),
                        c = {
                            number: t,
                            extraHeaders: r,
                            video: s,
                            wiredLocal: !0,
                            wiredRemote: !0,
                            forceActive: !1
                        },
                        l = this.getCallInstance(e, n, !0, c);
                    this._h264first && (l.rearangeCodecs = E.CodecSorterHelpers.H264Sorter);
                    var u = !1;
                    l.settings.active = !0, Object.keys(this._calls).length > 1 && (l.setActiveForce(!1), u = !0), void 0 === c.extraHeaders[g.Constants.DIRECT_CALL_HEADER] && "2" == this.protocolVersion ? (l.peerConnection = p.PCFactory.get().getPeerConnect(e), v.Client.getInstance().onIncomingCall(e, t, n, r, this.isSDPHasVideo(o))) : p.PCFactory.get().incomeDirectPC(e, {
                        receiveVideo: !0,
                        sendVideo: !0
                    }, o, u).then(function (a) {
                        l.peerConnection = a, v.Client.getInstance().onIncomingCall(e, t, n, r, i.isSDPHasVideo(o))
                    })
                }
            }, {
                key: "getCallInstance",
                value: function (e, t, n, r) {
                    var o = void 0;
                    return o = "3" == this.protocolVersion ? new _.CallExMedia(e, t, n, r) : void 0 !== r.extraHeaders[g.Constants.DIRECT_CALL_HEADER] ? new _.CallExMedia(e, t, n, r) : new y.CallExServer(e, t, n, r), this._calls[e] = o, o
                }
            }, {
                key: "isSDPHasVideo",
                value: function (e) {
                    var t = e.indexOf("m=video");
                    if (-1 === t) return !1;
                    var n = e.indexOf("a=sendrecv", t),
                        r = e.indexOf("a=sendonly", t),
                        o = e.indexOf("m=", t);
                    return -1 !== n && (n < o || -1 === o) || -1 !== r && (r < o || -1 === o)
                }
            }, {
                key: "findCall",
                value: function (e, t) {
                    var n = this._calls[e];
                    return "" === e && (n = this._calls[Object.keys(this._calls)[0]]), void 0 === n ? (this.log.warning("Received " + t + " for unknown call " + e), null) : n
                }
            }, {
                key: "handleRingOut",
                value: function (e) {
                    var t = this.findCall(e, "handleRingOut");
                    void 0 !== t && (v.Client.getInstance().playProgressTone(!0), t.onRingOut(), t.canStartSendingCandidates())
                }
            }, {
                key: "handleConnectionConnected",
                value: function (e, t, n) {
                    var r = this.findCall(e, "handleConnectionConnected");
                    if (r.signalingConnected = !0, r.canStartSendingCandidates(), void 0 !== r && (r.onConnected(t, n), void 0 !== n && n.length > 0)) {
                        var o = n.indexOf("m=video");
                        if (-1 !== o) {
                            var i = n.indexOf("a=sendrecv", o),
                                a = n.indexOf("a=sendonly", o),
                                s = n.indexOf("a=recvonly", o),
                                c = n.indexOf("a=inactive", o); - 1 === i && -1 === a && -1 === s && -1 === c && (n += "a=inactive\r\n")
                        }
                        r.peerConnection.processRemoteAnswer(t, n)
                    }
                }
            }, {
                key: "startEarlyMedia",
                value: function (e, t, n) {
                    var r = this.findCall(e, "startEarlyMedia");
                    r.settings.hasEarlyMedia = !0, M.EndpointManager.get().createDefaultEndPoint(r, r.settings.id), void 0 !== n && r.peerConnection.processRemoteAnswer(t, n), v.Client.getInstance().stopProgressTone()
                }
            }, {
                key: "handleConnectionDisconnected",
                value: function (e, t, n) {
                    var r = this,
                        o = this.findCall(e, "handleConnectionDisconnected");
                    o && (v.Client.getInstance().stopProgressTone(), o.onDisconnected(t, n).then(function () {
                        delete r._calls[e]
                    }).catch(function (t) {
                        r.log.error("Can't remove the call " + e + ": " + t.message)
                    }))
                }
            }, {
                key: "handleSIPInfo",
                value: function (e, t, n, r, o) {
                    var i = this.findCall(e, "handleSIPInfo");
                    void 0 !== i && i.onInfo(i, t, n, r, o)
                }
            }, {
                key: "stopRinging",
                value: function (e) {
                    var t = this.findCall(e, "stopRinging");
                    t.canStartSendingCandidates(), void 0 !== t && (v.Client.getInstance().stopProgressTone(), t.onStopRinging())
                }
            }, {
                key: "handleSipEvent",
                value: function (e) { }
            }, {
                key: "handleTransferStarted",
                value: function (e) { }
            }, {
                key: "handleTransferComplete",
                value: function (e) {
                    var t = this.findCall(e, "handleTransferComplete");
                    void 0 !== t && t.onTransferComplete()
                }
            }, {
                key: "handleTransferFailed",
                value: function (e) {
                    var t = this.findCall(e, "handleTransferFailed");
                    void 0 !== t && t.onTransferFailed()
                }
            }, {
                key: "handleReinvite",
                value: function (e, t, n) {
                    var r = this.findCall(e, "handleReinvite");
                    if (void 0 !== r) {
                        var o = this.isSDPHasVideo(n);
                        r.peerConnection.handleReinvite(t, n, o)
                    }
                }
            }, {
                key: "handleRejectReinvite",
                value: function (e, t, n) {
                    var r = this.findCall(e, "handleReinvite");
                    void 0 !== r && r.dispatchEvent({
                        code: 20,
                        call: r
                    })
                }
            }, {
                key: "handleInReinvite",
                value: function (e, t, n, r) {
                    var o = this.findCall(e, "handleReinvite");
                    void 0 !== o && (M.EndpointManager.get().setCallTrackInfo(o, r), o.runIncomingReInvite(t, n), o.dispatchEvent({
                        name: c.CallEvents.PendingUpdate,
                        result: !0,
                        call: o
                    }))
                }
            }, {
                key: "handleConnectionFailed",
                value: function (e, t, n, r) {
                    var o = this.findCall(e, "handleConnectionFailed");
                    void 0 !== o && (delete this._calls[e], v.Client.getInstance().stopProgressTone(), o.onFailed(t, n, r))
                }
            }, {
                key: "onSignalingConnected",
                value: function () { }
            }, {
                key: "onSignalingClosed",
                value: function () {
                    for (var e in this._calls) this._calls.hasOwnProperty(e) && (this._calls[e].hangup(), this._calls[e].onFailed(409, "Connection Closed", {}))
                }
            }, {
                key: "onSignalingConnectionFailed",
                value: function (e) { }
            }, {
                key: "onMediaConnectionFailed",
                value: function () { }
            }, {
                key: "recalculateNumCalls",
                value: function () {
                    this._numCalls = 0;
                    for (var e in this._calls) this._calls.hasOwnProperty(e) && this._numCalls++
                }
            }, {
                key: "transferCall",
                value: function (e, t) {
                    for (var n = [e, t], r = 0; r < n.length; r++) {
                        var o = this._calls[n[r].id()];
                        if (!o) return void this.log.error("trying to transfer unknown call " + o.id());
                        if (o.stateValue != s.CallState.CONNECTED) return void this.log.error("trying to transfer call " + o.id() + " in state " + o.state())
                    }
                    this.voxSignaling.callRemoteFunction(m.RemoteFunction.transferCall, e.id(), t.id())
                }
            }, {
                key: "removeCall",
                value: function (e) {
                    delete this._calls[e]
                }
            }, {
                key: "setProtocolVersion",
                value: function (e) {
                    this.protocolVersion = e
                }
            }, {
                key: "setAllCallsVolume",
                value: function (e) {
                    for (var t in this._calls) this._calls.hasOwnProperty(t) && M.EndpointManager.get().setCallVolume(this._calls[t], e)
                }
            }, {
                key: "useVideoSource",
                value: function (e) {
                    var t = this,
                        n = Object.keys(this._calls).length;
                    return new Promise(function (r, o) {
                        for (var i in t._calls)
                            if (t._calls.hasOwnProperty(i)) {
                                var a = t._calls[i];
                                R.default.CameraManager.get().setCallVideoSettings(a, Object.assign({}, R.default.CameraManager.get().getCallVideoSettings(a), {
                                    cameraId: e
                                })), R.default.StreamManager.get().updateCallStream(a).then(function (e) {
                                    --n <= 0 && r()
                                }, function (e) {
                                    o(e)
                                })
                            }
                    })
                }
            }, {
                key: "setVideoSettings",
                value: function (e) {
                    var t = this,
                        n = Object.keys(this._calls).length;
                    return new Promise(function (r, o) {
                        0 === n && r();
                        for (var i in t._calls)
                            if (t._calls.hasOwnProperty(i)) {
                                var a = t._calls[i];
                                R.default.CameraManager.get().setCallVideoSettings(a, R.default.CameraManager.legacyParamConverter(e)).then(function (e) {
                                    --n <= 0 && r()
                                }, function (e) {
                                    o(e)
                                })
                            }
                    })
                }
            }, {
                key: "useAudioSource",
                value: function (e) {
                    var t = this,
                        n = Object.keys(this._calls).length;
                    return new Promise(function (r, o) {
                        0 === n && r();
                        for (var i in t._calls)
                            if (t._calls.hasOwnProperty(i)) {
                                var a = t._calls[i];
                                R.default.AudioDeviceManager.get().setCallAudioSettings(a, Object.assign({}, R.default.AudioDeviceManager.get().getCallAudioSettings(a), {
                                    inputId: e
                                })).then(function (e) {
                                    --n <= 0 && r()
                                }, function (e) {
                                    o(e)
                                })
                            }
                    })
                }
            }, {
                key: "_traceName",
                value: function () {
                    return "CallManager"
                }
            }, {
                key: "numCalls",
                get: function () {
                    return this._numCalls
                }
            }, {
                key: "calls",
                get: function () {
                    return this._calls
                }
            }], [{
                key: "get",
                value: function () {
                    return void 0 === this.inst && (this.inst = new e), this.inst
                }
            }, {
                key: "addCustomDataToHeaders",
                value: function (e) {
                    return void 0 !== e.customData && (void 0 === e.extraHeaders && (e.extraHeaders = {}), e.extraHeaders["VI-CallData"] = e.customData), e
                }
            }, {
                key: "cleanHeaders",
                value: function (e) {
                    var t = {};
                    for (var n in e) "X-" != n.substring(0, 2) && n != g.Constants.CALL_DATA_HEADER || (t[n] = e[n]);
                    return t
                }
            }]), e
        }();
    a([f.LogManager.d_trace(f.LogCategory.CALLMANAGER)], T.prototype, "call", null), a([f.LogManager.d_trace(f.LogCategory.CALLMANAGER)], T.prototype, "handleIncomingConnection", null), a([f.LogManager.d_trace(f.LogCategory.CALLMANAGER)], T.prototype, "findCall", null), a([f.LogManager.d_trace(f.LogCategory.CALLMANAGER)], T.prototype, "handleRingOut", null), a([f.LogManager.d_trace(f.LogCategory.CALLMANAGER)], T.prototype, "handleConnectionConnected", null), a([f.LogManager.d_trace(f.LogCategory.CALLMANAGER)], T.prototype, "handleConnectionDisconnected", null), a([f.LogManager.d_trace(f.LogCategory.CALLEXSERVER)], T.prototype, "handleSIPInfo", null), a([f.LogManager.d_trace(f.LogCategory.CALLMANAGER)], T.prototype, "stopRinging", null), a([f.LogManager.d_trace(f.LogCategory.CALLMANAGER)], T.prototype, "handleSipEvent", null), a([f.LogManager.d_trace(f.LogCategory.CALLMANAGER)], T.prototype, "handleTransferStarted", null), a([f.LogManager.d_trace(f.LogCategory.CALLMANAGER)], T.prototype, "handleTransferComplete", null), a([f.LogManager.d_trace(f.LogCategory.CALLMANAGER)], T.prototype, "handleTransferFailed", null), a([f.LogManager.d_trace(f.LogCategory.CALLMANAGER)], T.prototype, "handleReinvite", null), a([f.LogManager.d_trace(f.LogCategory.CALLMANAGER)], T.prototype, "handleRejectReinvite", null), a([f.LogManager.d_trace(f.LogCategory.CALLMANAGER)], T.prototype, "handleInReinvite", null), a([f.LogManager.d_trace(f.LogCategory.CALLMANAGER)], T.prototype, "handleConnectionFailed", null), a([f.LogManager.d_trace(f.LogCategory.CALLMANAGER)], T.prototype, "onSignalingConnected", null), a([f.LogManager.d_trace(f.LogCategory.CALLMANAGER)], T.prototype, "onSignalingClosed", null), a([f.LogManager.d_trace(f.LogCategory.CALLMANAGER)], T.prototype, "onSignalingConnectionFailed", null), a([f.LogManager.d_trace(f.LogCategory.CALLMANAGER)], T.prototype, "removeCall", null), a([f.LogManager.d_trace(f.LogCategory.CALLMANAGER)], T.prototype, "setAllCallsVolume", null), a([f.LogManager.d_trace(f.LogCategory.CALLMANAGER)], T.prototype, "useVideoSource", null), a([f.LogManager.d_trace(f.LogCategory.CALLMANAGER)], T.prototype, "setVideoSettings", null), a([f.LogManager.d_trace(f.LogCategory.CALLMANAGER)], T.prototype, "useAudioSource", null), a([f.LogManager.d_trace(f.LogCategory.CALLMANAGER)], T, "addCustomDataToHeaders", null), t.CallManager = T
}, function (e, t, n) {
    "use strict";
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    ! function (e) {
        e[e.Connected = "Connected"] = "Connected", e[e.Disconnected = "Disconnected"] = "Disconnected", e[e.Failed = "Failed"] = "Failed", e[e.ProgressToneStart = "ProgressToneStart"] = "ProgressToneStart", e[e.ProgressToneStop = "ProgressToneStop"] = "ProgressToneStop", e[e.MessageReceived = "onSendMessage"] = "MessageReceived", e[e.InfoReceived = "InfoReceived"] = "InfoReceived", e[e.TransferComplete = "TransferComplete"] = "TransferComplete", e[e.TransferFailed = "TransferFailed"] = "TransferFailed", e[e.ICETimeout = "ICETimeout"] = "ICETimeout", e[e.RTCStatsReceived = "RTCStatsReceived"] = "RTCStatsReceived", e[e.MediaElementCreated = "MediaElementCreated"] = "MediaElementCreated", e[e.MediaElementRemoved = "MediaElementRemoved"] = "MediaElementRemoved", e[e.ICECompleted = "ICECompleted"] = "ICECompleted", e[e.Updated = "Updated"] = "Updated", e[e.PendingUpdate = "PendingUpdate"] = "PendingUpdate", e[e.UpdateFailed = "UpdateFailed"] = "UpdateFailed", e[e.LocalVideoStreamAdded = "LocalVideoStreamAdded"] = "LocalVideoStreamAdded", e[e.EndpointAdded = "EndpointAdded"] = "EndpointAdded"
    }(t.CallEvents || (t.CallEvents = {}))
}, function (e, t, n) {
    "use strict";
    var r = !0,
        o = !0,
        i = {
            disableLog: function (e) {
                return "boolean" != typeof e ? new Error("Argument type: " + typeof e + ". Please use a boolean.") : (r = e, e ? "adapter.js logging disabled" : "adapter.js logging enabled")
            },
            disableWarnings: function (e) {
                return "boolean" != typeof e ? new Error("Argument type: " + typeof e + ". Please use a boolean.") : (o = !e, "adapter.js deprecation warnings " + (e ? "disabled" : "enabled"))
            },
            log: function () {
                if ("object" == typeof window) {
                    if (r) return;
                    "undefined" != typeof console && "function" == typeof console.log && console.log.apply(console, arguments)
                }
            },
            deprecated: function (e, t) {
                o && console.warn(e + " is deprecated, please use " + t + " instead.")
            },
            extractVersion: function (e, t, n) {
                var r = e.match(t);
                return r && r.length >= n && parseInt(r[n], 10)
            },
            detectBrowser: function (e) {
                var t = e && e.navigator,
                    n = {};
                if (n.browser = null, n.version = null, void 0 === e || !e.navigator) return n.browser = "Not a browser.", n;
                if (t.mozGetUserMedia) n.browser = "firefox", n.version = this.extractVersion(t.userAgent, /Firefox\/(\d+)\./, 1);
                else if (t.webkitGetUserMedia)
                    if (e.webkitRTCPeerConnection) n.browser = "chrome", n.version = this.extractVersion(t.userAgent, /Chrom(e|ium)\/(\d+)\./, 2);
                    else {
                        if (!t.userAgent.match(/Version\/(\d+).(\d+)/)) return n.browser = "Unsupported webkit-based browser with GUM support but no WebRTC support.", n;
                        n.browser = "safari", n.version = this.extractVersion(t.userAgent, /AppleWebKit\/(\d+)\./, 1)
                    } else if (t.mediaDevices && t.userAgent.match(/Edge\/(\d+).(\d+)$/)) n.browser = "edge", n.version = this.extractVersion(t.userAgent, /Edge\/(\d+).(\d+)$/, 2);
                else {
                    if (!t.mediaDevices || !t.userAgent.match(/AppleWebKit\/(\d+)\./)) return n.browser = "Not a supported browser.", n;
                    n.browser = "safari", n.version = this.extractVersion(t.userAgent, /AppleWebKit\/(\d+)\./, 1)
                }
                return n
            }
        };
    e.exports = {
        log: i.log,
        deprecated: i.deprecated,
        disableLog: i.disableLog,
        disableWarnings: i.disableWarnings,
        extractVersion: i.extractVersion,
        shimCreateObjectURL: i.shimCreateObjectURL,
        detectBrowser: i.detectBrowser.bind(i)
    }
}, function (e, t, n) {
    "use strict";
    var r = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
        return typeof e
    } : function (e) {
        return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
    };
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var o, i = n(0),
        a = n(42),
        s = n(43),
        c = n(44),
        l = n(56),
        u = n(18),
        d = n(57);
    ! function (e) {
        function t(e, t, n) {
            var o = e;
            return "object" != (void 0 === o ? "undefined" : r(o)) && (o = {}), o[t] = {
                ideal: n
            }, o
        }

        function n(e, t, n) {
            switch (S) {
                case _.Firefox:
                case _.Webkit:
                case _.Safari:
                case _.Edge:
                    return new c.WebRTCPC(e, t, n);
                default:
                    return i.LogManager.get().writeMessage(i.LogCategory.RTC, "Core", i.LogLevel.INFO, "Unsupported browser " + navigator.userAgent), null
            }
        }

        function o() {
            return navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPod/i)
        }

        function g() {
            return S === _.Firefox || S === _.Webkit
        }

        function f(e) {
            return navigator.mediaDevices.getUserMedia(e)
        }

        function p(e, t) {
            return new u.SignalingDTMFSender(t)
        }

        function v() {
            return new Promise(function (e) {
                e(!1)
            })
        }

        function h(e) {
            var n = !1,
                r = !1;
            return e.audioEnabled && (n = !0, e.audioInputId && (n = t(n, "deviceId", e.audioInputId))), e.videoEnabled && (r = !0, e.videoSettings && (r = e.videoSettings), e.videoInputId && (r = t(r, "deviceId", e.videoInputId))), {
                peerIdentity: null,
                audio: n,
                video: r
            }
        }

        function y() {
            if (!1 === (!(arguments.length > 0 && void 0 !== arguments[0]) || arguments[0])) return "voxmobile";
            switch (S || m(), S) {
                case _.Firefox:
                    return "firefox";
                case _.Webkit:
                    return "chrome";
                case _.Safari:
                    return "safari";
                case _.Edge:
                    return "edge";
                default:
                    return ""
            }
        }

        function m() {
            navigator.mozGetUserMedia ? S = _.Firefox : navigator.webkitGetUserMedia ? S = _.Webkit : navigator.mediaDevices && navigator.userAgent.match(/Edge\/(\d+).(\d+)$/) ? S = _.Edge : navigator.getUserMedia && (S = _.Safari)
        }

        function C() {
            switch (S || m(), S && i.LogManager.get().writeMessage(i.LogCategory.RTC, "Core", i.LogLevel.INFO, "Detected browser " + _[S]), e.getUserMedia = f, e.getDTMFSender = p, e.screenSharingSupported = v, S) {
                case _.Firefox:
                    e.attachMedia = a.FF.attachStream, e.detachMedia = a.FF.detachStream, e.getScreenMedia = a.FF.getScreenMedia, e.getRTCStats = a.FF.getRTCStats, e.getUserMedia = a.FF.getUserMedia, e.screenSharingSupported = a.FF.screenSharingSupported, e.getDTMFSender = a.FF.getDTMFSender;
                    break;
                case _.Webkit:
                    e.attachMedia = s.Webkit.attachStream, e.detachMedia = s.Webkit.detachStream, e.getScreenMedia = s.Webkit.getScreenMedia, e.getRTCStats = s.Webkit.getRTCStats, e.getUserMedia = s.Webkit.getUserMedia, e.screenSharingSupported = s.Webkit.screenSharingSupported, e.getDTMFSender = s.Webkit.getDTMFSender;
                    break;
                case _.Safari:
                    e.attachMedia = d.Safari.attachStream, e.detachMedia = d.Safari.detachStream, e.getScreenMedia = d.Safari.getScreenMedia, e.getRTCStats = d.Safari.getRTCStats, e.getUserMedia = a.FF.getUserMedia, e.getDTMFSender = d.Safari.getDTMFSender;
                    break;
                case _.Edge:
                    e.attachMedia = l.Edge.attachStream, e.detachMedia = l.Edge.detachStream, e.getScreenMedia = l.Edge.getScreenMedia, e.screenSharingSupported = l.Edge.screenSharingSupported, e.getRTCStats = l.Edge.getRTCStats;
                    break;
                default:
                    i.LogManager.get().writeMessage(i.LogCategory.RTC, "Core", i.LogLevel.INFO, "Unsupported browser " + navigator.userAgent)
            }
            e.composeConstraints = h
        }
        var _ = void 0;
        ! function (e) {
            e[e.Firefox = 1] = "Firefox", e[e.Webkit = 2] = "Webkit", e[e.Edge = 3] = "Edge", e[e.Safari = 4] = "Safari"
        }(_ || (_ = {}));
        var S = void 0;
        e.peerConnectionFactory = n, e.isIphone = o, e.isScreenSharingSupported = g, e.getWSVendor = y, e.init = C
    }(o || (o = {})), t.default = o
}, function (e, t, n) {
    "use strict";

    function r(e, t) {
        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }
    var o = function () {
        function e(e, t) {
            for (var n = 0; n < t.length; n++) {
                var r = t[n];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
            }
        }
        return function (t, n, r) {
            return n && e(t.prototype, n), r && e(t, r), t
        }
    }(),
        i = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
            return typeof e
        } : function (e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        },
        a = function (e, t, n, r) {
            var o, a = arguments.length,
                s = a < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
            if ("object" === ("undefined" == typeof Reflect ? "undefined" : i(Reflect)) && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r);
            else
                for (var c = e.length - 1; c >= 0; c--)(o = e[c]) && (s = (a < 3 ? o(s) : a > 3 ? o(t, n, s) : o(t, n)) || s);
            return a > 3 && s && Object.defineProperty(t, n, s), s
        };
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var s = n(25),
        c = n(2),
        l = n(0),
        u = n(5),
        d = n(19),
        g = n(8),
        f = n(3),
        p = n(11),
        v = n(1),
        h = n(4),
        y = n(21),
        m = function () {
            function e() {
                var t = this;
                r(this, e), this.iceConfig = null, this._peerConnections = {}, this.waitingPeerConnections = {}, this.log = l.LogManager.get().createLogger(l.LogCategory.RTC, "PCFactory"), this._requireMedia = !0, c.VoxSignaling.get().setRPCHandler(p.RemoteEvent.createPC, function (e, n) {
                    t.rpcHandlerCreatePC(e, n)
                }), c.VoxSignaling.get().setRPCHandler(p.RemoteEvent.destroyPC, function (e) {
                    t.rpcHandlerDestroyPC(e)
                }), c.VoxSignaling.get().addHandler(this)
            }
            return o(e, [{
                key: "rpcHandlerCreatePC",
                value: function (t, n) {
                    n = y.SDPMuggle.addSetupAttribute(n);
                    var r = e.sdpOffersVideo(n),
                        o = s.PeerConnectionMode.CLIENT_SERVER_V1;
                    c.VoxSignaling.get().callRemoteFunction(f.RemoteFunction.muteLocal, t, !1);
                    var i = g.default.peerConnectionFactory(t, o, r);
                    this._peerConnections[t] = i;
                    var a = u.CallManager.get().calls[t];
                    h.default.StreamManager.get().getCallStream(a).then(function (e) {
                        i.fastAddCustomMedia(e), i.processRemoteOffer(n).then(function (e) {
                            void 0 === a || a.checkCallMode(d.CallMode.SERVER) ? c.VoxSignaling.get().callRemoteFunction(f.RemoteFunction.confirmPC, t, e) : c.VoxSignaling.get().callRemoteFunction(f.RemoteFunction.acceptCall, [t, u.CallManager.cleanHeaders(a.headers()), e]), "__default" !== t && void 0 !== u.CallManager.get().calls[t] && (u.CallManager.get().calls[t].peerConnection = i)
                        })
                    }).catch(function (e) {
                        void 0 !== a ? u.CallManager.get().handleConnectionFailed(a.id(), 403, "Media access denied", {}) : c.VoxSignaling.get().onConnectionFailedRPC()
                    })
                }
            }, {
                key: "rpcHandlerDestroyPC",
                value: function (e) {
                    this._peerConnections[e] && (this._peerConnections[e].close(), delete this._peerConnections[e]), delete this.waitingPeerConnections[e]
                }
            }, {
                key: "setupDirectPC",
                value: function (e, t, n, r) {
                    var o = this,
                        i = g.default.peerConnectionFactory(e, t, n);
                    i.setHoldKey(r);
                    var a = (v.Client.getInstance().config(), h.default.StreamManager.get()),
                        s = u.CallManager.get().calls[e];
                    return a.getCallStream(s).then(function (t) {
                        return null !== t && i.fastAddCustomMedia(t), o._peerConnections[e] = i, i.getLocalOffer()
                    })
                }
            }, {
                key: "incomeDirectPC",
                value: function (e, t, n, r) {
                    var o = this,
                        i = g.default.peerConnectionFactory(e, s.PeerConnectionMode.P2P, t);
                    return i.setHoldKey(r), i._setRemoteDescription(n).then(function () {
                        return o._peerConnections[e] = i, i
                    })
                }
            }, {
                key: "getPeerConnect",
                value: function (e) {
                    return this._peerConnections[e]
                }
            }, {
                key: "onSignalingConnected",
                value: function () { }
            }, {
                key: "onSignalingClosed",
                value: function () {
                    this.log.info("Closing all peer connections because signaling connection has closed"), this.waitingPeerConnections = {};
                    for (var e in this._peerConnections) this._peerConnections[e].close();
                    this._peerConnections = {}
                }
            }, {
                key: "onSignalingConnectionFailed",
                value: function (e) { }
            }, {
                key: "onMediaConnectionFailed",
                value: function () { }
            }, {
                key: "closeAll",
                value: function () {
                    for (var e in this._peerConnections) this._peerConnections[e].close();
                    this._peerConnections = {}
                }
            }, {
                key: "setBandwidthParams",
                value: function (e) {
                    this._bandwidthParams = e
                }
            }, {
                key: "addBandwidthParams",
                value: function (e) {
                    return this._bandwidthParams && (e.sdp = e.sdp.replace(/(a=mid:video.*\r\n)/g, "$1b=AS:" + this._bandwidthParams + "\r\n")), e
                }
            }, {
                key: "_traceName",
                value: function () {
                    return "PCFactory"
                }
            }, {
                key: "requireMedia",
                get: function () {
                    return this._requireMedia
                },
                set: function (e) {
                    this._requireMedia = e
                }
            }, {
                key: "peerConnections",
                get: function () {
                    return this._peerConnections
                }
            }], [{
                key: "get",
                value: function () {
                    return null === this.inst && (this.inst = new e), this.inst
                }
            }, {
                key: "sdpOffersVideo",
                value: function (e) {
                    return {
                        receiveVideo: -1 !== e.indexOf("m=video"),
                        sendVideo: !0
                    }
                }
            }]), e
        }();
    m.inst = null, m.hasTransceivers = !1, a([l.LogManager.d_trace(l.LogCategory.PCFACTORY)], m.prototype, "rpcHandlerCreatePC", null), a([l.LogManager.d_trace(l.LogCategory.PCFACTORY)], m.prototype, "rpcHandlerDestroyPC", null), a([l.LogManager.d_trace(l.LogCategory.PCFACTORY)], m.prototype, "setupDirectPC", null), a([l.LogManager.d_trace(l.LogCategory.PCFACTORY)], m.prototype, "incomeDirectPC", null), a([l.LogManager.d_trace(l.LogCategory.PCFACTORY)], m.prototype, "getPeerConnect", null), a([l.LogManager.d_trace(l.LogCategory.PCFACTORY)], m.prototype, "onSignalingConnected", null), a([l.LogManager.d_trace(l.LogCategory.PCFACTORY)], m.prototype, "onSignalingClosed", null), a([l.LogManager.d_trace(l.LogCategory.PCFACTORY)], m.prototype, "onSignalingConnectionFailed", null), a([l.LogManager.d_trace(l.LogCategory.PCFACTORY)], m.prototype, "onMediaConnectionFailed", null), a([l.LogManager.d_trace(l.LogCategory.PCFACTORY)], m.prototype, "closeAll", null), a([l.LogManager.d_trace(l.LogCategory.PCFACTORY)], m.prototype, "setBandwidthParams", null), a([l.LogManager.d_trace(l.LogCategory.PCFACTORY)], m.prototype, "addBandwidthParams", null), a([l.LogManager.d_trace(l.LogCategory.PCFACTORY)], m, "sdpOffersVideo", null), t.PCFactory = m
}, function (e, t, n) {
    "use strict";

    function r(e, t) {
        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }
    var o = function () {
        function e(e, t) {
            for (var n = 0; n < t.length; n++) {
                var r = t[n];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
            }
        }
        return function (t, n, r) {
            return n && e(t.prototype, n), r && e(t, r), t
        }
    }(),
        i = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
            return typeof e
        } : function (e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        },
        a = function (e, t, n, r) {
            var o, a = arguments.length,
                s = a < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
            if ("object" === ("undefined" == typeof Reflect ? "undefined" : i(Reflect)) && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r);
            else
                for (var c = e.length - 1; c >= 0; c--)(o = e[c]) && (s = (a < 3 ? o(s) : a > 3 ? o(t, n, s) : o(t, n)) || s);
            return a > 3 && s && Object.defineProperty(t, n, s), s
        };
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var s, c = n(2),
        l = n(0),
        u = n(9),
        d = n(3),
        g = n(11),
        f = n(22);
    ! function (e) {
        e[e.IDLE = 0] = "IDLE", e[e.IN_PROGRESS = 1] = "IN_PROGRESS"
    }(s = t.AuthenticatorState || (t.AuthenticatorState = {}));
    var p = function () {
        function e() {
            var t = this;
            r(this, e), this.FAIL_CODE_SECOND_STAGE = 301, this.FAIL_CODE_ONE_TIME_KEY = 302, this._displayName = null, this._username = null, this._authorized = !1, this.signaling = c.VoxSignaling.get(), this.currentState = s.IDLE, this.log = l.LogManager.get().createLogger(l.LogCategory.SIGNALING, "Authenticator"), this.signaling.setRPCHandler(g.RemoteEvent.loginFailed, function (e, n) {
                t.onLoginFailed(e, n)
            }), this.signaling.setRPCHandler(g.RemoteEvent.loginSuccessful, function (e, n) {
                t.onLoginSuccesful(e, n)
            }), this.signaling.setRPCHandler(g.RemoteEvent.refreshOauthTokenFailed, function (e) {
                t.handler.onRefreshTokenFailed(e)
            }), this.signaling.setRPCHandler(g.RemoteEvent.refreshOauthTokenSuccessful, function (e) {
                t.handler.onRefreshTokenSuccess(e.OAuth)
            }), this.signaling.addHandler(this)
        }
        return o(e, [{
            key: "setHandler",
            value: function (e) {
                this.handler = e
            }
        }, {
            key: "onLoginFailed",
            value: function (e, t) {
                switch (this.currentState = s.IDLE, e) {
                    case this.FAIL_CODE_ONE_TIME_KEY:
                        this.handler.onOneTimeKeyGenerated(t);
                        break;
                    case this.FAIL_CODE_SECOND_STAGE:
                        this.handler.onSecondStageInitiated();
                        break;
                    default:
                        this.handler.onLoginFailed(e)
                }
            }
        }, {
            key: "onLoginSuccesful",
            value: function (e, t) {
                this.currentState = s.IDLE, this._authorized = !0, t && (u.PCFactory.get().iceConfig = t.iceConfig), this._displayName = e, f.CallstatsIo.get().init({
                    userName: this._username,
                    aliasName: this._displayName
                }), this.handler.onLoginSuccessful(e, t.OAuth)
            }
        }, {
            key: "basicLogin",
            value: function (e, t, n) {
                if (this.currentState != s.IDLE) return void this.log.error("Login operation already in progress");
                this._username = e, this.currentState = s.IN_PROGRESS, this.signaling.callRemoteFunction(d.RemoteFunction.login, e, t, n)
            }
        }, {
            key: "tokenLogin",
            value: function (e, t) {
                if (this.currentState != s.IDLE) return void this.log.error("Login operation already in progress");
                this._username = e, this.currentState = s.IN_PROGRESS, this.signaling.callRemoteFunction(d.RemoteFunction.login, e, "", t)
            }
        }, {
            key: "tokenRefresh",
            value: function (e, t, n) {
                n ? this.signaling.callRemoteFunction(d.RemoteFunction.refreshOauthToken, e, {
                    refreshToken: t,
                    deviceToken: n
                }) : this.signaling.callRemoteFunction(d.RemoteFunction.refreshOauthToken, e, t)
            }
        }, {
            key: "loginUsingOneTimeKey",
            value: function (e, t, n) {
                if (this.currentState != s.IDLE) return void this.log.error("Login operation already in progress");
                this._username = e, this.currentState = s.IN_PROGRESS, this.signaling.callRemoteFunction(d.RemoteFunction.loginUsingOneTimeKey, e, t, n)
            }
        }, {
            key: "loginStage2",
            value: function (e, t, n) {
                if (this.currentState != s.IDLE) return void this.log.error("Login operation already in progress");
                this._username = e, this.currentState = s.IN_PROGRESS, this.signaling.callRemoteFunction(d.RemoteFunction.loginStage2, e, t, n)
            }
        }, {
            key: "generateOneTimeKey",
            value: function (e) {
                if (this.currentState != s.IDLE) return void this.log.error("Login operation already in progress");
                this.currentState = s.IN_PROGRESS, this.signaling.callRemoteFunction(d.RemoteFunction.loginGenerateOneTimeKey, e)
            }
        }, {
            key: "username",
            value: function () {
                return this._username
            }
        }, {
            key: "authorized",
            value: function () {
                return this._authorized
            }
        }, {
            key: "onSignalingConnected",
            value: function () { }
        }, {
            key: "onSignalingConnectionFailed",
            value: function (e) { }
        }, {
            key: "onSignalingClosed",
            value: function () {
                this._authorized = !1, this._displayName = null, this._username = null
            }
        }, {
            key: "onMediaConnectionFailed",
            value: function () { }
        }, {
            key: "ziAuthorized",
            value: function (e) {
                this._authorized = e
            }
        }, {
            key: "_traceName",
            value: function () {
                return "Authenticator"
            }
        }, {
            key: "displayName",
            get: function () {
                return this._displayName
            }
        }], [{
            key: "get",
            value: function () {
                return void 0 === this.inst && (this.inst = new e), this.inst
            }
        }]), e
    }();
    a([l.LogManager.d_trace(l.LogCategory.AUTHENTICATOR)], p.prototype, "setHandler", null), a([l.LogManager.d_trace(l.LogCategory.AUTHENTICATOR)], p.prototype, "onLoginFailed", null), a([l.LogManager.d_trace(l.LogCategory.AUTHENTICATOR)], p.prototype, "onLoginSuccesful", null), a([l.LogManager.d_trace(l.LogCategory.AUTHENTICATOR)], p.prototype, "basicLogin", null), a([l.LogManager.d_trace(l.LogCategory.AUTHENTICATOR)], p.prototype, "tokenLogin", null), a([l.LogManager.d_trace(l.LogCategory.AUTHENTICATOR)], p.prototype, "tokenRefresh", null), a([l.LogManager.d_trace(l.LogCategory.AUTHENTICATOR)], p.prototype, "loginUsingOneTimeKey", null), a([l.LogManager.d_trace(l.LogCategory.AUTHENTICATOR)], p.prototype, "loginStage2", null), a([l.LogManager.d_trace(l.LogCategory.AUTHENTICATOR)], p.prototype, "generateOneTimeKey", null), a([l.LogManager.d_trace(l.LogCategory.AUTHENTICATOR)], p.prototype, "username", null), a([l.LogManager.d_trace(l.LogCategory.AUTHENTICATOR)], p.prototype, "authorized", null), a([l.LogManager.d_trace(l.LogCategory.AUTHENTICATOR)], p.prototype, "onSignalingConnected", null), a([l.LogManager.d_trace(l.LogCategory.AUTHENTICATOR)], p.prototype, "onSignalingConnectionFailed", null), a([l.LogManager.d_trace(l.LogCategory.AUTHENTICATOR)], p.prototype, "onSignalingClosed", null), a([l.LogManager.d_trace(l.LogCategory.AUTHENTICATOR)], p.prototype, "onMediaConnectionFailed", null), t.Authenticator = p
}, function (e, t, n) {
    "use strict";
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    ! function (e) {
        e[e.loginFailed = "loginFailed"] = "loginFailed", e[e.loginSuccessful = "loginSuccessful"] = "loginSuccessful", e[e.handleError = "handleError"] = "handleError", e[e.onPCStats = "__onPCStats"] = "onPCStats", e[e.handleIncomingConnection = "handleIncomingConnection"] = "handleIncomingConnection", e[e.handleConnectionConnected = "handleConnectionConnected"] = "handleConnectionConnected", e[e.handleConnectionDisconnected = "handleConnectionDisconnected"] = "handleConnectionDisconnected", e[e.handleRingOut = "handleRingOut"] = "handleRingOut", e[e.startEarlyMedia = "startEarlyMedia"] = "startEarlyMedia", e[e.stopRinging = "stopRinging"] = "stopRinging", e[e.handleConnectionFailed = "handleConnectionFailed"] = "handleConnectionFailed", e[e.handleSIPInfo = "handleSIPInfo"] = "handleSIPInfo", e[e.handleSipEvent = "handleSipEvent"] = "handleSipEvent", e[e.handleTransferStarted = "handleTransferStarted"] = "handleTransferStarted", e[e.handleTransferComplete = "handleTransferComplete"] = "handleTransferComplete", e[e.handleTransferFailed = "handleTransferFailed"] = "handleTransferFailed", e[e.handleReInvite = "handleReInvite"] = "handleReInvite", e[e.handleAcceptReinvite = "handleAcceptReinvite"] = "handleAcceptReinvite", e[e.handleRejectReinvite = "handleRejectReinvite"] = "handleRejectReinvite", e[e.createPC = "__createPC"] = "createPC", e[e.destroyPC = "__destroyPC"] = "destroyPC", e[e.connectionSuccessful = "__connectionSuccessful"] = "connectionSuccessful", e[e.connectionFailed = "__connectionFailed"] = "connectionFailed", e[e.createConnection = "__createConnection"] = "createConnection", e[e.pong = "__pong"] = "pong", e[e.increaseGain = "increaseGain"] = "increaseGain", e[e.handlePreFlightCheckResult = "handlePreFlightCheckResult"] = "handlePreFlightCheckResult", e[e.handleVoicemail = "handleVoicemail"] = "handleVoicemail", e[e.onCallRemoteFunctionError = "onCallRemoteFunctionError"] = "onCallRemoteFunctionError", e[e.refreshOauthTokenFailed = "refreshOauthTokenFailed"] = "refreshOauthTokenFailed", e[e.refreshOauthTokenSuccessful = "refreshOauthTokenSuccessful"] = "refreshOauthTokenSuccessful", e[e.sipRegisterSuccessful = "sipRegisterSuccessful"] = "sipRegisterSuccessful", e[e.sipRegisterFailed = "sipRegisterFailed"] = "sipRegisterFailed", e[e.onACDStatus = "onACDStatus"] = "onACDStatus"
    }(t.RemoteEvent || (t.RemoteEvent = {}))
}, function (e, t, n) {
    "use strict";

    function r(e, t) {
        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var o = function e() {
        r(this, e)
    };
    o.DIRECT_CALL_HEADER = "X-DirectCall", o.VIAMEDIA_CALL_HEADER = "X-ViaMedia", o.CALLSTATSIOID_HEADER = "X-CallstatsIOID", o.CALL_DATA_HEADER = "VI-CallData", o.ZINGAYA_IM_MIME_TYPE = "application/zingaya-im", o.P2P_SPD_FRAG_MIME_TYPE = "voximplant/sdpfrag", o.VI_HOLD_EMUL = "vi/holdemul", o.VI_SPD_OFFER_MIME_TYPE = "vi/sdpoffer", o.VI_SPD_ANSWER_MIME_TYPE = "vi/sdpanswer", o.VI_CONF_PARTICIPANT_INFO_ADDED = "vi/conf-info-added", o.VI_CONF_PARTICIPANT_INFO_REMOVED = "vi/conf-info-removed", o.VI_CONF_PARTICIPANT_INFO_UPDATED = "vi/conf-info-updated", t.Constants = o
}, function (e, t, n) {
    "use strict";

    function r(e, t) {
        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }
    var o = function () {
        function e(e, t) {
            for (var n = 0; n < t.length; n++) {
                var r = t[n];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
            }
        }
        return function (t, n, r) {
            return n && e(t.prototype, n), r && e(t, r), t
        }
    }(),
        i = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
            return typeof e
        } : function (e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        },
        a = function (e, t, n, r) {
            var o, a = arguments.length,
                s = a < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
            if ("object" === ("undefined" == typeof Reflect ? "undefined" : i(Reflect)) && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r);
            else
                for (var c = e.length - 1; c >= 0; c--)(o = e[c]) && (s = (a < 3 ? o(s) : a > 3 ? o(t, n, s) : o(t, n)) || s);
            return a > 3 && s && Object.defineProperty(t, n, s), s
        };
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var s = n(0),
        c = function () {
            function e() {
                r(this, e), this.eventListeners = {}, this.defaultEventListeners = {}
            }
            return o(e, [{
                key: "addEventListener",
                value: function (e, t) {
                    this.on(e, t)
                }
            }, {
                key: "addDefaultEventListener",
                value: function (e, t) {
                    this.defaultEventListeners[e] = t
                }
            }, {
                key: "removeDefaultEventListener",
                value: function (e) {
                    this.defaultEventListeners[e] = void 0
                }
            }, {
                key: "dispatchEvent",
                value: function (e) {
                    s.LogManager.get().writeMessage(s.LogCategory.UTILS, "", s.LogLevel.INFO, e.name + " dispatched");
                    var t = e.name;
                    if (void 0 !== this.eventListeners[t])
                        for (var n = 0; n < this.eventListeners[t].length; n++)
                            if ("function" == typeof this.eventListeners[t][n]) try {
                                this.eventListeners[t][n](e)
                            } catch (e) {
                                console.error(e)
                            }
                    void 0 !== this.eventListeners[t] && 0 != this.eventListeners[t].length || (s.LogManager.get().writeMessage(s.LogCategory.UTILS, "", s.LogLevel.INFO, "The " + e.name + " event dispatched, but no handler registered for this event type."), this.defaultEventListeners[t] && this.defaultEventListeners[t](e))
                }
            }, {
                key: "removeEventListener",
                value: function (e, t) {
                    this.off(e, t)
                }
            }, {
                key: "on",
                value: function (e, t) {
                    void 0 === this.eventListeners[e] && (this.eventListeners[e] = []), this.eventListeners[e].push(t)
                }
            }, {
                key: "off",
                value: function (e, t) {
                    if (void 0 !== this.eventListeners[e])
                        if ("function" == typeof t) {
                            for (var n = 0; n < this.eventListeners[e].length; n++)
                                if (this.eventListeners[e][n] == t) {
                                    this.eventListeners[e].splice(n, 1);
                                    break
                                }
                        } else this.eventListeners[e] = []
                }
            }]), e
        }();
    a([s.LogManager.d_trace(s.LogCategory.EVENTTARGET)], c.prototype, "addEventListener", null), a([s.LogManager.d_trace(s.LogCategory.EVENTTARGET)], c.prototype, "addDefaultEventListener", null), a([s.LogManager.d_trace(s.LogCategory.EVENTTARGET)], c.prototype, "removeDefaultEventListener", null), a([s.LogManager.d_trace(s.LogCategory.EVENTTARGET)], c.prototype, "removeEventListener", null), a([s.LogManager.d_trace(s.LogCategory.EVENTTARGET)], c.prototype, "on", null), a([s.LogManager.d_trace(s.LogCategory.EVENTTARGET)], c.prototype, "off", null), t.EventTarget = c
}, function (e, t, n) {
    "use strict";

    function r(e, t) {
        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }

    function o(e, t) {
        if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
        return !t || "object" != typeof t && "function" != typeof t ? e : t
    }

    function i(e, t) {
        if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
        e.prototype = Object.create(t && t.prototype, {
            constructor: {
                value: e,
                enumerable: !1,
                writable: !0,
                configurable: !0
            }
        }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
    }
    var a = function () {
        function e(e, t) {
            for (var n = 0; n < t.length; n++) {
                var r = t[n];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
            }
        }
        return function (t, n, r) {
            return n && e(t.prototype, n), r && e(t, r), t
        }
    }(),
        s = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
            return typeof e
        } : function (e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        },
        c = function (e, t, n, r) {
            var o, i = arguments.length,
                a = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
            if ("object" === ("undefined" == typeof Reflect ? "undefined" : s(Reflect)) && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, n, r);
            else
                for (var c = e.length - 1; c >= 0; c--)(o = e[c]) && (a = (i < 3 ? o(a) : i > 3 ? o(t, n, a) : o(t, n)) || a);
            return i > 3 && a && Object.defineProperty(t, n, a), a
        };
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var l = n(0),
        u = n(15),
        d = n(2),
        g = n(13),
        f = function (e) {
            function t() {
                r(this, t);
                var e = o(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this));
                if (t.instance) throw new Error("Error - use Client.getMessagingInstance()");
                return e.query = [], setInterval(function () {
                    e.updateQuery()
                }, 220), e
            }
            return i(t, e), a(t, [{
                key: "handleWsData",
                value: function (e) {
                    if (-1 == ["onCreateConversation", "onEditConversation", "onRemoveConversation", "onJoinConversation", "onLeaveConversation", "onGetConversation", "onSendMessage", "onEditMessage", "onRemoveMessage", "onTyping", "onRetransmitEvents", "onEditUser", "onGetUser", "isRead", "isDelivered", "onError", "onSubscribe", "onUnSubscribe", "onSetStatus"].indexOf(e.event)) throw new Error("Unknown messaging event " + e.event + " with payload " + JSON.stringify(e.payload));
                    this.dispatchEvent(e.event, e)
                }
            }, {
                key: "sendPayload",
                value: function (e, t) {
                    var n = {
                        service: u.MsgService.Chat,
                        event: e,
                        payload: t
                    };
                    return this.query.push(n), !0
                }
            }, {
                key: "updateQuery",
                value: function () {
                    if (this.query.length) {
                        var e = this.query.splice(0, 1);
                        d.VoxSignaling.get().sendRaw(e[0])
                    }
                }
            }, {
                key: "dispatchEvent",
                value: function (e, t) {
                    if (void 0 !== this.eventListeners[e])
                        for (var n = 0; n < this.eventListeners[e].length; n++) "function" == typeof this.eventListeners[e][n] && this.eventListeners[e][n](t.payload)
                }
            }, {
                key: "_traceName",
                value: function () {
                    return "MsgSignaling"
                }
            }], [{
                key: "get",
                value: function () {
                    return t.instance = t.instance || new t, t.instance
                }
            }]), t
        }(g.EventTarget);
    c([l.LogManager.d_trace(l.LogCategory.MESSAGING)], f.prototype, "handleWsData", null), c([l.LogManager.d_trace(l.LogCategory.MESSAGING)], f.prototype, "sendPayload", null), t.MsgSignaling = f
}, function (e, t, n) {
    "use strict";
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    ! function (e) {
        e[e.Chat = "chat"] = "Chat"
    }(t.MsgService || (t.MsgService = {}));
    ! function (e) {
        e[e.createConversation = "createConversation"] = "createConversation", e[e.editConversation = "editConversation"] = "editConversation", e[e.removeConversation = "removeConversation"] = "removeConversation", e[e.joinConversation = "joinConversation"] = "joinConversation", e[e.leaveConversation = "leaveConversation"] = "leaveConversation", e[e.getConversation = "getConversation"] = "getConversation", e[e.getConversations = "getConversations"] = "getConversations", e[e.sendMessage = "sendMessage"] = "sendMessage", e[e.editMessage = "editMessage"] = "editMessage", e[e.removeMessage = "removeMessage"] = "removeMessage", e[e.typingMessage = "typingMessage"] = "typingMessage", e[e.editUser = "editUser"] = "editUser", e[e.getUser = "getUser"] = "getUser", e[e.getUsers = "getUsers"] = "getUsers", e[e.retransmitEvents = "retransmitEvents"] = "retransmitEvents", e[e.isRead = "isRead"] = "isRead", e[e.isDelivered = "isDelivered"] = "isDelivered", e[e.addParticipants = "addParticipants"] = "addParticipants", e[e.editParticipants = "editParticipants"] = "editParticipants", e[e.removeParticipants = "removeParticipants"] = "removeParticipants", e[e.addModerators = "addModerators"] = "addModerators", e[e.removeModerators = "removeModerators"] = "removeModerators", e[e.subscribe = "subscribe"] = "subscribe", e[e.unsubscribe = "unsubscribe"] = "unsubscribe", e[e.setStatus = "setStatus"] = "setStatus"
    }(t.MsgAction || (t.MsgAction = {}));
    ! function (e) {
        e[e.onCreateConversation = "onCreateConversation"] = "onCreateConversation", e[e.onEditConversation = "onEditConversation"] = "onEditConversation", e[e.onRemoveConversation = "onRemoveConversation"] = "onRemoveConversation", e[e.onJoinConversation = "onJoinConversation"] = "onJoinConversation", e[e.onLeaveConversation = "onLeaveConversation"] = "onLeaveConversation", e[e.onGetConversation = "onGetConversation"] = "onGetConversation", e[e.onSendMessage = "onSendMessage"] = "onSendMessage", e[e.onEditMessage = "onEditMessage"] = "onEditMessage", e[e.onRemoveMessage = "onRemoveMessage"] = "onRemoveMessage", e[e.onTyping = "onTyping"] = "onTyping", e[e.onRetransmitEvents = "onRetransmitEvents"] = "onRetransmitEvents", e[e.onEditUser = "onEditUser"] = "onEditUser", e[e.onGetUser = "onGetUser"] = "onGetUser", e[e.onError = "onError"] = "onError", e[e.isRead = "isRead"] = "isRead", e[e.isDelivered = "isDelivered"] = "isDelivered", e[e.onsubscribe = "onsubscribe"] = "onsubscribe", e[e.onUnSubscribe = "onUnSubscribe"] = "onUnSubscribe", e[e.onSetStatus = "onSetStatus"] = "onSetStatus"
    }(t.MsgEvent || (t.MsgEvent = {}))
}, function (e, t, n) {
    "use strict";

    function r(e, t) {
        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }

    function o(e, t) {
        if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
        return !t || "object" != typeof t && "function" != typeof t ? e : t
    }

    function i(e, t) {
        if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
        e.prototype = Object.create(t && t.prototype, {
            constructor: {
                value: e,
                enumerable: !1,
                writable: !0,
                configurable: !0
            }
        }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var a, s = n(71);
    ! function (e) {
        e.MessengerEventsCallbacks || (e.MessengerEventsCallbacks = {});
        var t = function (e) {
            function t() {
                return r(this, t), o(this, (t.__proto__ || Object.getPrototypeOf(t)).apply(this, arguments))
            }
            return i(t, e), t
        }(s.Conversation);
        e.Conversation = t;
        var n = function (e) {
            function t() {
                return r(this, t), o(this, (t.__proto__ || Object.getPrototypeOf(t)).apply(this, arguments))
            }
            return i(t, e), t
        }(s.Message);
        e.Message = n;
        var a = function (e) {
            function t() {
                return r(this, t), o(this, (t.__proto__ || Object.getPrototypeOf(t)).apply(this, arguments))
            }
            return i(t, e), t
        }(s.Messenger);
        e.Messenger = a;
        ! function (e) {
            e.CreateConversation = "CreateConversation", e.EditConversation = "EditConversation", e.RemoveConversation = "RemoveConversation", e.GetConversation = "GetConversation", e.SendMessage = "SendMessage", e.EditMessage = "EditMessage", e.RemoveMessage = "RemoveMessage", e.Typing = "Typing", e.EditUser = "EditUser", e.GetUser = "GetUser", e.Error = "Error", e.RetransmitEvents = "RetransmitEvents", e.Read = "Read", e.Delivered = "Delivered", e.Subscribe = "Subscribe", e.Unsubscribe = "Unsubscribe", e.SetStatus = "SetStatus"
        }(e.MessengerEvents || (e.MessengerEvents = {}));
        ! function (e) {
            e[e.createConversation = "createConversation"] = "createConversation", e[e.editConversation = "editConversation"] = "editConversation", e[e.removeConversation = "removeConversation"] = "removeConversation", e[e.joinConversation = "joinConversation"] = "joinConversation", e[e.leaveConversation = "leaveConversation"] = "leaveConversation", e[e.getConversation = "getConversation"] = "getConversation", e[e.getConversations = "getConversations"] = "getConversations", e[e.sendMessage = "sendMessage"] = "sendMessage", e[e.editMessage = "editMessage"] = "editMessage", e[e.removeMessage = "removeMessage"] = "removeMessage", e[e.typingMessage = "typingMessage"] = "typingMessage", e[e.editUser = "editUser"] = "editUser", e[e.getUser = "getUser"] = "getUser", e[e.getUsers = "getUsers"] = "getUsers", e[e.retransmitEvents = "retransmitEvents"] = "retransmitEvents", e[e.isRead = "isRead"] = "isRead", e[e.isDelivered = "isDelivered"] = "isDelivered", e[e.addParticipants = "addParticipants"] = "addParticipants", e[e.editParticipants = "editParticipants"] = "editParticipants", e[e.removeParticipants = "removeParticipants"] = "removeParticipants", e[e.addModerators = "addModerators"] = "addModerators", e[e.removeModerators = "removeModerators"] = "removeModerators", e[e.subscribe = "subscribe"] = "subscribe", e[e.unsubscribe = "unsubscribe"] = "unsubscribe", e[e.setStatus = "setStatus"] = "setStatus"
        }(e.MessengerAction || (e.MessengerAction = {}));
        ! function (e) {
            e[e.Error_1 = "1"] = "Error_1", e[e.Error_2 = "2"] = "Error_2", e[e.Error_3 = "3"] = "Error_3", e[e.Error_4 = "4"] = "Error_4", e[e.Error_5 = "5"] = "Error_5", e[e.Error_6 = "6"] = "Error_6", e[e.Error_7 = "7"] = "Error_7", e[e.Error_8 = "8"] = "Error_8", e[e.Error_9 = "9"] = "Error_9", e[e.Error_10 = "10"] = "Error_10", e[e.Error_11 = "11"] = "Error_11", e[e.Error_12 = "12"] = "Error_12", e[e.Error_13 = "13"] = "Error_13", e[e.Error_14 = "14"] = "Error_14", e[e.Error_15 = "15"] = "Error_15", e[e.Error_16 = "16"] = "Error_16", e[e.Error_17 = "17"] = "Error_17", e[e.Error_18 = "18"] = "Error_18", e[e.Error_19 = "19"] = "Error_19", e[e.Error_21 = "21"] = "Error_21", e[e.Error_22 = "22"] = "Error_22", e[e.Error_23 = "23"] = "Error_23", e[e.Error_24 = "24"] = "Error_24", e[e.Error_30 = "30"] = "Error_30", e[e.Error_500 = "500"] = "Error_500", e[e.Error_777 = "777"] = "Error_777"
        }(e.MessengerError || (e.MessengerError = {}))
    }(a = t.Messaging || (t.Messaging = {})), t.default = a
}, function (e, t, n) {
    "use strict";
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    ! function (e) {
        e[e.SDKReady = "SDKReady"] = "SDKReady", e[e.ConnectionEstablished = "ConnectionEstablished"] = "ConnectionEstablished", e[e.ConnectionFailed = "ConnectionFailed"] = "ConnectionFailed", e[e.ConnectionClosed = "ConnectionClosed"] = "ConnectionClosed", e[e.AuthResult = "AuthResult"] = "AuthResult", e[e.RefreshTokenResult = "RefreshTokenResult"] = "RefreshTokenResult", e[e.PlaybackFinished = "PlaybackFinished"] = "PlaybackFinished", e[e.MicAccessResult = "MicAccessResult"] = "MicAccessResult", e[e.IncomingCall = "IncomingCall"] = "IncomingCall", e[e.SourcesInfoUpdated = "SourcesInfoUpdated"] = "SourcesInfoUpdated", e[e.NetStatsReceived = "NetStatsReceived"] = "NetStatsReceived", e[e.SIPRegistrationSuccessful = "SIPRegistrationSuccessful"] = "SIPRegistrationSuccessful", e[e.SIPRegistrationFailed = "SIPRegistrationFailed"] = "SIPRegistrationFailed", e[e.ACDStatusUpdated = "ACDStatusUpdated"] = "ACDStatusUpdated"
    }(t.Events || (t.Events = {}))
}, function (e, t, n) {
    "use strict";

    function r(e, t) {
        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }
    var o = function () {
        function e(e, t) {
            for (var n = 0; n < t.length; n++) {
                var r = t[n];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
            }
        }
        return function (t, n, r) {
            return n && e(t.prototype, n), r && e(t, r), t
        }
    }();
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var i = n(2),
        a = n(3),
        s = function () {
            function e(t) {
                r(this, e), this._id = t
            }
            return o(e, [{
                key: "insertDTMF",
                value: function (e, t, n) {
                    var r = this;
                    e.split("").forEach(function (e) {
                        return r.sendKey(e)
                    })
                }
            }, {
                key: "sendKey",
                value: function (e) {
                    var t = void 0;
                    ((t = "*" == e ? 10 : "#" == e ? 11 : parseInt(e)) >= 0 || t <= 11) && i.VoxSignaling.get().callRemoteFunction(a.RemoteFunction.sendDTMF, this._id, t)
                }
            }, {
                key: "_traceName",
                value: function () {
                    return "SignalingDTMFSender"
                }
            }]), e
        }();
    t.SignalingDTMFSender = s
}, function (e, t, n) {
    "use strict";

    function r(e, t) {
        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }

    function o(e, t) {
        if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
        return !t || "object" != typeof t && "function" != typeof t ? e : t
    }

    function i(e, t) {
        if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
        e.prototype = Object.create(t && t.prototype, {
            constructor: {
                value: e,
                enumerable: !1,
                writable: !0,
                configurable: !0
            }
        }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
    }
    var a = function () {
        function e(e, t) {
            for (var n = 0; n < t.length; n++) {
                var r = t[n];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
            }
        }
        return function (t, n, r) {
            return n && e(t.prototype, n), r && e(t, r), t
        }
    }(),
        s = function e(t, n, r) {
            null === t && (t = Function.prototype);
            var o = Object.getOwnPropertyDescriptor(t, n);
            if (void 0 === o) {
                var i = Object.getPrototypeOf(t);
                return null === i ? void 0 : e(i, n, r)
            }
            if ("value" in o) return o.value;
            var a = o.get;
            if (void 0 !== a) return a.call(r)
        },
        c = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
            return typeof e
        } : function (e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        },
        l = function (e, t, n, r) {
            var o, i = arguments.length,
                a = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
            if ("object" === ("undefined" == typeof Reflect ? "undefined" : c(Reflect)) && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, n, r);
            else
                for (var s = e.length - 1; s >= 0; s--)(o = e[s]) && (a = (i < 3 ? o(a) : i > 3 ? o(t, n, a) : o(t, n)) || a);
            return i > 3 && a && Object.defineProperty(t, n, a), a
        };
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var u, d = n(6),
        g = n(2),
        f = n(12),
        p = n(0),
        v = n(9),
        h = n(5),
        y = n(8),
        m = n(3),
        C = n(1),
        _ = n(13),
        S = n(20),
        E = n(4),
        L = n(27);
    ! function (e) {
        e[e.ALERTING = "ALERTING"] = "ALERTING", e[e.PROGRESSING = "PROGRESSING"] = "PROGRESSING", e[e.CONNECTED = "CONNECTED"] = "CONNECTED", e[e.UPDATING = "UPDATING"] = "UPDATING", e[e.ENDED = "ENDED"] = "ENDED"
    }(u = t.CallState || (t.CallState = {}));
    var M;
    ! function (e) {
        e[e.P2P = 0] = "P2P", e[e.SERVER = 1] = "SERVER"
    }(M = t.CallMode || (t.CallMode = {}));
    var R = function (e) {
        function t(e, n, i, a) {
            r(this, t);
            var s = o(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this));
            s.remoteMuteState = !0, s.signalingConnected = !1, s.settings = a, s.settings.id = e, s.settings.displayName = n, s.settings.mode = M.P2P, s.settings.active = !0, s.settings.usedSinkId = null, s.settings.incoming = i, s.settings.state = i ? u.ALERTING : u.PROGRESSING;
            C.Client.getInstance().config();
            return s.settings.audioDirections = {
                sendAudio: !0
            }, s.settings.videoDirections = "boolean" == typeof a.video ? {
                sendVideo: a.video,
                receiveVideo: !0
            } : a.video, s.settings.hasEarlyMedia = !1, s.log = p.LogManager.get().createLogger(p.LogCategory.CALL, "Call " + e), s._callManager = h.CallManager.get(), s
        }
        return i(t, e), a(t, [{
            key: "id",
            value: function () {
                return this.settings.id
            }
        }, {
            key: "number",
            value: function () {
                return this.settings.number
            }
        }, {
            key: "displayName",
            value: function () {
                return this.settings.displayName
            }
        }, {
            key: "headers",
            value: function () {
                return this.settings.extraHeaders
            }
        }, {
            key: "active",
            value: function () {
                return this.settings.active
            }
        }, {
            key: "state",
            value: function () {
                return u[this.settings.state]
            }
        }, {
            key: "answer",
            value: function (e, t, n) {
                if (void 0 !== e && (void 0 !== t && "object" === (void 0 === t ? "undefined" : c(t)) || (t = {}), t[f.Constants.CALL_DATA_HEADER] = e), void 0 !== n && (n = {
                    sendVideo: C.Client.getInstance().config().videoSupport,
                    receiveVideo: C.Client.getInstance().config().videoSupport
                }), this.settings.state != u.ALERTING) throw new Error("WRONG_CALL_STATE");
                void 0 !== n && this._peerConnection.setVideoFlags(n)
            }
        }, {
            key: "decline",
            value: function (e) {
                if (this.settings.state != u.ALERTING) throw new Error("WRONG_CALL_STATE");
                g.VoxSignaling.get().callRemoteFunction(m.RemoteFunction.rejectCall, this.settings.id, !1, h.CallManager.cleanHeaders(e))
            }
        }, {
            key: "reject",
            value: function (e) {
                if (this.settings.state != u.ALERTING) throw new Error("WRONG_CALL_STATE");
                g.VoxSignaling.get().callRemoteFunction(m.RemoteFunction.rejectCall, this.settings.id, !0, h.CallManager.cleanHeaders(e))
            }
        }, {
            key: "hangup",
            value: function (e) {
                if (this.settings.state == u.CONNECTED || this.settings.state == u.UPDATING || this.settings.state == u.PROGRESSING) g.VoxSignaling.get().callRemoteFunction(m.RemoteFunction.disconnectCall, this.settings.id, h.CallManager.cleanHeaders(e));
                else {
                    if (this.settings.state != u.ALERTING) throw new Error("WRONG_CALL_STATE");
                    g.VoxSignaling.get().callRemoteFunction(m.RemoteFunction.rejectCall, this.settings.id, !0, h.CallManager.cleanHeaders(e))
                }
            }
        }, {
            key: "sendTone",
            value: function (e) {
                this.settings.active && this._peerConnection.sendDTMF(e)
            }
        }, {
            key: "mutePlayback",
            value: function () {
                this.remoteMuteState = !1, S.EndpointManager.get().getEndPointsByCall(this).forEach(function (e) {
                    e.mediaRenderers.forEach(function (e) {
                        e.setVolume(0)
                    })
                })
            }
        }, {
            key: "unmutePlayback",
            value: function () {
                this.remoteMuteState = !0, S.EndpointManager.get().getEndPointsByCall(this).forEach(function (e) {
                    e.mediaRenderers.forEach(function (e) {
                        e.setVolume(1)
                    })
                })
            }
        }, {
            key: "restoreRMute",
            value: function () {
                var e = this;
                this.settings.active && S.EndpointManager.get().getEndPointsByCall(this).forEach(function (t) {
                    t.mediaRenderers.forEach(function (t) {
                        t.setVolume(e.remoteMuteState ? 1 : 0)
                    })
                })
            }
        }, {
            key: "muteMicrophone",
            value: function () {
                this.peerConnection.muteMicrophone(!0)
            }
        }, {
            key: "unmuteMicrophone",
            value: function () {
                this.peerConnection.muteMicrophone(!1)
            }
        }, {
            key: "showRemoteVideo",
            value: function () {
                var e = !(arguments.length > 0 && void 0 !== arguments[0]) || arguments[0];
                void 0 === e && (e = !0);
                var t = S.EndpointManager.get().getEndPointsByCall(this);
                t && t.forEach(function (t) {
                    t.mediaRenderers && t.mediaRenderers.forEach(function (t) {
                        t.element && (t.element.style.display = e ? "block" : "none")
                    })
                })
            }
        }, {
            key: "setRemoteVideoPosition",
            value: function (e, t) {
                throw new Error("Deprecated: please use CSS to position '#voximplantcontainer' element")
            }
        }, {
            key: "setRemoteVideoSize",
            value: function (e, t) {
                throw new Error("Deprecated: please use CSS to set size of '#voximplantcontainer' element")
            }
        }, {
            key: "sendInfo",
            value: function (e, t, n) {
                var r, o, i = e.indexOf("/"); - 1 == i ? (r = "application", o = e) : (r = e.substring(0, i), o = e.substring(i + 1)), g.VoxSignaling.get().callRemoteFunction(m.RemoteFunction.sendSIPInfo, this.settings.id, r, o, t, h.CallManager.cleanHeaders(n))
            }
        }, {
            key: "sendMessage",
            value: function (e) {
                this.sendInfo(f.Constants.ZINGAYA_IM_MIME_TYPE, e, {})
            }
        }, {
            key: "setVideoSettings",
            value: function (e, t, n) {
                E.default.CameraManager.get().setCallVideoSettings(this, E.default.CameraManager.legacyParamConverter(e)), E.default.StreamManager.get().updateCallStream(this).then(function (e) {
                    t && t(e)
                }, function (e) {
                    n && n()
                })
            }
        }, {
            key: "getVideoElementId",
            value: function () {
                var e = S.EndpointManager.get().getEndPointsByCall(this);
                return void 0 !== e && e.forEach(function (e) {
                    e.mediaRenderers.forEach(function (e) {
                        if (e.stream) {
                            if (e.stream.getVideoTracks().length) return e.stream.getTracks()[0].id
                        }
                    })
                }), ""
            }
        }, {
            key: "addEventListener",
            value: function (e, n) {
                s(t.prototype.__proto__ || Object.getPrototypeOf(t.prototype), "addEventListener", this).call(this, e, n)
            }
        }, {
            key: "on",
            value: function (e, n) {
                s(t.prototype.__proto__ || Object.getPrototypeOf(t.prototype), "on", this).call(this, e, n)
            }
        }, {
            key: "removeEventListener",
            value: function (e, n) {
                s(t.prototype.__proto__ || Object.getPrototypeOf(t.prototype), "removeEventListener", this).call(this, e, n)
            }
        }, {
            key: "off",
            value: function (e, n) {
                s(t.prototype.__proto__ || Object.getPrototypeOf(t.prototype), "off", this).call(this, e, n)
            }
        }, {
            key: "dispatchEvent",
            value: function (e) {
                e.name !== d.CallEvents.Updated && e.name !== d.CallEvents.UpdateFailed || (this.settings.state = u.CONNECTED), s(t.prototype.__proto__ || Object.getPrototypeOf(t.prototype), "dispatchEvent", this).call(this, e)
            }
        }, {
            key: "checkState",
            value: function (e, t) {
                if (e)
                    if ("string" != typeof e) {
                        for (var n = !1, r = e, o = 0; o < r.length; o++) r[o] == this.settings.state && (n = !0);
                        if (!n) return this.log.warning("Received " + t + " in invalid state " + this.settings.state), !1
                    } else if (this.settings.state != e) return this.log.warning("Received " + t + " in invalid state " + this.settings.state), !1;
                return !0
            }
        }, {
            key: "onConnected",
            value: function (e, t) {
                if (this.signalingConnected) return !!this.checkState([u.PROGRESSING, u.ALERTING], "onConnected") && (this.settings.state = u.CONNECTED, this.startTime = Date.now(), this.dispatchEvent({
                    name: "Connected",
                    call: this,
                    headers: e
                }), void 0 === this.getEndpoints() ? S.EndpointManager.get().createDefaultEndPoint(this, this.settings.id) : void 0 === this.getEndpoints() || this.getEndpoints().some(function (e) {
                    return e.isDefault
                }) || S.EndpointManager.get().createDefaultEndPoint(this, this.settings.id), !0)
            }
        }, {
            key: "onDisconnected",
            value: function (e, t) {
                var n = this;
                return new Promise(function (r, o) {
                    if (n.stopSharingScreen(), !n.checkState([u.CONNECTED, u.ALERTING, u.PROGRESSING, u.UPDATING], "onDisconnected")) return o(new Error("Call in the wrong state " + n.state())), !1;
                    S.EndpointManager.get().clearEndPointByCall(n).then(function () {
                        r(!0), n.settings.state = u.ENDED, n.dispatchEvent({
                            name: "Disconnected",
                            call: n,
                            headers: e,
                            params: t
                        })
                    }).catch(function (e) {
                        o(new Error("Endpoint manager got some error: " + e.message))
                    })
                })
            }
        }, {
            key: "onFailed",
            value: function (e, t, n) {
                return this.dispatchEvent({
                    name: "Failed",
                    call: this,
                    headers: n,
                    code: e,
                    reason: t
                }), this.settings.state = u.ENDED, S.EndpointManager.get().clearEndPointByCall(this), !0
            }
        }, {
            key: "onStopRinging",
            value: function () {
                return !!this.checkState([u.PROGRESSING, u.CONNECTED], "onStopRinging") && (this.dispatchEvent({
                    name: "ProgressToneStop",
                    call: this
                }), !0)
            }
        }, {
            key: "onRingOut",
            value: function () {
                return !!this.checkState(u.PROGRESSING, "onRingOut") && (this.dispatchEvent({
                    name: "ProgressToneStart",
                    call: this
                }), !0)
            }
        }, {
            key: "onTransferComplete",
            value: function () {
                return !!this.checkState(u.CONNECTED, "onTransferComplete") && (this.dispatchEvent({
                    name: "TransferComplete",
                    call: this
                }), !0)
            }
        }, {
            key: "onTransferFailed",
            value: function () {
                return !!this.checkState(u.CONNECTED, "onTransferFailed") && (this.dispatchEvent({
                    name: "TransferFailed",
                    call: this
                }), !0)
            }
        }, {
            key: "onInfo",
            value: function (e, t, n, r, o) {
                if (e.stateValue == u.CONNECTED || e.stateValue == u.PROGRESSING || e.stateValue == u.ALERTING || e.stateValue == u.UPDATING) {
                    var i = t + "/" + n;
                    if (i == f.Constants.ZINGAYA_IM_MIME_TYPE) this.dispatchEvent({
                        name: "onSendMessage",
                        call: this,
                        text: r
                    });
                    else if (i == f.Constants.P2P_SPD_FRAG_MIME_TYPE) {
                        var a = JSON.parse(r);
                        for (var s in a) void 0 !== e && void 0 !== e.peerConnection ? e.peerConnection.addRemoteCandidate(a[s][1], a[s][0]) : this.log.info("Candidate skipped. Connection not created yet.")
                    } else if (i === f.Constants.VI_CONF_PARTICIPANT_INFO_ADDED || i === f.Constants.VI_CONF_PARTICIPANT_INFO_REMOVED || i === f.Constants.VI_CONF_PARTICIPANT_INFO_UPDATED) {
                        var c = JSON.parse(r);
                        if (c) {
                            var l = S.EndpointManager.get().getEndPointById(this, c.id);
                            if (i === f.Constants.VI_CONF_PARTICIPANT_INFO_ADDED) {
                                var d = new L.Endpoint(!1);
                                d.id = c.id, d.updateInfo(c), S.EndpointManager.get().addEndPoint(this, d)
                            } else i === f.Constants.VI_CONF_PARTICIPANT_INFO_UPDATED ? (l.updateInfo(c), S.EndpointManager.get().updateEndPoint(this, l)) : (l || this.log.trace("Endpoint ID:" + c.id + " not found. Ignored."), S.EndpointManager.get().rmEndPoint(this, l))
                        } else this.log.info("Wrong endpointInfo")
                    } else this.dispatchEvent({
                        name: "InfoReceived",
                        call: this,
                        body: r,
                        headers: o,
                        mimeType: i
                    });
                    return !0
                }
                this.log.warning("received handleSIPInfo for call: " + e.id() + " in invalid state: " + e.state())
            }
        }, {
            key: "setActive",
            value: function (e) {
                var t = this;
                return new Promise(function (n, r) {
                    return e === t.settings.active ? void n({
                        name: d.CallEvents.Updated,
                        result: !1,
                        call: t
                    }) : "firefox" === y.default.getWSVendor() ? (t.sendInfo(f.Constants.VI_HOLD_EMUL, JSON.stringify({
                        hold: !e
                    })), void n({
                        name: d.CallEvents.Updated,
                        call: t,
                        result: !0
                    })) : void (t.settings.state == u.CONNECTED ? (t.settings.state = u.UPDATING, t.settings.active = e, e ? g.VoxSignaling.get().callRemoteFunction(m.RemoteFunction.unhold, t.settings.id) : g.VoxSignaling.get().callRemoteFunction(m.RemoteFunction.hold, t.settings.id), n({
                        name: d.CallEvents.Updated,
                        result: !0,
                        call: t
                    })) : r({
                        name: d.CallEvents.UpdateFailed,
                        code: 11,
                        call: t
                    }))
                })
            }
        }, {
            key: "checkCallMode",
            value: function (e) {
                return this.settings.mode == e
            }
        }, {
            key: "canStartSendingCandidates",
            value: function () {
                void 0 === this._peerConnection && (this._peerConnection = v.PCFactory.get().peerConnections[this.settings.id]), this._peerConnection.canStartSendingCandidates()
            }
        }, {
            key: "notifyICETimeout",
            value: function () {
                this.dispatchEvent({
                    name: "ICETimeout",
                    call: this
                })
            }
        }, {
            key: "sendVideo",
            value: function (e) {
                var t = this;
                C.Client.getInstance().config();
                if (!this.peerConnection) return new Promise(function (e, n) {
                    e({
                        call: t,
                        name: d.CallEvents[d.CallEvents.Updated],
                        result: !0
                    })
                });
                var n = this.settings.videoDirections.sendVideo;
                return this.settings.videoDirections.sendVideo = e, this.peerConnection.hasLocalVideo() ? e || n ? (this.peerConnection.enableVideo(e), new Promise(function (e, n) {
                    e({
                        call: t,
                        name: d.CallEvents[d.CallEvents.Updated],
                        result: !0
                    })
                })) : this.sendMedia(null, e) : e ? E.default.StreamManager.get().updateCallStream(this) : void 0
            }
        }, {
            key: "receiveVideo",
            value: function () {
                var e = this;
                return this.settings.state = u.UPDATING, new Promise(function (t, n) {
                    if (!0 === e.settings.videoDirections.receiveVideo) return void n();
                    e.settings.videoDirections.receiveVideo = !0, e._peerConnection.hdnFRS().then(t, n)
                })
            }
        }, {
            key: "sendMedia",
            value: function (e, t) {
                var n = this;
                return this.settings.state = u.UPDATING, void 0 !== e && null !== e || (e = this.settings.audioDirections.sendAudio), void 0 !== t && null !== t || (t = this.settings.videoDirections.sendVideo), this.peerConnection.sendMedia(e, t).then(function (r) {
                    return void 0 !== t && null !== t && (n.settings.videoDirections.sendVideo = t), void 0 !== e && null !== e && (n.settings.audioDirections.sendAudio = e), r
                })
            }
        }, {
            key: "sendAudio",
            value: function (e) {
                var t = this;
                C.Client.getInstance().config();
                return this.settings.audioDirections.sendAudio = e, this.peerConnection.hasLocalAudio() ? (this.peerConnection.muteMicrophone(!e), new Promise(function (e, n) {
                    e({
                        call: t,
                        name: d.CallEvents[d.CallEvents.Updated],
                        result: !0
                    })
                })) : e ? this.sendMedia(null, e) : void 0
            }
        }, {
            key: "getLocalStream",
            value: function () {
                return E.default.StreamManager.get().getCallStream(this)
            }
        }, {
            key: "setLocalStream",
            value: function (e) {
                return new Promise(function (e, t) {
                    t(new Error("Not implemented"))
                })
            }
        }, {
            key: "shareScreen",
            value: function () {
                var e = this,
                    t = arguments.length > 0 && void 0 !== arguments[0] && arguments[0];
                return new Promise(function (n, r) {
                    E.default.StreamManager.get()._getScreenSharing(e).length && (e.log.warning("Screen sharing already active."), r(new Error("Screen sharing already active."))), y.default.isScreenSharingSupported ? E.default.StreamManager.get()._newScreenSharing(e, t).then(function (t) {
                        t.renderer && e.dispatchEvent({
                            name: d.CallEvents.LocalVideoStreamAdded,
                            type: t.renderer.kind,
                            element: t.renderer.element,
                            videoStream: t.renderer.stream
                        }), e.peerConnection.addCustomMedia(t.stream).then(function () {
                            n({
                                name: d.CallEvents.Updated,
                                result: !0,
                                call: e
                            })
                        })
                    }).catch(function (t) {
                        e.log.warning(t.message), r(t)
                    }) : (e.log.warning("Sorry, this browser does not support screen sharing."), r(new Error("Sorry, this browser does not support screen sharing.")))
                })
            }
        }, {
            key: "stopSharingScreen",
            value: function () {
                var e = this;
                return new Promise(function (t, n) {
                    var r = E.default.StreamManager.get()._getScreenSharing(e);
                    if (r) {
                        var o = r.length;
                        0 === o && t({
                            name: d.CallEvents.Updated,
                            result: !0,
                            call: e
                        }), r.forEach(function (r) {
                            var i = !r.renderer;
                            E.default.StreamManager.get()._clearScreenSharing(e, r).then(function () {
                                if (i) return e.peerConnection.removeCustomMedia(r.stream)
                            }).then(function () {
                                if (r.stream = void 0, --o <= 0) return void t({
                                    name: d.CallEvents.Updated,
                                    result: !0,
                                    call: e
                                })
                            }).catch(function (t) {
                                e.log.warning(t.message), n(t)
                            })
                        })
                    } else e.log.warning("Sorry, screen sharing not started yet."), n(new Error("Sorry, screen sharing not started yet."))
                })
            }
        }, {
            key: "addLocalStream",
            value: function (e) {
                arguments.length > 1 && void 0 !== arguments[1] && arguments[1];
                throw new Error("Not implemented")
            }
        }, {
            key: "wireRemoteStream",
            value: function () {
                return new Promise(function (e, t) {
                    e()
                })
            }
        }, {
            key: "getRemoteAudioStreams",
            value: function () {
                var e = this;
                return new Promise(function (t, n) {
                    e.peerConnection ? (e.peerConnection.remoteStreams.forEach(function (e) {
                        if (e.getAudioTracks().length) return void t(new MediaStream(e.getAudioTracks()))
                    }), n(new Error("We have no remote MediaStream for this call yet"))) : n(new Error("We have no PC for this call yet"))
                })
            }
        }, {
            key: "getRemoteVideoStreams",
            value: function () {
                var e = this;
                return new Promise(function (t, n) {
                    e.peerConnection ? void 0 !== e.peerConnection.remoteStreams[0] && 0 != e.peerConnection.remoteStreams[0].getVideoTracks().length ? t(new MediaStream(e.peerConnection.remoteStreams[0].getVideoTracks())) : n(new Error("We have no remote MediaStream for this call yet")) : n(new Error("We have no PC for this call yet"))
                })
            }
        }, {
            key: "getRemoteWiredState",
            value: function () {
                return this.settings.wiredRemote
            }
        }, {
            key: "getLocalWiredState",
            value: function () {
                return this.settings.wiredLocal
            }
        }, {
            key: "useAudioOutput",
            value: function (e) {
                S.EndpointManager.get().useAudioOutput(this, e)
            }
        }, {
            key: "getAudioElementId",
            value: function () {
                return (this._peerConnection.remoteStreams.length = 0) ? null : (this._peerConnection.remoteStreams[0].getAudioTracks().length = 0) ? null : this._peerConnection.remoteStreams[0].getAudioTracks()[0].id
            }
        }, {
            key: "getDirections",
            value: function () {
                if (void 0 !== this.peerConnection) return this.peerConnection.getDirections()
            }
        }, {
            key: "getStreamActivity",
            value: function () {
                return {}
            }
        }, {
            key: "hdnFRS",
            value: function () {
                this.peerConnection._hdnFRS()
            }
        }, {
            key: "hdnFRSPrep",
            value: function () {
                var e = this;
                void 0 !== this.peerConnection ? this.peerConnection._hdnFRSPrep() : setTimeout(function () {
                    e.hdnFRSPrep()
                }, 1e3)
            }
        }, {
            key: "runIncomingReInvite",
            value: function (e, t) {
                var n = this;
                if (this.settings.state === u.UPDATING) g.VoxSignaling.get().callRemoteFunction(m.RemoteFunction.rejectReInvite, this.settings.id, {});
                else {
                    this.settings.state = u.UPDATING;
                    var r = h.CallManager.get().isSDPHasVideo(t);
                    this.peerConnection.handleReinvite(e, t, r).then(function () {
                        n.peerConnection.restoreMute()
                    })
                }
            }
        }, {
            key: "setActiveForce",
            value: function (e) {
                this.settings.active = e
            }
        }, {
            key: "_traceName",
            value: function () {
                return "Call"
            }
        }, {
            key: "getCallDuration",
            value: function () {
                return Date.now() - this.startTime
            }
        }, {
            key: "getEndpoints",
            value: function () {
                return S.EndpointManager.get().getEndPointsByCall(this)
            }
        }, {
            key: "stateValue",
            get: function () {
                return this.settings.state
            }
        }, {
            key: "promise",
            get: function () {
                return this._promise
            }
        }, {
            key: "peerConnection",
            set: function (e) {
                this._peerConnection = e
            },
            get: function () {
                return this._peerConnection
            }
        }]), t
    }(_.EventTarget);
    l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "number", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "displayName", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "headers", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "active", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "state", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "answer", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "decline", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "reject", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "hangup", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "sendTone", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "mutePlayback", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "unmutePlayback", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "restoreRMute", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "muteMicrophone", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "unmuteMicrophone", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "showRemoteVideo", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "setRemoteVideoPosition", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "setRemoteVideoSize", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "sendInfo", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "sendMessage", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "setVideoSettings", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "getVideoElementId", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "addEventListener", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "on", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "removeEventListener", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "off", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "dispatchEvent", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "checkState", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "onConnected", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "onDisconnected", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "onFailed", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "onStopRinging", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "onRingOut", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "onTransferComplete", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "onTransferFailed", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "onInfo", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "setActive", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "checkCallMode", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "canStartSendingCandidates", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "notifyICETimeout", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "sendVideo", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "receiveVideo", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "sendAudio", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "getLocalStream", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "setLocalStream", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "shareScreen", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "stopSharingScreen", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "addLocalStream", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "wireRemoteStream", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "getRemoteAudioStreams", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "getRemoteVideoStreams", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "getRemoteWiredState", null), l([p.LogManager.d_trace(p.LogCategory.CALL)], R.prototype, "getLocalWiredState", null), l([p.LogManager.d_trace(p.LogCategory.CLIENT)], R.prototype, "useAudioOutput", null), l([p.LogManager.d_trace(p.LogCategory.CLIENT)], R.prototype, "getAudioElementId", null), l([p.LogManager.d_trace(p.LogCategory.CLIENT)], R.prototype, "getStreamActivity", null), l([p.LogManager.d_trace(p.LogCategory.CLIENT)], R.prototype, "hdnFRS", null), l([p.LogManager.d_trace(p.LogCategory.CLIENT)], R.prototype, "hdnFRSPrep", null), l([p.LogManager.d_trace(p.LogCategory.CLIENT)], R.prototype, "runIncomingReInvite", null), l([p.LogManager.d_trace(p.LogCategory.CLIENT)], R.prototype, "setActiveForce", null), t.Call = R
}, function (e, t, n) {
    "use strict";

    function r(e, t) {
        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }
    var o = function () {
        function e(e, t) {
            for (var n = 0; n < t.length; n++) {
                var r = t[n];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
            }
        }
        return function (t, n, r) {
            return n && e(t.prototype, n), r && e(t, r), t
        }
    }(),
        i = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
            return typeof e
        } : function (e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        },
        a = function (e, t, n, r) {
            var o, a = arguments.length,
                s = a < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
            if ("object" === ("undefined" == typeof Reflect ? "undefined" : i(Reflect)) && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r);
            else
                for (var c = e.length - 1; c >= 0; c--)(o = e[c]) && (s = (a < 3 ? o(s) : a > 3 ? o(t, n, s) : o(t, n)) || s);
            return a > 3 && s && Object.defineProperty(t, n, s), s
        };
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var s = n(0),
        c = n(9),
        l = n(46),
        u = n(51),
        d = function () {
            function e() {
                throw r(this, e), new Error("Please, use the EndpointManager.get() instead create new object")
            }
            return o(e, null, [{
                key: "get",
                value: function () {
                    return void 0 === this.instance && (c.PCFactory.hasTransceivers ? e.instance = new l.TransceiversEndpointManager : e.instance = new u.PlainEndpointManager), this.instance
                }
            }]), e
        }();
    a([s.LogManager.d_trace(s.LogCategory.ENDPOINT)], d, "get", null), t.EndpointManager = d
}, function (e, t, n) {
    "use strict";

    function r(e, t) {
        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }
    var o = function () {
        function e(e, t) {
            for (var n = 0; n < t.length; n++) {
                var r = t[n];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
            }
        }
        return function (t, n, r) {
            return n && e(t.prototype, n), r && e(t, r), t
        }
    }();
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var i = n(1),
        a = function () {
            function e() {
                r(this, e)
            }
            return o(e, [{
                key: "_traceName",
                value: function () {
                    return "SDPMuggle"
                }
            }], [{
                key: "detectDirections",
                value: function (t) {
                    var n = [],
                        r = t.split(/(\r\n|\r|\n)/).filter(e.validLine),
                        o = "";
                    return r.forEach(function (e) {
                        if (0 === e.indexOf("m=")) {
                            var t = e.substr(2);
                            o = t.split(" ")[0]
                        }
                        "" === o || "a=sendrecv" !== e && "a=sendonly" !== e && "a=recvonly" !== e && "a=inactive" !== e || (n.push({
                            type: o,
                            direction: e.substr(2)
                        }), o = "")
                    }), n
                }
            }, {
                key: "removeTelephoneEvents",
                value: function (t) {
                    if (-1 !== t.sdp.indexOf("a=rtpmap:127 telephone-event/8000")) {
                        for (var n = t.sdp.split(/(\r\n|\r|\n)/).filter(e.validLine), r = -1, o = 0; o < n.length; o++) {
                            if (-1 !== n[o].indexOf("m=audio")) {
                                var i = n[o];
                                "string" == typeof i && (n[o] = i.replace(" 127", ""))
                            } - 1 !== n[o].indexOf("a=rtpmap:127 telephone-event/8000") && (r = o)
                        }
                        n.splice(r, 1), t.sdp = n.join("\r\n") + "\r\n"
                    }
                    return t
                }
            }, {
                key: "removeDoubleOpus",
                value: function (t) {
                    if (-1 !== t.sdp.indexOf("a=rtpmap:109 opus") && -1 !== t.sdp.indexOf("a=rtpmap:111 opus")) {
                        for (var n = t.sdp.split(/(\r\n|\r|\n)/).filter(e.validLine), r = -1, o = 0; o < n.length; o++) {
                            if (-1 !== n[o].indexOf("m=audio")) {
                                var i = n[o];
                                "string" == typeof i && (n[o] = i.replace(" 109", ""))
                            } - 1 !== n[o].indexOf("a=rtpmap:109 opus") && (r = o)
                        }
                        n.splice(r, 1), t.sdp = n.join("\r\n") + "\r\n"
                    }
                    return t
                }
            }, {
                key: "removeTransportCC",
                value: function (t) {
                    if (-1 !== t.sdp.indexOf("http://www.ietf.org/id/draft-holmer-rmcat-transport-wide-cc-extensions-01")) {
                        var n = t.sdp.split(/(\r\n|\r|\n)/).filter(e.validLine),
                            r = [];
                        n.forEach(function (e, t) {
                            -1 !== e.indexOf("http://www.ietf.org/id/draft-holmer-rmcat-transport-wide-cc-extensions-01") && r.unshift(t)
                        }), r.forEach(function (e) {
                            return n.splice(e, 1)
                        }), t.sdp = n.join("\r\n") + "\r\n"
                    }
                    return t
                }
            }, {
                key: "removeTIAS",
                value: function (e) {
                    return e
                }
            }, {
                key: "fixVideoRecieve",
                value: function (t, n) {
                    if (-1 !== t.sdp.indexOf("m=video") && !n) {
                        var r = t.sdp.split(/(\r\n|\r|\n)/).filter(e.validLine),
                            o = null;
                        r = r.map(function (e, t) {
                            return null === o ? -1 !== e.indexOf("m=video") && (o = t) : "a=sendrecv" === e ? e = "a=sendonly" : "a=recvonly" === e && (e = "a=inactive"), e
                        }), t.sdp = r.join("\r\n") + "\r\n"
                    }
                    return t
                }
            }, {
                key: "addSetupAttribute",
                value: function (e) {
                    return -1 == e.indexOf("a=setup:") && (e += "a=setup:actpass\r\n"), e
                }
            }, {
                key: "findTrackByMid",
                value: function (t, n) {
                    for (var r = t.split(/(\r\n|\r|\n)/).filter(e.validLine), o = -1, i = 0; i < r.length && (-1 === r[i].indexOf("a=mid:") || -1 === o); i++)
                        if (-1 !== r[i].indexOf("a=mid:" + n) && (o = i), -1 !== r[i].indexOf("msid:") && -1 !== o) return r[i].split(" ").slice(-1).pop();
                    return ""
                }
            }, {
                key: "addXAS",
                value: function (e) {
                    if (i.Client.getInstance().config().experiments.xas && i.Client.getInstance().config().experiments.xas) {
                        var t = i.Client.getInstance().config().experiments.xas;
                        void 0 !== t.as && -1 !== t.as && (e.sdp = e.sdp.replace(/(a=mid:video.*\r\n)/g, "$1b=AS:" + t.as + "\r\n")), void 0 !== t.tias && -1 !== t.tias && (e.sdp = e.sdp.replace(/(a=mid:video.*\r\n)/g, "$1b=TIAS:" + t.as + "\r\n"))
                    }
                    return e
                }
            }]), e
        }();
    a.validLine = RegExp.prototype.test.bind(/^([a-z])=(.*)/), t.SDPMuggle = a
}, function (e, t, n) {
    "use strict";

    function r(e, t) {
        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }
    var o = function () {
        function e(e, t) {
            for (var n = 0; n < t.length; n++) {
                var r = t[n];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
            }
        }
        return function (t, n, r) {
            return n && e(t.prototype, n), r && e(t, r), t
        }
    }();
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var i = n(0);
    ! function (e) {
        e[e.multiplex = "multiplex"] = "multiplex", e[e.audio = "audio"] = "audio", e[e.video = "video"] = "video", e[e.screen = "screen"] = "screen", e[e.data = "data"] = "data", e[e.unbundled = "unbundled"] = "unbundled"
    }(t.CallstatsIoFabricUsage || (t.CallstatsIoFabricUsage = {}));
    ! function (e) {
        e[e.fabricHold = "fabricHold"] = "fabricHold", e[e.fabricResume = "fabricResume"] = "fabricResume", e[e.audioMute = "audioMute"] = "audioMute", e[e.audioUnmute = "audioUnmute"] = "audioUnmute", e[e.videoPause = "videoPause"] = "videoPause", e[e.videoResume = "videoResume"] = "videoResume", e[e.fabricTerminated = "fabricTerminated"] = "fabricTerminated", e[e.screenShareStart = "screenShareStart"] = "screenShareStart", e[e.screenShareStop = "screenShareStop"] = "screenShareStop", e[e.dominantSpeaker = "dominantSpeaker"] = "dominantSpeaker", e[e.activeDeviceList = "activeDeviceList"] = "activeDeviceList"
    }(t.CallstatsIoFabricEvent || (t.CallstatsIoFabricEvent = {}));
    ! function (e) {
        e[e.getUserMedia = "getUserMedia"] = "getUserMedia", e[e.createOffer = "createOffer"] = "createOffer", e[e.createAnswer = "createAnswer"] = "createAnswer", e[e.setLocalDescription = "setLocalDescription"] = "setLocalDescription", e[e.setRemoteDescription = "setRemoteDescription"] = "setRemoteDescription", e[e.addIceCandidate = "addIceCandidate"] = "addIceCandidate", e[e.iceConnectionFailure = "iceConnectionFailure"] = "iceConnectionFailure", e[e.signalingError = "signalingError"] = "signalingError", e[e.applicationLog = "applicationLog"] = "applicationLog"
    }(t.CallstatsioWrtcFuncNames || (t.CallstatsioWrtcFuncNames = {}));
    var a = function () {
        function e(t) {
            r(this, e), this._params = t, this.inited = !1, this.pendingFabric = [];
            var n = window;
            void 0 !== n.callstats && (this.callstats = new n.callstats(null, n.io))
        }
        return o(e, [{
            key: "init",
            value: function (t) {
                var n = this;
                return !!e.moduleEnabled && (i.LogManager.get().writeMessage(i.LogCategory.UTILS, "[CallstatsIo]", i.LogLevel.INFO, " Callstats.io SDK initialization start"), this.callstats.initialize(this._params.AppID, this._params.AppSecret, t, function () {
                    i.LogManager.get().writeMessage(i.LogCategory.UTILS, "[CallstatsIo]", i.LogLevel.INFO, " Callstats.io SDK initialization successful"), n.inited = !0, n.pendingFabric.map(function (e) {
                        n.callstats.addNewFabric(e.pc, e.remoteUser, e.fabricUsage, e.callID)
                    })
                }, function () { }, this.packParams()), !0)
            }
        }, {
            key: "packParams",
            value: function () {
                var e = {};
                return this._params.disableBeforeUnloadHandler && (e.disableBeforeUnloadHandler = this._params.disableBeforeUnloadHandler), this._params.applicationVersion && (e.applicationVersion = this._params.applicationVersion), e
            }
        }, {
            key: "addNewFabric",
            value: function (t, n, r, o) {
                if (!e.moduleEnabled) return !1;
                this.inited ? (i.LogManager.get().writeMessage(i.LogCategory.UTILS, "[CallstatsIo]", i.LogLevel.INFO, " Callstats.io addNewFabric"), this.callstats.addNewFabric(t, n, r, o)) : this.pendingFabric.push({
                    pc: t,
                    remoteUser: n,
                    fabricUsage: r,
                    callID: o
                })
            }
        }, {
            key: "sendFabricEvent",
            value: function (t, n, r) {
                if (!e.moduleEnabled) return !1;
                this.callstats.sendFabricEvent(t, n, r)
            }
        }, {
            key: "reportError",
            value: function (t, n, r, o, i, a) {
                if (!e.moduleEnabled) return !1;
                this.callstats.reportError(t, n, r, o, i, a)
            }
        }, {
            key: "_traceName",
            value: function () {
                return "CallstatsIo"
            }
        }], [{
            key: "isModuleEnabled",
            value: function () {
                return e.moduleEnabled
            }
        }, {
            key: "get",
            value: function (t) {
                return void 0 !== window.callstats && (e.moduleEnabled = !0), void 0 === e.instance && (e.instance = new e(t)), void 0 !== t && (e.instance._params = t), e.instance
            }
        }]), e
    }();
    a.moduleEnabled = !1, t.CallstatsIo = a
}, function (e, t, n) {
    "use strict";

    function r(e, t) {
        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }
    var o = function () {
        function e(e, t) {
            for (var n = 0; n < t.length; n++) {
                var r = t[n];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
            }
        }
        return function (t, n, r) {
            return n && e(t.prototype, n), r && e(t, r), t
        }
    }(),
        i = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
            return typeof e
        } : function (e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        },
        a = function (e, t, n, r) {
            var o, a = arguments.length,
                s = a < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
            if ("object" === ("undefined" == typeof Reflect ? "undefined" : i(Reflect)) && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r);
            else
                for (var c = e.length - 1; c >= 0; c--)(o = e[c]) && (s = (a < 3 ? o(s) : a > 3 ? o(t, n, s) : o(t, n)) || s);
            return a > 3 && s && Object.defineProperty(t, n, s), s
        };
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var s = n(29),
        c = n(0),
        l = n(15),
        u = n(14),
        d = function () {
            function e(t, n) {
                r(this, e), this._text = t, void 0 !== this.payload ? this._payload = n : this._payload = [], this._uuid = (new s.GUID).toString()
            }
            return o(e, [{
                key: "sendTo",
                value: function (e) {
                    this._conversation = e.uuid, u.MsgSignaling.get().sendPayload(l.MsgAction.sendMessage, this.getPayload())
                }
            }, {
                key: "getPayload",
                value: function () {
                    var e = {
                        uuid: this._uuid,
                        text: this._text,
                        conversation: this._conversation
                    };
                    return void 0 !== this._payload ? e.payload = this._payload : e.payload = [], e
                }
            }, {
                key: "update",
                value: function () {
                    u.MsgSignaling.get().sendPayload(l.MsgAction.editMessage, this.getPayload())
                }
            }, {
                key: "remove",
                value: function () {
                    u.MsgSignaling.get().sendPayload(l.MsgAction.removeMessage, {
                        uuid: this._uuid,
                        conversation: this.conversation
                    })
                }
            }, {
                key: "toCache",
                value: function () {
                    return {
                        seq: this._seq,
                        uuid: this._uuid,
                        text: this._text,
                        payload: this._payload,
                        conversation: this._conversation,
                        sender: this._sender
                    }
                }
            }, {
                key: "_traceName",
                value: function () {
                    return "Message"
                }
            }, {
                key: "uuid",
                get: function () {
                    return this._uuid
                }
            }, {
                key: "conversation",
                get: function () {
                    return this._conversation
                }
            }, {
                key: "text",
                get: function () {
                    return this._text
                },
                set: function (e) {
                    this._text = e
                }
            }, {
                key: "payload",
                get: function () {
                    return this._payload
                },
                set: function (e) {
                    this._payload = e
                }
            }, {
                key: "seq",
                get: function () {
                    return this._seq
                }
            }, {
                key: "sender",
                get: function () {
                    return this._sender
                }
            }], [{
                key: "_createFromBus",
                value: function (t, n) {
                    var r = new e(t.text, t.payload);
                    return r._uuid = t.uuid, r._conversation = t.conversation, r._sender = t.sender, r._seq = n, r
                }
            }, {
                key: "createFromCache",
                value: function (t) {
                    var n = new e(t.text, t.payload);
                    return n._uuid = t.uuid, n._conversation = t.conversation, n._sender = t.sender, n._seq = t.seq, n
                }
            }]), e
        }();
    a([c.LogManager.d_trace(c.LogCategory.MESSAGING)], d.prototype, "sendTo", null), a([c.LogManager.d_trace(c.LogCategory.MESSAGING)], d.prototype, "getPayload", null), a([c.LogManager.d_trace(c.LogCategory.MESSAGING)], d.prototype, "update", null), a([c.LogManager.d_trace(c.LogCategory.MESSAGING)], d.prototype, "remove", null), a([c.LogManager.d_trace(c.LogCategory.MESSAGING)], d.prototype, "toCache", null), a([c.LogManager.d_trace(c.LogCategory.MESSAGING)], d, "_createFromBus", null), a([c.LogManager.d_trace(c.LogCategory.MESSAGING)], d, "createFromCache", null), t.Message = d
}, function (e, t, n) {
    "use strict";

    function r(e, t) {
        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }
    var o = function () {
        function e(e, t) {
            for (var n = 0; n < t.length; n++) {
                var r = t[n];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
            }
        }
        return function (t, n, r) {
            return n && e(t.prototype, n), r && e(t, r), t
        }
    }(),
        i = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
            return typeof e
        } : function (e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        },
        a = function (e, t, n, r) {
            var o, a = arguments.length,
                s = a < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
            if ("object" === ("undefined" == typeof Reflect ? "undefined" : i(Reflect)) && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r);
            else
                for (var c = e.length - 1; c >= 0; c--)(o = e[c]) && (s = (a < 3 ? o(s) : a > 3 ? o(t, n, s) : o(t, n)) || s);
            return a > 3 && s && Object.defineProperty(t, n, s), s
        };
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var s = n(0),
        c = n(1),
        l = n(10),
        u = n(4),
        d = function () {
            function e() {
                r(this, e)
            }
            return o(e, [{
                key: "_traceName",
                value: function () {
                    return "Logger"
                }
            }], [{
                key: "extend",
                value: function () {
                    for (var e = arguments.length, t = Array(e), n = 0; n < e; n++) t[n] = arguments[n];
                    var r = {},
                        o = function (e) {
                            for (var t in e) Object.prototype.hasOwnProperty.call(e, t) && (r[t] = e[t])
                        };
                    o(arguments[0]);
                    for (var i = 1; i < arguments.length; i++) {
                        o(arguments[i])
                    }
                    return r
                }
            }, {
                key: "stringifyExtraHeaders",
                value: function (e) {
                    return e = "[object Object]" == Object.prototype.toString.call(e) ? JSON.stringify(e) : null
                }
            }, {
                key: "cadScript",
                value: function (e) {
                    return e.split(";").map(function (e) {
                        if (0 !== e.length) {
                            var t = e.match(/\([0-9\/\.,\*\+]*\)$/),
                                n = e.substring(0, t.index),
                                r = t.pop();
                            if (t.length) throw new Error("cadence script should be of the form `%f(%f/%f[,%f/%f])`");
                            if (n = "*" === n ? 1 / 0 : parseFloat(n), isNaN(n)) throw new Error("cadence length should be of the form `%f`");
                            return r = r.slice(1, r.length - 1).split(",").map(function (e) {
                                try {
                                    var t = e.split("/");
                                    if (t.length > 3) throw new Error;
                                    return t = t.map(function (e, t) {
                                        if (2 === t) return e.split("+").map(function (e) {
                                            var t = parseInt(e, 10);
                                            if (isNaN(t)) throw new Error;
                                            return t - 1
                                        });
                                        var n;
                                        if ("*" == e && (n = 1 / 0), n = n || parseFloat(e), isNaN(n)) throw new Error;
                                        return n
                                    }), {
                                            on: t[0],
                                            off: t[1],
                                            frequencies: t[2]
                                        }
                                } catch (e) {
                                    throw new Error("cadence segments should be of the form `%f/%f[%d[+%d]]`")
                                }
                            }), {
                                    duration: n,
                                    sections: r
                                }
                        }
                    })
                }
            }, {
                key: "freqScript",
                value: function (e) {
                    return e.split(",").map(function (e) {
                        try {
                            var t = e.split("@"),
                                n = parseInt(t.shift()),
                                r = parseFloat(t.shift());
                            if (t.length) throw Error();
                            return {
                                frequency: n,
                                decibels: r
                            }
                        } catch (e) {
                            throw new Error("freqScript pairs are expected to be of the form `%d@%f[,%d@%f]`")
                        }
                    })
                }
            }, {
                key: "toneScript",
                value: function (e) {
                    var t = e.split(";");
                    return {
                        frequencies: this.freqScript(t.shift()),
                        cadences: this.cadScript(t.join(";"))
                    }
                }
            }, {
                key: "playToneScript",
                value: function (e) {
                    var t = arguments.length > 1 && void 0 !== arguments[1] && arguments[1];
                    if (void 0 !== window.AudioContext || void 0 !== window.webkitAudioContext) {
                        var n = u.default.AudioDeviceManager.get().getAudioContext();
                        if (null === n) return;
                        var r = this.toneScript(e),
                            o = [],
                            i = 0,
                            a = function (e) {
                                for (var t = 0; t < n.sampleRate * e; t++) o.push(0)
                            },
                            s = function (e, t) {
                                for (var i = 0; i < n.sampleRate * t; i++) {
                                    for (var a = 0, s = 0; s < e.length; s++) a += Math.pow(10, r.frequencies[e[s]].decibels / 20) * Math.sin((o.length + i) * (3.14159265359 / n.sampleRate) * r.frequencies[e[s]].frequency), i < 10 && (a *= i / 10), i > n.sampleRate * t - 10 && (a *= (n.sampleRate * t - i) / 10);
                                    o.push(a)
                                }
                            },
                            c = function (e, t) {
                                if (t != 1 / 0) var n = t;
                                else n = t = 20;
                                if (0 !== e.off && e.off != 1 / 0)
                                    for (; n > 0;) {
                                        s(e.frequencies, e.on), n -= e.on, a(e.off), n -= e.off;
                                        n = parseInt(String(10 * n)) / 10
                                    } else s(e.frequencies, t)
                            };
                        this.source = n.createBufferSource();
                        for (var l = 0; l < r.cadences.length; l++) r.cadences[l].duration == 1 / 0 && (this.source.loop = !0),
                            function (e) {
                                e.duration != 1 / 0 ? i += e.duration : i += 20;
                                for (var t = 0; t < e.sections.length; t++) c(e.sections[t], e.duration)
                            }(r.cadences[l]);
                        this.source.connect(n.destination);
                        for (var d = n.createBuffer(1, i * n.sampleRate, n.sampleRate), g = d.getChannelData(0), f = 0; f < i * n.sampleRate; f++) g[f] = o[f];
                        o = null, this.source.buffer = d, !0 === t && (this.source.loop = !0), this.source.start(0)
                    }
                }
            }, {
                key: "stopPlayback",
                value: function () {
                    return void 0 !== this.source && null !== this.source && (this.source.stop(0), this.source = null, !0)
                }
            }, {
                key: "sendRequest",
                value: function (e, t, n, r) {
                    var o = !1,
                        i = function () {
                            for (var e, t = [function () {
                                return new XMLHttpRequest
                            }, function () {
                                return new ActiveXObject("Msxml2.XMLHTTP")
                            }, function () {
                                return new ActiveXObject("Msxml3.XMLHTTP")
                            }, function () {
                                return new ActiveXObject("Microsoft.XMLHTTP")
                            }], n = 0; n < t.length; n++) {
                                try {
                                    e = t[n](), 0 === n && (o = !0)
                                } catch (e) {
                                    continue
                                }
                                break
                            }
                            return e
                        }();
                    if (i) {
                        var a = r ? "POST" : "GET";
                        if (o) i.onerror = function () {
                            n(i)
                        }, i.ontimeout = function () {
                            n(i)
                        }, i.onload = function () {
                            t(i)
                        }, i.open(a, e), i.timeout = 5e3, i.send();
                        else {
                            if (i.open(a, e, !0), r && i.setRequestHeader("Content-type", "application/x-www-form-urlencoded"), i.onreadystatechange = function () {
                                if (4 == i.readyState) return 200 != i.status && 304 != i.status ? void n(i) : void t(i)
                            }, 4 == i.readyState) return;
                            i.send(r)
                        }
                    }
                }
            }, {
                key: "getServers",
                value: function (t) {
                    function n(n) {
                        null !== n ? t(n) : !0 !== r ? e.getServers(t, !0, o) : o.dispatchEvent({
                            name: "ConnectionFailed",
                            message: "VoxImplant Cloud is unavailable"
                        })
                    }
                    var r = arguments.length > 1 && void 0 !== arguments[1] && arguments[1],
                        o = arguments[2],
                        i = "https:" == document.location.protocol ? "https://" : "http://",
                        a = i + "balancer.voximplant.com/getNearestHost";
                    this.sendRequest(a, function (e) {
                        n(e.responseText)
                    }, function (e) {
                        n(null)
                    })
                }
            }, {
                key: "generateUUID",
                value: function () {
                    var e = this._gri,
                        t = this._ha;
                    return t(e(32), 8) + "-" + t(e(16), 4) + "-" + t(16384 | e(12), 4) + "-" + t(32768 | e(14), 4) + "-" + t(e(48), 12)
                }
            }, {
                key: "_gri",
                value: function (e) {
                    return e < 0 ? NaN : e <= 30 ? 0 | Math.random() * (1 << e) : e <= 53 ? (0 | Math.random() * (1 << 30)) + (0 | Math.random() * (1 << e - 30)) * (1 << 30) : NaN
                }
            }, {
                key: "_ha",
                value: function (e, t) {
                    for (var n = e.toString(16), r = t - n.length, o = "0"; r > 0; r >>>= 1, o += o) 1 & r && (n = o + n);
                    return n
                }
            }, {
                key: "filterXSS",
                value: function (e) {
                    var t = document.createElement("div");
                    return t.appendChild(document.createTextNode(e)), e = t.innerHTML
                }
            }, {
                key: "checkCA",
                value: function () {
                    if (!c.Client.getInstance().connected()) throw new Error("NOT_CONNECTED_TO_VOXIMPLANT");
                    if (!l.Authenticator.get().authorized()) throw new Error("NOT_AUTHORIZED")
                }
            }, {
                key: "canRTC",
                value: function (e) { }
            }, {
                key: "mixObjectToLeft",
                value: function (e, t) {
                    for (var n in e) void 0 !== t[n] && (e[n] = t[n]);
                    return e
                }
            }, {
                key: "makeRandomString",
                value: function (e) {
                    for (var t = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+/", n = "", r = 0; r < e; r++) n += t.charAt(Math.floor(Math.random() * t.length));
                    return n
                }
            }]), e
        }();
    d.source = null, a([s.LogManager.d_trace(s.LogCategory.UTILS)], d, "extend", null), a([s.LogManager.d_trace(s.LogCategory.UTILS)], d, "stringifyExtraHeaders", null), a([s.LogManager.d_trace(s.LogCategory.UTILS)], d, "cadScript", null), a([s.LogManager.d_trace(s.LogCategory.UTILS)], d, "freqScript", null), a([s.LogManager.d_trace(s.LogCategory.UTILS)], d, "toneScript", null), a([s.LogManager.d_trace(s.LogCategory.UTILS)], d, "playToneScript", null), a([s.LogManager.d_trace(s.LogCategory.UTILS)], d, "stopPlayback", null), a([s.LogManager.d_trace(s.LogCategory.UTILS)], d, "sendRequest", null), a([s.LogManager.d_trace(s.LogCategory.UTILS)], d, "getServers", null), a([s.LogManager.d_trace(s.LogCategory.UTILS)], d, "generateUUID", null), a([s.LogManager.d_trace(s.LogCategory.UTILS)], d, "_gri", null), a([s.LogManager.d_trace(s.LogCategory.UTILS)], d, "_ha", null), a([s.LogManager.d_trace(s.LogCategory.UTILS)], d, "filterXSS", null), a([s.LogManager.d_trace(s.LogCategory.UTILS)], d, "checkCA", null), t.Utils = d
}, function (e, t, n) {
    "use strict";

    function r(e, t) {
        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }
    var o = function () {
        function e(e, t) {
            for (var n = 0; n < t.length; n++) {
                var r = t[n];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
            }
        }
        return function (t, n, r) {
            return n && e(t.prototype, n), r && e(t, r), t
        }
    }(),
        i = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
            return typeof e
        } : function (e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        },
        a = function (e, t, n, r) {
            var o, a = arguments.length,
                s = a < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
            if ("object" === ("undefined" == typeof Reflect ? "undefined" : i(Reflect)) && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r);
            else
                for (var c = e.length - 1; c >= 0; c--)(o = e[c]) && (s = (a < 3 ? o(s) : a > 3 ? o(t, n, s) : o(t, n)) || s);
            return a > 3 && s && Object.defineProperty(t, n, s), s
        };
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var s, c = n(0),
        l = n(2),
        u = n(5),
        d = n(12),
        g = n(3),
        f = n(38),
        p = n(1),
        v = n(4);
    ! function (e) {
        e[e.IDLE = 0] = "IDLE", e[e.REMOTEOFFER = 1] = "REMOTEOFFER", e[e.LOCALOFFER = 2] = "LOCALOFFER", e[e.ESTABLISHING = 3] = "ESTABLISHING", e[e.ESTABLISHED = 4] = "ESTABLISHED", e[e.CLOSED = 5] = "CLOSED"
    }(s = t.PeerConnectionState || (t.PeerConnectionState = {}));
    var h;
    ! function (e) {
        e[e.CLIENT_SERVER_V1 = 0] = "CLIENT_SERVER_V1", e[e.P2P = 1] = "P2P"
    }(h = t.PeerConnectionMode || (t.PeerConnectionMode = {}));
    var y = function () {
        function e(t, n, o) {
            r(this, e), this.id = t, this.mode = n, this.videoEnabled = o, this.SEND_CANDIDATE_DELAY = 1e3, this.mediaRepository = [], this.candidateList = [], this.localCandidateTimer = -1, this.onHold = !1, this.muteMicState = !1, this.log = c.LogManager.get().createLogger(c.LogCategory.RTC, "PeerConnection " + t), this.state = s.IDLE, this.log.info("Created PC"), this.pendingCandidates = [], "_default" !== t && u.CallManager.get().calls[t] && (this.reInviteQ = new f.ReInviteQ(u.CallManager.get().calls[t], this._canReInvite))
        }
        return o(e, [{
            key: "getId",
            value: function () {
                return this.id
            }
        }, {
            key: "setState",
            value: function (e) {
                this.log.info("Transmitting from " + s[this.state] + " to " + s[e]), this.state = e
            }
        }, {
            key: "getState",
            value: function () {
                return this.state
            }
        }, {
            key: "processRemoteAnswer",
            value: function (e, t) {
                return this.log.info("Called processRemoteAnswer"), this.state = s.ESTABLISHING, this._processRemoteAnswer(e, t)
            }
        }, {
            key: "getLocalOffer",
            value: function () {
                return this.state === s.IDLE || this.state === s.ESTABLISHED || s.LOCALOFFER ? (this.log.info("Called getLocalOffer"), this.state = s.LOCALOFFER, this._getLocalOffer()) : (this.log.error("Called getLocalOffer in state " + s[this.state]), new Promise(function (e, t) {
                    t("Invalid state")
                }))
            }
        }, {
            key: "getLocalAnswer",
            value: function () {
                return this._getLocalAnswer()
            }
        }, {
            key: "processRemoteOffer",
            value: function (e) {
                return this.state === s.IDLE || this.state === s.ESTABLISHED ? (this.log.info("Called processRemoteOffer"), this.state = s.ESTABLISHING, this._processRemoteOffer(e)) : (this.log.error("Called processRemoteOffer in state " + s[this.state]), new Promise(function (e, t) {
                    t("Invalid state")
                }))
            }
        }, {
            key: "close",
            value: function () {
                this.log.info("Called close"), this._close()
            }
        }, {
            key: "addRemoteCandidate",
            value: function (e, t) {
                return this.log.info("Called addRemoteCandidate"), this._addRemoteCandidate(e, t)
            }
        }, {
            key: "sendLocalCandidateToPeer",
            value: function (e, t) {
                var n = this;
                switch (this._call = u.CallManager.get().calls[this.id], this.mode) {
                    case h.P2P:
                        this.candidateList.push([t, e]), this.localCandidateTimer <= 0 && (this.localCandidateTimer = window.setTimeout(function () {
                            window.clearTimeout(n.localCandidateTimer), n.localCandidateTimer = -1, u.CallManager.get().calls[n.id] && u.CallManager.get().calls[n.id].sendInfo(d.Constants.P2P_SPD_FRAG_MIME_TYPE, JSON.stringify(n.candidateList), {}), n.candidateList = []
                        }, 200));
                        break;
                    case h.CLIENT_SERVER_V1:
                        l.VoxSignaling.get().callRemoteFunction(g.RemoteFunction.addCandidate, this.id, e, t)
                }
            }
        }, {
            key: "handleReinvite",
            value: function (e, t, n) {
                return this._handleReinvite(e, t, n)
            }
        }, {
            key: "addCandidateToSend",
            value: function (e, t) {
                this.pendingCandidates.push([t, e]), this.canSendCandidates && this.startCandidateSendTimer()
            }
        }, {
            key: "startCandidateSendTimer",
            value: function () {
                var e = this;
                null !== this.candidateSendTimer && void 0 !== this.candidateSendTimer || (this.candidateSendTimer = setTimeout(function () {
                    e.candidateSendTimer = null, e.pendingCandidates.length > 0 && u.CallManager.get().calls[e.id] && u.CallManager.get().calls[e.id].sendInfo(d.Constants.P2P_SPD_FRAG_MIME_TYPE, JSON.stringify(e.pendingCandidates), {}), e.pendingCandidates = []
                }, this.SEND_CANDIDATE_DELAY))
            }
        }, {
            key: "canStartSendingCandidates",
            value: function () {
                this.canSendCandidates = !0, this.startCandidateSendTimer()
            }
        }, {
            key: "sendDTMF",
            value: function (e) {
                this._sendDTMF(e, 500, 50)
            }
        }, {
            key: "setVideoEnabled",
            value: function (e) {
                var t = this.videoEnabled.receiveVideo;
                this.videoEnabled = e, t != e.receiveVideo && this._hold(this.onHold)
            }
        }, {
            key: "setVideoFlags",
            value: function (e) {
                this.videoEnabled = e
            }
        }, {
            key: "getDirections",
            value: function () {
                return this._getDirections()
            }
        }, {
            key: "setHoldKey",
            value: function (e) {
                this.onHold = e
            }
        }, {
            key: "getTrackKind",
            value: function () {
                return this._call ? v.default.StreamManager.get()._getTracksKind(this._call) : {}
            }
        }, {
            key: "sendMedia",
            value: function (e, t) {
                var n = this;
                return new Promise(function (e, t) {
                    if (n.onHold) return void t({
                        result: !1,
                        call: n._call
                    });
                    n.reInviteQ.add({
                        fx: function () {
                            p.Client.getInstance().config();
                            v.default.StreamManager.get().updateCallStream(n._call)
                        },
                        reject: t,
                        resolve: function (t) {
                            n.restoreMute(), e(t)
                        }
                    })
                })
            }
        }, {
            key: "hold",
            value: function (e) {
                var t = this;
                return new Promise(function (n, r) {
                    t.reInviteQ.add({
                        fx: function () {
                            t._hold(e)
                        },
                        reject: r,
                        resolve: function (e) {
                            t.restoreMute(), n(e)
                        }
                    })
                })
            }
        }, {
            key: "hdnFRS",
            value: function () {
                var e = this;
                return new Promise(function (t, n) {
                    if (e.onHold) return void n({
                        result: !1,
                        call: e._call
                    });
                    e.reInviteQ.add({
                        fx: function () {
                            e._hdnFRS()
                        },
                        reject: n,
                        resolve: function (n) {
                            e.restoreMute(), t(n)
                        }
                    })
                })
            }
        }, {
            key: "muteMicrophone",
            value: function (e) {
                var t = this;
                this.muteMicState !== e && (this.muteMicState = e, v.default.StreamManager.get().getCallStream(this._call).then(function (e) {
                    e.getAudioTracks().forEach(function (e) {
                        e.enabled = !t.muteMicState
                    })
                }))
            }
        }, {
            key: "restoreMute",
            value: function () {
                var e = this;
                if (this._call.settings.active) {
                    var t = this;
                    setTimeout(function () {
                        e._call.restoreRMute(), v.default.StreamManager.get().getCallStream(e._call).then(function (e) {
                            e.getAudioTracks().forEach(function (e) {
                                e.enabled = !t.muteMicState
                            })
                        })
                    }, 300)
                }
            }
        }, {
            key: "addCustomMedia",
            value: function (e) {
                var t = this;
                return new Promise(function (n, r) {
                    t.reInviteQ.add({
                        fx: function () {
                            t._addCustomMedia(e)
                        },
                        reject: r,
                        resolve: function (e) {
                            t.restoreMute(), n()
                        }
                    })
                })
            }
        }, {
            key: "fastAddCustomMedia",
            value: function (e) {
                this._addCustomMedia(e)
            }
        }, {
            key: "fastRemoveCustomMedia",
            value: function (e) {
                this._removeCustomMedia(e)
            }
        }, {
            key: "removeCustomMedia",
            value: function (e) {
                var t = this;
                return new Promise(function (n, r) {
                    t.reInviteQ.add({
                        fx: function () {
                            t._removeCustomMedia(e)
                        },
                        reject: r,
                        resolve: function (e) {
                            t.restoreMute(), n()
                        }
                    })
                })
            }
        }, {
            key: "_traceName",
            value: function () {
                return "PeerConnection"
            }
        }, {
            key: "remoteStreams",
            get: function () {
                return this._remoteStreams
            }
        }]), e
    }();
    a([c.LogManager.d_trace(c.LogCategory.RTC)], y.prototype, "setState", null), a([c.LogManager.d_trace(c.LogCategory.RTC)], y.prototype, "getState", null), a([c.LogManager.d_trace(c.LogCategory.RTC)], y.prototype, "processRemoteAnswer", null), a([c.LogManager.d_trace(c.LogCategory.RTC)], y.prototype, "getLocalOffer", null), a([c.LogManager.d_trace(c.LogCategory.RTC)], y.prototype, "getLocalAnswer", null), a([c.LogManager.d_trace(c.LogCategory.RTC)], y.prototype, "processRemoteOffer", null), a([c.LogManager.d_trace(c.LogCategory.RTC)], y.prototype, "close", null), a([c.LogManager.d_trace(c.LogCategory.RTC)], y.prototype, "addRemoteCandidate", null), a([c.LogManager.d_trace(c.LogCategory.RTC)], y.prototype, "sendLocalCandidateToPeer", null), a([c.LogManager.d_trace(c.LogCategory.RTC)], y.prototype, "handleReinvite", null), a([c.LogManager.d_trace(c.LogCategory.RTC)], y.prototype, "addCandidateToSend", null), a([c.LogManager.d_trace(c.LogCategory.RTC)], y.prototype, "startCandidateSendTimer", null), a([c.LogManager.d_trace(c.LogCategory.RTC)], y.prototype, "canStartSendingCandidates", null), a([c.LogManager.d_trace(c.LogCategory.RTC)], y.prototype, "sendDTMF", null), a([c.LogManager.d_trace(c.LogCategory.RTC)], y.prototype, "setVideoEnabled", null), a([c.LogManager.d_trace(c.LogCategory.RTC)], y.prototype, "setVideoFlags", null), a([c.LogManager.d_trace(c.LogCategory.RTC)], y.prototype, "setHoldKey", null), a([c.LogManager.d_trace(c.LogCategory.RTC)], y.prototype, "getTrackKind", null), a([c.LogManager.d_trace(c.LogCategory.RTC)], y.prototype, "sendMedia", null), a([c.LogManager.d_trace(c.LogCategory.RTC)], y.prototype, "hold", null), a([c.LogManager.d_trace(c.LogCategory.RTC)], y.prototype, "hdnFRS", null), a([c.LogManager.d_trace(c.LogCategory.RTC)], y.prototype, "muteMicrophone", null), a([c.LogManager.d_trace(c.LogCategory.RTC)], y.prototype, "restoreMute", null), a([c.LogManager.d_trace(c.LogCategory.RTC)], y.prototype, "addCustomMedia", null), a([c.LogManager.d_trace(c.LogCategory.RTC)], y.prototype, "fastAddCustomMedia", null), a([c.LogManager.d_trace(c.LogCategory.RTC)], y.prototype, "fastRemoveCustomMedia", null), a([c.LogManager.d_trace(c.LogCategory.RTC)], y.prototype, "removeCustomMedia", null), t.PeerConnection = y
}, function (e, t, n) {
    "use strict";

    function r(e, t) {
        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }

    function o(e, t) {
        if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
        return !t || "object" != typeof t && "function" != typeof t ? e : t
    }

    function i(e, t) {
        if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
        e.prototype = Object.create(t && t.prototype, {
            constructor: {
                value: e,
                enumerable: !1,
                writable: !0,
                configurable: !0
            }
        }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
    }
    var a = function () {
        function e(e, t) {
            for (var n = 0; n < t.length; n++) {
                var r = t[n];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
            }
        }
        return function (t, n, r) {
            return n && e(t.prototype, n), r && e(t, r), t
        }
    }(),
        s = function e(t, n, r) {
            null === t && (t = Function.prototype);
            var o = Object.getOwnPropertyDescriptor(t, n);
            if (void 0 === o) {
                var i = Object.getPrototypeOf(t);
                return null === i ? void 0 : e(i, n, r)
            }
            if ("value" in o) return o.value;
            var a = o.get;
            if (void 0 !== a) return a.call(r)
        },
        c = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
            return typeof e
        } : function (e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        },
        l = function (e, t, n, r) {
            var o, i = arguments.length,
                a = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
            if ("object" === ("undefined" == typeof Reflect ? "undefined" : c(Reflect)) && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, n, r);
            else
                for (var s = e.length - 1; s >= 0; s--)(o = e[s]) && (a = (i < 3 ? o(a) : i > 3 ? o(t, n, a) : o(t, n)) || a);
            return i > 3 && a && Object.defineProperty(t, n, a), a
        };
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var u = n(0),
        d = n(4),
        g = n(13),
        f = n(34),
        p = n(8),
        v = n(17),
        h = n(1),
        y = n(33),
        m = n(35),
        C = function (e) {
            function t() {
                r(this, t);
                var e = o(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this));
                if (void 0 !== t.instance) throw new Error("Error - use StreamManager.get()");
                return e._callStreams = {}, e._localMediaRenderers = [], e._sharingStreams = {}, e
            }
            return i(t, e), a(t, [{
                key: "onMirrorEnded",
                value: function () {
                    var e = this;
                    this.remMirrorStream(), this.getMirrorStream().then(function (t) {
                        e.dispatchEvent({
                            name: d.default.HardwareEvents.MediaRendererRemoved,
                            mediaRenderer: t
                        })
                    })
                }
            }, {
                key: "getMirrorStream",
                value: function () {
                    var e = this;
                    return new Promise(function (t, n) {
                        if (void 0 === e._mirrorStream) return navigator.mediaDevices.getUserMedia({
                            video: f.CameraManager.get().getCallConstraints("__local__")
                        }).then(function (n) {
                            e._mirrorStream = n, e._mirrorStream.getTracks().forEach(function (t) {
                                t.onended = function () {
                                    e.onMirrorEnded()
                                }, t.onmute = function () {
                                    e.onMirrorEnded
                                }
                            }), t(n)
                        }, n);
                        t(e._mirrorStream)
                    })
                }
            }, {
                key: "remMirrorStream",
                value: function () {
                    void 0 !== this._mirrorStream && (this._mirrorStream.getTracks().forEach(function (e) {
                        e.onended = void 0, e.onmute = void 0, e.stop()
                    }), this._mirrorStream = void 0)
                }
            }, {
                key: "getCallStream",
                value: function (e) {
                    var t = this,
                        n = arguments.length > 1 && void 0 !== arguments[1] && arguments[1];
                    return new Promise(function (r, o) {
                        var i = void 0 === e ? "__default" : e.id();
                        if (t._callStreams[i] && !n) r(t._callStreams[i]);
                        else {
                            var a = t._composeConstraints(e);
                            if (!a.audio && !a.video && "__default" !== i) return void r(null);
                            if ("firefox" !== p.default.getWSVendor()) navigator.mediaDevices.getUserMedia(a).then(function (e) {
                                if (p.default.isIphone() && t._mirrorStream) {
                                    u.LogManager.get().writeMessage(u.LogCategory.HARDWARE, "Device detected", u.LogLevel.TRACE, "iOS detected."), t._localMediaRenderers.forEach(function (e) {
                                        e.clear()
                                    });
                                    var n = new m.MediaRenderer(e, "video", !0, !0, "voximplantlocalvideo");
                                    t._localMediaRenderers.push(n), t._mirrorMediaRendererId = n.id, t.dispatchEvent({
                                        name: d.default.HardwareEvents.MediaRendererAdded,
                                        renderer: n
                                    })
                                }
                                t._callStreams[i] = e, e.getTracks().forEach(function (e) {
                                    e.onended = t.onCallEnded, e.onmute = t.onCallEnded
                                }), h.Client.getInstance().dispatchEvent({
                                    name: v.Events.MicAccessResult,
                                    result: !0,
                                    stream: e
                                }), r(e)
                            }, function (n) {
                                if ("NotFoundError" === n.name) {
                                    var a = {
                                        audio: !0
                                    };
                                    void 0 !== e && (e.settings.videoDirections.sendVideo = !1, a = t._composeConstraints(e)), navigator.mediaDevices.getUserMedia(a).then(function (e) {
                                        t._callStreams[i] = e, e.getTracks().forEach(function (e) {
                                            e.onended = t.onCallEnded, e.onmute = t.onCallEnded
                                        }), h.Client.getInstance().dispatchEvent({
                                            name: v.Events.MicAccessResult,
                                            result: !0,
                                            stream: e
                                        }), r(e)
                                    }, function (e) {
                                        h.Client.getInstance().dispatchEvent({
                                            name: v.Events.MicAccessResult,
                                            result: !1,
                                            stream: null
                                        }), o(e)
                                    })
                                } else h.Client.getInstance().dispatchEvent({
                                    name: v.Events.MicAccessResult,
                                    result: !1,
                                    stream: null
                                }), o(n)
                            });
                            else {
                                var s = null,
                                    c = null;
                                a.audio && (s = {
                                    audio: a.audio
                                }), a.video && (c = {
                                    video: a.video
                                }), navigator.mediaDevices.getUserMedia(s).then(function (e) {
                                    c ? navigator.mediaDevices.getUserMedia(c).then(function (n) {
                                        var o = new MediaStream;
                                        e.getTracks().forEach(function (e) {
                                            o.addTrack(e)
                                        }), n.getTracks().forEach(function (e) {
                                            o.addTrack(e)
                                        }), t._callStreams[i] = o, o.getTracks().forEach(function (e) {
                                            e.onended = t.onCallEnded, e.onmute = t.onCallEnded
                                        }), h.Client.getInstance().dispatchEvent({
                                            name: v.Events.MicAccessResult,
                                            result: !0,
                                            stream: o
                                        }), r(o)
                                    }, function () {
                                        t._callStreams[i] = e, e.getTracks().forEach(function (e) {
                                            e.onended = t.onCallEnded, e.onmute = t.onCallEnded
                                        }), h.Client.getInstance().dispatchEvent({
                                            name: v.Events.MicAccessResult,
                                            result: !0,
                                            stream: e
                                        }), r(e)
                                    }) : (t._callStreams[i] = e, e.getTracks().forEach(function (e) {
                                        e.onended = t.onCallEnded, e.onmute = t.onCallEnded
                                    }), h.Client.getInstance().dispatchEvent({
                                        name: v.Events.MicAccessResult,
                                        result: !0,
                                        stream: e
                                    }), r(e))
                                }, function (e) {
                                    h.Client.getInstance().dispatchEvent({
                                        name: v.Events.MicAccessResult,
                                        result: !1,
                                        stream: null
                                    }), o(e)
                                })
                            }
                        }
                    })
                }
            }, {
                key: "onCallEnded",
                value: function () { }
            }, {
                key: "_updateCallStream",
                value: function (e) {
                    return this.remCallStream(e), this.getCallStream(e)
                }
            }, {
                key: "updateCallStream",
                value: function (e) {
                    var t = this;
                    return new Promise(function (n, r) {
                        var o = t._callStreams[e.id()];
                        t.getCallStream(e, !0).then(function (i) {
                            e.peerConnection.fastRemoveCustomMedia(o), t._remCallStream(o), e.peerConnection.addCustomMedia(i).then(function (e) {
                                n(e)
                            }, function (e) {
                                return r(e)
                            })
                        })
                    })
                }
            }, {
                key: "remCallStream",
                value: function (e) {
                    var t = void 0 === e ? "__default" : e.id();
                    this._callStreams[t] && (this._remCallStream(this._callStreams[t]), this._callStreams[t] = void 0, delete this._callStreams[t], p.default.isIphone() && this._mirrorStream && (this.hideLocalVideo(), this.showLocalVideo()))
                }
            }, {
                key: "_remCallStream",
                value: function (e) {
                    e && e.getTracks().forEach(function (t) {
                        t.onended = void 0, t.onmute = void 0, t.stop(), e.removeTrack(t)
                    })
                }
            }, {
                key: "clear",
                value: function () {
                    this._mirrorStream && this._mirrorStream.getTracks().forEach(function (e) {
                        e.onended = void 0, e.onmute = void 0, e.stop()
                    }), this._mirrorStream = void 0;
                    for (var e in this._callStreams)
                        if (this._callStreams.hasOwnProperty(e)) {
                            var t = this._callStreams[e];
                            t && t.getTracks().forEach(function (e) {
                                e.onended = void 0, e.onmute = void 0, e.stop()
                            })
                        }
                    this._callStreams = {}
                }
            }, {
                key: "_composeConstraints",
                value: function (e) {
                    var t = void 0 === e ? "__default" : e.id(),
                        n = {};
                    return "__default" !== t && void 0 !== e && e.settings.videoDirections.sendVideo ? n.video = f.CameraManager.get().getCallConstraints(t) : n.video = !1, "__default" === t || e.settings.audioDirections.sendAudio ? n.audio = y.AudioDeviceManager.get().getCallConstraints(t) : n.audio = !1, n
                }
            }, {
                key: "getLocalMediaRenderers",
                value: function () {
                    return this._localMediaRenderers
                }
            }, {
                key: "showLocalVideo",
                value: function () {
                    var e = this;
                    if (this._mirrorMediaRendererId) throw new Error("Local video already displayed. Please, use Hardware.StreamManager.get().hideLocalVideo before request a new one.");
                    return new Promise(function (t, n) {
                        d.default.StreamManager.get().getMirrorStream().then(function (n) {
                            var r = new m.MediaRenderer(n, "video", !0, !0, "voximplantlocalvideo");
                            e._localMediaRenderers.push(r), e._mirrorMediaRendererId = r.id, t(r), e.dispatchEvent({
                                name: d.default.HardwareEvents.MediaRendererAdded,
                                renderer: r
                            })
                        }).catch(n)
                    })
                }
            }, {
                key: "hideLocalVideo",
                value: function () {
                    var e = this;
                    if (this._mirrorMediaRendererId) return new Promise(function (t, n) {
                        var r = e._localMediaRenderers.filter(function (t) {
                            return t.id === e._mirrorMediaRendererId
                        });
                        r && r.forEach(function (e) {
                            e.clear()
                        }), e.remMirrorStream(), e._localMediaRenderers = e._localMediaRenderers.filter(function (t) {
                            return t.id !== e._mirrorMediaRendererId
                        }), e._mirrorMediaRendererId = void 0, t()
                    });
                    throw new Error("Local video not displayed yet. Please, use Hardware.StreamManager.get().showLocalVideo to request a new one.")
                }
            }, {
                key: "on",
                value: function (e, n) {
                    s(t.prototype.__proto__ || Object.getPrototypeOf(t.prototype), "on", this).call(this, e, n)
                }
            }, {
                key: "off",
                value: function (e, n) {
                    s(t.prototype.__proto__ || Object.getPrototypeOf(t.prototype), "off", this).call(this, e, n)
                }
            }, {
                key: "_newScreenSharing",
                value: function (e, t) {
                    var n = this;
                    return new Promise(function (r, o) {
                        p.default.getScreenMedia().then(function (o) {
                            var i = {
                                stream: o,
                                renderer: null
                            };
                            if (t) {
                                var a = new m.MediaRenderer(o, "sharing", !0, !0);
                                i.renderer = a, n.dispatchEvent({
                                    name: d.default.HardwareEvents.MediaRendererAdded,
                                    renderer: a
                                }), a.onBeforeDestroy = function () {
                                    i.renderer = null, n._sharingStreams[e.id()] = n._sharingStreams[e.id()].filter(function (e) {
                                        return e.stream.id !== o.id
                                    }), e.peerConnection.removeCustomMedia(i.stream).then(function () {
                                        i.stream = void 0
                                    })
                                }
                            } else o.getTracks().forEach(function (t) {
                                t.onended = function () {
                                    o.getTracks().some(function (e) {
                                        return "live" === e.readyState
                                    }) || (n._sharingStreams[e.id()] = n._sharingStreams[e.id()].filter(function (e) {
                                        return e.stream.id !== o.id
                                    }), e.peerConnection.removeCustomMedia(i.stream).then(function () {
                                        i.stream = void 0
                                    }))
                                }
                            });
                            void 0 === n._sharingStreams[e.id()] && (n._sharingStreams[e.id()] = []), n._sharingStreams[e.id()].push(i), r(i)
                        }).catch(function (e) {
                            return o(e)
                        })
                    })
                }
            }, {
                key: "_getScreenSharing",
                value: function (e) {
                    return void 0 !== this._sharingStreams[e.id()] ? this._sharingStreams[e.id()] : []
                }
            }, {
                key: "_clearScreenSharing",
                value: function (e, t) {
                    var n = this;
                    return new Promise(function (r, o) {
                        n._sharingStreams[e.id()] = n._sharingStreams[e.id()].filter(function (e) {
                            return e.stream.id !== t.stream.id
                        }), t.renderer && (t.renderer.clear(), t.renderer = void 0), t.stream.getTracks().forEach(function (e) {
                            e.stop()
                        }), r()
                    })
                }
            }, {
                key: "_getTracksKind",
                value: function (e) {
                    var t = {},
                        n = this._callStreams[e.id()];
                    void 0 !== n && n.getTracks().forEach(function (e) {
                        return t[e.id] = e.kind
                    });
                    var r = this._sharingStreams[e.id()];
                    return void 0 !== r && r.forEach(function (e) {
                        e.stream.getTracks().forEach(function (e) {
                            return t[e.id] = "sharing"
                        })
                    }), t
                }
            }, {
                key: "_traceName",
                value: function () {
                    return "StreamManager"
                }
            }], [{
                key: "get",
                value: function () {
                    return void 0 === t.instance && (t.instance = new t), t.instance
                }
            }]), t
        }(g.EventTarget);
    l([u.LogManager.d_trace(u.LogCategory.HARDWARE)], C.prototype, "onMirrorEnded", null), l([u.LogManager.d_trace(u.LogCategory.HARDWARE)], C.prototype, "getMirrorStream", null), l([u.LogManager.d_trace(u.LogCategory.HARDWARE)], C.prototype, "remMirrorStream", null), l([u.LogManager.d_trace(u.LogCategory.HARDWARE)], C.prototype, "getCallStream", null), l([u.LogManager.d_trace(u.LogCategory.HARDWARE)], C.prototype, "onCallEnded", null), l([u.LogManager.d_trace(u.LogCategory.HARDWARE)], C.prototype, "_updateCallStream", null), l([u.LogManager.d_trace(u.LogCategory.HARDWARE)], C.prototype, "remCallStream", null), l([u.LogManager.d_trace(u.LogCategory.HARDWARE)], C.prototype, "_remCallStream", null), l([u.LogManager.d_trace(u.LogCategory.HARDWARE)], C.prototype, "clear", null), l([u.LogManager.d_trace(u.LogCategory.HARDWARE)], C.prototype, "_composeConstraints", null), t.StreamManager = C
}, function (e, t, n) {
    "use strict";

    function r(e, t) {
        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }

    function o(e, t) {
        if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
        return !t || "object" != typeof t && "function" != typeof t ? e : t
    }

    function i(e, t) {
        if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
        e.prototype = Object.create(t && t.prototype, {
            constructor: {
                value: e,
                enumerable: !1,
                writable: !0,
                configurable: !0
            }
        }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
    }
    var a = function () {
        function e(e, t) {
            for (var n = 0; n < t.length; n++) {
                var r = t[n];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
            }
        }
        return function (t, n, r) {
            return n && e(t.prototype, n), r && e(t, r), t
        }
    }(),
        s = function e(t, n, r) {
            null === t && (t = Function.prototype);
            var o = Object.getOwnPropertyDescriptor(t, n);
            if (void 0 === o) {
                var i = Object.getPrototypeOf(t);
                return null === i ? void 0 : e(i, n, r)
            }
            if ("value" in o) return o.value;
            var a = o.get;
            if (void 0 !== a) return a.call(r)
        };
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var c = n(13),
        l = n(28),
        u = function (e) {
            function t() {
                var e = !(arguments.length > 0 && void 0 !== arguments[0]) || arguments[0];
                r(this, t);
                var n = o(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this));
                return n.isDefault = e, n.mediaRenderers = [], n.addDefaultEventListener(l.EndpointEvents.RemoteMediaAdded, function (e) {
                    e.mediaRenderer.renderDefault()
                }), n
            }
            return i(t, e), a(t, [{
                key: "useAudioOutput",
                value: function (e) {
                    this.mediaRenderers && this.mediaRenderers.forEach(function (t) {
                        return t.useAudioOutput(e)
                    })
                }
            }, {
                key: "updateInfo",
                value: function (e) {
                    this.place = e.place, this.sipUri = e.sipURI, this.displayName = e.displayName, this.userName = e.username
                }
            }, {
                key: "_traceName",
                value: function () {
                    return "Endpoint"
                }
            }, {
                key: "on",
                value: function (e, n) {
                    s(t.prototype.__proto__ || Object.getPrototypeOf(t.prototype), "on", this).call(this, e, n)
                }
            }, {
                key: "off",
                value: function (e, n) {
                    s(t.prototype.__proto__ || Object.getPrototypeOf(t.prototype), "off", this).call(this, e, n)
                }
            }]), t
        }(c.EventTarget);
    t.Endpoint = u
}, function (e, t, n) {
    "use strict";
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    ! function (e) {
        e[e.InfoUpdated = "InfoUpdated"] = "InfoUpdated", e[e.Removed = "Removed"] = "Removed", e[e.RemoteMediaAdded = "RemoteMediaAdded"] = "RemoteMediaAdded", e[e.RemoteMediaRemoved = "RemoteMediaRemoved"] = "RemoteMediaRemoved", e[e.RTCStatsReceived = "RTCStatsReceived"] = "RTCStatsReceived"
    }(t.EndpointEvents || (t.EndpointEvents = {}))
}, function (e, t, n) {
    "use strict";

    function r(e, t) {
        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }
    var o = function () {
        function e(e, t) {
            for (var n = 0; n < t.length; n++) {
                var r = t[n];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
            }
        }
        return function (t, n, r) {
            return n && e(t.prototype, n), r && e(t, r), t
        }
    }();
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var i = function () {
        function e(t) {
            r(this, e), this.str = t || e.getNewGUIDString()
        }
        return o(e, [{
            key: "toString",
            value: function () {
                return this.str
            }
        }, {
            key: "_traceName",
            value: function () {
                return "GUID"
            }
        }], [{
            key: "getNewGUIDString",
            value: function () {
                var e = (new Date).getTime();
                return window.performance && "function" == typeof window.performance.now && (e += performance.now()), "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (t) {
                    var n = (e + 16 * Math.random()) % 16 | 0;
                    return e = Math.floor(e / 16), ("x" == t ? n : 3 & n | 8).toString(16)
                })
            }
        }]), e
    }();
    t.GUID = i
}, function (e, t, n) {
    "use strict";

    function r(e, t) {
        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }
    var o = function () {
        function e(e, t) {
            for (var n = 0; n < t.length; n++) {
                var r = t[n];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
            }
        }
        return function (t, n, r) {
            return n && e(t.prototype, n), r && e(t, r), t
        }
    }(),
        i = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
            return typeof e
        } : function (e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        },
        a = function (e, t, n, r) {
            var o, a = arguments.length,
                s = a < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
            if ("object" === ("undefined" == typeof Reflect ? "undefined" : i(Reflect)) && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r);
            else
                for (var c = e.length - 1; c >= 0; c--)(o = e[c]) && (s = (a < 3 ? o(s) : a > 3 ? o(t, n, s) : o(t, n)) || s);
            return a > 3 && s && Object.defineProperty(t, n, s), s
        };
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var s = n(0),
        c = n(29),
        l = n(23),
        u = n(15),
        d = n(14),
        g = n(40),
        f = n(16),
        p = n(31),
        v = function () {
            function e(t, n, o, i, a) {
                r(this, e), this._distinct = n, this._publicJoin = o, this._participants = t, this._customData = i, this._moderators = a
            }
            return o(e, [{
                key: "_getPayload",
                value: function () {
                    if (void 0 === this._uuid) throw Error("You must create UUID with createUUID() function!");
                    var e = {
                        uuid: this._uuid,
                        participants: this._prepareParticipants(this._participants)
                    };
                    return void 0 !== this._title ? e.title = this._title : e.title = "", void 0 !== this._moderators ? e.moderators = this._moderators : e.moderators = [], void 0 !== this._lastRead && (e.last_readed = this._lastRead), void 0 !== this._distinct ? e.distinct = this._distinct : e.distinct = !1, void 0 !== this._publicJoin ? e.enable_public_join = this._publicJoin : e.enable_public_join = !1, void 0 !== this._customData ? e.custom_data = this._customData : e.custom_data = {}, void 0 !== this._createdAt && (e.created_at = this._createdAt), void 0 !== this._createdAt && (e.uber_conversation = this._uberConversation), e
                }
            }, {
                key: "_getSimplePayload",
                value: function () {
                    if (void 0 === this._uuid) throw Error("You must create UUID with createUUID() function!");
                    return {
                        uuid: this._uuid,
                        title: void 0 !== this._title ? this._title : "",
                        distinct: void 0 !== this._distinct && this._distinct,
                        enable_public_join: void 0 !== this._publicJoin && this._publicJoin,
                        custom_data: void 0 !== this._customData ? this._customData : {}
                    }
                }
            }, {
                key: "_createUUID",
                value: function () {
                    if (void 0 !== this._uuid) throw Error("UUID already created!");
                    this._uuid = (new c.GUID).toString()
                }
            }, {
                key: "toCache",
                value: function () {
                    return {
                        uuid: this._uuid,
                        seq: this._lastSeq,
                        lastUpdate: this._lastUpdate,
                        moderators: this._moderators,
                        title: this._title,
                        createdAt: this._createdAt,
                        lastRead: this._lastRead,
                        distinct: this._distinct,
                        publicJoin: this._publicJoin,
                        participants: this._participants,
                        customData: this._customData
                    }
                }
            }, {
                key: "sendMessage",
                value: function (e, t) {
                    var n = new l.Message(e, t);
                    return n.sendTo(this), n
                }
            }, {
                key: "typing",
                value: function () {
                    var e = this;
                    return !this._debounceLock && (setTimeout(function () {
                        e._debounceLock = !1
                    }, 1e4), this._debounceLock = !0, d.MsgSignaling.get().sendPayload(u.MsgAction.typingMessage, {
                        conversation: this._uuid
                    }), !0)
                }
            }, {
                key: "markAsRead",
                value: function (e) {
                    d.MsgSignaling.get().sendPayload(u.MsgAction.isRead, {
                        conversation: this._uuid,
                        seq: e
                    }), this._lastRead = e
                }
            }, {
                key: "markAsDelivered",
                value: function (e) {
                    d.MsgSignaling.get().sendPayload(u.MsgAction.isDelivered, {
                        conversation: this._uuid,
                        seq: e
                    })
                }
            }, {
                key: "remove",
                value: function () {
                    g.ConversationManager.get().removeConversation(this._uuid)
                }
            }, {
                key: "update",
                value: function () {
                    g.ConversationManager.get().editConversation(this)
                }
            }, {
                key: "setTitle",
                value: function (e) {
                    this._title = e, g.ConversationManager.get().editConversation(this)
                }
            }, {
                key: "setPublicJoin",
                value: function (e) {
                    this._publicJoin = e, g.ConversationManager.get().editConversation(this)
                }
            }, {
                key: "setDistinct",
                value: function (e) {
                    this._distinct = e, g.ConversationManager.get().editConversation(this)
                }
            }, {
                key: "setCustomData",
                value: function (e) {
                    this._customData = e, g.ConversationManager.get().editConversation(this)
                }
            }, {
                key: "addParticipants",
                value: function (e) {
                    var t = this;
                    return new Promise(function (n, r) {
                        0 == e.length && r(), d.MsgSignaling.get().sendPayload(u.MsgAction.addParticipants, {
                            uuid: t._uuid,
                            participants: t._prepareParticipants(e)
                        }), p.Messenger.getInstance()._registerPromise(f.default.MessengerEvents.EditConversation, n, r)
                    })
                }
            }, {
                key: "editParticipants",
                value: function (e) {
                    var t = this;
                    return new Promise(function (n, r) {
                        0 == e.length && r(), d.MsgSignaling.get().sendPayload(u.MsgAction.editParticipants, {
                            uuid: t._uuid,
                            participants: t._prepareParticipants(e)
                        }), p.Messenger.getInstance()._registerPromise(f.default.MessengerEvents.EditConversation, n, r)
                    })
                }
            }, {
                key: "removeParticipants",
                value: function (e) {
                    var t = this;
                    return new Promise(function (n, r) {
                        0 == e.length && r(), d.MsgSignaling.get().sendPayload(u.MsgAction.removeParticipants, {
                            uuid: t._uuid,
                            participants: e.map(function (e) {
                                if (void 0 !== e.userId) return e.userId
                            })
                        }), p.Messenger.getInstance()._registerPromise(f.default.MessengerEvents.EditConversation, n, r)
                    })
                }
            }, {
                key: "addModerators",
                value: function (e) {
                    var t = this;
                    return new Promise(function (n, r) {
                        0 == e.length && r(), d.MsgSignaling.get().sendPayload(u.MsgAction.addModerators, {
                            uuid: t._uuid,
                            moderators: e
                        }), p.Messenger.getInstance()._registerPromise(f.default.MessengerEvents.EditConversation, n, r)
                    })
                }
            }, {
                key: "removeModerators",
                value: function (e) {
                    var t = this;
                    return new Promise(function (n, r) {
                        0 == e.length && r(), d.MsgSignaling.get().sendPayload(u.MsgAction.removeModerators, {
                            uuid: t._uuid,
                            moderators: e
                        }), p.Messenger.getInstance()._registerPromise(f.default.MessengerEvents.EditConversation, n, r)
                    })
                }
            }, {
                key: "retransmitEvents",
                value: function (e, t) {
                    var n = this;
                    return new Promise(function (r, o) {
                        e |= 0, t |= 0;
                        var i = function e(t) {
                            f.default.Messenger.getInstance().off(f.default.MessengerEvents.RetransmitEvents, e), f.default.Messenger.getInstance().off(f.default.MessengerEvents.Error, a), r(t)
                        },
                            a = function e(t) {
                                t.messengerAction == f.default.MessengerAction.getConversation && (f.default.Messenger.getInstance().off(f.default.MessengerEvents.RetransmitEvents, i), f.default.Messenger.getInstance().off(f.default.MessengerEvents.Error, e), o(t))
                            };
                        f.default.Messenger.getInstance().on(f.default.MessengerEvents.RetransmitEvents, i), f.default.Messenger.getInstance().on(f.default.MessengerEvents.Error, a), d.MsgSignaling.get().sendPayload(u.MsgAction.retransmitEvents, {
                            uuid: n._uuid,
                            eventsFrom: e,
                            eventsTo: t
                        })
                    })
                }
            }, {
                key: "updateSeq",
                value: function (e) {
                    e > this._lastSeq && (this._lastSeq = e), this._lastUpdate = Date.now() / 1e3 | 0
                }
            }, {
                key: "_traceName",
                value: function () {
                    return "Conversation"
                }
            }, {
                key: "_prepareParticipants",
                value: function (e) {
                    var t = [],
                        n = !0,
                        r = !1,
                        o = void 0;
                    try {
                        for (var i, a = e[Symbol.iterator](); !(n = (i = a.next()).done); n = !0) {
                            var s = i.value;
                            if (void 0 !== s.userId) {
                                var c = {
                                    user_id: g.ConversationManager.extractUserName(s.userId)
                                };
                                c.can_write = void 0 === s.canWrite || s.canWrite, c.can_manage_participants = void 0 !== s.canManageParticipants && s.canManageParticipants, t.push(c)
                            }
                        }
                    } catch (e) {
                        r = !0, o = e
                    } finally {
                        try {
                            !n && a.return && a.return()
                        } finally {
                            if (r) throw o
                        }
                    }
                    return t
                }
            }, {
                key: "uuid",
                get: function () {
                    return this._uuid
                }
            }, {
                key: "moderators",
                get: function () {
                    return this._moderators
                }
            }, {
                key: "createdAt",
                get: function () {
                    return this._createdAt
                }
            }, {
                key: "title",
                get: function () {
                    return this._title
                },
                set: function (e) {
                    this._title = e
                }
            }, {
                key: "distinct",
                get: function () {
                    return this._distinct
                },
                set: function (e) {
                    this._distinct = e
                }
            }, {
                key: "publicJoin",
                get: function () {
                    return this._publicJoin
                },
                set: function (e) {
                    this._publicJoin = e
                }
            }, {
                key: "participants",
                get: function () {
                    return this._participants
                }
            }, {
                key: "customData",
                get: function () {
                    return this._customData
                },
                set: function (e) {
                    this._customData = e
                }
            }, {
                key: "lastSeq",
                get: function () {
                    return this._lastSeq
                }
            }, {
                key: "lastUpdate",
                get: function () {
                    return this._lastUpdate
                }
            }, {
                key: "lastRead",
                get: function () {
                    return this._lastRead
                }
            }, {
                key: "uberConversation",
                get: function () {
                    return this._uberConversation
                }
            }], [{
                key: "_createFromBus",
                value: function (t, n) {
                    var r = new e([]);
                    return r._lastSeq = n, r._uuid = t.uuid, r._title = t.title, r._moderators = t.moderators, r._createdAt = t.created_at, r._lastRead = t.last_read, r._distinct = t.distinct, r._publicJoin = t.enable_public_join, r._uberConversation = t.uber_conversation, t.participants && (r._participants = t.participants.map(function (e) {
                        return {
                            userId: e.user_id,
                            canWrite: e.can_write,
                            canManageParticipants: e.can_manage_participants
                        }
                    })), t.custom_data && (r._customData = t.custom_data), r._lastUpdate = t.last_update, r
                }
            }, {
                key: "createFromCache",
                value: function (t) {
                    var n = new e([]);
                    return n._uuid = t.uuid, n._lastSeq = t.seq, n._lastUpdate = t.lastUpdate, n._title = t.title, n._moderators = t.moderators, n._createdAt = t.createdAt, n._lastRead = t.lastRead, n._distinct = t.distinct, n._publicJoin = t.publicJoin, n._participants = t.participants, n._customData = t.customData, n._uberConversation = t.uberConversation, n
                }
            }]), e
        }();
    a([s.LogManager.d_trace(s.LogCategory.MESSAGING)], v.prototype, "_getPayload", null), a([s.LogManager.d_trace(s.LogCategory.MESSAGING)], v.prototype, "_getSimplePayload", null), a([s.LogManager.d_trace(s.LogCategory.MESSAGING)], v.prototype, "_createUUID", null), a([s.LogManager.d_trace(s.LogCategory.MESSAGING)], v.prototype, "toCache", null), a([s.LogManager.d_trace(s.LogCategory.MESSAGING)], v.prototype, "markAsRead", null), a([s.LogManager.d_trace(s.LogCategory.MESSAGING)], v.prototype, "markAsDelivered", null), a([s.LogManager.d_trace(s.LogCategory.MESSAGING)], v.prototype, "remove", null), a([s.LogManager.d_trace(s.LogCategory.MESSAGING)], v.prototype, "update", null), a([s.LogManager.d_trace(s.LogCategory.MESSAGING)], v.prototype, "addParticipants", null), a([s.LogManager.d_trace(s.LogCategory.MESSAGING)], v.prototype, "editParticipants", null), a([s.LogManager.d_trace(s.LogCategory.MESSAGING)], v.prototype, "removeParticipants", null), a([s.LogManager.d_trace(s.LogCategory.MESSAGING)], v.prototype, "addModerators", null), a([s.LogManager.d_trace(s.LogCategory.MESSAGING)], v.prototype, "removeModerators", null), a([s.LogManager.d_trace(s.LogCategory.MESSAGING)], v.prototype, "retransmitEvents", null), a([s.LogManager.d_trace(s.LogCategory.MESSAGING)], v.prototype, "updateSeq", null), a([s.LogManager.d_trace(s.LogCategory.MESSAGING)], v.prototype, "_prepareParticipants", null), a([s.LogManager.d_trace(s.LogCategory.MESSAGING)], v, "_createFromBus", null), a([s.LogManager.d_trace(s.LogCategory.MESSAGING)], v, "createFromCache", null), t.Conversation = v
}, function (e, t, n) {
    "use strict";

    function r(e, t) {
        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }
    var o = function () {
        function e(e, t) {
            for (var n = 0; n < t.length; n++) {
                var r = t[n];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
            }
        }
        return function (t, n, r) {
            return n && e(t.prototype, n), r && e(t, r), t
        }
    }(),
        i = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
            return typeof e
        } : function (e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        },
        a = function (e, t, n, r) {
            var o, a = arguments.length,
                s = a < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
            if ("object" === ("undefined" == typeof Reflect ? "undefined" : i(Reflect)) && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r);
            else
                for (var c = e.length - 1; c >= 0; c--)(o = e[c]) && (s = (a < 3 ? o(s) : a > 3 ? o(t, n, s) : o(t, n)) || s);
            return a > 3 && s && Object.defineProperty(t, n, s), s
        };
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var s = n(14),
        c = n(40),
        l = n(15),
        u = n(0),
        d = n(10),
        g = n(30),
        f = n(23),
        p = n(16),
        v = function () {
            function e() {
                var t = this;
                if (r(this, e), e.instance) throw new Error("Error - use Client.getIM()");
                this.eventListeners = {}, this.signalling = s.MsgSignaling.get(), this.cm = c.ConversationManager.get(), this.signalling.addEventListener(l.MsgEvent.onError, function (e) {
                    t._dispatchEvent(p.default.MessengerEvents.Error, e)
                }), c.ConversationManager.get(), this.signalling.addEventListener(l.MsgEvent.onEditUser, function (e) {
                    var n = e.object,
                        r = {
                            user: {
                                customData: n.custom_data,
                                privateCustomData: n.private_custom_data,
                                userId: n.user_id
                            },
                            userId: e.user_id,
                            seq: e.seq,
                            onIncomingEvent: e.on_incoming_event
                        };
                    t._dispatchEvent(p.default.MessengerEvents.EditUser, r)
                }), this.signalling.addEventListener(l.MsgEvent.onGetUser, function (e) {
                    var n = e.object,
                        r = {
                            user: {
                                conversationsList: n.conversations_list,
                                leaveConversationList: n.leave_conversation_list,
                                customData: n.custom_data,
                                privateCustomData: n.private_custom_data,
                                userId: n.user_id
                            },
                            userId: e.user_id,
                            seq: e.seq,
                            onIncomingEvent: e.on_incoming_event
                        };
                    t._dispatchEvent(p.default.MessengerEvents.GetUser, r)
                }), this.signalling.addEventListener(l.MsgEvent.onsubscribe, function (e) {
                    t._dispatchEvent(p.default.MessengerEvents.Subscribe, {
                        users: e.users
                    })
                }), this.signalling.addEventListener(l.MsgEvent.onUnSubscribe, function (e) {
                    t._dispatchEvent(p.default.MessengerEvents.Unsubscribe, {
                        users: e.users
                    })
                }), this.signalling.addEventListener(l.MsgEvent.onSetStatus, function (e) {
                    t._dispatchEvent(p.default.MessengerEvents.SetStatus, {
                        user: {
                            userId: e.object.user_id,
                            online: e.object.online,
                            timestamp: e.object.timestamp
                        },
                        userId: e.user_id,
                        seq: e.seq,
                        onIncomingEvent: e.on_incoming_event
                    })
                }), this.awaitPromiseList = []
            }
            return o(e, [{
                key: "createConversation",
                value: function (e, t, n, r, o, i) {
                    this.cm.createConversation(e, t, n, r, o, i)
                }
            }, {
                key: "getConversation",
                value: function (e) {
                    this.cm.getConversation(e)
                }
            }, {
                key: "getConversations",
                value: function (e) {
                    return e.length > 30 ? void u.LogManager.get().writeMessage(u.LogCategory.MESSAGING, "Rate limit", u.LogLevel.ERROR, "you can get maximum 30 conversation in one getConversations") : this.cm.getConversations(e)
                }
            }, {
                key: "getRawConversations",
                value: function (e) {
                    return this.cm.getConversations(e)
                }
            }, {
                key: "removeConversation",
                value: function (e) {
                    this.cm.removeConversation(e)
                }
            }, {
                key: "joinConversation",
                value: function (e) {
                    this.signalling.sendPayload(l.MsgAction.joinConversation, {
                        uuid: e
                    })
                }
            }, {
                key: "leaveConversation",
                value: function (e) {
                    this.signalling.sendPayload(l.MsgAction.leaveConversation, {
                        uuid: e
                    })
                }
            }, {
                key: "getUser",
                value: function (e) {
                    var t = this;
                    return new Promise(function (n, r) {
                        var o = function r(o) {
                            o.user.userId === e && (n(o), t.off(p.default.MessengerEvents.GetUser, r), t.off(p.default.MessengerEvents.Error, i))
                        },
                            i = function e(n) {
                                n.messengerAction == p.default.MessengerAction.getUser && (r(n), t.off(p.default.MessengerEvents.GetUser, o), t.off(p.default.MessengerEvents.Error, e))
                            };
                        t.on(p.default.MessengerEvents.GetUser, o), t.on(p.default.MessengerEvents.Error, i), t.signalling.sendPayload(l.MsgAction.getUser, {
                            user_id: e
                        })
                    })
                }
            }, {
                key: "getMe",
                value: function () {
                    return c.ConversationManager.extractUserName(d.Authenticator.get().username())
                }
            }, {
                key: "editUser",
                value: function (e, t) {
                    var n = {
                        user_id: c.ConversationManager.extractUserName(d.Authenticator.get().username())
                    };
                    e && (n.custom_data = e), t && (n.private_custom_data = t), this.signalling.sendPayload(l.MsgAction.editUser, n)
                }
            }, {
                key: "getUsers",
                value: function (e) {
                    this.signalling.sendPayload(l.MsgAction.getUsers, {
                        users: e
                    })
                }
            }, {
                key: "addEventListener",
                value: function (e, t) {
                    void 0 === this.eventListeners[e] && (this.eventListeners[e] = []), this.eventListeners[e].push(t)
                }
            }, {
                key: "removeEventListener",
                value: function (e, t) {
                    if (void 0 !== this.eventListeners[e])
                        if ("function" == typeof t) {
                            for (var n = 0; n < this.eventListeners[e].length; n++)
                                if (this.eventListeners[e][n] === t) {
                                    this.eventListeners[e].splice(n, 1);
                                    break
                                }
                        } else this.eventListeners[e] = []
                }
            }, {
                key: "_dispatchEvent",
                value: function (e, t) {
                    if (t.name = p.default.MessengerEvents[e], void 0 !== this.eventListeners[e] && this.eventListeners[e].forEach(function (e) {
                        "function" == typeof e && e(t)
                    }), void 0 !== this.awaitPromiseList[e] && 0 != this.awaitPromiseList[e].length) {
                        var n = this.awaitPromiseList[e].splice(0, 1);
                        n.resolve(t), window.clearTimeout(n.expire)
                    }
                }
            }, {
                key: "on",
                value: function (e, t) {
                    this.addEventListener(e, t)
                }
            }, {
                key: "off",
                value: function (e, t) {
                    this.removeEventListener(e, t)
                }
            }, {
                key: "_registerPromise",
                value: function (e, t, n) {
                    void 0 === this.awaitPromiseList[e] && (this.awaitPromiseList[e] = []), this.awaitPromiseList[e].push({
                        resolve: t,
                        reject: n,
                        expire: setTimeout(function () {
                            n()
                        }, 2e4)
                    })
                }
            }, {
                key: "createConversationFromCache",
                value: function (e) {
                    return void 0 === e ? null : g.Conversation.createFromCache(e)
                }
            }, {
                key: "createMessageFromCache",
                value: function (e) {
                    return void 0 === e ? null : f.Message.createFromCache(e)
                }
            }, {
                key: "subscribe",
                value: function (e) {
                    this.signalling.sendPayload(l.MsgAction.subscribe, {
                        users: e
                    })
                }
            }, {
                key: "unsubscribe",
                value: function (e) {
                    this.signalling.sendPayload(l.MsgAction.unsubscribe, {
                        users: e
                    })
                }
            }, {
                key: "setPresence",
                value: function (e) {
                    this.setStatus(e)
                }
            }, {
                key: "setStatus",
                value: function (e) {
                    this.signalling.sendPayload(l.MsgAction.setStatus, {
                        online: e
                    })
                }
            }, {
                key: "_traceName",
                value: function () {
                    return "Messenger"
                }
            }], [{
                key: "getInstance",
                value: function () {
                    return e.instance = e.instance || new e, e.instance
                }
            }]), e
        }();
    a([u.LogManager.d_trace(u.LogCategory.MESSAGING)], v.prototype, "createConversation", null), a([u.LogManager.d_trace(u.LogCategory.MESSAGING)], v.prototype, "getConversation", null), a([u.LogManager.d_trace(u.LogCategory.MESSAGING)], v.prototype, "getConversations", null), a([u.LogManager.d_trace(u.LogCategory.MESSAGING)], v.prototype, "getRawConversations", null), a([u.LogManager.d_trace(u.LogCategory.MESSAGING)], v.prototype, "removeConversation", null), a([u.LogManager.d_trace(u.LogCategory.MESSAGING)], v.prototype, "joinConversation", null), a([u.LogManager.d_trace(u.LogCategory.MESSAGING)], v.prototype, "leaveConversation", null), a([u.LogManager.d_trace(u.LogCategory.MESSAGING)], v.prototype, "getUser", null), a([u.LogManager.d_trace(u.LogCategory.MESSAGING)], v.prototype, "getMe", null), a([u.LogManager.d_trace(u.LogCategory.MESSAGING)], v.prototype, "editUser", null), a([u.LogManager.d_trace(u.LogCategory.MESSAGING)], v.prototype, "getUsers", null), a([u.LogManager.d_trace(u.LogCategory.MESSAGING)], v.prototype, "addEventListener", null), a([u.LogManager.d_trace(u.LogCategory.MESSAGING)], v.prototype, "removeEventListener", null), a([u.LogManager.d_trace(u.LogCategory.MESSAGING)], v.prototype, "_dispatchEvent", null), a([u.LogManager.d_trace(u.LogCategory.MESSAGING)], v.prototype, "on", null), a([u.LogManager.d_trace(u.LogCategory.MESSAGING)], v.prototype, "off", null), a([u.LogManager.d_trace(u.LogCategory.MESSAGING)], v.prototype, "_registerPromise", null), a([u.LogManager.d_trace(u.LogCategory.MESSAGING)], v.prototype, "createConversationFromCache", null), a([u.LogManager.d_trace(u.LogCategory.MESSAGING)], v.prototype, "createMessageFromCache", null), a([u.LogManager.d_trace(u.LogCategory.MESSAGING)], v.prototype, "subscribe", null), a([u.LogManager.d_trace(u.LogCategory.MESSAGING)], v.prototype, "unsubscribe", null), a([u.LogManager.d_trace(u.LogCategory.MESSAGING)], v.prototype, "setPresence", null), a([u.LogManager.d_trace(u.LogCategory.MESSAGING)], v.prototype, "setStatus", null), a([u.LogManager.d_trace(u.LogCategory.MESSAGING)], v, "getInstance", null), t.Messenger = v
}, function (e, t, n) {
    "use strict";
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    ! function (e) {
        e[e.Offline = "OFFLINE"] = "Offline", e[e.Online = "ONLINE"] = "Online", e[e.Ready = "READY"] = "Ready", e[e.InService = "IN_SERVICE"] = "InService", e[e.AfterService = "AFTER_SERVICE"] = "AfterService", e[e.Timeout = "TIMEOUT"] = "Timeout", e[e.DND = "DND"] = "DND"
    }(t.OperatorACDStatuses || (t.OperatorACDStatuses = {}))
}, function (e, t, n) {
    "use strict";

    function r(e, t) {
        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }
    var o = function () {
        function e(e, t) {
            for (var n = 0; n < t.length; n++) {
                var r = t[n];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
            }
        }
        return function (t, n, r) {
            return n && e(t.prototype, n), r && e(t, r), t
        }
    }(),
        i = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
            return typeof e
        } : function (e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        },
        a = function (e, t, n, r) {
            var o, a = arguments.length,
                s = a < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
            if ("object" === ("undefined" == typeof Reflect ? "undefined" : i(Reflect)) && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r);
            else
                for (var c = e.length - 1; c >= 0; c--)(o = e[c]) && (s = (a < 3 ? o(s) : a > 3 ? o(t, n, s) : o(t, n)) || s);
            return a > 3 && s && Object.defineProperty(t, n, s), s
        };
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var s = n(0),
        c = n(26),
        l = function () {
            function e() {
                if (r(this, e), void 0 !== e.instance) throw new Error("Error - use StreamManager.get()");
                navigator.mediaDevices.getSupportedConstraints ? this._supportedConstraints = navigator.mediaDevices.getSupportedConstraints() : this._supportedConstraints = {}, this.__defaultParams = {}, this._lastAudioInputDevices = [], this._lastAudioOutputDevices = [], this._callParams = {}
            }
            return o(e, [{
                key: "getAudioContext",
                value: function () {
                    if (this.audioContext) return this.audioContext;
                    if (void 0 !== window.AudioContext || void 0 !== window.webkitAudioContext) {
                        window.AudioContext = window.AudioContext || window.webkitAudioContext;
                        try {
                            return this.audioContext = new AudioContext, this.audioContext
                        } catch (e) {
                            return this.audioContext = null, null
                        }
                    }
                }
            }, {
                key: "prepareAudioContext",
                value: function () {
                    this.getAudioContext()
                }
            }, {
                key: "getInputDevices",
                value: function () {
                    var e = this;
                    return navigator.mediaDevices.enumerateDevices().then(function (t) {
                        return e._lastAudioInputDevices = t.map(function (e) {
                            if ("audio" === e.kind || "audioinput" === e.kind) return {
                                id: e.deviceId,
                                name: e.label,
                                group: e.groupId
                            }
                        }).filter(function (e) {
                            return void 0 !== e
                        }), e._lastAudioInputDevices
                    })
                }
            }, {
                key: "getOutputDevices",
                value: function () {
                    var e = this;
                    return navigator.mediaDevices.enumerateDevices().then(function (t) {
                        return e._lastAudioOutputDevices = t.map(function (e) {
                            if ("audiooutput" === e.kind) return {
                                id: e.deviceId,
                                name: e.label,
                                group: e.groupId
                            }
                        }).filter(function (e) {
                            return void 0 !== e
                        }), e._lastAudioOutputDevices
                    })
                }
            }, {
                key: "getDefaultAudioSettings",
                value: function () {
                    return this.__defaultParams
                }
            }, {
                key: "setDefaultAudioSettings",
                value: function (e) {
                    this.__defaultParams = e
                }
            }, {
                key: "setCallAudioSettings",
                value: function (e, t) {
                    var n = this;
                    return new Promise(function (r, o) {
                        n._callParams[e.id()] === t && r();
                        var i = n._callParams[e.id()].outputId !== t.outputId,
                            a = n._callParams[e.id()].inputId !== t.inputId || n._callParams[e.id()].noiseSuppression !== t.noiseSuppression || n._callParams[e.id()].echoCancellation !== t.echoCancellation || n._callParams[e.id()].disableAudio !== t.disableAudio;
                        n._callParams[e.id()] = t, i && e.getEndpoints().forEach(function (e) {
                            e.mediaRenderers.forEach(function (e) {
                                return e.useAudioOutput(t.outputId)
                            })
                        }), a ? c.StreamManager.get().updateCallStream(e).then(function () {
                            r()
                        }).catch(function (e) {
                            o(e)
                        }) : r()
                    })
                }
            }, {
                key: "getCallAudioSettings",
                value: function (e) {
                    return this._callParams[e.id()]
                }
            }, {
                key: "getCallConstraints",
                value: function (e) {
                    return this._callParams[e] ? this._getAudioConstraints(this._callParams[e]) : (this._callParams[e] = this.__defaultParams, this._getAudioConstraints(this.__defaultParams))
                }
            }, {
                key: "_getAudioConstraints",
                value: function (e) {
                    if (e.disableAudio) return !1;
                    var t = "ideal";
                    e.strict && (t = "exact");
                    var n = {};
                    return e.inputId && (this._lastAudioInputDevices && this._lastAudioInputDevices.some(function (t) {
                        return t.id === e.inputId
                    }) ? (n.deviceId = {}, n.deviceId[t] = e.inputId) : s.LogManager.get().writeMessage(s.LogCategory.USERMEDIA, "Warning:", s.LogLevel.WARNING, "There is no audio input device with id " + e.inputId)), e.echoCancellation && this._supportedConstraints.echoCancellation && (n.echoCancellation = e.echoCancellation), e.noiseSuppression && this._supportedConstraints.noiseSuppression && (n.noiseSuppression = e.echoCancellation), e.autoGainControl && this._supportedConstraints.autoGainControl && (n.autoGainControl = e.autoGainControl), !Object.keys(n) || n
                }
            }, {
                key: "_traceName",
                value: function () {
                    return "AudioDeviceManager"
                }
            }], [{
                key: "get",
                value: function () {
                    return void 0 === e.instance && (e.instance = new e), e.instance
                }
            }]), e
        }();
    a([s.LogManager.d_trace(s.LogCategory.HARDWARE)], l.prototype, "getInputDevices", null), a([s.LogManager.d_trace(s.LogCategory.HARDWARE)], l.prototype, "getOutputDevices", null), a([s.LogManager.d_trace(s.LogCategory.HARDWARE)], l.prototype, "getDefaultAudioSettings", null), a([s.LogManager.d_trace(s.LogCategory.HARDWARE)], l.prototype, "setDefaultAudioSettings", null), a([s.LogManager.d_trace(s.LogCategory.HARDWARE)], l.prototype, "setCallAudioSettings", null), a([s.LogManager.d_trace(s.LogCategory.HARDWARE)], l.prototype, "getCallAudioSettings", null), a([s.LogManager.d_trace(s.LogCategory.HARDWARE)], l.prototype, "getCallConstraints", null), a([s.LogManager.d_trace(s.LogCategory.HARDWARE)], l.prototype, "_getAudioConstraints", null), a([s.LogManager.d_trace(s.LogCategory.HARDWARE)], l, "get", null), t.AudioDeviceManager = l
}, function (e, t, n) {
    "use strict";

    function r(e, t) {
        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }
    var o = function () {
        function e(e, t) {
            for (var n = 0; n < t.length; n++) {
                var r = t[n];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
            }
        }
        return function (t, n, r) {
            return n && e(t.prototype, n), r && e(t, r), t
        }
    }(),
        i = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
            return typeof e
        } : function (e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        },
        a = function (e, t, n, r) {
            var o, a = arguments.length,
                s = a < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
            if ("object" === ("undefined" == typeof Reflect ? "undefined" : i(Reflect)) && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r);
            else
                for (var c = e.length - 1; c >= 0; c--)(o = e[c]) && (s = (a < 3 ? o(s) : a > 3 ? o(t, n, s) : o(t, n)) || s);
            return a > 3 && s && Object.defineProperty(t, n, s), s
        };
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var s = n(0),
        c = n(26),
        l = n(4),
        u = function () {
            function e() {
                if (r(this, e), void 0 !== e.instance) throw new Error("Error - use StreamManager.get()");
                navigator.mediaDevices.getSupportedConstraints ? this._supportedConstraints = navigator.mediaDevices.getSupportedConstraints() : this._supportedConstraints = {}, this._callParams = {}, this._lastCameraDevices = [], this.__defaultParams = {}
            }
            return o(e, [{
                key: "setDefaultVideoSettings",
                value: function (e) {
                    var t = this;
                    return new Promise(function (n, r) {
                        var o = t._validateCameraParams(e);
                        t.__defaultParams = o, n(null)
                    })
                }
            }, {
                key: "getDefaultVideoSettings",
                value: function () {
                    return this.__defaultParams
                }
            }, {
                key: "setCallVideoSettings",
                value: function (e, t) {
                    var n = this._validateCameraParams(t);
                    return this._callParams[e.id()] = n, new Promise(function (t, n) {
                        c.StreamManager.get().updateCallStream(e).then(function () {
                            t()
                        }).catch(function (e) {
                            n(e)
                        })
                    })
                }
            }, {
                key: "getCallVideoSettings",
                value: function (e) {
                    return this._callParams[e.id()]
                }
            }, {
                key: "getCallConstraints",
                value: function (e) {
                    return this._callParams[e] ? this._getVideoConstraints(this._callParams[e]) : (this._callParams[e] = this.__defaultParams, this._getVideoConstraints(this.__defaultParams))
                }
            }, {
                key: "getInputDevices",
                value: function () {
                    var e = this;
                    return navigator.mediaDevices.enumerateDevices().then(function (t) {
                        return e._lastCameraDevices = t.map(function (e) {
                            if ("video" === e.kind || "videoinput" === e.kind) return {
                                id: e.deviceId,
                                name: e.label,
                                group: e.groupId
                            }
                        }).filter(function (e) {
                            return void 0 !== e
                        }), e._lastCameraDevices
                    })
                }
            }, {
                key: "_getVideoConstraints",
                value: function (e) {
                    var t = {};
                    return e.cameraId ? this._lastCameraDevices && this._lastCameraDevices.some(function (t) {
                        return t.id === e.cameraId
                    }) ? (t.deviceId = {}, t.deviceId.ideal = e.cameraId) : s.LogManager.get().writeMessage(s.LogCategory.USERMEDIA, "Warning:", s.LogLevel.WARNING, "There is no video device with id " + e.cameraId) : void 0 !== e.facingMode && (!1 === e.facingMode ? t.facingMode = "environment" : t.facingMode = "user"), e.frameHeight && (t.height = {}, e.strict ? t.height.min = e.frameHeight : t.height.ideal = e.frameHeight), e.frameWidth && (t.width = {}, e.strict ? t.width.min = e.frameWidth : t.width.ideal = e.frameWidth), e.frameRate && e.frameRate > 0 && this._supportedConstraints.frameRate && (t.frameRate = e.frameRate + ""), !Object.keys(t) || t
                }
            }, {
                key: "_validateCameraParams",
                value: function (e) {
                    if (e.videoQuality) {
                        (e.frameHeight || e.frameWidth) && s.LogManager.get().writeMessage(s.LogCategory.USERMEDIA, "Warning:", s.LogLevel.WARNING, '"videoQuality" parameter detected. The "frameHeight" and the "frameWidth" params will be ignored');
                        var t = this._videoQualityToSize(e.videoQuality);
                        e.frameWidth = t.w, e.frameHeight = t.h
                    }
                    return e
                }
            }, {
                key: "_videoQualityToSize",
                value: function (e) {
                    switch (e) {
                        case l.Hardware.VideoQuality.VIDEO_QUALITY_HIGH:
                            return {
                                w: 1280,
                                h: 720
                            };
                        case l.Hardware.VideoQuality.VIDEO_QUALITY_MEDIUM:
                            return {
                                w: 640,
                                h: 480
                            };
                        case l.Hardware.VideoQuality.VIDEO_QUALITY_LOW:
                            return {
                                w: 320,
                                h: 240
                            };
                        case l.Hardware.VideoQuality.VIDEO_SIZE_QQVGA:
                            return {
                                w: 160,
                                h: 120
                            };
                        case l.Hardware.VideoQuality.VIDEO_SIZE_QCIF:
                            return {
                                w: 176,
                                h: 144
                            };
                        case l.Hardware.VideoQuality.VIDEO_SIZE_QVGA:
                            return {
                                w: 320,
                                h: 240
                            };
                        case l.Hardware.VideoQuality.VIDEO_SIZE_CIF:
                            return {
                                w: 352,
                                h: 288
                            };
                        case l.Hardware.VideoQuality.VIDEO_SIZE_nHD:
                            return {
                                w: 640,
                                h: 360
                            };
                        case l.Hardware.VideoQuality.VIDEO_SIZE_VGA:
                            return {
                                w: 640,
                                h: 480
                            };
                        case l.Hardware.VideoQuality.VIDEO_SIZE_SVGA:
                            return {
                                w: 800,
                                h: 600
                            };
                        case l.Hardware.VideoQuality.VIDEO_SIZE_HD:
                            return {
                                w: 1280,
                                h: 720
                            };
                        case l.Hardware.VideoQuality.VIDEO_SIZE_UXGA:
                            return {
                                w: 1600,
                                h: 1200
                            };
                        case l.Hardware.VideoQuality.VIDEO_SIZE_FHD:
                            return {
                                w: 1920,
                                h: 1080
                            };
                        case l.Hardware.VideoQuality.VIDEO_SIZE_UHD:
                            return {
                                w: 3840,
                                h: 2160
                            };
                        default:
                            return {
                                w: 320,
                                h: 240
                            }
                    }
                }
            }, {
                key: "testResolutions",
                value: function (e) {
                    var t = this,
                        n = [l.Hardware.VideoQuality.VIDEO_SIZE_QQVGA, l.Hardware.VideoQuality.VIDEO_SIZE_QCIF, l.Hardware.VideoQuality.VIDEO_SIZE_QVGA, l.Hardware.VideoQuality.VIDEO_SIZE_CIF, l.Hardware.VideoQuality.VIDEO_SIZE_nHD, l.Hardware.VideoQuality.VIDEO_SIZE_VGA, l.Hardware.VideoQuality.VIDEO_SIZE_SVGA, l.Hardware.VideoQuality.VIDEO_SIZE_HD, l.Hardware.VideoQuality.VIDEO_SIZE_UXGA, l.Hardware.VideoQuality.VIDEO_SIZE_FHD, l.Hardware.VideoQuality.VIDEO_SIZE_UHD];
                    return this._lastResolutionTestResult ? new Promise(function (e, n) {
                        e(t._lastResolutionTestResult)
                    }) : this._testResolutions(n, {}, e)
                }
            }, {
                key: "_testResolutions",
                value: function (e, t, n) {
                    var r = this;
                    if (e.length) {
                        var o = e.shift(),
                            i = {
                                strict: !0
                            },
                            a = this._videoQualityToSize(o);
                        i.frameWidth = a.w, i.frameHeight = a.h, n && (i.cameraId = n);
                        var s = {
                            video: this._getVideoConstraints(i)
                        };
                        return navigator.mediaDevices.getUserMedia(s).then(function (i) {
                            return i.getTracks().forEach(function (e) {
                                return e.stop()
                            }), t[l.Hardware.VideoQuality[o]] = !0, r._testResolutions(e, t, n)
                        }, function (i) {
                            return t[l.Hardware.VideoQuality[o]] = !1, r._testResolutions(e, t, n)
                        })
                    }
                    return this._lastResolutionTestResult = t, t
                }
            }, {
                key: "loadResolutionTestResult",
                value: function (e) {
                    return !![l.Hardware.VideoQuality.VIDEO_SIZE_QQVGA, l.Hardware.VideoQuality.VIDEO_SIZE_QCIF, l.Hardware.VideoQuality.VIDEO_SIZE_QVGA, l.Hardware.VideoQuality.VIDEO_SIZE_CIF, l.Hardware.VideoQuality.VIDEO_SIZE_nHD, l.Hardware.VideoQuality.VIDEO_SIZE_VGA, l.Hardware.VideoQuality.VIDEO_SIZE_SVGA, l.Hardware.VideoQuality.VIDEO_SIZE_HD, l.Hardware.VideoQuality.VIDEO_SIZE_UXGA, l.Hardware.VideoQuality.VIDEO_SIZE_FHD, l.Hardware.VideoQuality.VIDEO_SIZE_UHD].every(function (t) {
                        return void 0 !== e[l.Hardware.VideoQuality[t]]
                    }) && (this._lastResolutionTestResult = e, !0)
                }
            }, {
                key: "_traceName",
                value: function () {
                    return "CameraManager"
                }
            }], [{
                key: "get",
                value: function () {
                    return void 0 === e.instance && (e.instance = new e), e.instance
                }
            }, {
                key: "legacyParamConverter",
                value: function (e) {
                    var t = {
                        videoQuality: l.Hardware.VideoQuality.VIDEO_QUALITY_MEDIUM
                    };
                    return e.width && ("string" == typeof e.width || "number" == typeof e.width ? (delete t.videoQuality, t.frameWidth = e.width) : "string" == typeof e.width.exact || "number" == typeof e.width.exact ? (delete t.videoQuality, t.frameWidth = e.width.exact, t.strict = !0) : "string" == typeof e.width.min || "number" == typeof e.width.min ? (delete t.videoQuality, t.frameWidth = e.width.min) : "string" == typeof e.width.max || "number" == typeof e.width.max ? (delete t.videoQuality, t.frameWidth = e.width.max) : "string" != typeof e.width.ideal && "number" != typeof e.width.ideal || (delete t.videoQuality, t.frameWidth = e.width.ideal)), e.height && ("string" == typeof e.height || "number" == typeof e.height ? (delete t.videoQuality, t.frameHeight = e.height) : "string" == typeof e.height.exact || "number" == typeof e.height.exact ? (delete t.videoQuality, t.frameHeight = e.height.exact, t.strict = !0) : "string" == typeof e.height.min || "number" == typeof e.height.min ? (delete t.videoQuality, t.frameHeight = e.height.min) : "string" == typeof e.height.max || "number" == typeof e.height.max ? (delete t.videoQuality, t.frameHeight = e.height.max) : "string" != typeof e.height.ideal && "number" != typeof e.height.ideal || (delete t.videoQuality, t.frameHeight = e.height.ideal)), t
                }
            }]), e
        }();
    a([s.LogManager.d_trace(s.LogCategory.HARDWARE)], u.prototype, "setDefaultVideoSettings", null), a([s.LogManager.d_trace(s.LogCategory.HARDWARE)], u.prototype, "getDefaultVideoSettings", null), a([s.LogManager.d_trace(s.LogCategory.HARDWARE)], u.prototype, "setCallVideoSettings", null), a([s.LogManager.d_trace(s.LogCategory.HARDWARE)], u.prototype, "getCallVideoSettings", null), a([s.LogManager.d_trace(s.LogCategory.HARDWARE)], u.prototype, "getCallConstraints", null), a([s.LogManager.d_trace(s.LogCategory.HARDWARE)], u.prototype, "getInputDevices", null), a([s.LogManager.d_trace(s.LogCategory.HARDWARE)], u.prototype, "_getVideoConstraints", null), a([s.LogManager.d_trace(s.LogCategory.HARDWARE)], u.prototype, "_validateCameraParams", null), a([s.LogManager.d_trace(s.LogCategory.HARDWARE)], u, "get", null), a([s.LogManager.d_trace(s.LogCategory.HARDWARE)], u, "legacyParamConverter", null), t.CameraManager = u
}, function (e, t, n) {
    "use strict";

    function r(e, t) {
        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }
    var o = function () {
        function e(e, t) {
            for (var n = 0; n < t.length; n++) {
                var r = t[n];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
            }
        }
        return function (t, n, r) {
            return n && e(t.prototype, n), r && e(t, r), t
        }
    }(),
        i = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
            return typeof e
        } : function (e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        },
        a = function (e, t, n, r) {
            var o, a = arguments.length,
                s = a < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
            if ("object" === ("undefined" == typeof Reflect ? "undefined" : i(Reflect)) && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r);
            else
                for (var c = e.length - 1; c >= 0; c--)(o = e[c]) && (s = (a < 3 ? o(s) : a > 3 ? o(t, n, s) : o(t, n)) || s);
            return a > 3 && s && Object.defineProperty(t, n, s), s
        };
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var s = n(8),
        c = n(1),
        l = n(0),
        u = n(24),
        d = n(20),
        g = function () {
            function e(t, n) {
                var o = arguments.length > 2 && void 0 !== arguments[2] && arguments[2],
                    i = this,
                    a = arguments.length > 3 && void 0 !== arguments[3] && arguments[3],
                    c = arguments[4];
                r(this, e), this.stream = t, this.kind = n, this.placeOnDom = o, this.isLocal = a, this.isRemoving = !1, void 0 === this.kind && (this.kind = t.getVideoTracks().length ? "video" : "audio"), this._id = u.Utils.generateUUID(), this._logger = new l.Logger(l.LogCategory.USERMEDIA, "MediaRenderer", l.LogManager.get()), this.stream.getTracks().forEach(function (e) {
                    e.onended = function () {
                        i.checkStreamActive(t)
                    }
                }), this.stream.getTracks().length && (this.element = document.createElement("sharing" === this.kind ? "video" : this.kind), this.element.autoplay = !0, this.isLocal && (this.element.muted = !0), this.element.setAttribute("playsinline", null), c ? document.getElementById("voximplantlocalvideo") ? this.element.id = this.stream.getTracks()[0].id : this.element.id = "voximplantlocalvideo" : this.element.id = this.stream.getTracks()[0].id, "audio" !== n && (this.element.width = 400, this.element.height = 300), s.default.attachMedia(this.stream, this.element), this.placeOnDom && this.renderDefault())
            }
            return o(e, [{
                key: "checkStreamActive",
                value: function (e) {
                    e.getTracks().some(function (e) {
                        return "live" === e.readyState
                    }) || this.clear()
                }
            }, {
                key: "renderDefault",
                value: function () {
                    var e = this.isLocal ? c.Client.getInstance().localVideoContainerId : c.Client.getInstance().remoteVideoContainerId,
                        t = document.getElementById(e);
                    this.render(t)
                }
            }, {
                key: "render",
                value: function (e) {
                    var t = this,
                        n = e || document.body;
                    void 0 !== this.element.parentElement && null !== this.element.parentElement ? this.element.parentElement && this.element.parentElement.removeChild(this.element) : this.element.parentNode && this.element.parentNode && this.element.parentNode.removeChild(this.element), n.appendChild(this.element), this.element.play().then(function () { }, function (e) {
                        setTimeout(function () {
                            t.element.play().then(function () { }, function (e) {
                                t._logger.warning("Can't start playing MediaRenderer ID:" + t._id)
                            })
                        }, 400)
                    })
                }
            }, {
                key: "clear",
                value: function () {
                    var e = this;
                    if (this.isRemoving) return void this._logger.trace("MediaRendered ID:" + this._id + " already removing. Ignored.");
                    this.isRemoving = !0;
                    var t = d.EndpointManager.get().getEndPointByMediaRenderer(this);
                    t && (t.mediaRenderers = t.mediaRenderers.filter(function (t) {
                        return t.stream.id !== e.stream.id
                    })), this.onBeforeDestroy && this.onBeforeDestroy(), this.element && (s.default.detachMedia(this.element), this.element.id = "", void 0 !== this.element.parentElement && null !== this.element.parentElement ? this.element.parentElement && this.element.parentElement.removeChild(this.element) : this.element.parentNode && this.element.parentNode && this.element.parentNode.removeChild(this.element)), this.onDestroy && this.onDestroy()
                }
            }, {
                key: "setVolume",
                value: function (e) {
                    this.element && (this.element.volume = e)
                }
            }, {
                key: "useAudioOutput",
                value: function (e) {
                    try {
                        this.element.setSinkId(e)
                    } catch (e) {
                        this._logger.warning("Set audio output is impossible. Browser not support this option.")
                    }
                }
            }, {
                key: "id",
                get: function () {
                    return this._id
                }
            }]), e
        }();
    a([l.LogManager.d_trace(l.LogCategory.USERMEDIA)], g.prototype, "checkStreamActive", null), a([l.LogManager.d_trace(l.LogCategory.USERMEDIA)], g.prototype, "renderDefault", null), a([l.LogManager.d_trace(l.LogCategory.USERMEDIA)], g.prototype, "render", null), a([l.LogManager.d_trace(l.LogCategory.USERMEDIA)], g.prototype, "clear", null), a([l.LogManager.d_trace(l.LogCategory.USERMEDIA)], g.prototype, "setVolume", null), a([l.LogManager.d_trace(l.LogCategory.USERMEDIA)], g.prototype, "useAudioOutput", null), t.MediaRenderer = g
}, function (e, t, n) {
    "use strict";

    function r(e, t) {
        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }
    var o = function () {
        function e(e, t) {
            for (var n = 0; n < t.length; n++) {
                var r = t[n];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
            }
        }
        return function (t, n, r) {
            return n && e(t.prototype, n), r && e(t, r), t
        }
    }(),
        i = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
            return typeof e
        } : function (e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        },
        a = function (e, t, n, r) {
            var o, a = arguments.length,
                s = a < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
            if ("object" === ("undefined" == typeof Reflect ? "undefined" : i(Reflect)) && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r);
            else
                for (var c = e.length - 1; c >= 0; c--)(o = e[c]) && (s = (a < 3 ? o(s) : a > 3 ? o(t, n, s) : o(t, n)) || s);
            return a > 3 && s && Object.defineProperty(t, n, s), s
        };
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var s = n(0),
        c = n(27),
        l = n(35),
        u = n(28),
        d = n(6),
        g = n(47),
        f = n(5),
        p = function () {
            function e() {
                var t = this;
                r(this, e), this._logger = new s.Logger(s.LogCategory.ENDPOINT, "EndpointManager", s.LogManager.get()), this._endPointMap = {}, this._trackListMap = {}, this._pendingEndpoints = {}, this._pendingStreams = {}, setInterval(function () {
                    for (var e in t._pendingStreams)
                        if (t._pendingStreams.hasOwnProperty(e) && t._pendingStreams[e].length) {
                            var n = f.CallManager.get().calls[e];
                            t.solveEndpoints(n)
                        }
                }, 1e3)
            }
            return o(e, [{
                key: "setCallVolume",
                value: function (e, t) {
                    var n = this._endPointMap[e.id()];
                    n && n.forEach(function (e) {
                        e.mediaRenderers && e.mediaRenderers.forEach(function (e) {
                            return e.setVolume(t)
                        })
                    })
                }
            }, {
                key: "_getEndpointByTrackId",
                value: function (e, t) {
                    var n = this._trackListMap[e.id()];
                    if (n && n.endpoints) {
                        for (var r in n.endpoints)
                            if (n.endpoints.hasOwnProperty(r) && n.endpoints[r].tracks[t]) {
                                var o = this._endPointMap[e.id()];
                                if (void 0 !== o)
                                    for (var i = 0; i < o.length; i++)
                                        if (o[i].id === r) return o[i];
                                this._logger.trace("Endpoint with " + t + " not found in call ID:" + e.id())
                            }
                        this._logger.info("Endpoint with " + t + " not in track list map for call ID:" + e.id())
                    } else if (this._endPointMap[e.id()]) return this._endPointMap[e.id()][0]
                }
            }, {
                key: "_getMediaTypeTrackId",
                value: function (e, t) {
                    var n = this._trackListMap[e.id()];
                    if (n && n.endpoints) {
                        for (var r in n.endpoints)
                            if (n.endpoints.hasOwnProperty(r) && n.endpoints[r].tracks[t]) return n.endpoints[r].tracks[t];
                        this._logger.info("Endpoint with " + t + " not in track list map for call ID:" + e.id())
                    }
                }
            }, {
                key: "setCallTrackInfo",
                value: function (e, t) {
                    var n = this;
                    t.endpoints[""] && (t.endpoints[e.id()] = t.endpoints[""], delete t.endpoints[""], null === this.getEndPointById(e, e.id()) && this.createDefaultEndPoint(e, e.id())), this._endPointMap[e.id()] && (this._endPointMap[e.id()] = this._endPointMap[e.id()].filter(function (r) {
                        return !!t.endpoints[r.id] || (r.isDefault && (void 0 === n._pendingStreams[e.id()] && (n._pendingStreams[e.id()] = []), r.mediaRenderers && r.mediaRenderers.forEach(function (t) {
                            n._pendingStreams[e.id()].push(t.stream)
                        })), n.deleteEndpoint(e, r), !1)
                    })), this._pendingEndpoints[e.id()] && (this._pendingEndpoints[e.id()] = this._pendingEndpoints[e.id()].filter(function (e) {
                        return !!t.endpoints[e.id]
                    })), this._trackListMap[e.id()] = t, this._pendingEndpoints[e.id()] && this._pendingEndpoints[e.id()].length && this.solveEndpoints(e)
                }
            }, {
                key: "mediaStreamAdded",
                value: function (e, t) {
                    var n = this._endPointMap[e.id()];
                    if (n && n.some(function (e) {
                        return e.mediaRenderers && e.mediaRenderers.some(function (e) {
                            return e.stream.id === t.id
                        })
                    })) return void this._logger.trace("MediaStream ID:" + t.id + " already exists in the call ID:" + e.id());
                    var r = this._pendingStreams[e.id()];
                    if (r && r.some(function (e) {
                        return e.id === t.id
                    })) return void this._logger.trace("MediaStream ID:" + t.id + " already pending in the call ID:" + e.id());
                    void 0 === this._pendingStreams[e.id()] && (this._pendingStreams[e.id()] = []), this._pendingStreams[e.id()].push(t), this.solveTracks(e)
                }
            }, {
                key: "addEndPoint",
                value: function (e, t) {
                    var n = this._endPointMap[e.id()];
                    return t.isDefault ? (void 0 === this._endPointMap[e.id()] && (this._endPointMap[e.id()] = []), this._endPointMap[e.id()].push(t), e.dispatchEvent({
                        name: d.CallEvents.EndpointAdded,
                        call: e,
                        endpoint: t
                    }), this.solveTracks(e), this._endPointMap[e.id()]) : n && n.some(function (e) {
                        return e.id === t.id
                    }) ? (this._logger.trace("Endpoint ID:" + t.id + " already exists in the call ID:" + e.id()), n) : (void 0 === this._pendingEndpoints[e.id()] && (this._pendingEndpoints[e.id()] = []), this._pendingEndpoints[e.id()].some(function (e) {
                        return e.id === t.id
                    }) || this._pendingEndpoints[e.id()].push(t), this.solveEndpoints(e), n)
                }
            }, {
                key: "solveEndpoints",
                value: function (e) {
                    var t = this;
                    return this._pendingEndpoints[e.id()] && (this._pendingEndpoints[e.id()] = this._pendingEndpoints[e.id()].filter(function (n) {
                        return !t._trackListMap[e.id()].endpoints[n.id] || (void 0 === t._endPointMap[e.id()] && (t._endPointMap[e.id()] = []), t._endPointMap[e.id()].push(n), e.dispatchEvent({
                            name: d.CallEvents.EndpointAdded,
                            call: e,
                            endpoint: n
                        }), !1)
                    })), this.solveTracks(e), this._endPointMap[e.id()]
                }
            }, {
                key: "solveTracks",
                value: function (e) {
                    var t = this,
                        n = this._pendingStreams[e.id()];
                    n && (this._pendingStreams[e.id()] = n.filter(function (n) {
                        if (n.getTracks().length) {
                            var r = n.getTracks()[0];
                            if ("ended" === r.readyState) return !1;
                            var o = t.getEndpointByTrackId(e, r.id);
                            return !(!g.isNull(o) && void 0 !== o && (t.addStreamToEndpoint(e, o, n), 1))
                        }
                        return !0
                    }))
                }
            }, {
                key: "getEndPointById",
                value: function (e, t) {
                    if (!this._endPointMap[e.id()]) return null;
                    var n = this._endPointMap[e.id()].filter(function (e) {
                        return e.id === t
                    });
                    try {
                        return n[0]
                    } catch (n) {
                        return this._logger.info("Endpoint with id:" + t + " not found on the call: " + e.id()), null
                    }
                }
            }, {
                key: "getCallMediaRenderers",
                value: function (e) {
                    var t = this._endPointMap[e.id()];
                    return t ? t.reduce(function (e, t) {
                        return e = e.concat(t.mediaRenderers)
                    }, []) : []
                }
            }, {
                key: "useAudioOutput",
                value: function (e, t) {
                    var n = this._endPointMap[e.id()];
                    n && n.forEach(function (e) {
                        return e.useAudioOutput(t)
                    })
                }
            }, {
                key: "updateEndPoint",
                value: function (e, t) {
                    var n = this._endPointMap[e.id()],
                        r = this.findEndpointIndex(n, t);
                    return n && -1 !== r ? (n[r] = t, this._endPointMap[e.id()] = n, e.dispatchEvent({
                        name: u.EndpointEvents.InfoUpdated,
                        call: e,
                        endpoint: t
                    })) : this._logger.error("Trying update non existing endpoint endpoint with id:" + t.id + " on the call: " + e.id()), n
                }
            }, {
                key: "clearEndPointByCall",
                value: function (e) {
                    var t = this;
                    return new Promise(function (n, r) {
                        var o = t._endPointMap[e.id()];
                        o && o.forEach(function (n) {
                            t._logger.info("Remove endpoint " + n.id), n.mediaRenderers && n.mediaRenderers.forEach(function (e) {
                                t._logger.info("Remove media renderer " + e.id + " in the Endpoint " + n.id), e.clear()
                            }), n.mediaRenderers = [], n.dispatchEvent({
                                name: u.EndpointEvents.Removed,
                                call: e,
                                endpoint: n
                            })
                        }), delete t._endPointMap[e.id()], delete t._pendingStreams[e.id()], delete t._pendingEndpoints[e.id()], delete t._trackListMap[e.id()], n()
                    })
                }
            }, {
                key: "getEndPointsByCall",
                value: function (e) {
                    return this._endPointMap[e.id()]
                }
            }, {
                key: "findEndpointIndex",
                value: function (e, t) {
                    return e && t ? e.reduce(function (e, n, r) {
                        return n.id === t.id ? r : e
                    }, -1) : -1
                }
            }, {
                key: "createDefaultEndPoint",
                value: function (e, t) {
                    var n = new c.Endpoint;
                    return n.id = t, n.isDefault = !0, this.addEndPoint(e, n), n
                }
            }, {
                key: "rmEndPoint",
                value: function (e, t) {
                    var n = this._endPointMap[e.id()],
                        r = this.findEndpointIndex(n, t);
                    return n && -1 !== r ? (this.deleteEndpoint(e, t), n = n.filter(function (e) {
                        return e.id !== t.id
                    }), this._endPointMap[e.id()] = n) : this._logger.error("Trying remove non existing endpoint with id:" + t.id + " on the call: " + e.id()), n
                }
            }, {
                key: "deleteEndpoint",
                value: function (e, t) {
                    t.mediaRenderers && t.mediaRenderers.forEach(function (e) {
                        return e.clear()
                    }), t.dispatchEvent({
                        name: u.EndpointEvents.Removed,
                        call: e,
                        endpoint: t
                    })
                }
            }, {
                key: "clearDefaultEndpoint",
                value: function (e) {
                    var t = this,
                        n = this._endPointMap[e.id()];
                    if (n) {
                        var r = n.filter(function (t) {
                            return t.id === e.id()
                        });
                        r && r.forEach(function (n) {
                            return t.deleteEndpoint(e, n)
                        }), this._endPointMap[e.id()] = n.filter(function (t) {
                            return t.id !== e.id()
                        })
                    }
                }
            }, {
                key: "addStreamToEndpoint",
                value: function (e, t, n) {
                    if (!t.mediaRenderers.some(function (e) {
                        return e.stream.getTracks()[0].id === n.getTracks()[0].id
                    })) {
                        var r = this.getMediaTypeTrackId(e, n.getTracks()[0].id),
                            o = new l.MediaRenderer(n, r, !1);
                        t.mediaRenderers.push(o), t.dispatchEvent({
                            name: u.EndpointEvents.RemoteMediaAdded,
                            call: e,
                            endpoint: t,
                            mediaRenderer: o
                        }), e.dispatchEvent({
                            name: d.CallEvents.MediaElementCreated,
                            call: e,
                            stream: o.stream,
                            element: o.element,
                            type: o.kind
                        }), o.onBeforeDestroy = function () {
                            t.dispatchEvent({
                                name: u.EndpointEvents.RemoteMediaRemoved,
                                call: e,
                                endpoint: t,
                                mediaRenderer: o
                            }), e.dispatchEvent({
                                name: d.CallEvents.MediaElementRemoved,
                                call: e,
                                stream: o.stream,
                                element: o.element,
                                type: o.kind
                            })
                        }
                    }
                }
            }, {
                key: "_traceName",
                value: function () {
                    return "AbstractEndpointManager"
                }
            }, {
                key: "getEndPointByMediaRenderer",
                value: function (e) {
                    for (var t in this._endPointMap)
                        if (this._endPointMap.hasOwnProperty(t)) {
                            var n = this._endPointMap[t].filter(function (t) {
                                return t.mediaRenderers.some(function (t) {
                                    return t.stream.id == e.stream.id
                                })
                            });
                            if (n) return n[0]
                        }
                }
            }]), e
        }();
    a([s.LogManager.d_trace(s.LogCategory.ENDPOINT)], p.prototype, "setCallVolume", null), a([s.LogManager.d_trace(s.LogCategory.ENDPOINT)], p.prototype, "_getEndpointByTrackId", null), a([s.LogManager.d_trace(s.LogCategory.ENDPOINT)], p.prototype, "_getMediaTypeTrackId", null), a([s.LogManager.d_trace(s.LogCategory.ENDPOINT)], p.prototype, "setCallTrackInfo", null), a([s.LogManager.d_trace(s.LogCategory.ENDPOINT)], p.prototype, "mediaStreamAdded", null), a([s.LogManager.d_trace(s.LogCategory.ENDPOINT)], p.prototype, "addEndPoint", null), a([s.LogManager.d_trace(s.LogCategory.ENDPOINT)], p.prototype, "solveEndpoints", null), a([s.LogManager.d_trace(s.LogCategory.ENDPOINT)], p.prototype, "solveTracks", null), a([s.LogManager.d_trace(s.LogCategory.ENDPOINT)], p.prototype, "getEndPointById", null), a([s.LogManager.d_trace(s.LogCategory.ENDPOINT)], p.prototype, "getCallMediaRenderers", null), a([s.LogManager.d_trace(s.LogCategory.ENDPOINT)], p.prototype, "useAudioOutput", null), a([s.LogManager.d_trace(s.LogCategory.ENDPOINT)], p.prototype, "updateEndPoint", null), a([s.LogManager.d_trace(s.LogCategory.ENDPOINT)], p.prototype, "clearEndPointByCall", null), a([s.LogManager.d_trace(s.LogCategory.ENDPOINT)], p.prototype, "getEndPointsByCall", null), a([s.LogManager.d_trace(s.LogCategory.ENDPOINT)], p.prototype, "findEndpointIndex", null), a([s.LogManager.d_trace(s.LogCategory.ENDPOINT)], p.prototype, "createDefaultEndPoint", null), a([s.LogManager.d_trace(s.LogCategory.ENDPOINT)], p.prototype, "rmEndPoint", null), a([s.LogManager.d_trace(s.LogCategory.ENDPOINT)], p.prototype, "deleteEndpoint", null), a([s.LogManager.d_trace(s.LogCategory.ENDPOINT)], p.prototype, "clearDefaultEndpoint", null), a([s.LogManager.d_trace(s.LogCategory.ENDPOINT)], p.prototype, "addStreamToEndpoint", null), t.AbstractEndpointManager = p
}, function (e, t) {
    var n;
    n = function () {
        return this
    }();
    try {
        n = n || Function("return this")() || (0, eval)("this")
    } catch (e) {
        "object" == typeof window && (n = window)
    }
    e.exports = n
}, function (e, t, n) {
    "use strict";

    function r(e, t) {
        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }
    var o = function () {
        function e(e, t) {
            for (var n = 0; n < t.length; n++) {
                var r = t[n];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
            }
        }
        return function (t, n, r) {
            return n && e(t.prototype, n), r && e(t, r), t
        }
    }(),
        i = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
            return typeof e
        } : function (e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        },
        a = function (e, t, n, r) {
            var o, a = arguments.length,
                s = a < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
            if ("object" === ("undefined" == typeof Reflect ? "undefined" : i(Reflect)) && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r);
            else
                for (var c = e.length - 1; c >= 0; c--)(o = e[c]) && (s = (a < 3 ? o(s) : a > 3 ? o(t, n, s) : o(t, n)) || s);
            return a > 3 && s && Object.defineProperty(t, n, s), s
        };
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var s = n(6),
        c = n(0),
        l = function () {
            function e(t, n) {
                var o = this;
                r(this, e), this._pcStatus = n, this._q = [], t.on(s.CallEvents.Updated, function (t) {
                    if (c.LogManager.get().writeMessage(c.LogCategory.REINVITEQ, "CallEvent", c.LogLevel.TRACE, "Updated with result " + t.result), e._currentReinvite) {
                        var n = e._currentReinvite;
                        t.result ? n.resolve(t) : n.reject(t), e._currentReinvite = void 0
                    }
                    o.runNext()
                }), t.on(s.CallEvents.PendingUpdate, function (t) {
                    c.LogManager.get().writeMessage(c.LogCategory.REINVITEQ, "CallEvent", c.LogLevel.TRACE, "IncomingUpdate. Local RI==" + i(e._currentReinvite)), e._currentReinvite && (e._currentReinvite.reject(), e._currentReinvite = void 0)
                }), t.on(s.CallEvents.UpdateFailed, function (t) {
                    c.LogManager.get().writeMessage(c.LogCategory.REINVITEQ, "CallEvent", c.LogLevel.TRACE, "UpdateFailed"), e._currentReinvite && (e._currentReinvite.reject(), e._currentReinvite = void 0)
                })
            }
            return o(e, [{
                key: "runNext",
                value: function () {
                    void 0 === e._currentReinvite && this._q.length > 0 && this._pcStatus() && (e._currentReinvite = this._q.splice(0, 1)[0], e._currentReinvite.fx())
                }
            }, {
                key: "add",
                value: function (e) {
                    this._q.push(e), this.runNext()
                }
            }, {
                key: "clear",
                value: function () {
                    this._q.forEach(function (e) {
                        e.reject()
                    })
                }
            }, {
                key: "_traceName",
                value: function () {
                    return "ReInviteQ"
                }
            }]), e
        }();
    a([c.LogManager.d_trace(c.LogCategory.REINVITEQ)], l.prototype, "runNext", null), a([c.LogManager.d_trace(c.LogCategory.REINVITEQ)], l.prototype, "add", null), a([c.LogManager.d_trace(c.LogCategory.REINVITEQ)], l.prototype, "clear", null), t.ReInviteQ = l
}, function (e, t, n) {
    "use strict";
    var r = {};
    r.generateIdentifier = function () {
        return Math.random().toString(36).substr(2, 10)
    }, r.localCName = r.generateIdentifier(), r.splitLines = function (e) {
        return e.trim().split("\n").map(function (e) {
            return e.trim()
        })
    }, r.splitSections = function (e) {
        return e.split("\nm=").map(function (e, t) {
            return (t > 0 ? "m=" + e : e).trim() + "\r\n"
        })
    }, r.getDescription = function (e) {
        var t = r.splitSections(e);
        return t && t[0]
    }, r.getMediaSections = function (e) {
        var t = r.splitSections(e);
        return t.shift(), t
    }, r.matchPrefix = function (e, t) {
        return r.splitLines(e).filter(function (e) {
            return 0 === e.indexOf(t)
        })
    }, r.parseCandidate = function (e) {
        var t;
        t = 0 === e.indexOf("a=candidate:") ? e.substring(12).split(" ") : e.substring(10).split(" ");
        for (var n = {
            foundation: t[0],
            component: parseInt(t[1], 10),
            protocol: t[2].toLowerCase(),
            priority: parseInt(t[3], 10),
            ip: t[4],
            port: parseInt(t[5], 10),
            type: t[7]
        }, r = 8; r < t.length; r += 2) switch (t[r]) {
            case "raddr":
                n.relatedAddress = t[r + 1];
                break;
            case "rport":
                n.relatedPort = parseInt(t[r + 1], 10);
                break;
            case "tcptype":
                n.tcpType = t[r + 1];
                break;
            case "ufrag":
                n.ufrag = t[r + 1], n.usernameFragment = t[r + 1];
                break;
            default:
                n[t[r]] = t[r + 1]
        }
        return n
    }, r.writeCandidate = function (e) {
        var t = [];
        t.push(e.foundation), t.push(e.component), t.push(e.protocol.toUpperCase()), t.push(e.priority), t.push(e.ip), t.push(e.port);
        var n = e.type;
        return t.push("typ"), t.push(n), "host" !== n && e.relatedAddress && e.relatedPort && (t.push("raddr"), t.push(e.relatedAddress), t.push("rport"), t.push(e.relatedPort)), e.tcpType && "tcp" === e.protocol.toLowerCase() && (t.push("tcptype"), t.push(e.tcpType)), (e.usernameFragment || e.ufrag) && (t.push("ufrag"), t.push(e.usernameFragment || e.ufrag)), "candidate:" + t.join(" ")
    }, r.parseIceOptions = function (e) {
        return e.substr(14).split(" ")
    }, r.parseRtpMap = function (e) {
        var t = e.substr(9).split(" "),
            n = {
                payloadType: parseInt(t.shift(), 10)
            };
        return t = t[0].split("/"), n.name = t[0], n.clockRate = parseInt(t[1], 10), n.channels = 3 === t.length ? parseInt(t[2], 10) : 1, n.numChannels = n.channels, n
    }, r.writeRtpMap = function (e) {
        var t = e.payloadType;
        void 0 !== e.preferredPayloadType && (t = e.preferredPayloadType);
        var n = e.channels || e.numChannels || 1;
        return "a=rtpmap:" + t + " " + e.name + "/" + e.clockRate + (1 !== n ? "/" + n : "") + "\r\n"
    }, r.parseExtmap = function (e) {
        var t = e.substr(9).split(" ");
        return {
            id: parseInt(t[0], 10),
            direction: t[0].indexOf("/") > 0 ? t[0].split("/")[1] : "sendrecv",
            uri: t[1]
        }
    }, r.writeExtmap = function (e) {
        return "a=extmap:" + (e.id || e.preferredId) + (e.direction && "sendrecv" !== e.direction ? "/" + e.direction : "") + " " + e.uri + "\r\n"
    }, r.parseFmtp = function (e) {
        for (var t, n = {}, r = e.substr(e.indexOf(" ") + 1).split(";"), o = 0; o < r.length; o++) t = r[o].trim().split("="), n[t[0].trim()] = t[1];
        return n
    }, r.writeFmtp = function (e) {
        var t = "",
            n = e.payloadType;
        if (void 0 !== e.preferredPayloadType && (n = e.preferredPayloadType), e.parameters && Object.keys(e.parameters).length) {
            var r = [];
            Object.keys(e.parameters).forEach(function (t) {
                e.parameters[t] ? r.push(t + "=" + e.parameters[t]) : r.push(t)
            }), t += "a=fmtp:" + n + " " + r.join(";") + "\r\n"
        }
        return t
    }, r.parseRtcpFb = function (e) {
        var t = e.substr(e.indexOf(" ") + 1).split(" ");
        return {
            type: t.shift(),
            parameter: t.join(" ")
        }
    }, r.writeRtcpFb = function (e) {
        var t = "",
            n = e.payloadType;
        return void 0 !== e.preferredPayloadType && (n = e.preferredPayloadType), e.rtcpFeedback && e.rtcpFeedback.length && e.rtcpFeedback.forEach(function (e) {
            t += "a=rtcp-fb:" + n + " " + e.type + (e.parameter && e.parameter.length ? " " + e.parameter : "") + "\r\n"
        }), t
    }, r.parseSsrcMedia = function (e) {
        var t = e.indexOf(" "),
            n = {
                ssrc: parseInt(e.substr(7, t - 7), 10)
            },
            r = e.indexOf(":", t);
        return r > -1 ? (n.attribute = e.substr(t + 1, r - t - 1), n.value = e.substr(r + 1)) : n.attribute = e.substr(t + 1), n
    }, r.getMid = function (e) {
        var t = r.matchPrefix(e, "a=mid:")[0];
        if (t) return t.substr(6)
    }, r.parseFingerprint = function (e) {
        var t = e.substr(14).split(" ");
        return {
            algorithm: t[0].toLowerCase(),
            value: t[1]
        }
    }, r.getDtlsParameters = function (e, t) {
        return {
            role: "auto",
            fingerprints: r.matchPrefix(e + t, "a=fingerprint:").map(r.parseFingerprint)
        }
    }, r.writeDtlsParameters = function (e, t) {
        var n = "a=setup:" + t + "\r\n";
        return e.fingerprints.forEach(function (e) {
            n += "a=fingerprint:" + e.algorithm + " " + e.value + "\r\n"
        }), n
    }, r.getIceParameters = function (e, t) {
        var n = r.splitLines(e);
        return n = n.concat(r.splitLines(t)), {
            usernameFragment: n.filter(function (e) {
                return 0 === e.indexOf("a=ice-ufrag:")
            })[0].substr(12),
            password: n.filter(function (e) {
                return 0 === e.indexOf("a=ice-pwd:")
            })[0].substr(10)
        }
    }, r.writeIceParameters = function (e) {
        return "a=ice-ufrag:" + e.usernameFragment + "\r\na=ice-pwd:" + e.password + "\r\n"
    }, r.parseRtpParameters = function (e) {
        for (var t = {
            codecs: [],
            headerExtensions: [],
            fecMechanisms: [],
            rtcp: []
        }, n = r.splitLines(e), o = n[0].split(" "), i = 3; i < o.length; i++) {
            var a = o[i],
                s = r.matchPrefix(e, "a=rtpmap:" + a + " ")[0];
            if (s) {
                var c = r.parseRtpMap(s),
                    l = r.matchPrefix(e, "a=fmtp:" + a + " ");
                switch (c.parameters = l.length ? r.parseFmtp(l[0]) : {}, c.rtcpFeedback = r.matchPrefix(e, "a=rtcp-fb:" + a + " ").map(r.parseRtcpFb), t.codecs.push(c), c.name.toUpperCase()) {
                    case "RED":
                    case "ULPFEC":
                        t.fecMechanisms.push(c.name.toUpperCase())
                }
            }
        }
        return r.matchPrefix(e, "a=extmap:").forEach(function (e) {
            t.headerExtensions.push(r.parseExtmap(e))
        }), t
    }, r.writeRtpDescription = function (e, t) {
        var n = "";
        n += "m=" + e + " ", n += t.codecs.length > 0 ? "9" : "0", n += " UDP/TLS/RTP/SAVPF ", n += t.codecs.map(function (e) {
            return void 0 !== e.preferredPayloadType ? e.preferredPayloadType : e.payloadType
        }).join(" ") + "\r\n", n += "c=IN IP4 0.0.0.0\r\n", n += "a=rtcp:9 IN IP4 0.0.0.0\r\n", t.codecs.forEach(function (e) {
            n += r.writeRtpMap(e), n += r.writeFmtp(e), n += r.writeRtcpFb(e)
        });
        var o = 0;
        return t.codecs.forEach(function (e) {
            e.maxptime > o && (o = e.maxptime)
        }), o > 0 && (n += "a=maxptime:" + o + "\r\n"), n += "a=rtcp-mux\r\n", t.headerExtensions && t.headerExtensions.forEach(function (e) {
            n += r.writeExtmap(e)
        }), n
    }, r.parseRtpEncodingParameters = function (e) {
        var t, n = [],
            o = r.parseRtpParameters(e),
            i = -1 !== o.fecMechanisms.indexOf("RED"),
            a = -1 !== o.fecMechanisms.indexOf("ULPFEC"),
            s = r.matchPrefix(e, "a=ssrc:").map(function (e) {
                return r.parseSsrcMedia(e)
            }).filter(function (e) {
                return "cname" === e.attribute
            }),
            c = s.length > 0 && s[0].ssrc,
            l = r.matchPrefix(e, "a=ssrc-group:FID").map(function (e) {
                return e.substr(17).split(" ").map(function (e) {
                    return parseInt(e, 10)
                })
            });
        l.length > 0 && l[0].length > 1 && l[0][0] === c && (t = l[0][1]), o.codecs.forEach(function (e) {
            if ("RTX" === e.name.toUpperCase() && e.parameters.apt) {
                var r = {
                    ssrc: c,
                    codecPayloadType: parseInt(e.parameters.apt, 10)
                };
                c && t && (r.rtx = {
                    ssrc: t
                }), n.push(r), i && (r = JSON.parse(JSON.stringify(r)), r.fec = {
                    ssrc: t,
                    mechanism: a ? "red+ulpfec" : "red"
                }, n.push(r))
            }
        }), 0 === n.length && c && n.push({
            ssrc: c
        });
        var u = r.matchPrefix(e, "b=");
        return u.length && (u = 0 === u[0].indexOf("b=TIAS:") ? parseInt(u[0].substr(7), 10) : 0 === u[0].indexOf("b=AS:") ? 1e3 * parseInt(u[0].substr(5), 10) * .95 - 16e3 : void 0, n.forEach(function (e) {
            e.maxBitrate = u
        })), n
    }, r.parseRtcpParameters = function (e) {
        var t = {},
            n = r.matchPrefix(e, "a=ssrc:").map(function (e) {
                return r.parseSsrcMedia(e)
            }).filter(function (e) {
                return "cname" === e.attribute
            })[0];
        n && (t.cname = n.value, t.ssrc = n.ssrc);
        var o = r.matchPrefix(e, "a=rtcp-rsize");
        t.reducedSize = o.length > 0, t.compound = 0 === o.length;
        var i = r.matchPrefix(e, "a=rtcp-mux");
        return t.mux = i.length > 0, t
    }, r.parseMsid = function (e) {
        var t, n = r.matchPrefix(e, "a=msid:");
        if (1 === n.length) return t = n[0].substr(7).split(" "), {
            stream: t[0],
            track: t[1]
        };
        var o = r.matchPrefix(e, "a=ssrc:").map(function (e) {
            return r.parseSsrcMedia(e)
        }).filter(function (e) {
            return "msid" === e.attribute
        });
        return o.length > 0 ? (t = o[0].value.split(" "), {
            stream: t[0],
            track: t[1]
        }) : void 0
    }, r.generateSessionId = function () {
        return Math.random().toString().substr(2, 21)
    }, r.writeSessionBoilerplate = function (e, t) {
        var n = void 0 !== t ? t : 2;
        return "v=0\r\no=thisisadapterortc " + (e || r.generateSessionId()) + " " + n + " IN IP4 127.0.0.1\r\ns=-\r\nt=0 0\r\n"
    }, r.writeMediaSection = function (e, t, n, o) {
        var i = r.writeRtpDescription(e.kind, t);
        if (i += r.writeIceParameters(e.iceGatherer.getLocalParameters()), i += r.writeDtlsParameters(e.dtlsTransport.getLocalParameters(), "offer" === n ? "actpass" : "active"), i += "a=mid:" + e.mid + "\r\n", e.direction ? i += "a=" + e.direction + "\r\n" : e.rtpSender && e.rtpReceiver ? i += "a=sendrecv\r\n" : e.rtpSender ? i += "a=sendonly\r\n" : e.rtpReceiver ? i += "a=recvonly\r\n" : i += "a=inactive\r\n", e.rtpSender) {
            var a = "msid:" + o.id + " " + e.rtpSender.track.id + "\r\n";
            i += "a=" + a, i += "a=ssrc:" + e.sendEncodingParameters[0].ssrc + " " + a, e.sendEncodingParameters[0].rtx && (i += "a=ssrc:" + e.sendEncodingParameters[0].rtx.ssrc + " " + a, i += "a=ssrc-group:FID " + e.sendEncodingParameters[0].ssrc + " " + e.sendEncodingParameters[0].rtx.ssrc + "\r\n")
        }
        return i += "a=ssrc:" + e.sendEncodingParameters[0].ssrc + " cname:" + r.localCName + "\r\n", e.rtpSender && e.sendEncodingParameters[0].rtx && (i += "a=ssrc:" + e.sendEncodingParameters[0].rtx.ssrc + " cname:" + r.localCName + "\r\n"), i
    }, r.getDirection = function (e, t) {
        for (var n = r.splitLines(e), o = 0; o < n.length; o++) switch (n[o]) {
            case "a=sendrecv":
            case "a=sendonly":
            case "a=recvonly":
            case "a=inactive":
                return n[o].substr(2)
        }
        return t ? r.getDirection(t) : "sendrecv"
    }, r.getKind = function (e) {
        return r.splitLines(e)[0].split(" ")[0].substr(2)
    }, r.isRejected = function (e) {
        return "0" === e.split(" ", 2)[1]
    }, r.parseMLine = function (e) {
        var t = r.splitLines(e),
            n = t[0].substr(2).split(" ");
        return {
            kind: n[0],
            port: parseInt(n[1], 10),
            protocol: n[2],
            fmt: n.slice(3).join(" ")
        }
    }, r.parseOLine = function (e) {
        var t = r.matchPrefix(e, "o=")[0],
            n = t.substr(2).split(" ");
        return {
            username: n[0],
            sessionId: n[1],
            sessionVersion: parseInt(n[2], 10),
            netType: n[3],
            addressType: n[4],
            address: n[5]
        }
    }, e.exports = r
}, function (e, t, n) {
    "use strict";

    function r(e, t) {
        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }
    var o = function () {
        function e(e, t) {
            for (var n = 0; n < t.length; n++) {
                var r = t[n];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
            }
        }
        return function (t, n, r) {
            return n && e(t.prototype, n), r && e(t, r), t
        }
    }(),
        i = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
            return typeof e
        } : function (e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        },
        a = function (e, t, n, r) {
            var o, a = arguments.length,
                s = a < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
            if ("object" === ("undefined" == typeof Reflect ? "undefined" : i(Reflect)) && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r);
            else
                for (var c = e.length - 1; c >= 0; c--)(o = e[c]) && (s = (a < 3 ? o(s) : a > 3 ? o(t, n, s) : o(t, n)) || s);
            return a > 3 && s && Object.defineProperty(t, n, s), s
        };
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var s = n(14),
        c = n(30),
        l = n(15),
        u = n(31),
        d = n(23),
        g = n(0),
        f = n(16),
        p = function () {
            function e() {
                var t = this;
                if (r(this, e), e.instance) throw new Error("Error - use ConversationManager.get()");
                this.signalling = s.MsgSignaling.get(), this.awaitingConversations = {}, this.signalling.addEventListener(l.MsgEvent.onEditConversation, function (e) {
                    t.resolveEvent(e, f.default.MessengerEvents.EditConversation)
                }), this.signalling.addEventListener(l.MsgEvent.onGetConversation, function (e) {
                    t.resolveEvent(e, f.default.MessengerEvents.GetConversation)
                }), this.signalling.addEventListener(l.MsgEvent.onRemoveConversation, function (e) {
                    t.resolveEvent(e, f.default.MessengerEvents.RemoveConversation)
                }), this.signalling.addEventListener(l.MsgEvent.onCreateConversation, function (e) {
                    t.resolveEvent(e, f.default.MessengerEvents.CreateConversation)
                }), this.signalling.addEventListener(l.MsgEvent.onSendMessage, function (e) {
                    t.resolveMessageEvent(e, f.default.MessengerEvents.SendMessage)
                }), this.signalling.addEventListener(l.MsgEvent.onEditMessage, function (e) {
                    t.resolveMessageEvent(e, f.default.MessengerEvents.EditMessage)
                }), this.signalling.addEventListener(l.MsgEvent.onRemoveMessage, function (e) {
                    t.resolveMessageEvent(e, f.default.MessengerEvents.RemoveMessage)
                }), this.signalling.addEventListener(l.MsgEvent.isRead, function (e) {
                    var t = e.object;
                    u.Messenger.getInstance()._dispatchEvent(f.default.MessengerEvents.Read, {
                        conversation: t.conversation,
                        timestamp: new Date(1e3 * t.timestamp),
                        userId: e.user_id,
                        seq: t.seq,
                        onIncomingEvent: e.on_incoming_event,
                        name: f.default.MessengerEvents[f.default.MessengerEvents.Read]
                    })
                }), this.signalling.addEventListener(l.MsgEvent.isDelivered, function (e) {
                    var t = e.object;
                    u.Messenger.getInstance()._dispatchEvent(f.default.MessengerEvents.Delivered, {
                        conversation: t.conversation,
                        timestamp: new Date(1e3 * t.timestamp),
                        userId: e.user_id,
                        seq: t.seq,
                        onIncomingEvent: e.on_incoming_event,
                        name: f.default.MessengerEvents[f.default.MessengerEvents.Delivered]
                    })
                }), this.signalling.addEventListener(l.MsgEvent.onTyping, function (e) {
                    u.Messenger.getInstance()._dispatchEvent(f.default.MessengerEvents.Typing, {
                        name: f.default.MessengerEvents[f.default.MessengerEvents.Typing],
                        conversation: e.object.conversation,
                        userId: e.user_id,
                        onIncomingEvent: e.on_incoming_event
                    })
                }), this.signalling.addEventListener(l.MsgEvent.onRetransmitEvents, function (e) {
                    u.Messenger.getInstance()._dispatchEvent(f.default.MessengerEvents.RetransmitEvents, {
                        events: e.object.map(function (e) {
                            if (e.event) return -1 == e.event.indexOf("Message") ? {
                                name: e.event,
                                conversation: c.Conversation._createFromBus(e.payload.object, e.payload.seq),
                                userId: e.payload.user_id,
                                seq: e.payload.seq,
                                onIncomingEvent: e.payload.on_incoming_event
                            } : {
                                    name: e.event,
                                    message: d.Message._createFromBus(e.payload.object, e.payload.seq),
                                    userId: e.payload.user_id,
                                    seq: e.payload.seq,
                                    onIncomingEvent: e.payload.on_incoming_event
                                }
                        }),
                        userId: e.user_id,
                        seq: e.seq,
                        from: e.from,
                        to: e.to,
                        onIncomingEvent: e.on_incoming_event
                    })
                }), this._converasationList = []
            }
            return o(e, [{
                key: "createConversation",
                value: function (e, t, n, r, o, i) {
                    var a = new c.Conversation(e, n, r, o, i);
                    a.title = t, a._createUUID(), this.signalling.sendPayload(l.MsgAction.createConversation, a._getPayload())
                }
            }, {
                key: "removeConversation",
                value: function (e) {
                    this.signalling.sendPayload(l.MsgAction.removeConversation, {
                        uuid: e
                    })
                }
            }, {
                key: "editConversation",
                value: function (e) {
                    this.signalling.sendPayload(l.MsgAction.editConversation, e._getSimplePayload())
                }
            }, {
                key: "getConversation",
                value: function (e) {
                    this.signalling.sendPayload(l.MsgAction.getConversation, {
                        uuid: e
                    })
                }
            }, {
                key: "getConversationByUUID",
                value: function (e) {
                    var t = this;
                    return new Promise(function (n, r) {
                        var o = !0,
                            i = !1,
                            a = void 0;
                        try {
                            for (var s, c = t._converasationList[Symbol.iterator](); !(o = (s = c.next()).done); o = !0) {
                                var l = s.value;
                                if (l.uuid === e) return u.Messenger.getInstance()._dispatchEvent(f.default.MessengerEvents.GetConversation, {
                                    conversation: l
                                }), void n(l)
                            }
                        } catch (e) {
                            i = !0, a = e
                        } finally {
                            try {
                                !o && c.return && c.return()
                            } finally {
                                if (i) throw a
                            }
                        }
                        t.awaitingConversations[e] = n
                    })
                }
            }, {
                key: "getConversations",
                value: function (e) {
                    this.signalling.sendPayload(l.MsgAction.getConversations, {
                        uuids: e
                    })
                }
            }, {
                key: "_traceName",
                value: function () {
                    return "ConversationManager"
                }
            }, {
                key: "resolveEvent",
                value: function (e, t) {
                    if (f.default.MessengerEvents[t] === f.default.MessengerEvents[f.default.MessengerEvents.RemoveConversation]) {
                        var n = e.object;
                        return void u.Messenger.getInstance()._dispatchEvent(t, {
                            name: f.default.MessengerEvents[t],
                            uuid: n.uuid,
                            userId: e.user_id,
                            seq: e.seq,
                            onIncomingEvent: e.on_incoming_event
                        })
                    }
                    var r = c.Conversation._createFromBus(e.object, e.seq);
                    this.registerConversation(r), void 0 !== r && r.updateSeq(e.seq), u.Messenger.getInstance()._dispatchEvent(t, {
                        name: f.default.MessengerEvents[t],
                        conversation: r,
                        userId: e.user_id,
                        seq: e.seq,
                        onIncomingEvent: e.on_incoming_event
                    }), t === f.default.MessengerEvents.GetConversation && void 0 !== this.awaitingConversations[r.uuid] && (this.awaitingConversations[r.uuid](r), delete this.awaitingConversations[r.uuid])
                }
            }, {
                key: "resolveMessageEvent",
                value: function (e, t) {
                    var n = d.Message._createFromBus(e.object, e.seq);
                    void 0 !== this._converasationList[n.conversation] && this._converasationList[n.conversation].updateSeq(e.seq), u.Messenger.getInstance()._dispatchEvent(t, {
                        name: f.default.MessengerEvents[t],
                        message: n,
                        userId: e.user_id,
                        seq: e.seq,
                        onIncomingEvent: e.on_incoming_event
                    })
                }
            }, {
                key: "registerConversation",
                value: function (e) {
                    this._converasationList.filter(function (t) {
                        return t.uuid !== e.uuid
                    }), this._converasationList.push(e)
                }
            }], [{
                key: "get",
                value: function () {
                    return e.instance = e.instance || new e, e.instance
                }
            }, {
                key: "extractUserName",
                value: function (e) {
                    var t = e.split("@");
                    return t[1] = t[1].split(".").splice(0, 2).join("."), t.join("@")
                }
            }, {
                key: "deserialize",
                value: function (e) {
                    return c.Conversation.createFromCache(e)
                }
            }, {
                key: "serialize",
                value: function (e) {
                    return e.toCache()
                }
            }]), e
        }();
    a([g.LogManager.d_trace(g.LogCategory.MESSAGING)], p.prototype, "createConversation", null), a([g.LogManager.d_trace(g.LogCategory.MESSAGING)], p.prototype, "removeConversation", null), a([g.LogManager.d_trace(g.LogCategory.MESSAGING)], p.prototype, "editConversation", null), a([g.LogManager.d_trace(g.LogCategory.MESSAGING)], p.prototype, "getConversation", null), a([g.LogManager.d_trace(g.LogCategory.MESSAGING)], p.prototype, "getConversationByUUID", null), a([g.LogManager.d_trace(g.LogCategory.MESSAGING)], p.prototype, "getConversations", null), a([g.LogManager.d_trace(g.LogCategory.MESSAGING)], p.prototype, "resolveEvent", null), a([g.LogManager.d_trace(g.LogCategory.MESSAGING)], p.prototype, "resolveMessageEvent", null), a([g.LogManager.d_trace(g.LogCategory.MESSAGING)], p.prototype, "registerConversation", null), a([g.LogManager.d_trace(g.LogCategory.MESSAGING)], p, "get", null), a([g.LogManager.d_trace(g.LogCategory.MESSAGING)], p, "extractUserName", null), a([g.LogManager.d_trace(g.LogCategory.MESSAGING)], p, "deserialize", null), a([g.LogManager.d_trace(g.LogCategory.MESSAGING)], p, "serialize", null), t.ConversationManager = p
}, function (e, t, n) {
    "use strict";

    function r() {
        return i.Client.getInstance()
    }

    function o() {
        if (!a.Authenticator.get().authorized()) throw new Error("NOT_AUTHORIZED");
        return s.default.Messenger.getInstance()
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var i = n(1);
    n(60);
    var a = n(10),
        s = n(16),
        c = n(17);
    t.Events = c.Events;
    var l = n(6);
    t.CallEvents = l.CallEvents;
    var u = n(27);
    t.Endpoint = u.Endpoint;
    var d = n(28);
    t.EndpointEvents = d.EndpointEvents;
    var g = n(16);
    t.Messaging = g.Messaging;
    var f = n(32);
    t.OperatorACDStatuses = f.OperatorACDStatuses;
    var p = n(0);
    t.LogCategory = p.LogCategory, t.LogLevel = p.LogLevel, t.ClientState = p.ClientState,
        function (e) {
            for (var n in e) t.hasOwnProperty(n) || (t[n] = e[n])
        }(n(4)), t.getInstance = r, t.version = i.Client.getInstance().version, t.getMessenger = o
}, function (e, t, n) {
    "use strict";

    function r(e, t) {
        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }
    var o = function () {
        function e(e, t) {
            for (var n = 0; n < t.length; n++) {
                var r = t[n];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
            }
        }
        return function (t, n, r) {
            return n && e(t.prototype, n), r && e(t, r), t
        }
    }();
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var i = n(18),
        a = n(0),
        s = function () {
            function e() {
                r(this, e)
            }
            return o(e, [{
                key: "_traceName",
                value: function () {
                    return "FF"
                }
            }], [{
                key: "attachStream",
                value: function (e, t) {
                    void 0 === t.srcObject ? t.mozSrcObject = e : t.srcObject = e, t.load(), t.play()
                }
            }, {
                key: "detachStream",
                value: function (e) {
                    void 0 === e.srcObject ? e.mozSrcObject = null : e.srcObject = null, e.load(), e.src = ""
                }
            }, {
                key: "screenSharingSupported",
                value: function () {
                    return new Promise(function (e, t) {
                        if ("https:" != window.location.protocol) return void e(!1);
                        e(!0)
                    })
                }
            }, {
                key: "getScreenMedia",
                value: function () {
                    var e = {
                        audio: !1,
                        video: {
                            mediaSource: "window"
                        }
                    };
                    return a.LogManager.get().writeMessage(a.LogCategory.USERMEDIA, "[constraints]", a.LogLevel.TRACE, JSON.stringify(e)), navigator.mediaDevices.getUserMedia(e)
                }
            }, {
                key: "getRTCStats",
                value: function (e) {
                    return new Promise(function (t, n) {
                        e.getStats(null).then(function (e) {
                            var n = [];
                            e.forEach(function (e) {
                                "inboundrtp" != e.type && "outboundrtp" != e.type || n.push(e)
                            }), t(n)
                        }).catch(n)
                    })
                }
            }, {
                key: "getUserMedia",
                value: function (e) {
                    return navigator.mediaDevices.getUserMedia(e)
                }
            }, {
                key: "getDTMFSender",
                value: function (e, t) {
                    var n = /Firefox\/([0-9\.]+)(?:\s|$)/,
                        r = navigator.userAgent;
                    if (n.test(r)) {
                        if (+n.exec(r)[1].split(".")[0] >= 53) {
                            var o = e.getSenders().map(function (e) {
                                if (e.track && "audio" === e.track.kind && e.dtmf) return e.dtmf
                            });
                            if (o.length > 0) return o[0]
                        }
                    }
                    return new i.SignalingDTMFSender(t)
                }
            }]), e
        }();
    t.FF = s
}, function (e, t, n) {
    "use strict";

    function r(e, t) {
        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }
    var o = function () {
        function e(e, t) {
            for (var n = 0; n < t.length; n++) {
                var r = t[n];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
            }
        }
        return function (t, n, r) {
            return n && e(t.prototype, n), r && e(t, r), t
        }
    }();
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var i = n(18),
        a = n(1),
        s = n(0),
        c = function () {
            function e() {
                r(this, e)
            }
            return o(e, [{
                key: "_traceName",
                value: function () {
                    return "Webkit"
                }
            }], [{
                key: "attachStream",
                value: function (e, t) {
                    try {
                        if (t.srcObject = e, t.load(), t instanceof HTMLVideoElement) t.play().catch(function (e) { });
                        else {
                            t.play().catch(function (e) { });
                            var n = a.Client.getInstance()._defaultSinkId;
                            null != n && t.setSinkId(n)
                        }
                    } catch (e) {
                        s.LogManager.get().writeMessage(s.LogCategory.USERMEDIA, "Webkit: ", s.LogLevel.WARNING, JSON.stringify(e))
                    }
                }
            }, {
                key: "detachStream",
                value: function (e) {
                    if (e.srcObject = null, e instanceof HTMLVideoElement) {
                        var t = e.pause();
                        void 0 !== t && t.catch(function (e) { })
                    } else e.pause();
                    e.src = ""
                }
            }, {
                key: "getDTMFSender",
                value: function (e, t) {
                    if (!e.createDTMFSender) return new i.SignalingDTMFSender(t);
                    var n = [];
                    return e.getLocalStreams().forEach(function (e) {
                        e.getAudioTracks().forEach(function (e) {
                            n.push(e)
                        })
                    }), n.length ? e.createDTMFSender(n[0]) : void 0
                }
            }, {
                key: "getUserMedia",
                value: function (e) {
                    return navigator.mediaDevices.getUserMedia(e)
                }
            }, {
                key: "screenSharingSupported",
                value: function () {
                    return new Promise(function (e, t) {
                        var n = function t(n) {
                            n.origin === window.location.origin && "VoximplantWebsdkExtensionLoaded" === n.data && (window.removeEventListener("message", t), clearTimeout(r), e(!0))
                        };
                        window.addEventListener("message", n), window.postMessage("VoximplantWebsdkCheckExtension", "*");
                        var r = setTimeout(function () {
                            window.removeEventListener("message", n), e(!1)
                        }, 800)
                    })
                }
            }, {
                key: "getScreenMedia",
                value: function () {
                    return new Promise(function (e, t) {
                        window.postMessage("voximplantWebsdkGetSourceId", "*");
                        var n = function n(r) {
                            if (r.data && r.origin === window.location.origin && r.data.result) {
                                if ("err" === r.data.result) return t(new Error(r.data.reason));
                                if ("ok" === r.data.result && void 0 !== r.data.sourceId) {
                                    window.removeEventListener("message", n);
                                    var o = {
                                        audio: !1,
                                        video: {
                                            mandatory: {
                                                chromeMediaSource: "desktop",
                                                maxWidth: screen.width > 1920 ? screen.width : 1920,
                                                maxHeight: screen.height > 1080 ? screen.height : 1080,
                                                chromeMediaSourceId: r.data.sourceId
                                            },
                                            optional: [{
                                                googTemporalLayeredScreencast: !0
                                            }]
                                        }
                                    };
                                    s.LogManager.get().writeMessage(s.LogCategory.USERMEDIA, "[constraints]", s.LogLevel.TRACE, JSON.stringify(o)), navigator.mediaDevices.getUserMedia(o).then(e, t)
                                }
                            }
                        };
                        window.addEventListener("message", n)
                    })
                }
            }, {
                key: "getRTCStats",
                value: function (e) {
                    return new Promise(function (t, n) {
                        var r = [];
                        e.getStats(null).then(function (e) {
                            e.forEach(function (e) {
                                "outbound-rtp" != e.type && "inbound-rtp" != e.type || r.push(e)
                            }), t(r)
                        }).catch(n)
                    })
                }
            }]), e
        }();
    t.Webkit = c
}, function (e, t, n) {
    "use strict";

    function r(e, t) {
        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }

    function o(e, t) {
        if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
        return !t || "object" != typeof t && "function" != typeof t ? e : t
    }

    function i(e, t) {
        if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
        e.prototype = Object.create(t && t.prototype, {
            constructor: {
                value: e,
                enumerable: !1,
                writable: !0,
                configurable: !0
            }
        }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
    }
    var a = function () {
        function e(e, t) {
            for (var n = 0; n < t.length; n++) {
                var r = t[n];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
            }
        }
        return function (t, n, r) {
            return n && e(t.prototype, n), r && e(t, r), t
        }
    }(),
        s = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
            return typeof e
        } : function (e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        },
        c = function (e, t, n, r) {
            var o, i = arguments.length,
                a = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
            if ("object" === ("undefined" == typeof Reflect ? "undefined" : s(Reflect)) && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, n, r);
            else
                for (var c = e.length - 1; c >= 0; c--)(o = e[c]) && (a = (i < 3 ? o(a) : i > 3 ? o(t, n, a) : o(t, n)) || a);
            return i > 3 && a && Object.defineProperty(t, n, a), a
        };
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var l, u = n(25),
        d = n(8),
        g = n(9),
        f = n(0),
        p = n(2),
        v = n(5),
        h = n(3),
        y = n(55),
        m = n(6),
        C = n(22),
        _ = n(12),
        S = n(21),
        E = n(1),
        L = n(38),
        M = n(4),
        R = n(20);
    ! function (e) {
        e[e.offer = "offer"] = "offer", e[e.answer = "answer"] = "answer", e[e.pranswer = "pranswer"] = "pranswer", e[e.rollback = "rollback"] = "rollback"
    }(l || (l = {}));
    var T;
    ! function (e) {
        e[e.controlling = "controlling"] = "controlling", e[e.controlled = "controlled"] = "controlled"
    }(T || (T = {}));
    var b = function (e) {
        function t(e, n, i) {
            r(this, t);
            var a = o(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this, e, n, i));
            a.iceTimer = null, a.needTransportRestart = !0, a.ICE_TIMEOUT = 2e4, a.RENEGOTIATION_TIMEOUT = 15e3, a._canReInvite = function () {
                return "connected" === a.impl.iceConnectionState || "completed" === a.impl.iceConnectionState
            };
            var s = g.PCFactory.get().iceConfig,
                c = s;
            if (void 0 !== c && null !== c || (c = {
                gatherPolicy: "all",
                iceServers: []
            }), c.bundlePolicy = "max-compat", a.impl = new RTCPeerConnection(c), a.impl.getTransceivers && "firefox" === d.default.getWSVendor() && (g.PCFactory.hasTransceivers = !0), void 0 !== a.impl.ontrack ? a.impl.ontrack = function (e) {
                return a.onAddTrack(e)
            } : void 0 !== a.impl.onaddtrack ? a.impl.onaddtrack = function (e) {
                return a.onAddTrack(e)
            } : a.impl.onaddstream = function (e) {
                return a.onAddStream(e)
            }, a.impl.onicecandidate = function (e) {
                a.onICECandidate(e.candidate)
            }, a.impl.oniceconnectionstatechange = function (e) {
                "completed" !== a.impl.iceConnectionState && "connected" !== a.impl.iceConnectionState || (a.iceTimer && clearTimeout(a.iceTimer), a.iceTimer = null, a.reInviteQ && a.reInviteQ.runNext())
            }, a.rtpSenders = [], a.renegotiationInProgress = !1, a.impl.onnegotiationneeded = function (e) {
                return a.onRenegotiation()
            }, a.impl.onsignalingstatechange = function (e) {
                return a.onSignalingStateChange()
            }, a.impl.oniceconnectionstatechange = function (e) {
                return a.onConnectionChange()
            }, a.iceRole = T.controlling, a._remoteStreams = [], a.banReinviteAnswer = !1, a._call = v.CallManager.get().calls[a.id], void 0 !== a._call ? a.onHold = !a._call.active() : a.onHold = !1, a.rtcCollectingCycle = setInterval(function () {
                a.getPCStats()
            }, v.CallManager.get().rtcStatsCollectionInterval), void 0 !== a._call) {
                var l = a._call.headers()[_.Constants.CALLSTATSIOID_HEADER];
                void 0 === l && (l = a._call.id()), C.CallstatsIo.get().addNewFabric(a.impl, a._call.number(), i ? C.CallstatsIoFabricUsage.multiplex : C.CallstatsIoFabricUsage.audio, l)
            }
            return a.needTransportRestart = !1, "_default" !== e && v.CallManager.get().calls[e] && (a.reInviteQ = new L.ReInviteQ(v.CallManager.get().calls[e], a._canReInvite)), a
        }
        return i(t, e), a(t, [{
            key: "onSignalingStateChange",
            value: function () {
                this.log.info("Signal state changed to " + this.impl.signalingState + " for PC:" + this.id), this.impl.signalingState
            }
        }, {
            key: "getPCStats",
            value: function () {
                var e = this;
                d.default.getRTCStats(this.impl).then(function (t) {
                    void 0 !== e._call && e._call.dispatchEvent({
                        name: "RTCStatsReceived",
                        stats: t
                    })
                })
            }
        }, {
            key: "onConnectionChange",
            value: function () {
                "completed" === this.impl.iceConnectionState && void 0 !== this._call && this._call.dispatchEvent({
                    name: "ICECompleted",
                    call: this._call
                }), "completed" !== this.impl.iceConnectionState && "connected" !== this.impl.iceConnectionState || (this.iceTimer && clearTimeout(this.iceTimer), this.iceTimer = null, this.reInviteQ && this.reInviteQ.runNext())
            }
        }, {
            key: "onRenegotiation",
            value: function () {
                var e = this;
                if (void 0 !== this.impl) {
                    if ("disconnected" === this.impl.connectionState || "failed" === this.impl.connectionState) return void this.log.info("Renegotiation requested on closed PeerConnection");
                    if (null === this.impl.localDescription) return void this.log.info("Renegotiation needed, but no local SD, skipping");
                    if ("connected" !== this.impl.iceConnectionState && "completed" !== this.impl.iceConnectionState) return this.log.info("Renegotiation requested while ice state is " + this.impl.iceConnectionState + ". Postponing"), void setTimeout(this.onRenegotiation, 100);
                    if (this.renegotiationInProgress) return void this.log.info("Renegotiation in progress. Queueing");
                    if (this.log.info("Renegotiation started"), !1 === this.renegotiationInProgress) {
                        this.renegotiationInProgress = !0;
                        var t = {};
                        this.impl.getTransceivers && "firefox" === d.default.getWSVendor() || (t = this.getReceiveOptions()), this.updateHoldState(), this.impl.createOffer(t).then(function (t) {
                            return e.codecRearrange(t)
                        }).then(function (t) {
                            var n = {
                                type: t.type,
                                sdp: t.sdp
                            };
                            return n = g.PCFactory.get().addBandwidthParams(n), n = S.SDPMuggle.removeTelephoneEvents(n), n = S.SDPMuggle.removeDoubleOpus(n), n = S.SDPMuggle.fixVideoRecieve(n, e.videoEnabled.receiveVideo), E.Client.getInstance().config().experiments && E.Client.getInstance().config().experiments.removeTransportCC && (n = S.SDPMuggle.removeTransportCC(n)), E.Client.getInstance().config().experiments && E.Client.getInstance().config().experiments.xas && (n = S.SDPMuggle.addXAS(n)), n
                        }).then(function (t) {
                            return e.srcLocalSDP = t.sdp, e.pendingOffer = t, t
                        }).then(function () {
                            var t = {
                                tracks: e.getTrackKind()
                            };
                            p.VoxSignaling.get().callRemoteFunction(h.RemoteFunction.reInvite, e._call.id(), {}, e.pendingOffer.sdp, t)
                        }).catch(function (t) {
                            e.log.error("Error when renegatiation start " + t.message)
                        })
                    } else this.log.error("Another renegatiation in progress")
                }
            }
        }, {
            key: "getReceiveOptions",
            value: function () {
                return {
                    offerToReceiveAudio: this.onHold ? 0 : 1,
                    offerToReceiveVideo: this.videoEnabled.receiveVideo && !this.onHold ? 1 : 0
                }
            }
        }, {
            key: "updateHoldState",
            value: function () {
                var e = this;
                this.impl.getLocalStreams().forEach(function (t) {
                    t.getTracks().forEach(function (t) {
                        t.enabled = !e.onHold
                    })
                }), this.impl.getRemoteStreams().forEach(function (t) {
                    t.getTracks().forEach(function (t) {
                        t.enabled = !e.onHold
                    })
                })
            }
        }, {
            key: "onICECandidate",
            value: function (e) {
                e && null !== e ? this.sendLocalCandidateToPeer("a=" + e.candidate, e.sdpMLineIndex) : this.log.info("End of candidates")
            }
        }, {
            key: "onAddTrack",
            value: function (e) {
                var t = new MediaStream([e.track]);
                this._call && R.EndpointManager.get().mediaStreamAdded(this._call, t)
            }
        }, {
            key: "onAddStream",
            value: function (e) {
                this._call && (R.EndpointManager.get().mediaStreamAdded(this._call, e.stream), e.stream.ontrack = this.onAddTrack)
            }
        }, {
            key: "checkMediaAttached",
            value: function (e) {
                return this._remoteStreams.length > 0 && this._remoteStreams.reduce(function (t, n) {
                    return "audio" === e ? t && n.getAudioTracks().length > 0 : "video" === e ? t && n.getVideoTracks().length > 0 : t
                }, !0)
            }
        }, {
            key: "_processRemoteAnswer",
            value: function (e, t) {
                var n = this;
                if (this.iceTimer = setTimeout(function () {
                    n._call.notifyICETimeout()
                }, this.ICE_TIMEOUT), this.pendingEvent = [e, t], null === this.impl.remoteDescription || "" == this.impl.remoteDescription.sdp) {
                    var r = {
                        sdp: t,
                        type: l.answer
                    };
                    return this.srcRemoteSDP = t, r = S.SDPMuggle.removeTIAS(r), this.impl.setRemoteDescription(r)
                }
            }
        }, {
            key: "_getLocalOffer",
            value: function () {
                var e = this;
                return this.iceRole = T.controlling, new Promise(function (t, n) {
                    var r = e.getReceiveOptions();
                    e.impl.createOffer(r).then(function (t) {
                        var n = {
                            type: t.type,
                            sdp: t.sdp
                        };
                        return n = g.PCFactory.get().addBandwidthParams(n), E.Client.getInstance().config().experiments && E.Client.getInstance().config().experiments.removeTransportCC && (n = S.SDPMuggle.removeTransportCC(n)), E.Client.getInstance().config().experiments && E.Client.getInstance().config().experiments.xas && (n = S.SDPMuggle.addXAS(n)), e.codecRearrange(n)
                    }).then(function (t) {
                        return e.srcLocalSDP = t.sdp, e.impl.setLocalDescription(t)
                    }).then(function () {
                        t(e.impl.localDescription)
                    }).catch(function (e) {
                        n(e)
                    })
                })
            }
        }, {
            key: "_getLocalAnswer",
            value: function () {
                var e = this;
                return this.iceRole = T.controlled, new Promise(function (t, n) {
                    var r = {
                        mandatory: e.getReceiveOptions()
                    };
                    e.impl.createAnswer(r).then(function (t) {
                        var n = {
                            type: t.type,
                            sdp: t.sdp
                        };
                        return n = g.PCFactory.get().addBandwidthParams(n), n = S.SDPMuggle.fixVideoRecieve(n, e._call.settings.videoDirections.receiveVideo), E.Client.getInstance().config().experiments && E.Client.getInstance().config().experiments.removeTransportCC && (n = S.SDPMuggle.removeTransportCC(n)), E.Client.getInstance().config().experiments && E.Client.getInstance().config().experiments.xas && (n = S.SDPMuggle.addXAS(n)), e.codecRearrange(n)
                    }).then(function (t) {
                        return e.srcLocalSDP = t.sdp, e.impl.setLocalDescription(t)
                    }).then(function () {
                        t({
                            type: l.answer,
                            sdp: e.impl.localDescription.sdp
                        })
                    }).catch(function (e) {
                        n(e)
                    })
                })
            }
        }, {
            key: "_setRemoteDescription",
            value: function (e) {
                var t = new RTCSessionDescription({
                    sdp: e,
                    type: l.offer
                });
                return t = S.SDPMuggle.removeTIAS(t), E.Client.getInstance().config().experiments && E.Client.getInstance().config().experiments.removeTransportCC && (t = S.SDPMuggle.removeTransportCC(t)), this.srcRemoteSDP = e, this.impl.setRemoteDescription(t)
            }
        }, {
            key: "_processRemoteOffer",
            value: function (e) {
                var t = this;
                return this.iceRole = T.controlled, new Promise(function (n, r) {
                    var o = new RTCSessionDescription({
                        sdp: e,
                        type: l.offer
                    });
                    E.Client.getInstance().config().experiments && E.Client.getInstance().config().experiments.removeTransportCC && (o = S.SDPMuggle.removeTransportCC(o)), t.srcRemoteSDP = e, o = S.SDPMuggle.removeTIAS(o), t.impl.setRemoteDescription(o).then(function () {
                        var e = {
                            mandatory: t.getReceiveOptions()
                        };
                        return t.impl.createAnswer(e)
                    }).then(function (e) {
                        return E.Client.getInstance().config().experiments && E.Client.getInstance().config().experiments.removeTransportCC && (e = S.SDPMuggle.removeTransportCC(e)), E.Client.getInstance().config().experiments && E.Client.getInstance().config().experiments.xas && (e = S.SDPMuggle.addXAS(e)), t.codecRearrange(e)
                    }).then(function (e) {
                        return t.srcLocalSDP = e.sdp, t.impl.setLocalDescription(e)
                    }).then(function () {
                        n(t.impl.localDescription.sdp)
                    }).catch(function (e) {
                        r(e)
                    })
                })
            }
        }, {
            key: "_close",
            value: function () {
                var e = this;
                clearInterval(this.rtcCollectingCycle), this.impl.onnegotiationneeded = null;
                E.Client.getInstance().config();
                this.impl.removeTrack ? (this.rtpSenders.forEach(function (t) {
                    e.impl.removeTrack(t)
                }), M.default.StreamManager.get().remCallStream(this._call)) : (M.default.StreamManager.get().remCallStream(this._call), this.impl.getLocalStreams().forEach(function (t) {
                    e.impl.removeStream(t)
                })), this.impl.close(), void 0 !== this._call && C.CallstatsIo.get().sendFabricEvent(this.impl, C.CallstatsIoFabricEvent.fabricTerminated, this._call.id()), this._localStream = null, this._remoteStreams = null
            }
        }, {
            key: "_addRemoteCandidate",
            value: function (e, t) {
                var n = this;
                return new Promise(function (r, o) {
                    try {
                        n.impl.addIceCandidate(new RTCIceCandidate({
                            candidate: e.substring(2),
                            sdpMLineIndex: t
                        })).then(function () {
                            r()
                        }).catch(function () {
                            r()
                        })
                    } catch (e) {
                        r()
                    }
                })
            }
        }, {
            key: "_handleReinvite",
            value: function (e, t) {
                var n = this;
                return new Promise(function (r, o) {
                    if (n.banReinviteAnswer && o(new Error), !1 === n.renegotiationInProgress) {
                        n.renegotiationInProgress = !0;
                        var i = {
                            sdp: t,
                            type: l.offer
                        };
                        E.Client.getInstance().config().experiments && E.Client.getInstance().config().experiments.removeTransportCC && (i = S.SDPMuggle.removeTransportCC(i)), n.srcRemoteSDP = t, i = S.SDPMuggle.removeTIAS(i), n.impl.setRemoteDescription(i).then(function () {
                            var t = {
                                mandatory: n.getReceiveOptions()
                            };
                            n.impl.createAnswer(t).then(function (t) {
                                var i = {
                                    type: t.type,
                                    sdp: t.sdp
                                };
                                i = S.SDPMuggle.removeDoubleOpus(i), i = S.SDPMuggle.fixVideoRecieve(i, n.videoEnabled.receiveVideo), E.Client.getInstance().config().experiments && E.Client.getInstance().config().experiments.removeTransportCC && (i = S.SDPMuggle.removeTransportCC(i)), E.Client.getInstance().config().experiments && E.Client.getInstance().config().experiments.xas && (i = S.SDPMuggle.addXAS(i)), n.srcLocalSDP = i.sdp;
                                try {
                                    n.impl.setLocalDescription(i).then(function () {
                                        var t = {
                                            tracks: n.getTrackKind()
                                        };
                                        p.VoxSignaling.get().callRemoteFunction(h.RemoteFunction.acceptReInvite, n._call.id(), e, n.impl.localDescription.sdp, t), n.renegotiationInProgress = !1, n._call.dispatchEvent({
                                            name: m.CallEvents.Updated,
                                            result: !0,
                                            call: n._call
                                        }), n.updateHoldState(), r()
                                    })
                                } catch (e) {
                                    n.renegotiationInProgress = !1, o(e)
                                }
                            })
                        })
                    } else if (!0 === n.renegotiationInProgress) {
                        var a = {
                            sdp: t,
                            type: l.answer
                        };
                        n.renegotiationInProgress = !1, E.Client.getInstance().config().experiments && E.Client.getInstance().config().experiments.removeTransportCC && (a = S.SDPMuggle.removeTransportCC(a)), E.Client.getInstance().config().experiments && E.Client.getInstance().config().experiments.xas && (a = S.SDPMuggle.addXAS(a)), n.srcRemoteSDP = t, a = S.SDPMuggle.removeTIAS(a), n.impl.setLocalDescription(n.pendingOffer).then(function () {
                            try {
                                n.impl.setRemoteDescription(a).then(function () {
                                    n._call.dispatchEvent({
                                        name: m.CallEvents.Updated,
                                        result: !0,
                                        call: n._call
                                    }), n.updateHoldState(), r()
                                })
                            } catch (e) {
                                n._call.dispatchEvent({
                                    name: m.CallEvents.Updated,
                                    result: !1,
                                    call: n._call
                                }), n.renegotiationInProgress = !1, n.log.error(JSON.stringify(e)), o(e)
                            }
                            clearTimeout(n.renegotiationTimer)
                        })
                    } else o(new Error("Universe was broken!"))
                })
            }
        }, {
            key: "codecRearrange",
            value: function (e) {
                var t = this;
                return new Promise(function (n, r) {
                    var o = v.CallManager.get().calls[t.id];
                    if (void 0 !== o) {
                        var i = new y.CodecSorter(e.sdp),
                            a = i.getUserCodecList();
                        void 0 !== o.rearangeCodecs ? o.rearangeCodecs(a, o.settings.incoming).then(function (t) {
                            i.setUserCodecList(t), n({
                                type: e.type,
                                sdp: i.getSDP()
                            })
                        }, function (e) {
                            t.log.error(JSON.stringify(e)), r(e)
                        }) : (t.log.info("No sdp transformer registered"), i.setUserCodecList(a), n({
                            type: e.type,
                            sdp: i.getSDP()
                        }))
                    } else n(e)
                })
            }
        }, {
            key: "_sendDTMF",
            value: function (e, t, n) {
                void 0 !== this.dtmfSender && this.dtmfSender.insertDTMF(e, t, n)
            }
        }, {
            key: "_hold",
            value: function (e) {
                C.CallstatsIo.get().sendFabricEvent(this.impl, e ? C.CallstatsIoFabricEvent.fabricHold : C.CallstatsIoFabricEvent.fabricResume, this._call.id()), this.onHold = e, this.impl.getTransceivers && "firefox" === d.default.getWSVendor() ? this.impl.getTransceivers().forEach(function (t) {
                    t.direction = e ? "sendonly" : "sendrecv"
                }) : this.onRenegotiation()
            }
        }, {
            key: "_getDirections",
            value: function () {
                var e = {};
                return e.local = S.SDPMuggle.detectDirections(this.impl.localDescription.sdp), e.remote = S.SDPMuggle.detectDirections(this.impl.remoteDescription.sdp), e
            }
        }, {
            key: "_getStreamActivity",
            value: function () {
                var e = {};
                return e.local = this.getMediaActivity(this.impl.getLocalStreams()), e.remote = this.getMediaActivity(this.impl.getRemoteStreams()), e
            }
        }, {
            key: "getMediaActivity",
            value: function (e) {
                return e.map(function (e) {
                    return e.getTracks().map(function (e) {
                        return {
                            id: e.id,
                            kind: e.kind,
                            mutted: e.muted,
                            active: e.enabled,
                            label: e.label
                        }
                    })
                })
            }
        }, {
            key: "_hdnFRSPrep",
            value: function () {
                this.banReinviteAnswer = !0
            }
        }, {
            key: "_hdnFRS",
            value: function () {
                this.renegotiationInProgress = !1, this.onRenegotiation()
            }
        }, {
            key: "_traceName",
            value: function () {
                return "WebRTCPC"
            }
        }, {
            key: "hasLocalAudio",
            value: function () {
                return this.impl.getLocalStreams().some(function (e) {
                    return !!e.getAudioTracks().length
                })
            }
        }, {
            key: "hasLocalVideo",
            value: function () {
                var e = this;
                return this.impl.getLocalStreams().some(function (t) {
                    return t.getVideoTracks().some(function (t) {
                        return !e.shareScreenMedia || !e.shareScreenMedia.some(function (e) {
                            return e.getTracks().some(function (e) {
                                return e.id === t.id
                            })
                        })
                    })
                })
            }
        }, {
            key: "enableVideo",
            value: function (e) {
                var t = this;
                this.impl.getLocalStreams().forEach(function (n) {
                    n.getVideoTracks().forEach(function (n) {
                        t.shareScreenMedia && t.shareScreenMedia.some(function (e) {
                            return e.getTracks().some(function (e) {
                                return e.id === n.id
                            })
                        }) || (n.enabled = e)
                    })
                })
            }
        }, {
            key: "getTransceivers",
            value: function () {
                return this.impl.getTransceivers ? this.impl.getTransceivers() : []
            }
        }, {
            key: "getRemoteDescription",
            value: function () {
                return this.impl.remoteDescription && this.impl.remoteDescription.sdp ? this.impl.remoteDescription.sdp : ""
            }
        }, {
            key: "_addCustomMedia",
            value: function (e) {
                var t = this;
                if (e && ("firefox" === d.default.getWSVendor() ? e.getTracks().forEach(function (e) {
                    var n = new MediaStream([e]);
                    t.rtpSenders.push(t.impl.addTrack(e, n)), t.onRenegotiation()
                }) : this.impl.addStream(e), !this.dtmfSender && this._call)) {
                    var n = d.default.getDTMFSender(this.impl, this._call.id());
                    n && (this.dtmfSender = n)
                }
            }
        }, {
            key: "_removeCustomMedia",
            value: function (e) {
                var t = this;
                if (e)
                    if ("firefox" === d.default.getWSVendor())
                        if (this.impl.getTransceivers) {
                            var n = this.impl.getTransceivers();
                            n.forEach(function (t) {
                                console.error(t.sender.track.id), console.error(e.getTracks()), console.error(e.getTracks().find(function (e) {
                                    return e.id === t.sender.track.id
                                })), e.getTracks().find(function (e) {
                                    return e.id === t.sender.track.id
                                }) && console.error("Transceiver stopped"), t.stop()
                            })
                        } else this.impl.getSenders().forEach(function (n) {
                            -1 !== e.getTracks().indexOf(n.track.id) && t.impl.removeTrack(n)
                        });
                    else this.impl.removeStream(e)
            }
        }, {
            key: "_fixFFSoundBug",
            value: function () {
                var e = this;
                "firefox" === d.default.getWSVendor() && g.PCFactory.hasTransceivers && setTimeout(function () {
                    if (e.impl.getSenders) {
                        e.impl.getSenders().forEach(function (e) {
                            if (e.replaceTrack) {
                                var t = e.track;
                                e.replaceTrack(t)
                            }
                        })
                    }
                }, 1e3)
            }
        }]), t
    }(u.PeerConnection);
    c([f.LogManager.d_trace(f.LogCategory.RTC)], b.prototype, "onSignalingStateChange", null), c([f.LogManager.d_trace(f.LogCategory.RTC)], b.prototype, "getPCStats", null), c([f.LogManager.d_trace(f.LogCategory.RTC)], b.prototype, "onConnectionChange", null), c([f.LogManager.d_trace(f.LogCategory.RTC)], b.prototype, "onRenegotiation", null), c([f.LogManager.d_trace(f.LogCategory.RTC)], b.prototype, "getReceiveOptions", null), c([f.LogManager.d_trace(f.LogCategory.RTC)], b.prototype, "updateHoldState", null), c([f.LogManager.d_trace(f.LogCategory.RTC)], b.prototype, "onICECandidate", null), c([f.LogManager.d_trace(f.LogCategory.RTC)], b.prototype, "onAddTrack", null), c([f.LogManager.d_trace(f.LogCategory.RTC)], b.prototype, "onAddStream", null), c([f.LogManager.d_trace(f.LogCategory.RTC)], b.prototype, "checkMediaAttached", null), c([f.LogManager.d_trace(f.LogCategory.RTC)], b.prototype, "_processRemoteAnswer", null), c([f.LogManager.d_trace(f.LogCategory.RTC)], b.prototype, "_getLocalOffer", null), c([f.LogManager.d_trace(f.LogCategory.RTC)], b.prototype, "_getLocalAnswer", null), c([f.LogManager.d_trace(f.LogCategory.RTC)], b.prototype, "_setRemoteDescription", null), c([f.LogManager.d_trace(f.LogCategory.RTC)], b.prototype, "_processRemoteOffer", null), c([f.LogManager.d_trace(f.LogCategory.RTC)], b.prototype, "_close", null), c([f.LogManager.d_trace(f.LogCategory.RTC)], b.prototype, "_addRemoteCandidate", null), c([f.LogManager.d_trace(f.LogCategory.RTC)], b.prototype, "_handleReinvite", null), c([f.LogManager.d_trace(f.LogCategory.RTC)], b.prototype, "codecRearrange", null), c([f.LogManager.d_trace(f.LogCategory.RTC)], b.prototype, "_sendDTMF", null), c([f.LogManager.d_trace(f.LogCategory.RTC)], b.prototype, "_hold", null), c([f.LogManager.d_trace(f.LogCategory.RTC)], b.prototype, "_getStreamActivity", null), c([f.LogManager.d_trace(f.LogCategory.RTC)], b.prototype, "getMediaActivity", null), c([f.LogManager.d_trace(f.LogCategory.RTC)], b.prototype, "_hdnFRSPrep", null), c([f.LogManager.d_trace(f.LogCategory.RTC)], b.prototype, "_hdnFRS", null), c([f.LogManager.d_trace(f.LogCategory.RTC)], b.prototype, "hasLocalAudio", null), c([f.LogManager.d_trace(f.LogCategory.RTC)], b.prototype, "hasLocalVideo", null), c([f.LogManager.d_trace(f.LogCategory.RTC)], b.prototype, "enableVideo", null), c([f.LogManager.d_trace(f.LogCategory.RTC)], b.prototype, "_addCustomMedia", null), c([f.LogManager.d_trace(f.LogCategory.RTC)], b.prototype, "_removeCustomMedia", null), c([f.LogManager.d_trace(f.LogCategory.RTC)], b.prototype, "_fixFFSoundBug", null), t.WebRTCPC = b
}, function (e, t, n) {
    "use strict";
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var r = n(33);
    t.AudioDeviceManager = r.AudioDeviceManager;
    var o = n(34);
    t.CameraManager = o.CameraManager;
    var i = n(26);
    t.StreamManager = i.StreamManager
}, function (e, t, n) {
    "use strict";

    function r(e, t) {
        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }

    function o(e, t) {
        if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
        return !t || "object" != typeof t && "function" != typeof t ? e : t
    }

    function i(e, t) {
        if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
        e.prototype = Object.create(t && t.prototype, {
            constructor: {
                value: e,
                enumerable: !1,
                writable: !0,
                configurable: !0
            }
        }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
    }
    var a = function () {
        function e(e, t) {
            for (var n = 0; n < t.length; n++) {
                var r = t[n];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
            }
        }
        return function (t, n, r) {
            return n && e(t.prototype, n), r && e(t, r), t
        }
    }(),
        s = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
            return typeof e
        } : function (e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        },
        c = function (e, t, n, r) {
            var o, i = arguments.length,
                a = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
            if ("object" === ("undefined" == typeof Reflect ? "undefined" : s(Reflect)) && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, n, r);
            else
                for (var c = e.length - 1; c >= 0; c--)(o = e[c]) && (a = (i < 3 ? o(a) : i > 3 ? o(t, n, a) : o(t, n)) || a);
            return i > 3 && a && Object.defineProperty(t, n, a), a
        };
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var l = n(36),
        u = n(0),
        d = n(21),
        g = function (e) {
            function t() {
                return r(this, t), o(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this))
            }
            return i(t, e), a(t, [{
                key: "getEndpointByTrackId",
                value: function (e, t) {
                    var n = this.getRtrackId(e, t);
                    return this._getEndpointByTrackId(e, n)
                }
            }, {
                key: "getRtrackId",
                value: function (e, t) {
                    if (void 0 === e.peerConnection) return "";
                    for (var n = e.peerConnection.getTransceivers(), r = 0; r < n.length; r++)
                        if (null !== n[r].mid && n[r].receiver.track.id === t) {
                            var o = d.SDPMuggle.findTrackByMid(e.peerConnection.getRemoteDescription(), n[r].mid);
                            if (o) return o
                        }
                }
            }, {
                key: "getMediaTypeTrackId",
                value: function (e, t) {
                    var n = this.getRtrackId(e, t);
                    return this._getMediaTypeTrackId(e, n)
                }
            }, {
                key: "_traceName",
                value: function () {
                    return "TransceiversEndpointManager"
                }
            }]), t
        }(l.AbstractEndpointManager);
    c([u.LogManager.d_trace(u.LogCategory.ENDPOINT)], g.prototype, "getEndpointByTrackId", null), c([u.LogManager.d_trace(u.LogCategory.ENDPOINT)], g.prototype, "getMediaTypeTrackId", null), t.TransceiversEndpointManager = g
}, function (e, t, n) {
    (function (e, r) {
        function o(e, n) {
            var r = {
                seen: [],
                stylize: a
            };
            return arguments.length >= 3 && (r.depth = arguments[2]), arguments.length >= 4 && (r.colors = arguments[3]), v(n) ? r.showHidden = n : n && t._extend(r, n), S(r.showHidden) && (r.showHidden = !1), S(r.depth) && (r.depth = 2), S(r.colors) && (r.colors = !1), S(r.customInspect) && (r.customInspect = !0), r.colors && (r.stylize = i), c(r, e, r.depth)
        }

        function i(e, t) {
            var n = o.styles[t];
            return n ? "[" + o.colors[n][0] + "m" + e + "[" + o.colors[n][1] + "m" : e
        }

        function a(e, t) {
            return e
        }

        function s(e) {
            var t = {};
            return e.forEach(function (e, n) {
                t[e] = !0
            }), t
        }

        function c(e, n, r) {
            if (e.customInspect && n && T(n.inspect) && n.inspect !== t.inspect && (!n.constructor || n.constructor.prototype !== n)) {
                var o = n.inspect(r, e);
                return C(o) || (o = c(e, o, r)), o
            }
            var i = l(e, n);
            if (i) return i;
            var a = Object.keys(n),
                v = s(a);
            if (e.showHidden && (a = Object.getOwnPropertyNames(n)), R(n) && (a.indexOf("message") >= 0 || a.indexOf("description") >= 0)) return u(n);
            if (0 === a.length) {
                if (T(n)) {
                    var h = n.name ? ": " + n.name : "";
                    return e.stylize("[Function" + h + "]", "special")
                }
                if (E(n)) return e.stylize(RegExp.prototype.toString.call(n), "regexp");
                if (M(n)) return e.stylize(Date.prototype.toString.call(n), "date");
                if (R(n)) return u(n)
            }
            var y = "",
                m = !1,
                _ = ["{", "}"];
            if (p(n) && (m = !0, _ = ["[", "]"]), T(n)) {
                y = " [Function" + (n.name ? ": " + n.name : "") + "]"
            }
            if (E(n) && (y = " " + RegExp.prototype.toString.call(n)), M(n) && (y = " " + Date.prototype.toUTCString.call(n)), R(n) && (y = " " + u(n)), 0 === a.length && (!m || 0 == n.length)) return _[0] + y + _[1];
            if (r < 0) return E(n) ? e.stylize(RegExp.prototype.toString.call(n), "regexp") : e.stylize("[Object]", "special");
            e.seen.push(n);
            var S;
            return S = m ? d(e, n, r, v, a) : a.map(function (t) {
                return g(e, n, r, v, t, m)
            }), e.seen.pop(), f(S, y, _)
        }

        function l(e, t) {
            if (S(t)) return e.stylize("undefined", "undefined");
            if (C(t)) {
                var n = "'" + JSON.stringify(t).replace(/^"|"$/g, "").replace(/'/g, "\\'").replace(/\\"/g, '"') + "'";
                return e.stylize(n, "string")
            }
            return m(t) ? e.stylize("" + t, "number") : v(t) ? e.stylize("" + t, "boolean") : h(t) ? e.stylize("null", "null") : void 0
        }

        function u(e) {
            return "[" + Error.prototype.toString.call(e) + "]"
        }

        function d(e, t, n, r, o) {
            for (var i = [], a = 0, s = t.length; a < s; ++a) w(t, String(a)) ? i.push(g(e, t, n, r, String(a), !0)) : i.push("");
            return o.forEach(function (o) {
                o.match(/^\d+$/) || i.push(g(e, t, n, r, o, !0))
            }), i
        }

        function g(e, t, n, r, o, i) {
            var a, s, l;
            if (l = Object.getOwnPropertyDescriptor(t, o) || {
                value: t[o]
            }, l.get ? s = l.set ? e.stylize("[Getter/Setter]", "special") : e.stylize("[Getter]", "special") : l.set && (s = e.stylize("[Setter]", "special")), w(r, o) || (a = "[" + o + "]"), s || (e.seen.indexOf(l.value) < 0 ? (s = h(n) ? c(e, l.value, null) : c(e, l.value, n - 1), s.indexOf("\n") > -1 && (s = i ? s.split("\n").map(function (e) {
                return "  " + e
            }).join("\n").substr(2) : "\n" + s.split("\n").map(function (e) {
                return "   " + e
            }).join("\n"))) : s = e.stylize("[Circular]", "special")), S(a)) {
                if (i && o.match(/^\d+$/)) return s;
                a = JSON.stringify("" + o), a.match(/^"([a-zA-Z_][a-zA-Z_0-9]*)"$/) ? (a = a.substr(1, a.length - 2), a = e.stylize(a, "name")) : (a = a.replace(/'/g, "\\'").replace(/\\"/g, '"').replace(/(^"|"$)/g, "'"), a = e.stylize(a, "string"))
            }
            return a + ": " + s
        }

        function f(e, t, n) {
            var r = 0;
            return e.reduce(function (e, t) {
                return r++ , t.indexOf("\n") >= 0 && r++ , e + t.replace(/\u001b\[\d\d?m/g, "").length + 1
            }, 0) > 60 ? n[0] + ("" === t ? "" : t + "\n ") + " " + e.join(",\n  ") + " " + n[1] : n[0] + t + " " + e.join(", ") + " " + n[1]
        }

        function p(e) {
            return Array.isArray(e)
        }

        function v(e) {
            return "boolean" == typeof e
        }

        function h(e) {
            return null === e
        }

        function y(e) {
            return null == e
        }

        function m(e) {
            return "number" == typeof e
        }

        function C(e) {
            return "string" == typeof e
        }

        function _(e) {
            return "symbol" == typeof e
        }

        function S(e) {
            return void 0 === e
        }

        function E(e) {
            return L(e) && "[object RegExp]" === P(e)
        }

        function L(e) {
            return "object" == typeof e && null !== e
        }

        function M(e) {
            return L(e) && "[object Date]" === P(e)
        }

        function R(e) {
            return L(e) && ("[object Error]" === P(e) || e instanceof Error)
        }

        function T(e) {
            return "function" == typeof e
        }

        function b(e) {
            return null === e || "boolean" == typeof e || "number" == typeof e || "string" == typeof e || "symbol" == typeof e || void 0 === e
        }

        function P(e) {
            return Object.prototype.toString.call(e)
        }

        function I(e) {
            return e < 10 ? "0" + e.toString(10) : e.toString(10)
        }

        function k() {
            var e = new Date,
                t = [I(e.getHours()), I(e.getMinutes()), I(e.getSeconds())].join(":");
            return [e.getDate(), N[e.getMonth()], t].join(" ")
        }

        function w(e, t) {
            return Object.prototype.hasOwnProperty.call(e, t)
        }
        var O = /%[sdj%]/g;
        t.format = function (e) {
            if (!C(e)) {
                for (var t = [], n = 0; n < arguments.length; n++) t.push(o(arguments[n]));
                return t.join(" ")
            }
            for (var n = 1, r = arguments, i = r.length, a = String(e).replace(O, function (e) {
                if ("%%" === e) return "%";
                if (n >= i) return e;
                switch (e) {
                    case "%s":
                        return String(r[n++]);
                    case "%d":
                        return Number(r[n++]);
                    case "%j":
                        try {
                            return JSON.stringify(r[n++])
                        } catch (e) {
                            return "[Circular]"
                        }
                    default:
                        return e
                }
            }), s = r[n]; n < i; s = r[++n]) h(s) || !L(s) ? a += " " + s : a += " " + o(s);
            return a
        }, t.deprecate = function (n, o) {
            function i() {
                if (!a) {
                    if (r.throwDeprecation) throw new Error(o);
                    r.traceDeprecation ? console.trace(o) : console.error(o), a = !0
                }
                return n.apply(this, arguments)
            }
            if (S(e.process)) return function () {
                return t.deprecate(n, o).apply(this, arguments)
            };
            if (!0 === r.noDeprecation) return n;
            var a = !1;
            return i
        };
        var A, D = {};
        t.debuglog = function (e) {
            if (S(A) && (A = r.env.NODE_DEBUG || ""), e = e.toUpperCase(), !D[e])
                if (new RegExp("\\b" + e + "\\b", "i").test(A)) {
                    var n = r.pid;
                    D[e] = function () {
                        var r = t.format.apply(t, arguments);
                        console.error("%s %d: %s", e, n, r)
                    }
                } else D[e] = function () { };
            return D[e]
        }, t.inspect = o, o.colors = {
            bold: [1, 22],
            italic: [3, 23],
            underline: [4, 24],
            inverse: [7, 27],
            white: [37, 39],
            grey: [90, 39],
            black: [30, 39],
            blue: [34, 39],
            cyan: [36, 39],
            green: [32, 39],
            magenta: [35, 39],
            red: [31, 39],
            yellow: [33, 39]
        }, o.styles = {
            special: "cyan",
            number: "yellow",
            boolean: "yellow",
            undefined: "grey",
            null: "bold",
            string: "green",
            date: "magenta",
            regexp: "red"
        }, t.isArray = p, t.isBoolean = v, t.isNull = h, t.isNullOrUndefined = y, t.isNumber = m, t.isString = C, t.isSymbol = _, t.isUndefined = S, t.isRegExp = E, t.isObject = L, t.isDate = M, t.isError = R, t.isFunction = T, t.isPrimitive = b, t.isBuffer = n(49);
        var N = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        t.log = function () {
            console.log("%s - %s", k(), t.format.apply(t, arguments))
        }, t.inherits = n(50), t._extend = function (e, t) {
            if (!t || !L(t)) return e;
            for (var n = Object.keys(t), r = n.length; r--;) e[n[r]] = t[n[r]];
            return e
        }
    }).call(t, n(37), n(48))
}, function (e, t) {
    function n() {
        throw new Error("setTimeout has not been defined")
    }

    function r() {
        throw new Error("clearTimeout has not been defined")
    }

    function o(e) {
        if (u === setTimeout) return setTimeout(e, 0);
        if ((u === n || !u) && setTimeout) return u = setTimeout, setTimeout(e, 0);
        try {
            return u(e, 0)
        } catch (t) {
            try {
                return u.call(null, e, 0)
            } catch (t) {
                return u.call(this, e, 0)
            }
        }
    }

    function i(e) {
        if (d === clearTimeout) return clearTimeout(e);
        if ((d === r || !d) && clearTimeout) return d = clearTimeout, clearTimeout(e);
        try {
            return d(e)
        } catch (t) {
            try {
                return d.call(null, e)
            } catch (t) {
                return d.call(this, e)
            }
        }
    }

    function a() {
        v && f && (v = !1, f.length ? p = f.concat(p) : h = -1, p.length && s())
    }

    function s() {
        if (!v) {
            var e = o(a);
            v = !0;
            for (var t = p.length; t;) {
                for (f = p, p = []; ++h < t;) f && f[h].run();
                h = -1, t = p.length
            }
            f = null, v = !1, i(e)
        }
    }

    function c(e, t) {
        this.fun = e, this.array = t
    }

    function l() { }
    var u, d, g = e.exports = {};
    ! function () {
        try {
            u = "function" == typeof setTimeout ? setTimeout : n
        } catch (e) {
            u = n
        }
        try {
            d = "function" == typeof clearTimeout ? clearTimeout : r
        } catch (e) {
            d = r
        }
    }();
    var f, p = [],
        v = !1,
        h = -1;
    g.nextTick = function (e) {
        var t = new Array(arguments.length - 1);
        if (arguments.length > 1)
            for (var n = 1; n < arguments.length; n++) t[n - 1] = arguments[n];
        p.push(new c(e, t)), 1 !== p.length || v || o(s)
    }, c.prototype.run = function () {
        this.fun.apply(null, this.array)
    }, g.title = "browser", g.browser = !0, g.env = {}, g.argv = [], g.version = "", g.versions = {}, g.on = l, g.addListener = l, g.once = l, g.off = l, g.removeListener = l, g.removeAllListeners = l, g.emit = l, g.prependListener = l, g.prependOnceListener = l, g.listeners = function (e) {
        return []
    }, g.binding = function (e) {
        throw new Error("process.binding is not supported")
    }, g.cwd = function () {
        return "/"
    }, g.chdir = function (e) {
        throw new Error("process.chdir is not supported")
    }, g.umask = function () {
        return 0
    }
}, function (e, t) {
    e.exports = function (e) {
        return e && "object" == typeof e && "function" == typeof e.copy && "function" == typeof e.fill && "function" == typeof e.readUInt8
    }
}, function (e, t) {
    "function" == typeof Object.create ? e.exports = function (e, t) {
        e.super_ = t, e.prototype = Object.create(t.prototype, {
            constructor: {
                value: e,
                enumerable: !1,
                writable: !0,
                configurable: !0
            }
        })
    } : e.exports = function (e, t) {
        e.super_ = t;
        var n = function () { };
        n.prototype = t.prototype, e.prototype = new n, e.prototype.constructor = e
    }
}, function (e, t, n) {
    "use strict";

    function r(e, t) {
        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }

    function o(e, t) {
        if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
        return !t || "object" != typeof t && "function" != typeof t ? e : t
    }

    function i(e, t) {
        if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
        e.prototype = Object.create(t && t.prototype, {
            constructor: {
                value: e,
                enumerable: !1,
                writable: !0,
                configurable: !0
            }
        }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
    }
    var a = function () {
        function e(e, t) {
            for (var n = 0; n < t.length; n++) {
                var r = t[n];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
            }
        }
        return function (t, n, r) {
            return n && e(t.prototype, n), r && e(t, r), t
        }
    }();
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var s = n(36),
        c = function (e) {
            function t() {
                return r(this, t), o(this, (t.__proto__ || Object.getPrototypeOf(t)).apply(this, arguments))
            }
            return i(t, e), a(t, [{
                key: "_traceName",
                value: function () {
                    return "PlainEndpointManager"
                }
            }, {
                key: "getEndpointByTrackId",
                value: function (e, t) {
                    return this._getEndpointByTrackId(e, t)
                }
            }, {
                key: "getMediaTypeTrackId",
                value: function (e, t) {
                    return this._getMediaTypeTrackId(e, t)
                }
            }]), t
        }(s.AbstractEndpointManager);
    t.PlainEndpointManager = c
}, function (e, t, n) {
    "use strict";

    function r(e, t) {
        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }

    function o(e, t) {
        if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
        return !t || "object" != typeof t && "function" != typeof t ? e : t
    }

    function i(e, t) {
        if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
        e.prototype = Object.create(t && t.prototype, {
            constructor: {
                value: e,
                enumerable: !1,
                writable: !0,
                configurable: !0
            }
        }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
    }
    var a = function () {
        function e(e, t) {
            for (var n = 0; n < t.length; n++) {
                var r = t[n];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
            }
        }
        return function (t, n, r) {
            return n && e(t.prototype, n), r && e(t, r), t
        }
    }(),
        s = function e(t, n, r) {
            null === t && (t = Function.prototype);
            var o = Object.getOwnPropertyDescriptor(t, n);
            if (void 0 === o) {
                var i = Object.getPrototypeOf(t);
                return null === i ? void 0 : e(i, n, r)
            }
            if ("value" in o) return o.value;
            var a = o.get;
            if (void 0 !== a) return a.call(r)
        },
        c = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
            return typeof e
        } : function (e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        },
        l = function (e, t, n, r) {
            var o, i = arguments.length,
                a = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
            if ("object" === ("undefined" == typeof Reflect ? "undefined" : c(Reflect)) && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, n, r);
            else
                for (var s = e.length - 1; s >= 0; s--)(o = e[s]) && (a = (i < 3 ? o(a) : i > 3 ? o(t, n, a) : o(t, n)) || a);
            return i > 3 && a && Object.defineProperty(t, n, a), a
        };
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var u = n(19),
        d = n(2),
        g = n(5),
        f = n(0),
        p = n(3),
        v = function (e) {
            function t(e, n, i, a) {
                r(this, t);
                var s = o(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this, e, n, i, a));
                return s.settings.mode = u.CallMode.SERVER, s
            }
            return i(t, e), a(t, [{
                key: "answer",
                value: function (e, n) {
                    s(t.prototype.__proto__ || Object.getPrototypeOf(t.prototype), "answer", this).call(this, e, n);
                    var r = {
                        tracks: this.peerConnection.getTrackKind()
                    };
                    d.VoxSignaling.get().callRemoteFunction(p.RemoteFunction.acceptCall, this.settings.id, g.CallManager.cleanHeaders(n), r)
                }
            }, {
                key: "_traceName",
                value: function () {
                    return "CallExServer"
                }
            }]), t
        }(u.Call);
    l([f.LogManager.d_trace(f.LogCategory.CALLEXSERVER)], v.prototype, "answer", null), t.CallExServer = v
}, function (e, t, n) {
    "use strict";

    function r(e, t) {
        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }

    function o(e, t) {
        if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
        return !t || "object" != typeof t && "function" != typeof t ? e : t
    }

    function i(e, t) {
        if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
        e.prototype = Object.create(t && t.prototype, {
            constructor: {
                value: e,
                enumerable: !1,
                writable: !0,
                configurable: !0
            }
        }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
    }
    var a = function () {
        function e(e, t) {
            for (var n = 0; n < t.length; n++) {
                var r = t[n];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
            }
        }
        return function (t, n, r) {
            return n && e(t.prototype, n), r && e(t, r), t
        }
    }(),
        s = function e(t, n, r) {
            null === t && (t = Function.prototype);
            var o = Object.getOwnPropertyDescriptor(t, n);
            if (void 0 === o) {
                var i = Object.getPrototypeOf(t);
                return null === i ? void 0 : e(i, n, r)
            }
            if ("value" in o) return o.value;
            var a = o.get;
            if (void 0 !== a) return a.call(r)
        },
        c = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
            return typeof e
        } : function (e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        },
        l = function (e, t, n, r) {
            var o, i = arguments.length,
                a = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
            if ("object" === ("undefined" == typeof Reflect ? "undefined" : c(Reflect)) && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, n, r);
            else
                for (var s = e.length - 1; s >= 0; s--)(o = e[s]) && (a = (i < 3 ? o(a) : i > 3 ? o(t, n, a) : o(t, n)) || a);
            return i > 3 && a && Object.defineProperty(t, n, a), a
        };
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var u = n(19),
        d = n(2),
        g = n(0),
        f = n(3),
        p = n(12),
        v = n(1),
        h = n(4),
        y = n(6),
        m = function (e) {
            function t() {
                return r(this, t), o(this, (t.__proto__ || Object.getPrototypeOf(t)).apply(this, arguments))
            }
            return i(t, e), a(t, [{
                key: "answer",
                value: function (e, n, r) {
                    var o = this;
                    s(t.prototype.__proto__ || Object.getPrototypeOf(t.prototype), "answer", this).call(this, e, n), void 0 !== e && (void 0 !== n && "object" === (void 0 === n ? "undefined" : c(n)) || (n = {}), n[p.Constants.CALL_DATA_HEADER] = e);
                    v.Client.getInstance().config();
                    return new Promise(function (e, t) {
                        "object" === (void 0 === r ? "undefined" : c(r)) && (o.settings.videoDirections = r), h.default.StreamManager.get().getCallStream(o).then(function (t) {
                            o._peerConnection.fastAddCustomMedia(t), o._peerConnection.getLocalAnswer().then(function (t) {
                                var r = {
                                    tracks: o.peerConnection.getTrackKind()
                                };
                                d.VoxSignaling.get().callRemoteFunction(f.RemoteFunction.acceptCall, o.id(), n, t.sdp, r), o._peerConnection._fixFFSoundBug(), e()
                            })
                        }).catch(function (e) {
                            return t(e)
                        })
                    })
                }
            }, {
                key: "setActive",
                value: function (e) {
                    var t = this;
                    return e === this.settings.active ? new Promise(function (e, n) {
                        n({
                            name: y.CallEvents.Updated,
                            result: !1,
                            call: t
                        })
                    }) : (this.settings.active = e, this.peerConnection.hold(!e))
                }
            }, {
                key: "_traceName",
                value: function () {
                    return "CallExMedia"
                }
            }]), t
        }(u.Call);
    l([g.LogManager.d_trace(g.LogCategory.CALL)], m.prototype, "answer", null), l([g.LogManager.d_trace(g.LogCategory.CALL)], m.prototype, "setActive", null), t.CallExMedia = m
}, function (e, t, n) {
    "use strict";

    function r(e, t) {
        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }
    var o = function () {
        function e(e, t) {
            for (var n = 0; n < t.length; n++) {
                var r = t[n];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
            }
        }
        return function (t, n, r) {
            return n && e(t.prototype, n), r && e(t, r), t
        }
    }(),
        i = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
            return typeof e
        } : function (e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        },
        a = function (e, t, n, r) {
            var o, a = arguments.length,
                s = a < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
            if ("object" === ("undefined" == typeof Reflect ? "undefined" : i(Reflect)) && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r);
            else
                for (var c = e.length - 1; c >= 0; c--)(o = e[c]) && (s = (a < 3 ? o(s) : a > 3 ? o(t, n, s) : o(t, n)) || s);
            return a > 3 && s && Object.defineProperty(t, n, s), s
        };
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var s = n(0),
        c = function () {
            function e() {
                r(this, e)
            }
            return o(e, [{
                key: "_traceName",
                value: function () {
                    return "CodecSorterHelpers"
                }
            }], [{
                key: "H264Sorter",
                value: function (e, t) {
                    return t ? new Promise(function (t) {
                        for (var n = 0; n < e.sections.length; n++)
                            if ("video" == e.sections[n].kind.toLowerCase()) {
                                var r = e.sections[n].codec.filter(function (e) {
                                    return -1 != e.toLowerCase().indexOf("h264") && -1 == e.toLowerCase().indexOf("uc")
                                });
                                r.length && (e.sections[n].codec = r)
                            }
                        t(e)
                    }) : new Promise(function (t) {
                        for (var n = 0; n < e.sections.length; n++) "video" == e.sections[n].kind.toLowerCase() && e.sections[n].codec.sort(function (e, t) {
                            return -1 != e.toLowerCase().indexOf("h264") && -1 == e.toLowerCase().indexOf("uc") ? -1 : -1 != t.toLowerCase().indexOf("h264") && -1 == t.toLowerCase().indexOf("uc") ? 1 : 0
                        });
                        t(e)
                    })
                }
            }, {
                key: "VP8Sorter",
                value: function (e, t) {
                    return new Promise(function (t, n) {
                        for (var r = 0; r < e.sections.length; r++) "video" == e.sections[r].kind.toLowerCase() && e.sections[r].codec.sort(function (e, t) {
                            return -1 != e.toLowerCase().indexOf("vp8") ? -1 : -1 != t.toLowerCase().indexOf("vp8") ? 1 : 0
                        });
                        t(e)
                    })
                }
            }]), e
        }();
    a([s.LogManager.d_trace(s.LogCategory.RTC)], c, "H264Sorter", null), a([s.LogManager.d_trace(s.LogCategory.RTC)], c, "VP8Sorter", null), t.CodecSorterHelpers = c
}, function (e, t, n) {
    "use strict";

    function r(e, t) {
        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }
    var o = function () {
        function e(e, t) {
            for (var n = 0; n < t.length; n++) {
                var r = t[n];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
            }
        }
        return function (t, n, r) {
            return n && e(t.prototype, n), r && e(t, r), t
        }
    }();
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var i = function () {
        function e(t) {
            r(this, e), this.originalSDP = t
        }
        return o(e, [{
            key: "getCodecList",
            value: function () {
                this.originalCodecList = {
                    prefix: "",
                    sections: [],
                    sufix: ""
                };
                var t = RegExp.prototype.test.bind(/^([a-z])=(.*)/),
                    n = e.splitSections(this.originalSDP);
                this.originalCodecList.prefix = n[0];
                for (var r = 1; r < n.length; r++) {
                    var o = {
                        kind: "audio",
                        firstLine: "",
                        prefix: "",
                        sufix: "",
                        codec: []
                    },
                        i = n[r].split("\na=rtpmap");
                    i = i.map(function (e, t) {
                        return (t > 0 ? "a=rtpmap" + e : e).trim() + "\r\n"
                    }), o.prefix = i.shift();
                    var a = i.pop();
                    a = a.split(/(\r\n|\r|\n)/).filter(t);
                    var s = !0;
                    for (i.push(""); s;)
                        if (s = !1, 0 !== a.length) {
                            var c = a.shift();
                            0 === c.indexOf("a=rtpmap") || 0 === c.indexOf("a=rtcp-fb") || 0 === c.indexOf("a=fmtp") || 0 === c.indexOf("a=x-caps") || 0 === c.indexOf("a=maxptime") ? (i[i.length - 1] += c + "\r\n", s = !0) : a.unshift(c)
                        }
                    for (var l = 0; l < i.length; l++) o.codec.push(i[l].split(/(\r\n|\r|\n)/).filter(t));
                    var u = o.prefix.split(/(\r\n|\r|\n)/).filter(t);
                    o.firstLine = u.shift();
                    var d = o.firstLine.split(" ");
                    d.splice(-1 * o.codec.length, o.codec.length), o.kind = d[0].substring(2), o.prefix = u.join("\r\n") + "\r\n", o.firstLine = d.join(" "), a.length > 0 && (o.sufix = a.join("\r\n") + "\r\n"), this.originalCodecList.sections.push(o)
                }
                return this.originalCodecList
            }
        }, {
            key: "getUserCodecList",
            value: function () {
                void 0 === this.originalCodecList && this.getCodecList();
                var t = {
                    sections: []
                };
                return t.sections = this.originalCodecList.sections.filter(function (e) {
                    return "video" === e.kind || "audio" === e.kind
                }).map(function (t, n, r) {
                    var o = {
                        kind: t.kind,
                        codec: t.codec.map(function (t) {
                            return e.codecToUserCodec(t)
                        })
                    },
                        i = [],
                        a = !0,
                        s = !1,
                        c = void 0;
                    try {
                        for (var l, u = o.codec[Symbol.iterator](); !(a = (l = u.next()).done); a = !0) {
                            var d = l.value; - 1 === i.indexOf(d) && i.push(d)
                        }
                    } catch (e) {
                        s = !0, c = e
                    } finally {
                        try {
                            !a && u.return && u.return()
                        } finally {
                            if (s) throw c
                        }
                    }
                    return o.codec = i, o
                }), t
            }
        }, {
            key: "setUserCodecList",
            value: function (t) {
                void 0 === this.originalCodecList && this.getCodecList();
                for (var n = 0; n < t.sections.length; n++) t.sections[n].kind === this.originalCodecList.sections[n].kind && (this.originalCodecList.sections[n].codec = e.resortSection(t.sections[n].codec, this.originalCodecList.sections[n].codec))
            }
        }, {
            key: "getSDP",
            value: function () {
                for (var e = this.originalCodecList.prefix, t = 0; t < this.originalCodecList.sections.length; t++) {
                    for (var n = "", r = [], o = 0; o < this.originalCodecList.sections[t].codec.length; o++) r.push(this.originalCodecList.sections[t].codec[o][0].split(" ")[0].substring(9)), n += this.originalCodecList.sections[t].codec[o].join("\r\n") + "\r\n";
                    e += this.originalCodecList.sections[t].firstLine + " " + r.join(" ") + "\r\n", e += this.originalCodecList.sections[t].prefix, e += n, e += this.originalCodecList.sections[t].sufix
                }
                return e
            }
        }, {
            key: "_traceName",
            value: function () {
                return "CodecSorter"
            }
        }], [{
            key: "splitSections",
            value: function (e) {
                return e.split("\nm=").map(function (e, t) {
                    return (t > 0 ? "m=" + e : e).trim() + "\r\n"
                })
            }
        }, {
            key: "codecToUserCodec",
            value: function (e) {
                var t = e[0].split(" ");
                return t.shift(), t.join(" ")
            }
        }, {
            key: "resortSection",
            value: function (t, n) {
                for (var r = [], o = 0; o < t.length; o++)
                    for (var i = 0; i < n.length; i++) t[o] === e.codecToUserCodec(n[i]) && r.push(n[i]);
                return r
            }
        }, {
            key: "downOpusBandwidth",
            value: function (e) {
                return new Promise(function (t, n) {
                    for (var r = RegExp.prototype.test.bind(/^([a-z])=(.*)/), o = e.sdp.split(/(\r\n|\r|\n)/).filter(r), i = !1, a = 0; a < o.length; a++) - 1 !== o[a].indexOf("a=fmtp:114") && (o[a] = "a=fmtp:114 minptime=10; useinbandfec=1; sprop-maxcapturerate=8000", i = !0), -1 !== o[a].indexOf("a=fmtp:111") && (o[a] = "a=fmtp:111 minptime=10; useinbandfec=1; sprop-maxcapturerate=8000", i = !0);
                    i || n(e), e.sdp = o.join("\r\n") + "\r\n", t(e)
                })
            }
        }]), e
    }();
    t.CodecSorter = i
}, function (e, t, n) {
    "use strict";

    function r(e, t) {
        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }
    var o = function () {
        function e(e, t) {
            for (var n = 0; n < t.length; n++) {
                var r = t[n];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
            }
        }
        return function (t, n, r) {
            return n && e(t.prototype, n), r && e(t, r), t
        }
    }();
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var i = function () {
        function e() {
            r(this, e)
        }
        return o(e, [{
            key: "_traceName",
            value: function () {
                return "Edge"
            }
        }], [{
            key: "attachStream",
            value: function (e, t) {
                t.srcObject = e, t.play()
            }
        }, {
            key: "detachStream",
            value: function (e) {
                e.pause(), e.src = ""
            }
        }, {
            key: "getScreenMedia",
            value: function () {
                return navigator.getDisplayMedia ? navigator.getDisplayMedia() : new Promise(function (e, t) {
                    t(new Error("Screen sharing not allowed for you platform"))
                })
            }
        }, {
            key: "getRTCStats",
            value: function (e) {
                return new Promise(function (e, t) {
                    t(new Error("RTCStats sharing not allowed for you platform"))
                })
            }
        }, {
            key: "screenSharingSupported",
            value: function () {
                return new Promise(function (e, t) {
                    if (navigator.getDisplayMedia) return void e(!0);
                    e(!1)
                })
            }
        }]), e
    }();
    t.Edge = i
}, function (e, t, n) {
    "use strict";

    function r(e, t) {
        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }
    var o = function () {
        function e(e, t) {
            for (var n = 0; n < t.length; n++) {
                var r = t[n];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
            }
        }
        return function (t, n, r) {
            return n && e(t.prototype, n), r && e(t, r), t
        }
    }();
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var i = n(18),
        a = function () {
            function e() {
                r(this, e)
            }
            return o(e, [{
                key: "_traceName",
                value: function () {
                    return "Safari"
                }
            }], [{
                key: "attachStream",
                value: function (e, t) {
                    t.srcObject = e
                }
            }, {
                key: "detachStream",
                value: function (e) {
                    if (e instanceof HTMLVideoElement) {
                        var t = e.pause();
                        void 0 !== t && t.catch(function (e) { })
                    } else e.pause();
                    e.src = ""
                }
            }, {
                key: "getDTMFSender",
                value: function (e, t) {
                    return e.createDTMFSender ? e.createDTMFSender(e.getLocalStreams()[0].getAudioTracks()[0]) : new i.SignalingDTMFSender(t)
                }
            }, {
                key: "getScreenMedia",
                value: function () {
                    return new Promise(function (e, t) {
                        window.postMessage("get-sourceId", "*"), window.addEventListener("message", function (n) {
                            if (n.origin == window.location.origin && ("PermissionDeniedError" == n.data && t(new Error("PermissionDeniedError")), "string" != typeof n.data && void 0 !== n.data.sourceId)) {
                                var r = {
                                    audio: !1,
                                    video: {
                                        mandatory: {
                                            chromeMediaSource: "desktop",
                                            maxWidth: screen.width > 1920 ? screen.width : 1920,
                                            maxHeight: screen.height > 1080 ? screen.height : 1080,
                                            chromeMediaSourceId: n.data.sourceId
                                        },
                                        optional: [{
                                            googTemporalLayeredScreencast: !0
                                        }]
                                    }
                                };
                                navigator.mediaDevices.getUserMedia(r).then(function (t) {
                                    e(t)
                                }).catch(function (e) {
                                    t(e)
                                })
                            }
                        })
                    })
                }
            }, {
                key: "getRTCStats",
                value: function (e) {
                    return new Promise(function (t, n) {
                        var r = [];
                        e.getStats(null).then(function (e) {
                            e.forEach(function (e) {
                                if ("ssrc" == e.type) {
                                    var t = {};
                                    t.id = e.id, t.type = e.type, t.timestamp = e.timestamp, r.push(t)
                                }
                            }), t(r)
                        }).catch(n)
                    })
                }
            }]), e
        }();
    t.Safari = a
}, function (e, t, n) {
    "use strict";

    function r(e, t) {
        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }
    var o = function () {
        function e(e, t) {
            for (var n = 0; n < t.length; n++) {
                var r = t[n];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
            }
        }
        return function (t, n, r) {
            return n && e(t.prototype, n), r && e(t, r), t
        }
    }();
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var i = n(17),
        a = n(5),
        s = n(2),
        c = n(3),
        l = n(11),
        u = n(10),
        d = n(6),
        g = function () {
            function e(t) {
                var n = this;
                r(this, e), this.client = t, this.currentCall = null, this.onConnectionFailed = null, this.onConnectionEstablished = null, this.onCheckComplete = null, this.onCallFailed = null, this.onCallConnected = null, this.onCallEnded = null, this.onCallRinging = null, this.onCallMediaStarted = null, this.onVoicemail = null, this.onNetStatsReceived = null, a.CallManager.get().protocolVersion, t.on(i.Events.ConnectionFailed, function (e) {
                    return n.runLegacyCallback(n.onConnectionFailed, e)
                }), t.on(i.Events.ConnectionEstablished, function (e) {
                    return n.runLegacyCallback(n.onConnectionEstablished, e)
                }), s.VoxSignaling.get().setRPCHandler(l.RemoteEvent.handlePreFlightCheckResult, function (e, t, r) {
                    return n.onCheckComplete(e, t, r)
                }), s.VoxSignaling.get().setRPCHandler(l.RemoteEvent.handleVoicemail, function (e) {
                    return n.runLegacyCallback(n.onVoicemail, e)
                })
            }
            return o(e, [{
                key: "connectTo",
                value: function (e, t, n, r) {
                    var o = s.VoxSignaling.get();
                    u.Authenticator.get().ziAuthorized(!0), o.lagacyConnectTo(e, t, n, r)
                }
            }, {
                key: "connect",
                value: function () { }
            }, {
                key: "requestMedia",
                value: function (e, t, n, r) { }
            }, {
                key: "hangupCall",
                value: function (e, t) {
                    a.CallManager.get().calls[e].hangup(t), s.VoxSignaling.get().callRemoteFunction(c.RemoteFunction.disconnectCall, e, {})
                }
            }, {
                key: "callTo",
                value: function (e, t, n, r) {
                    return this.currentCall = this.client.call({
                        number: e,
                        video: t,
                        extraHeaders: n,
                        extraParams: r
                    }), this.bindCurrentCall(), this.currentCall.id()
                }
            }, {
                key: "voicemailPromptFinished",
                value: function (e) {
                    s.VoxSignaling.get().callRemoteFunction(c.RemoteFunction.zPromptFinished, e)
                }
            }, {
                key: "makeid",
                value: function (e) {
                    for (var t = "", n = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789", r = 0; r < e; r++) t += n.charAt(Math.floor(Math.random() * n.length));
                    return t
                }
            }, {
                key: "muteMicrophone",
                value: function (e) {
                    var t = a.CallManager.get();
                    for (var n in t.calls) t.calls.hasOwnProperty(n) && (e ? t.calls[n].muteMicrophone() : t.calls[n].unmuteMicrophone())
                }
            }, {
                key: "sendDigit",
                value: function (e, t) {
                    s.VoxSignaling.get().callRemoteFunction(c.RemoteFunction.sendDTMF, e, t)
                }
            }, {
                key: "startPreFlightCheck",
                value: function (e, t) {
                    s.VoxSignaling.get().callRemoteFunction(c.RemoteFunction.zStartPreFlightCheck, !!e, !!t)
                }
            }, {
                key: "runLegacyCallback",
                value: function (e, t) {
                    void 0 !== e && null !== e && e(t)
                }
            }, {
                key: "bindCurrentCall",
                value: function () {
                    var e = this;
                    window.currentCall = this.currentCall, this.currentCall.on(d.CallEvents.Failed, function (t) {
                        e.runLegacyCallback(e.onCallFailed, t), e.unbindCurrentCall()
                    }), this.currentCall.on(d.CallEvents.Connected, function (t) {
                        e.runLegacyCallback(e.onCallConnected, t), e.runLegacyCallback(e.onCallMediaStarted, t);
                        a.CallManager.get();
                        setTimeout(function () {
                            var e = document.getElementById(window.currentCall.peerConnection.impl.getRemoteStreams()[0].getTracks()[0].id);
                            e.srcObject = window.currentCall.peerConnection.impl.getRemoteStreams()[0], e.load(), e.play()
                        }, 1e3)
                    }), this.currentCall.on(d.CallEvents.Disconnected, function (t) {
                        e.runLegacyCallback(e.onCallEnded, t), e.unbindCurrentCall()
                    }), this.client.on(i.Events.NetStatsReceived, function (t) {
                        return e.onNetStatsReceived(t)
                    })
                }
            }, {
                key: "unbindCurrentCall",
                value: function () {
                    this.currentCall.off(d.CallEvents.Failed), this.currentCall.off(d.CallEvents.Connected), this.currentCall.off(d.CallEvents.Disconnected), this.currentCall.off(d.CallEvents.ProgressToneStart), this.currentCall.off(d.CallEvents.Connected), this.client.off(i.Events.NetStatsReceived)
                }
            }, {
                key: "_traceName",
                value: function () {
                    return "ZingayaAPI"
                }
            }]), e
        }();
    t.ZingayaAPI = g
}, function (e, t, n) {
    "use strict";

    function r(e, t) {
        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }
    var o = function () {
        function e(e, t) {
            for (var n = 0; n < t.length; n++) {
                var r = t[n];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
            }
        }
        return function (t, n, r) {
            return n && e(t.prototype, n), r && e(t, r), t
        }
    }();
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var i = n(2),
        a = n(3),
        s = function () {
            function e() {
                r(this, e)
            }
            return o(e, [{
                key: "_traceName",
                value: function () {
                    return "PushService"
                }
            }], [{
                key: "register",
                value: function (e) {
                    return new Promise(function (t, n) {
                        i.VoxSignaling.get().callRemoteFunction(a.RemoteFunction.registerPushToken, e) ? t() : n()
                    })
                }
            }, {
                key: "unregister",
                value: function (e) {
                    return new Promise(function (t, n) {
                        i.VoxSignaling.get().callRemoteFunction(a.RemoteFunction.unregisterPushToken, e) ? t() : n()
                    })
                }
            }, {
                key: "incomingPush",
                value: function (e) {
                    return new Promise(function (t, n) {
                        i.VoxSignaling.get().callRemoteFunction(a.RemoteFunction.pushFeedback, e) ? t() : n()
                    })
                }
            }]), e
        }();
    t.PushService = s
}, function (e, t, n) {
    "use strict";
    (function (t) {
        var r = n(61);
        e.exports = r({
            window: t.window
        })
    }).call(t, n(37))
}, function (e, t, n) {
    "use strict";
    var r = n(7);
    e.exports = function (e, t) {
        var o = e && e.window,
            i = {
                shimChrome: !0,
                shimFirefox: !0,
                shimEdge: !0,
                shimSafari: !0
            };
        for (var a in t) hasOwnProperty.call(t, a) && (i[a] = t[a]);
        var s = r.log,
            c = r.detectBrowser(o),
            l = {
                browserDetails: c,
                extractVersion: r.extractVersion,
                disableLog: r.disableLog,
                disableWarnings: r.disableWarnings
            },
            u = n(62) || null,
            d = n(64) || null,
            g = n(67) || null,
            f = n(69) || null,
            p = n(70) || null;
        switch (c.browser) {
            case "chrome":
                if (!u || !u.shimPeerConnection || !i.shimChrome) return s("Chrome shim is not included in this adapter release."), l;
                s("adapter.js shimming chrome."), l.browserShim = u, p.shimCreateObjectURL(o), u.shimGetUserMedia(o), u.shimMediaStream(o), u.shimSourceObject(o), u.shimPeerConnection(o), u.shimOnTrack(o), u.shimAddTrackRemoveTrack(o), u.shimGetSendersWithDtmf(o), p.shimRTCIceCandidate(o);
                break;
            case "firefox":
                if (!g || !g.shimPeerConnection || !i.shimFirefox) return s("Firefox shim is not included in this adapter release."), l;
                s("adapter.js shimming firefox."), l.browserShim = g, p.shimCreateObjectURL(o), g.shimGetUserMedia(o), g.shimSourceObject(o), g.shimPeerConnection(o), g.shimOnTrack(o), g.shimRemoveStream(o), p.shimRTCIceCandidate(o);
                break;
            case "edge":
                if (!d || !d.shimPeerConnection || !i.shimEdge) return s("MS edge shim is not included in this adapter release."), l;
                s("adapter.js shimming edge."), l.browserShim = d, p.shimCreateObjectURL(o), d.shimGetUserMedia(o), d.shimPeerConnection(o), d.shimReplaceTrack(o);
                break;
            case "safari":
                if (!f || !i.shimSafari) return s("Safari shim is not included in this adapter release."), l;
                s("adapter.js shimming safari."), l.browserShim = f, p.shimCreateObjectURL(o), f.shimRTCIceServerUrls(o), f.shimCallbacksAPI(o), f.shimLocalStreamsAPI(o), f.shimRemoteStreamsAPI(o), f.shimTrackEventTransceiver(o), f.shimGetUserMedia(o), f.shimCreateOfferLegacy(o), p.shimRTCIceCandidate(o);
                break;
            default:
                s("Unsupported browser!")
        }
        return l
    }
}, function (e, t, n) {
    "use strict";
    var r = n(7),
        o = r.log,
        i = {
            shimMediaStream: function (e) {
                e.MediaStream = e.MediaStream || e.webkitMediaStream
            },
            shimOnTrack: function (e) {
                if ("object" == typeof e && e.RTCPeerConnection && !("ontrack" in e.RTCPeerConnection.prototype)) {
                    Object.defineProperty(e.RTCPeerConnection.prototype, "ontrack", {
                        get: function () {
                            return this._ontrack
                        },
                        set: function (e) {
                            this._ontrack && this.removeEventListener("track", this._ontrack), this.addEventListener("track", this._ontrack = e)
                        }
                    });
                    var t = e.RTCPeerConnection.prototype.setRemoteDescription;
                    e.RTCPeerConnection.prototype.setRemoteDescription = function () {
                        var n = this;
                        return n._ontrackpoly || (n._ontrackpoly = function (t) {
                            t.stream.addEventListener("addtrack", function (r) {
                                var o;
                                o = e.RTCPeerConnection.prototype.getReceivers ? n.getReceivers().find(function (e) {
                                    return e.track && e.track.id === r.track.id
                                }) : {
                                        track: r.track
                                    };
                                var i = new Event("track");
                                i.track = r.track, i.receiver = o, i.transceiver = {
                                    receiver: o
                                }, i.streams = [t.stream], n.dispatchEvent(i)
                            }), t.stream.getTracks().forEach(function (r) {
                                var o;
                                o = e.RTCPeerConnection.prototype.getReceivers ? n.getReceivers().find(function (e) {
                                    return e.track && e.track.id === r.id
                                }) : {
                                        track: r
                                    };
                                var i = new Event("track");
                                i.track = r, i.receiver = o, i.transceiver = {
                                    receiver: o
                                }, i.streams = [t.stream], n.dispatchEvent(i)
                            })
                        }, n.addEventListener("addstream", n._ontrackpoly)), t.apply(n, arguments)
                    }
                }
            },
            shimGetSendersWithDtmf: function (e) {
                if ("object" == typeof e && e.RTCPeerConnection && !("getSenders" in e.RTCPeerConnection.prototype) && "createDTMFSender" in e.RTCPeerConnection.prototype) {
                    var t = function (e, t) {
                        return {
                            track: t,
                            get dtmf() {
                                return void 0 === this._dtmf && ("audio" === t.kind ? this._dtmf = e.createDTMFSender(t) : this._dtmf = null), this._dtmf
                            },
                            _pc: e
                        }
                    };
                    if (!e.RTCPeerConnection.prototype.getSenders) {
                        e.RTCPeerConnection.prototype.getSenders = function () {
                            return this._senders = this._senders || [], this._senders.slice()
                        };
                        var n = e.RTCPeerConnection.prototype.addTrack;
                        e.RTCPeerConnection.prototype.addTrack = function (e, r) {
                            var o = this,
                                i = n.apply(o, arguments);
                            return i || (i = t(o, e), o._senders.push(i)), i
                        };
                        var r = e.RTCPeerConnection.prototype.removeTrack;
                        e.RTCPeerConnection.prototype.removeTrack = function (e) {
                            var t = this;
                            r.apply(t, arguments);
                            var n = t._senders.indexOf(e); - 1 !== n && t._senders.splice(n, 1)
                        }
                    }
                    var o = e.RTCPeerConnection.prototype.addStream;
                    e.RTCPeerConnection.prototype.addStream = function (e) {
                        var n = this;
                        n._senders = n._senders || [], o.apply(n, [e]), e.getTracks().forEach(function (e) {
                            n._senders.push(t(n, e))
                        })
                    };
                    var i = e.RTCPeerConnection.prototype.removeStream;
                    e.RTCPeerConnection.prototype.removeStream = function (e) {
                        var t = this;
                        t._senders = t._senders || [], i.apply(t, [e]), e.getTracks().forEach(function (e) {
                            var n = t._senders.find(function (t) {
                                return t.track === e
                            });
                            n && t._senders.splice(t._senders.indexOf(n), 1)
                        })
                    }
                } else if ("object" == typeof e && e.RTCPeerConnection && "getSenders" in e.RTCPeerConnection.prototype && "createDTMFSender" in e.RTCPeerConnection.prototype && e.RTCRtpSender && !("dtmf" in e.RTCRtpSender.prototype)) {
                    var a = e.RTCPeerConnection.prototype.getSenders;
                    e.RTCPeerConnection.prototype.getSenders = function () {
                        var e = this,
                            t = a.apply(e, []);
                        return t.forEach(function (t) {
                            t._pc = e
                        }), t
                    }, Object.defineProperty(e.RTCRtpSender.prototype, "dtmf", {
                        get: function () {
                            return void 0 === this._dtmf && ("audio" === this.track.kind ? this._dtmf = this._pc.createDTMFSender(this.track) : this._dtmf = null), this._dtmf
                        }
                    })
                }
            },
            shimSourceObject: function (e) {
                var t = e && e.URL;
                "object" == typeof e && (!e.HTMLMediaElement || "srcObject" in e.HTMLMediaElement.prototype || Object.defineProperty(e.HTMLMediaElement.prototype, "srcObject", {
                    get: function () {
                        return this._srcObject
                    },
                    set: function (e) {
                        var n = this;
                        if (this._srcObject = e, this.src && t.revokeObjectURL(this.src), !e) return void (this.src = "");
                        this.src = t.createObjectURL(e), e.addEventListener("addtrack", function () {
                            n.src && t.revokeObjectURL(n.src), n.src = t.createObjectURL(e)
                        }), e.addEventListener("removetrack", function () {
                            n.src && t.revokeObjectURL(n.src), n.src = t.createObjectURL(e)
                        })
                    }
                }))
            },
            shimAddTrackRemoveTrack: function (e) {
                function t(e, t) {
                    var n = t.sdp;
                    return Object.keys(e._reverseStreams || []).forEach(function (t) {
                        var r = e._reverseStreams[t],
                            o = e._streams[r.id];
                        n = n.replace(new RegExp(o.id, "g"), r.id)
                    }), new RTCSessionDescription({
                        type: t.type,
                        sdp: n
                    })
                }

                function n(e, t) {
                    var n = t.sdp;
                    return Object.keys(e._reverseStreams || []).forEach(function (t) {
                        var r = e._reverseStreams[t],
                            o = e._streams[r.id];
                        n = n.replace(new RegExp(r.id, "g"), o.id)
                    }), new RTCSessionDescription({
                        type: t.type,
                        sdp: n
                    })
                }
                var o = r.detectBrowser(e);
                if (!(e.RTCPeerConnection.prototype.addTrack && o.version >= 64)) {
                    var i = e.RTCPeerConnection.prototype.getLocalStreams;
                    e.RTCPeerConnection.prototype.getLocalStreams = function () {
                        var e = this,
                            t = i.apply(this);
                        return e._reverseStreams = e._reverseStreams || {}, t.map(function (t) {
                            return e._reverseStreams[t.id]
                        })
                    };
                    var a = e.RTCPeerConnection.prototype.addStream;
                    e.RTCPeerConnection.prototype.addStream = function (t) {
                        var n = this;
                        if (n._streams = n._streams || {}, n._reverseStreams = n._reverseStreams || {}, t.getTracks().forEach(function (e) {
                            if (n.getSenders().find(function (t) {
                                return t.track === e
                            })) throw new DOMException("Track already exists.", "InvalidAccessError")
                        }), !n._reverseStreams[t.id]) {
                            var r = new e.MediaStream(t.getTracks());
                            n._streams[t.id] = r, n._reverseStreams[r.id] = t, t = r
                        }
                        a.apply(n, [t])
                    };
                    var s = e.RTCPeerConnection.prototype.removeStream;
                    e.RTCPeerConnection.prototype.removeStream = function (e) {
                        var t = this;
                        t._streams = t._streams || {}, t._reverseStreams = t._reverseStreams || {}, s.apply(t, [t._streams[e.id] || e]), delete t._reverseStreams[t._streams[e.id] ? t._streams[e.id].id : e.id], delete t._streams[e.id]
                    }, e.RTCPeerConnection.prototype.addTrack = function (t, n) {
                        var r = this;
                        if ("closed" === r.signalingState) throw new DOMException("The RTCPeerConnection's signalingState is 'closed'.", "InvalidStateError");
                        var o = [].slice.call(arguments, 1);
                        if (1 !== o.length || !o[0].getTracks().find(function (e) {
                            return e === t
                        })) throw new DOMException("The adapter.js addTrack polyfill only supports a single  stream which is associated with the specified track.", "NotSupportedError");
                        if (r.getSenders().find(function (e) {
                            return e.track === t
                        })) throw new DOMException("Track already exists.", "InvalidAccessError");
                        r._streams = r._streams || {}, r._reverseStreams = r._reverseStreams || {};
                        var i = r._streams[n.id];
                        if (i) i.addTrack(t), Promise.resolve().then(function () {
                            r.dispatchEvent(new Event("negotiationneeded"))
                        });
                        else {
                            var a = new e.MediaStream([t]);
                            r._streams[n.id] = a, r._reverseStreams[a.id] = n, r.addStream(a)
                        }
                        return r.getSenders().find(function (e) {
                            return e.track === t
                        })
                    }, ["createOffer", "createAnswer"].forEach(function (n) {
                        var r = e.RTCPeerConnection.prototype[n];
                        e.RTCPeerConnection.prototype[n] = function () {
                            var e = this,
                                n = arguments;
                            return arguments.length && "function" == typeof arguments[0] ? r.apply(e, [function (r) {
                                var o = t(e, r);
                                n[0].apply(null, [o])
                            }, function (e) {
                                n[1] && n[1].apply(null, e)
                            }, arguments[2]]) : r.apply(e, arguments).then(function (n) {
                                return t(e, n)
                            })
                        }
                    });
                    var c = e.RTCPeerConnection.prototype.setLocalDescription;
                    e.RTCPeerConnection.prototype.setLocalDescription = function () {
                        var e = this;
                        return arguments.length && arguments[0].type ? (arguments[0] = n(e, arguments[0]), c.apply(e, arguments)) : c.apply(e, arguments)
                    };
                    var l = Object.getOwnPropertyDescriptor(e.RTCPeerConnection.prototype, "localDescription");
                    Object.defineProperty(e.RTCPeerConnection.prototype, "localDescription", {
                        get: function () {
                            var e = this,
                                n = l.get.apply(this);
                            return "" === n.type ? n : t(e, n)
                        }
                    }), e.RTCPeerConnection.prototype.removeTrack = function (e) {
                        var t = this;
                        if ("closed" === t.signalingState) throw new DOMException("The RTCPeerConnection's signalingState is 'closed'.", "InvalidStateError");
                        if (!e._pc) throw new DOMException("Argument 1 of RTCPeerConnection.removeTrack does not implement interface RTCRtpSender.", "TypeError");
                        if (e._pc !== t) throw new DOMException("Sender was not created by this connection.", "InvalidAccessError");
                        t._streams = t._streams || {};
                        var n;
                        Object.keys(t._streams).forEach(function (r) {
                            t._streams[r].getTracks().find(function (t) {
                                return e.track === t
                            }) && (n = t._streams[r])
                        }), n && (1 === n.getTracks().length ? t.removeStream(t._reverseStreams[n.id]) : n.removeTrack(e.track), t.dispatchEvent(new Event("negotiationneeded")))
                    }
                }
            },
            shimPeerConnection: function (e) {
                var t = r.detectBrowser(e);
                if (e.RTCPeerConnection) {
                    var n = e.RTCPeerConnection;
                    e.RTCPeerConnection = function (e, t) {
                        if (e && e.iceServers) {
                            for (var o = [], i = 0; i < e.iceServers.length; i++) {
                                var a = e.iceServers[i];
                                !a.hasOwnProperty("urls") && a.hasOwnProperty("url") ? (r.deprecated("RTCIceServer.url", "RTCIceServer.urls"), a = JSON.parse(JSON.stringify(a)), a.urls = a.url, o.push(a)) : o.push(e.iceServers[i])
                            }
                            e.iceServers = o
                        }
                        return new n(e, t)
                    }, e.RTCPeerConnection.prototype = n.prototype, Object.defineProperty(e.RTCPeerConnection, "generateCertificate", {
                        get: function () {
                            return n.generateCertificate
                        }
                    })
                } else e.RTCPeerConnection = function (t, n) {
                    return o("PeerConnection"), t && t.iceTransportPolicy && (t.iceTransports = t.iceTransportPolicy), new e.webkitRTCPeerConnection(t, n)
                }, e.RTCPeerConnection.prototype = e.webkitRTCPeerConnection.prototype, e.webkitRTCPeerConnection.generateCertificate && Object.defineProperty(e.RTCPeerConnection, "generateCertificate", {
                    get: function () {
                        return e.webkitRTCPeerConnection.generateCertificate
                    }
                });
                var i = e.RTCPeerConnection.prototype.getStats;
                e.RTCPeerConnection.prototype.getStats = function (e, t, n) {
                    var r = this,
                        o = arguments;
                    if (arguments.length > 0 && "function" == typeof e) return i.apply(this, arguments);
                    if (0 === i.length && (0 === arguments.length || "function" != typeof arguments[0])) return i.apply(this, []);
                    var a = function (e) {
                        var t = {};
                        return e.result().forEach(function (e) {
                            var n = {
                                id: e.id,
                                timestamp: e.timestamp,
                                type: {
                                    localcandidate: "local-candidate",
                                    remotecandidate: "remote-candidate"
                                }[e.type] || e.type
                            };
                            e.names().forEach(function (t) {
                                n[t] = e.stat(t)
                            }), t[n.id] = n
                        }), t
                    },
                        s = function (e) {
                            return new Map(Object.keys(e).map(function (t) {
                                return [t, e[t]]
                            }))
                        };
                    if (arguments.length >= 2) {
                        var c = function (e) {
                            o[1](s(a(e)))
                        };
                        return i.apply(this, [c, arguments[0]])
                    }
                    return new Promise(function (e, t) {
                        i.apply(r, [function (t) {
                            e(s(a(t)))
                        }, t])
                    }).then(t, n)
                }, t.version < 51 && ["setLocalDescription", "setRemoteDescription", "addIceCandidate"].forEach(function (t) {
                    var n = e.RTCPeerConnection.prototype[t];
                    e.RTCPeerConnection.prototype[t] = function () {
                        var e = arguments,
                            t = this,
                            r = new Promise(function (r, o) {
                                n.apply(t, [e[0], r, o])
                            });
                        return e.length < 2 ? r : r.then(function () {
                            e[1].apply(null, [])
                        }, function (t) {
                            e.length >= 3 && e[2].apply(null, [t])
                        })
                    }
                }), t.version < 52 && ["createOffer", "createAnswer"].forEach(function (t) {
                    var n = e.RTCPeerConnection.prototype[t];
                    e.RTCPeerConnection.prototype[t] = function () {
                        var e = this;
                        if (arguments.length < 1 || 1 === arguments.length && "object" == typeof arguments[0]) {
                            var t = 1 === arguments.length ? arguments[0] : void 0;
                            return new Promise(function (r, o) {
                                n.apply(e, [r, o, t])
                            })
                        }
                        return n.apply(this, arguments)
                    }
                }), ["setLocalDescription", "setRemoteDescription", "addIceCandidate"].forEach(function (t) {
                    var n = e.RTCPeerConnection.prototype[t];
                    e.RTCPeerConnection.prototype[t] = function () {
                        return arguments[0] = new ("addIceCandidate" === t ? e.RTCIceCandidate : e.RTCSessionDescription)(arguments[0]), n.apply(this, arguments)
                    }
                });
                var a = e.RTCPeerConnection.prototype.addIceCandidate;
                e.RTCPeerConnection.prototype.addIceCandidate = function () {
                    return arguments[0] ? a.apply(this, arguments) : (arguments[1] && arguments[1].apply(null), Promise.resolve())
                }
            }
        };
    e.exports = {
        shimMediaStream: i.shimMediaStream,
        shimOnTrack: i.shimOnTrack,
        shimAddTrackRemoveTrack: i.shimAddTrackRemoveTrack,
        shimGetSendersWithDtmf: i.shimGetSendersWithDtmf,
        shimSourceObject: i.shimSourceObject,
        shimPeerConnection: i.shimPeerConnection,
        shimGetUserMedia: n(63)
    }
}, function (e, t, n) {
    "use strict";
    var r = n(7),
        o = r.log;
    e.exports = function (e) {
        var t = r.detectBrowser(e),
            n = e && e.navigator,
            i = function (e) {
                if ("object" != typeof e || e.mandatory || e.optional) return e;
                var t = {};
                return Object.keys(e).forEach(function (n) {
                    if ("require" !== n && "advanced" !== n && "mediaSource" !== n) {
                        var r = "object" == typeof e[n] ? e[n] : {
                            ideal: e[n]
                        };
                        void 0 !== r.exact && "number" == typeof r.exact && (r.min = r.max = r.exact);
                        var o = function (e, t) {
                            return e ? e + t.charAt(0).toUpperCase() + t.slice(1) : "deviceId" === t ? "sourceId" : t
                        };
                        if (void 0 !== r.ideal) {
                            t.optional = t.optional || [];
                            var i = {};
                            "number" == typeof r.ideal ? (i[o("min", n)] = r.ideal, t.optional.push(i), i = {}, i[o("max", n)] = r.ideal, t.optional.push(i)) : (i[o("", n)] = r.ideal, t.optional.push(i))
                        }
                        void 0 !== r.exact && "number" != typeof r.exact ? (t.mandatory = t.mandatory || {}, t.mandatory[o("", n)] = r.exact) : ["min", "max"].forEach(function (e) {
                            void 0 !== r[e] && (t.mandatory = t.mandatory || {}, t.mandatory[o(e, n)] = r[e])
                        })
                    }
                }), e.advanced && (t.optional = (t.optional || []).concat(e.advanced)), t
            },
            a = function (e, r) {
                if (t.version >= 61) return r(e);
                if ((e = JSON.parse(JSON.stringify(e))) && "object" == typeof e.audio) {
                    var a = function (e, t, n) {
                        t in e && !(n in e) && (e[n] = e[t], delete e[t])
                    };
                    e = JSON.parse(JSON.stringify(e)), a(e.audio, "autoGainControl", "googAutoGainControl"), a(e.audio, "noiseSuppression", "googNoiseSuppression"), e.audio = i(e.audio)
                }
                if (e && "object" == typeof e.video) {
                    var s = e.video.facingMode;
                    s = s && ("object" == typeof s ? s : {
                        ideal: s
                    });
                    var c = t.version < 66;
                    if (s && ("user" === s.exact || "environment" === s.exact || "user" === s.ideal || "environment" === s.ideal) && (!n.mediaDevices.getSupportedConstraints || !n.mediaDevices.getSupportedConstraints().facingMode || c)) {
                        delete e.video.facingMode;
                        var l;
                        if ("environment" === s.exact || "environment" === s.ideal ? l = ["back", "rear"] : "user" !== s.exact && "user" !== s.ideal || (l = ["front"]), l) return n.mediaDevices.enumerateDevices().then(function (t) {
                            t = t.filter(function (e) {
                                return "videoinput" === e.kind
                            });
                            var n = t.find(function (e) {
                                return l.some(function (t) {
                                    return -1 !== e.label.toLowerCase().indexOf(t)
                                })
                            });
                            return !n && t.length && -1 !== l.indexOf("back") && (n = t[t.length - 1]), n && (e.video.deviceId = s.exact ? {
                                exact: n.deviceId
                            } : {
                                    ideal: n.deviceId
                                }), e.video = i(e.video), o("chrome: " + JSON.stringify(e)), r(e)
                        })
                    }
                    e.video = i(e.video)
                }
                return o("chrome: " + JSON.stringify(e)), r(e)
            },
            s = function (e) {
                return {
                    name: {
                        PermissionDeniedError: "NotAllowedError",
                        InvalidStateError: "NotReadableError",
                        DevicesNotFoundError: "NotFoundError",
                        ConstraintNotSatisfiedError: "OverconstrainedError",
                        TrackStartError: "NotReadableError",
                        MediaDeviceFailedDueToShutdown: "NotReadableError",
                        MediaDeviceKillSwitchOn: "NotReadableError"
                    }[e.name] || e.name,
                    message: e.message,
                    constraint: e.constraintName,
                    toString: function () {
                        return this.name + (this.message && ": ") + this.message
                    }
                }
            },
            c = function (e, t, r) {
                a(e, function (e) {
                    n.webkitGetUserMedia(e, t, function (e) {
                        r && r(s(e))
                    })
                })
            };
        n.getUserMedia = c;
        var l = function (e) {
            return new Promise(function (t, r) {
                n.getUserMedia(e, t, r)
            })
        };
        if (n.mediaDevices || (n.mediaDevices = {
            getUserMedia: l,
            enumerateDevices: function () {
                return new Promise(function (t) {
                    var n = {
                        audio: "audioinput",
                        video: "videoinput"
                    };
                    return e.MediaStreamTrack.getSources(function (e) {
                        t(e.map(function (e) {
                            return {
                                label: e.label,
                                kind: n[e.kind],
                                deviceId: e.id,
                                groupId: ""
                            }
                        }))
                    })
                })
            },
            getSupportedConstraints: function () {
                return {
                    deviceId: !0,
                    echoCancellation: !0,
                    facingMode: !0,
                    frameRate: !0,
                    height: !0,
                    width: !0
                }
            }
        }), n.mediaDevices.getUserMedia) {
            var u = n.mediaDevices.getUserMedia.bind(n.mediaDevices);
            n.mediaDevices.getUserMedia = function (e) {
                return a(e, function (e) {
                    return u(e).then(function (t) {
                        if (e.audio && !t.getAudioTracks().length || e.video && !t.getVideoTracks().length) throw t.getTracks().forEach(function (e) {
                            e.stop()
                        }), new DOMException("", "NotFoundError");
                        return t
                    }, function (e) {
                        return Promise.reject(s(e))
                    })
                })
            }
        } else n.mediaDevices.getUserMedia = function (e) {
            return l(e)
        };
        void 0 === n.mediaDevices.addEventListener && (n.mediaDevices.addEventListener = function () {
            o("Dummy mediaDevices.addEventListener called.")
        }), void 0 === n.mediaDevices.removeEventListener && (n.mediaDevices.removeEventListener = function () {
            o("Dummy mediaDevices.removeEventListener called.")
        })
    }
}, function (e, t, n) {
    "use strict";
    var r = n(7),
        o = n(65);
    e.exports = {
        shimGetUserMedia: n(66),
        shimPeerConnection: function (e) {
            var t = r.detectBrowser(e);
            if (e.RTCIceGatherer && (e.RTCIceCandidate || (e.RTCIceCandidate = function (e) {
                return e
            }), e.RTCSessionDescription || (e.RTCSessionDescription = function (e) {
                return e
            }), t.version < 15025)) {
                var n = Object.getOwnPropertyDescriptor(e.MediaStreamTrack.prototype, "enabled");
                Object.defineProperty(e.MediaStreamTrack.prototype, "enabled", {
                    set: function (e) {
                        n.set.call(this, e);
                        var t = new Event("enabled");
                        t.enabled = e, this.dispatchEvent(t)
                    }
                })
            } !e.RTCRtpSender || "dtmf" in e.RTCRtpSender.prototype || Object.defineProperty(e.RTCRtpSender.prototype, "dtmf", {
                get: function () {
                    return void 0 === this._dtmf && ("audio" === this.track.kind ? this._dtmf = new e.RTCDtmfSender(this) : "video" === this.track.kind && (this._dtmf = null)), this._dtmf
                }
            }), e.RTCPeerConnection = o(e, t.version)
        },
        shimReplaceTrack: function (e) {
            !e.RTCRtpSender || "replaceTrack" in e.RTCRtpSender.prototype || (e.RTCRtpSender.prototype.replaceTrack = e.RTCRtpSender.prototype.setTrack)
        }
    }
}, function (e, t, n) {
    "use strict";

    function r(e) {
        return {
            inboundrtp: "inbound-rtp",
            outboundrtp: "outbound-rtp",
            candidatepair: "candidate-pair",
            localcandidate: "local-candidate",
            remotecandidate: "remote-candidate"
        }[e.type] || e.type
    }

    function o(e, t, n, r, o) {
        var i = u.writeRtpDescription(e.kind, t);
        if (i += u.writeIceParameters(e.iceGatherer.getLocalParameters()), i += u.writeDtlsParameters(e.dtlsTransport.getLocalParameters(), "offer" === n ? "actpass" : o || "active"), i += "a=mid:" + e.mid + "\r\n", e.rtpSender && e.rtpReceiver ? i += "a=sendrecv\r\n" : e.rtpSender ? i += "a=sendonly\r\n" : e.rtpReceiver ? i += "a=recvonly\r\n" : i += "a=inactive\r\n", e.rtpSender) {
            var a = e.rtpSender._initialTrackId || e.rtpSender.track.id;
            e.rtpSender._initialTrackId = a;
            var s = "msid:" + (r ? r.id : "-") + " " + a + "\r\n";
            i += "a=" + s, i += "a=ssrc:" + e.sendEncodingParameters[0].ssrc + " " + s, e.sendEncodingParameters[0].rtx && (i += "a=ssrc:" + e.sendEncodingParameters[0].rtx.ssrc + " " + s, i += "a=ssrc-group:FID " + e.sendEncodingParameters[0].ssrc + " " + e.sendEncodingParameters[0].rtx.ssrc + "\r\n")
        }
        return i += "a=ssrc:" + e.sendEncodingParameters[0].ssrc + " cname:" + u.localCName + "\r\n", e.rtpSender && e.sendEncodingParameters[0].rtx && (i += "a=ssrc:" + e.sendEncodingParameters[0].rtx.ssrc + " cname:" + u.localCName + "\r\n"), i
    }

    function i(e, t) {
        var n = !1;
        return e = JSON.parse(JSON.stringify(e)), e.filter(function (e) {
            if (e && (e.urls || e.url)) {
                var r = e.urls || e.url;
                e.url && !e.urls && console.warn("RTCIceServer.url is deprecated! Use urls instead.");
                var o = "string" == typeof r;
                return o && (r = [r]), r = r.filter(function (e) {
                    return 0 !== e.indexOf("turn:") || -1 === e.indexOf("transport=udp") || -1 !== e.indexOf("turn:[") || n ? 0 === e.indexOf("stun:") && t >= 14393 && -1 === e.indexOf("?transport=udp") : (n = !0, !0)
                }), delete e.url, e.urls = o ? r[0] : r, !!r.length
            }
        })
    }

    function a(e, t) {
        var n = {
            codecs: [],
            headerExtensions: [],
            fecMechanisms: []
        },
            r = function (e, t) {
                e = parseInt(e, 10);
                for (var n = 0; n < t.length; n++)
                    if (t[n].payloadType === e || t[n].preferredPayloadType === e) return t[n]
            },
            o = function (e, t, n, o) {
                var i = r(e.parameters.apt, n),
                    a = r(t.parameters.apt, o);
                return i && a && i.name.toLowerCase() === a.name.toLowerCase()
            };
        return e.codecs.forEach(function (r) {
            for (var i = 0; i < t.codecs.length; i++) {
                var a = t.codecs[i];
                if (r.name.toLowerCase() === a.name.toLowerCase() && r.clockRate === a.clockRate) {
                    if ("rtx" === r.name.toLowerCase() && r.parameters && a.parameters.apt && !o(r, a, e.codecs, t.codecs)) continue;
                    a = JSON.parse(JSON.stringify(a)), a.numChannels = Math.min(r.numChannels, a.numChannels), n.codecs.push(a), a.rtcpFeedback = a.rtcpFeedback.filter(function (e) {
                        for (var t = 0; t < r.rtcpFeedback.length; t++)
                            if (r.rtcpFeedback[t].type === e.type && r.rtcpFeedback[t].parameter === e.parameter) return !0;
                        return !1
                    });
                    break
                }
            }
        }), e.headerExtensions.forEach(function (e) {
            for (var r = 0; r < t.headerExtensions.length; r++) {
                var o = t.headerExtensions[r];
                if (e.uri === o.uri) {
                    n.headerExtensions.push(o);
                    break
                }
            }
        }), n
    }

    function s(e, t, n) {
        return -1 !== {
            offer: {
                setLocalDescription: ["stable", "have-local-offer"],
                setRemoteDescription: ["stable", "have-remote-offer"]
            },
            answer: {
                setLocalDescription: ["have-remote-offer", "have-local-pranswer"],
                setRemoteDescription: ["have-local-offer", "have-remote-pranswer"]
            }
        }[t][e].indexOf(n)
    }

    function c(e, t) {
        var n = e.getRemoteCandidates().find(function (e) {
            return t.foundation === e.foundation && t.ip === e.ip && t.port === e.port && t.priority === e.priority && t.protocol === e.protocol && t.type === e.type
        });
        return n || e.addRemoteCandidate(t), !n
    }

    function l(e, t) {
        var n = new Error(t);
        return n.name = e, n.code = {
            NotSupportedError: 9,
            InvalidStateError: 11,
            InvalidAccessError: 15,
            TypeError: void 0,
            OperationError: void 0
        }[e], n
    }
    var u = n(39);
    e.exports = function (e, t) {
        function n(t, n) {
            n.addTrack(t), n.dispatchEvent(new e.MediaStreamTrackEvent("addtrack", {
                track: t
            }))
        }

        function d(t, n) {
            n.removeTrack(t), n.dispatchEvent(new e.MediaStreamTrackEvent("removetrack", {
                track: t
            }))
        }

        function g(t, n, r, o) {
            var i = new Event("track");
            i.track = n, i.receiver = r, i.transceiver = {
                receiver: r
            }, i.streams = o, e.setTimeout(function () {
                t._dispatchEvent("track", i)
            })
        }
        var f = function (n) {
            var r = this,
                o = document.createDocumentFragment();
            if (["addEventListener", "removeEventListener", "dispatchEvent"].forEach(function (e) {
                r[e] = o[e].bind(o)
            }), this.canTrickleIceCandidates = null, this.needNegotiation = !1, this.localStreams = [], this.remoteStreams = [], this.localDescription = null, this.remoteDescription = null, this.signalingState = "stable", this.iceConnectionState = "new", this.connectionState = "new", this.iceGatheringState = "new", n = JSON.parse(JSON.stringify(n || {})), this.usingBundle = "max-bundle" === n.bundlePolicy, "negotiate" === n.rtcpMuxPolicy) throw l("NotSupportedError", "rtcpMuxPolicy 'negotiate' is not supported");
            switch (n.rtcpMuxPolicy || (n.rtcpMuxPolicy = "require"), n.iceTransportPolicy) {
                case "all":
                case "relay":
                    break;
                default:
                    n.iceTransportPolicy = "all"
            }
            switch (n.bundlePolicy) {
                case "balanced":
                case "max-compat":
                case "max-bundle":
                    break;
                default:
                    n.bundlePolicy = "balanced"
            }
            if (n.iceServers = i(n.iceServers || [], t), this._iceGatherers = [], n.iceCandidatePoolSize)
                for (var a = n.iceCandidatePoolSize; a > 0; a--) this._iceGatherers.push(new e.RTCIceGatherer({
                    iceServers: n.iceServers,
                    gatherPolicy: n.iceTransportPolicy
                }));
            else n.iceCandidatePoolSize = 0;
            this._config = n, this.transceivers = [], this._sdpSessionId = u.generateSessionId(), this._sdpSessionVersion = 0, this._dtlsRole = void 0, this._isClosed = !1
        };
        f.prototype.onicecandidate = null, f.prototype.onaddstream = null, f.prototype.ontrack = null, f.prototype.onremovestream = null, f.prototype.onsignalingstatechange = null, f.prototype.oniceconnectionstatechange = null, f.prototype.onconnectionstatechange = null, f.prototype.onicegatheringstatechange = null, f.prototype.onnegotiationneeded = null, f.prototype.ondatachannel = null, f.prototype._dispatchEvent = function (e, t) {
            this._isClosed || (this.dispatchEvent(t), "function" == typeof this["on" + e] && this["on" + e](t))
        }, f.prototype._emitGatheringStateChange = function () {
            var e = new Event("icegatheringstatechange");
            this._dispatchEvent("icegatheringstatechange", e)
        }, f.prototype.getConfiguration = function () {
            return this._config
        }, f.prototype.getLocalStreams = function () {
            return this.localStreams
        }, f.prototype.getRemoteStreams = function () {
            return this.remoteStreams
        }, f.prototype._createTransceiver = function (e, t) {
            var n = this.transceivers.length > 0,
                r = {
                    track: null,
                    iceGatherer: null,
                    iceTransport: null,
                    dtlsTransport: null,
                    localCapabilities: null,
                    remoteCapabilities: null,
                    rtpSender: null,
                    rtpReceiver: null,
                    kind: e,
                    mid: null,
                    sendEncodingParameters: null,
                    recvEncodingParameters: null,
                    stream: null,
                    associatedRemoteMediaStreams: [],
                    wantReceive: !0
                };
            if (this.usingBundle && n) r.iceTransport = this.transceivers[0].iceTransport, r.dtlsTransport = this.transceivers[0].dtlsTransport;
            else {
                var o = this._createIceAndDtlsTransports();
                r.iceTransport = o.iceTransport, r.dtlsTransport = o.dtlsTransport
            }
            return t || this.transceivers.push(r), r
        }, f.prototype.addTrack = function (t, n) {
            if (this._isClosed) throw l("InvalidStateError", "Attempted to call addTrack on a closed peerconnection.");
            if (this.transceivers.find(function (e) {
                return e.track === t
            })) throw l("InvalidAccessError", "Track already exists.");
            for (var r, o = 0; o < this.transceivers.length; o++) this.transceivers[o].track || this.transceivers[o].kind !== t.kind || (r = this.transceivers[o]);
            return r || (r = this._createTransceiver(t.kind)), this._maybeFireNegotiationNeeded(), -1 === this.localStreams.indexOf(n) && this.localStreams.push(n), r.track = t, r.stream = n, r.rtpSender = new e.RTCRtpSender(t, r.dtlsTransport), r.rtpSender
        }, f.prototype.addStream = function (e) {
            var n = this;
            if (t >= 15025) e.getTracks().forEach(function (t) {
                n.addTrack(t, e)
            });
            else {
                var r = e.clone();
                e.getTracks().forEach(function (e, t) {
                    var n = r.getTracks()[t];
                    e.addEventListener("enabled", function (e) {
                        n.enabled = e.enabled
                    })
                }), r.getTracks().forEach(function (e) {
                    n.addTrack(e, r)
                })
            }
        }, f.prototype.removeTrack = function (t) {
            if (this._isClosed) throw l("InvalidStateError", "Attempted to call removeTrack on a closed peerconnection.");
            if (!(t instanceof e.RTCRtpSender)) throw new TypeError("Argument 1 of RTCPeerConnection.removeTrack does not implement interface RTCRtpSender.");
            var n = this.transceivers.find(function (e) {
                return e.rtpSender === t
            });
            if (!n) throw l("InvalidAccessError", "Sender was not created by this connection.");
            var r = n.stream;
            n.rtpSender.stop(), n.rtpSender = null, n.track = null, n.stream = null, -1 === this.transceivers.map(function (e) {
                return e.stream
            }).indexOf(r) && this.localStreams.indexOf(r) > -1 && this.localStreams.splice(this.localStreams.indexOf(r), 1), this._maybeFireNegotiationNeeded()
        }, f.prototype.removeStream = function (e) {
            var t = this;
            e.getTracks().forEach(function (e) {
                var n = t.getSenders().find(function (t) {
                    return t.track === e
                });
                n && t.removeTrack(n)
            })
        }, f.prototype.getSenders = function () {
            return this.transceivers.filter(function (e) {
                return !!e.rtpSender
            }).map(function (e) {
                return e.rtpSender
            })
        }, f.prototype.getReceivers = function () {
            return this.transceivers.filter(function (e) {
                return !!e.rtpReceiver
            }).map(function (e) {
                return e.rtpReceiver
            })
        }, f.prototype._createIceGatherer = function (t, n) {
            var r = this;
            if (n && t > 0) return this.transceivers[0].iceGatherer;
            if (this._iceGatherers.length) return this._iceGatherers.shift();
            var o = new e.RTCIceGatherer({
                iceServers: this._config.iceServers,
                gatherPolicy: this._config.iceTransportPolicy
            });
            return Object.defineProperty(o, "state", {
                value: "new",
                writable: !0
            }), this.transceivers[t].bufferedCandidateEvents = [], this.transceivers[t].bufferCandidates = function (e) {
                var n = !e.candidate || 0 === Object.keys(e.candidate).length;
                o.state = n ? "completed" : "gathering", null !== r.transceivers[t].bufferedCandidateEvents && r.transceivers[t].bufferedCandidateEvents.push(e)
            }, o.addEventListener("localcandidate", this.transceivers[t].bufferCandidates), o
        }, f.prototype._gather = function (t, n) {
            var r = this,
                o = this.transceivers[n].iceGatherer;
            if (!o.onlocalcandidate) {
                var i = this.transceivers[n].bufferedCandidateEvents;
                this.transceivers[n].bufferedCandidateEvents = null, o.removeEventListener("localcandidate", this.transceivers[n].bufferCandidates), o.onlocalcandidate = function (e) {
                    if (!(r.usingBundle && n > 0)) {
                        var i = new Event("icecandidate");
                        i.candidate = {
                            sdpMid: t,
                            sdpMLineIndex: n
                        };
                        var a = e.candidate,
                            s = !a || 0 === Object.keys(a).length;
                        if (s) "new" !== o.state && "gathering" !== o.state || (o.state = "completed");
                        else {
                            "new" === o.state && (o.state = "gathering"), a.component = 1, a.ufrag = o.getLocalParameters().usernameFragment;
                            var c = u.writeCandidate(a);
                            i.candidate = Object.assign(i.candidate, u.parseCandidate(c)), i.candidate.candidate = c, i.candidate.toJSON = function () {
                                return {
                                    candidate: i.candidate.candidate,
                                    sdpMid: i.candidate.sdpMid,
                                    sdpMLineIndex: i.candidate.sdpMLineIndex,
                                    usernameFragment: i.candidate.usernameFragment
                                }
                            }
                        }
                        var l = u.getMediaSections(r.localDescription.sdp);
                        l[i.candidate.sdpMLineIndex] += s ? "a=end-of-candidates\r\n" : "a=" + i.candidate.candidate + "\r\n", r.localDescription.sdp = u.getDescription(r.localDescription.sdp) + l.join("");
                        var d = r.transceivers.every(function (e) {
                            return e.iceGatherer && "completed" === e.iceGatherer.state
                        });
                        "gathering" !== r.iceGatheringState && (r.iceGatheringState = "gathering", r._emitGatheringStateChange()), s || r._dispatchEvent("icecandidate", i), d && (r._dispatchEvent("icecandidate", new Event("icecandidate")), r.iceGatheringState = "complete", r._emitGatheringStateChange())
                    }
                }, e.setTimeout(function () {
                    i.forEach(function (e) {
                        o.onlocalcandidate(e)
                    })
                }, 0)
            }
        }, f.prototype._createIceAndDtlsTransports = function () {
            var t = this,
                n = new e.RTCIceTransport(null);
            n.onicestatechange = function () {
                t._updateIceConnectionState(), t._updateConnectionState()
            };
            var r = new e.RTCDtlsTransport(n);
            return r.ondtlsstatechange = function () {
                t._updateConnectionState()
            }, r.onerror = function () {
                Object.defineProperty(r, "state", {
                    value: "failed",
                    writable: !0
                }), t._updateConnectionState()
            }, {
                    iceTransport: n,
                    dtlsTransport: r
                }
        }, f.prototype._disposeIceAndDtlsTransports = function (e) {
            var t = this.transceivers[e].iceGatherer;
            t && (delete t.onlocalcandidate, delete this.transceivers[e].iceGatherer);
            var n = this.transceivers[e].iceTransport;
            n && (delete n.onicestatechange, delete this.transceivers[e].iceTransport);
            var r = this.transceivers[e].dtlsTransport;
            r && (delete r.ondtlsstatechange, delete r.onerror, delete this.transceivers[e].dtlsTransport)
        }, f.prototype._transceive = function (e, n, r) {
            var o = a(e.localCapabilities, e.remoteCapabilities);
            n && e.rtpSender && (o.encodings = e.sendEncodingParameters, o.rtcp = {
                cname: u.localCName,
                compound: e.rtcpParameters.compound
            }, e.recvEncodingParameters.length && (o.rtcp.ssrc = e.recvEncodingParameters[0].ssrc), e.rtpSender.send(o)), r && e.rtpReceiver && o.codecs.length > 0 && ("video" === e.kind && e.recvEncodingParameters && t < 15019 && e.recvEncodingParameters.forEach(function (e) {
                delete e.rtx
            }), e.recvEncodingParameters.length ? o.encodings = e.recvEncodingParameters : o.encodings = [{}], o.rtcp = {
                compound: e.rtcpParameters.compound
            }, e.rtcpParameters.cname && (o.rtcp.cname = e.rtcpParameters.cname), e.sendEncodingParameters.length && (o.rtcp.ssrc = e.sendEncodingParameters[0].ssrc), e.rtpReceiver.receive(o))
        }, f.prototype.setLocalDescription = function (e) {
            var t = this;
            if (-1 === ["offer", "answer"].indexOf(e.type)) return Promise.reject(l("TypeError", 'Unsupported type "' + e.type + '"'));
            if (!s("setLocalDescription", e.type, t.signalingState) || t._isClosed) return Promise.reject(l("InvalidStateError", "Can not set local " + e.type + " in state " + t.signalingState));
            var n, r;
            if ("offer" === e.type) n = u.splitSections(e.sdp), r = n.shift(), n.forEach(function (e, n) {
                var r = u.parseRtpParameters(e);
                t.transceivers[n].localCapabilities = r
            }), t.transceivers.forEach(function (e, n) {
                t._gather(e.mid, n)
            });
            else if ("answer" === e.type) {
                n = u.splitSections(t.remoteDescription.sdp), r = n.shift();
                var o = u.matchPrefix(r, "a=ice-lite").length > 0;
                n.forEach(function (e, n) {
                    var i = t.transceivers[n],
                        s = i.iceGatherer,
                        c = i.iceTransport,
                        l = i.dtlsTransport,
                        d = i.localCapabilities,
                        g = i.remoteCapabilities;
                    if (!(u.isRejected(e) && 0 === u.matchPrefix(e, "a=bundle-only").length || i.rejected)) {
                        var f = u.getIceParameters(e, r),
                            p = u.getDtlsParameters(e, r);
                        o && (p.role = "server"), t.usingBundle && 0 !== n || (t._gather(i.mid, n), "new" === c.state && c.start(s, f, o ? "controlling" : "controlled"), "new" === l.state && l.start(p));
                        var v = a(d, g);
                        t._transceive(i, v.codecs.length > 0, !1)
                    }
                })
            }
            return t.localDescription = {
                type: e.type,
                sdp: e.sdp
            }, "offer" === e.type ? t._updateSignalingState("have-local-offer") : t._updateSignalingState("stable"), Promise.resolve()
        }, f.prototype.setRemoteDescription = function (r) {
            var o = this;
            if (-1 === ["offer", "answer"].indexOf(r.type)) return Promise.reject(l("TypeError", 'Unsupported type "' + r.type + '"'));
            if (!s("setRemoteDescription", r.type, o.signalingState) || o._isClosed) return Promise.reject(l("InvalidStateError", "Can not set remote " + r.type + " in state " + o.signalingState));
            var i = {};
            o.remoteStreams.forEach(function (e) {
                i[e.id] = e
            });
            var a = [],
                f = u.splitSections(r.sdp),
                p = f.shift(),
                v = u.matchPrefix(p, "a=ice-lite").length > 0,
                h = u.matchPrefix(p, "a=group:BUNDLE ").length > 0;
            o.usingBundle = h;
            var y = u.matchPrefix(p, "a=ice-options:")[0];
            return o.canTrickleIceCandidates = !!y && y.substr(14).split(" ").indexOf("trickle") >= 0, f.forEach(function (s, l) {
                var g = u.splitLines(s),
                    f = u.getKind(s),
                    y = u.isRejected(s) && 0 === u.matchPrefix(s, "a=bundle-only").length,
                    m = g[0].substr(2).split(" ")[2],
                    C = u.getDirection(s, p),
                    _ = u.parseMsid(s),
                    S = u.getMid(s) || u.generateIdentifier();
                if ("application" === f && "DTLS/SCTP" === m || y) return void (o.transceivers[l] = {
                    mid: S,
                    kind: f,
                    rejected: !0
                });
                !y && o.transceivers[l] && o.transceivers[l].rejected && (o.transceivers[l] = o._createTransceiver(f, !0));
                var E, L, M, R, T, b, P, I, k, w, O, A = u.parseRtpParameters(s);
                y || (w = u.getIceParameters(s, p), O = u.getDtlsParameters(s, p), O.role = "client"), P = u.parseRtpEncodingParameters(s);
                var D = u.parseRtcpParameters(s),
                    N = u.matchPrefix(s, "a=end-of-candidates", p).length > 0,
                    x = u.matchPrefix(s, "a=candidate:").map(function (e) {
                        return u.parseCandidate(e)
                    }).filter(function (e) {
                        return 1 === e.component
                    });
                if (("offer" === r.type || "answer" === r.type) && !y && h && l > 0 && o.transceivers[l] && (o._disposeIceAndDtlsTransports(l), o.transceivers[l].iceGatherer = o.transceivers[0].iceGatherer, o.transceivers[l].iceTransport = o.transceivers[0].iceTransport, o.transceivers[l].dtlsTransport = o.transceivers[0].dtlsTransport, o.transceivers[l].rtpSender && o.transceivers[l].rtpSender.setTransport(o.transceivers[0].dtlsTransport), o.transceivers[l].rtpReceiver && o.transceivers[l].rtpReceiver.setTransport(o.transceivers[0].dtlsTransport)), "offer" !== r.type || y) "answer" !== r.type || y || (E = o.transceivers[l], L = E.iceGatherer, M = E.iceTransport, R = E.dtlsTransport, T = E.rtpReceiver, b = E.sendEncodingParameters, I = E.localCapabilities, o.transceivers[l].recvEncodingParameters = P, o.transceivers[l].remoteCapabilities = A, o.transceivers[l].rtcpParameters = D, x.length && "new" === M.state && (!v && !N || h && 0 !== l ? x.forEach(function (e) {
                    c(E.iceTransport, e)
                }) : M.setRemoteCandidates(x)), h && 0 !== l || ("new" === M.state && M.start(L, w, "controlling"), "new" === R.state && R.start(O)), o._transceive(E, "sendrecv" === C || "recvonly" === C, "sendrecv" === C || "sendonly" === C), !T || "sendrecv" !== C && "sendonly" !== C ? delete E.rtpReceiver : (k = T.track, _ ? (i[_.stream] || (i[_.stream] = new e.MediaStream), n(k, i[_.stream]), a.push([k, T, i[_.stream]])) : (i.default || (i.default = new e.MediaStream), n(k, i.default), a.push([k, T, i.default]))));
                else {
                    E = o.transceivers[l] || o._createTransceiver(f), E.mid = S, E.iceGatherer || (E.iceGatherer = o._createIceGatherer(l, h)), x.length && "new" === E.iceTransport.state && (!N || h && 0 !== l ? x.forEach(function (e) {
                        c(E.iceTransport, e)
                    }) : E.iceTransport.setRemoteCandidates(x)), I = e.RTCRtpReceiver.getCapabilities(f), t < 15019 && (I.codecs = I.codecs.filter(function (e) {
                        return "rtx" !== e.name
                    })), b = E.sendEncodingParameters || [{
                        ssrc: 1001 * (2 * l + 2)
                    }];
                    var j = !1;
                    if ("sendrecv" === C || "sendonly" === C) {
                        if (j = !E.rtpReceiver, T = E.rtpReceiver || new e.RTCRtpReceiver(E.dtlsTransport, f), j) {
                            var F;
                            k = T.track, _ && "-" === _.stream || (_ ? (i[_.stream] || (i[_.stream] = new e.MediaStream, Object.defineProperty(i[_.stream], "id", {
                                get: function () {
                                    return _.stream
                                }
                            })), Object.defineProperty(k, "id", {
                                get: function () {
                                    return _.track
                                }
                            }), F = i[_.stream]) : (i.default || (i.default = new e.MediaStream), F = i.default)), F && (n(k, F), E.associatedRemoteMediaStreams.push(F)), a.push([k, T, F])
                        }
                    } else E.rtpReceiver && E.rtpReceiver.track && (E.associatedRemoteMediaStreams.forEach(function (e) {
                        var t = e.getTracks().find(function (e) {
                            return e.id === E.rtpReceiver.track.id
                        });
                        t && d(t, e)
                    }), E.associatedRemoteMediaStreams = []);
                    E.localCapabilities = I, E.remoteCapabilities = A, E.rtpReceiver = T, E.rtcpParameters = D, E.sendEncodingParameters = b, E.recvEncodingParameters = P, o._transceive(o.transceivers[l], !1, j)
                }
            }), void 0 === o._dtlsRole && (o._dtlsRole = "offer" === r.type ? "active" : "passive"), o.remoteDescription = {
                type: r.type,
                sdp: r.sdp
            }, "offer" === r.type ? o._updateSignalingState("have-remote-offer") : o._updateSignalingState("stable"), Object.keys(i).forEach(function (t) {
                var n = i[t];
                if (n.getTracks().length) {
                    if (-1 === o.remoteStreams.indexOf(n)) {
                        o.remoteStreams.push(n);
                        var r = new Event("addstream");
                        r.stream = n, e.setTimeout(function () {
                            o._dispatchEvent("addstream", r)
                        })
                    }
                    a.forEach(function (e) {
                        var t = e[0],
                            r = e[1];
                        n.id === e[2].id && g(o, t, r, [n])
                    })
                }
            }), a.forEach(function (e) {
                e[2] || g(o, e[0], e[1], [])
            }), e.setTimeout(function () {
                o && o.transceivers && o.transceivers.forEach(function (e) {
                    e.iceTransport && "new" === e.iceTransport.state && e.iceTransport.getRemoteCandidates().length > 0 && (console.warn("Timeout for addRemoteCandidate. Consider sending an end-of-candidates notification"), e.iceTransport.addRemoteCandidate({}))
                })
            }, 4e3), Promise.resolve()
        }, f.prototype.close = function () {
            this.transceivers.forEach(function (e) {
                e.iceTransport && e.iceTransport.stop(), e.dtlsTransport && e.dtlsTransport.stop(), e.rtpSender && e.rtpSender.stop(), e.rtpReceiver && e.rtpReceiver.stop()
            }), this._isClosed = !0, this._updateSignalingState("closed")
        }, f.prototype._updateSignalingState = function (e) {
            this.signalingState = e;
            var t = new Event("signalingstatechange");
            this._dispatchEvent("signalingstatechange", t)
        }, f.prototype._maybeFireNegotiationNeeded = function () {
            var t = this;
            "stable" === this.signalingState && !0 !== this.needNegotiation && (this.needNegotiation = !0, e.setTimeout(function () {
                if (t.needNegotiation) {
                    t.needNegotiation = !1;
                    var e = new Event("negotiationneeded");
                    t._dispatchEvent("negotiationneeded", e)
                }
            }, 0))
        }, f.prototype._updateIceConnectionState = function () {
            var e, t = {
                new: 0,
                closed: 0,
                checking: 0,
                connected: 0,
                completed: 0,
                disconnected: 0,
                failed: 0
            };
            if (this.transceivers.forEach(function (e) {
                t[e.iceTransport.state]++
            }), e = "new", t.failed > 0 ? e = "failed" : t.checking > 0 ? e = "checking" : t.disconnected > 0 ? e = "disconnected" : t.new > 0 ? e = "new" : t.connected > 0 ? e = "connected" : t.completed > 0 && (e = "completed"), e !== this.iceConnectionState) {
                this.iceConnectionState = e;
                var n = new Event("iceconnectionstatechange");
                this._dispatchEvent("iceconnectionstatechange", n)
            }
        }, f.prototype._updateConnectionState = function () {
            var e, t = {
                new: 0,
                closed: 0,
                connecting: 0,
                connected: 0,
                completed: 0,
                disconnected: 0,
                failed: 0
            };
            if (this.transceivers.forEach(function (e) {
                t[e.iceTransport.state]++ , t[e.dtlsTransport.state]++
            }), t.connected += t.completed, e = "new", t.failed > 0 ? e = "failed" : t.connecting > 0 ? e = "connecting" : t.disconnected > 0 ? e = "disconnected" : t.new > 0 ? e = "new" : t.connected > 0 && (e = "connected"), e !== this.connectionState) {
                this.connectionState = e;
                var n = new Event("connectionstatechange");
                this._dispatchEvent("connectionstatechange", n)
            }
        }, f.prototype.createOffer = function () {
            var n = this;
            if (n._isClosed) return Promise.reject(l("InvalidStateError", "Can not call createOffer after close"));
            var r = n.transceivers.filter(function (e) {
                return "audio" === e.kind
            }).length,
                i = n.transceivers.filter(function (e) {
                    return "video" === e.kind
                }).length,
                a = arguments[0];
            if (a) {
                if (a.mandatory || a.optional) throw new TypeError("Legacy mandatory/optional constraints not supported.");
                void 0 !== a.offerToReceiveAudio && (r = !0 === a.offerToReceiveAudio ? 1 : !1 === a.offerToReceiveAudio ? 0 : a.offerToReceiveAudio), void 0 !== a.offerToReceiveVideo && (i = !0 === a.offerToReceiveVideo ? 1 : !1 === a.offerToReceiveVideo ? 0 : a.offerToReceiveVideo)
            }
            for (n.transceivers.forEach(function (e) {
                "audio" === e.kind ? --r < 0 && (e.wantReceive = !1) : "video" === e.kind && --i < 0 && (e.wantReceive = !1)
            }); r > 0 || i > 0;) r > 0 && (n._createTransceiver("audio"), r--), i > 0 && (n._createTransceiver("video"), i--);
            var s = u.writeSessionBoilerplate(n._sdpSessionId, n._sdpSessionVersion++);
            n.transceivers.forEach(function (r, o) {
                var i = r.track,
                    a = r.kind,
                    s = r.mid || u.generateIdentifier();
                r.mid = s, r.iceGatherer || (r.iceGatherer = n._createIceGatherer(o, n.usingBundle));
                var c = e.RTCRtpSender.getCapabilities(a);
                t < 15019 && (c.codecs = c.codecs.filter(function (e) {
                    return "rtx" !== e.name
                })), c.codecs.forEach(function (e) {
                    "H264" === e.name && void 0 === e.parameters["level-asymmetry-allowed"] && (e.parameters["level-asymmetry-allowed"] = "1"), r.remoteCapabilities && r.remoteCapabilities.codecs && r.remoteCapabilities.codecs.forEach(function (t) {
                        e.name.toLowerCase() === t.name.toLowerCase() && e.clockRate === t.clockRate && (e.preferredPayloadType = t.payloadType)
                    })
                }), c.headerExtensions.forEach(function (e) {
                    (r.remoteCapabilities && r.remoteCapabilities.headerExtensions || []).forEach(function (t) {
                        e.uri === t.uri && (e.id = t.id)
                    })
                });
                var l = r.sendEncodingParameters || [{
                    ssrc: 1001 * (2 * o + 1)
                }];
                i && t >= 15019 && "video" === a && !l[0].rtx && (l[0].rtx = {
                    ssrc: l[0].ssrc + 1
                }), r.wantReceive && (r.rtpReceiver = new e.RTCRtpReceiver(r.dtlsTransport, a)), r.localCapabilities = c, r.sendEncodingParameters = l
            }), "max-compat" !== n._config.bundlePolicy && (s += "a=group:BUNDLE " + n.transceivers.map(function (e) {
                return e.mid
            }).join(" ") + "\r\n"), s += "a=ice-options:trickle\r\n", n.transceivers.forEach(function (e, t) {
                s += o(e, e.localCapabilities, "offer", e.stream, n._dtlsRole), s += "a=rtcp-rsize\r\n", !e.iceGatherer || "new" === n.iceGatheringState || 0 !== t && n.usingBundle || (e.iceGatherer.getLocalCandidates().forEach(function (e) {
                    e.component = 1, s += "a=" + u.writeCandidate(e) + "\r\n"
                }), "completed" === e.iceGatherer.state && (s += "a=end-of-candidates\r\n"))
            });
            var c = new e.RTCSessionDescription({
                type: "offer",
                sdp: s
            });
            return Promise.resolve(c)
        }, f.prototype.createAnswer = function () {
            var n = this;
            if (n._isClosed) return Promise.reject(l("InvalidStateError", "Can not call createAnswer after close"));
            if ("have-remote-offer" !== n.signalingState && "have-local-pranswer" !== n.signalingState) return Promise.reject(l("InvalidStateError", "Can not call createAnswer in signalingState " + n.signalingState));
            var r = u.writeSessionBoilerplate(n._sdpSessionId, n._sdpSessionVersion++);
            n.usingBundle && (r += "a=group:BUNDLE " + n.transceivers.map(function (e) {
                return e.mid
            }).join(" ") + "\r\n");
            var i = u.getMediaSections(n.remoteDescription.sdp).length;
            n.transceivers.forEach(function (e, s) {
                if (!(s + 1 > i)) {
                    if (e.rejected) return "application" === e.kind ? r += "m=application 0 DTLS/SCTP 5000\r\n" : "audio" === e.kind ? r += "m=audio 0 UDP/TLS/RTP/SAVPF 0\r\na=rtpmap:0 PCMU/8000\r\n" : "video" === e.kind && (r += "m=video 0 UDP/TLS/RTP/SAVPF 120\r\na=rtpmap:120 VP8/90000\r\n"), void (r += "c=IN IP4 0.0.0.0\r\na=inactive\r\na=mid:" + e.mid + "\r\n");
                    if (e.stream) {
                        var c;
                        "audio" === e.kind ? c = e.stream.getAudioTracks()[0] : "video" === e.kind && (c = e.stream.getVideoTracks()[0]), c && t >= 15019 && "video" === e.kind && !e.sendEncodingParameters[0].rtx && (e.sendEncodingParameters[0].rtx = {
                            ssrc: e.sendEncodingParameters[0].ssrc + 1
                        })
                    }
                    var l = a(e.localCapabilities, e.remoteCapabilities);
                    !l.codecs.filter(function (e) {
                        return "rtx" === e.name.toLowerCase()
                    }).length && e.sendEncodingParameters[0].rtx && delete e.sendEncodingParameters[0].rtx, r += o(e, l, "answer", e.stream, n._dtlsRole), e.rtcpParameters && e.rtcpParameters.reducedSize && (r += "a=rtcp-rsize\r\n")
                }
            });
            var s = new e.RTCSessionDescription({
                type: "answer",
                sdp: r
            });
            return Promise.resolve(s)
        }, f.prototype.addIceCandidate = function (e) {
            var t, n = this;
            return e && void 0 === e.sdpMLineIndex && !e.sdpMid ? Promise.reject(new TypeError("sdpMLineIndex or sdpMid required")) : new Promise(function (r, o) {
                if (!n.remoteDescription) return o(l("InvalidStateError", "Can not add ICE candidate without a remote description"));
                if (e && "" !== e.candidate) {
                    var i = e.sdpMLineIndex;
                    if (e.sdpMid)
                        for (var a = 0; a < n.transceivers.length; a++)
                            if (n.transceivers[a].mid === e.sdpMid) {
                                i = a;
                                break
                            }
                    var s = n.transceivers[i];
                    if (!s) return o(l("OperationError", "Can not add ICE candidate"));
                    if (s.rejected) return r();
                    var d = Object.keys(e.candidate).length > 0 ? u.parseCandidate(e.candidate) : {};
                    if ("tcp" === d.protocol && (0 === d.port || 9 === d.port)) return r();
                    if (d.component && 1 !== d.component) return r();
                    if ((0 === i || i > 0 && s.iceTransport !== n.transceivers[0].iceTransport) && !c(s.iceTransport, d)) return o(l("OperationError", "Can not add ICE candidate"));
                    var g = e.candidate.trim();
                    0 === g.indexOf("a=") && (g = g.substr(2)), t = u.getMediaSections(n.remoteDescription.sdp), t[i] += "a=" + (d.type ? g : "end-of-candidates") + "\r\n", n.remoteDescription.sdp = u.getDescription(n.remoteDescription.sdp) + t.join("")
                } else
                    for (var f = 0; f < n.transceivers.length && (n.transceivers[f].rejected || (n.transceivers[f].iceTransport.addRemoteCandidate({}), t = u.getMediaSections(n.remoteDescription.sdp), t[f] += "a=end-of-candidates\r\n", n.remoteDescription.sdp = u.getDescription(n.remoteDescription.sdp) + t.join(""), !n.usingBundle)); f++);
                r()
            })
        }, f.prototype.getStats = function (t) {
            if (t && t instanceof e.MediaStreamTrack) {
                var n = null;
                if (this.transceivers.forEach(function (e) {
                    e.rtpSender && e.rtpSender.track === t ? n = e.rtpSender : e.rtpReceiver && e.rtpReceiver.track === t && (n = e.rtpReceiver)
                }), !n) throw l("InvalidAccessError", "Invalid selector.");
                return n.getStats()
            }
            var r = [];
            return this.transceivers.forEach(function (e) {
                ["rtpSender", "rtpReceiver", "iceGatherer", "iceTransport", "dtlsTransport"].forEach(function (t) {
                    e[t] && r.push(e[t].getStats())
                })
            }), Promise.all(r).then(function (e) {
                var t = new Map;
                return e.forEach(function (e) {
                    e.forEach(function (e) {
                        t.set(e.id, e)
                    })
                }), t
            })
        }, ["RTCRtpSender", "RTCRtpReceiver", "RTCIceGatherer", "RTCIceTransport", "RTCDtlsTransport"].forEach(function (t) {
            var n = e[t];
            if (n && n.prototype && n.prototype.getStats) {
                var o = n.prototype.getStats;
                n.prototype.getStats = function () {
                    return o.apply(this).then(function (e) {
                        var t = new Map;
                        return Object.keys(e).forEach(function (n) {
                            e[n].type = r(e[n]), t.set(n, e[n])
                        }), t
                    })
                }
            }
        });
        var p = ["createOffer", "createAnswer"];
        return p.forEach(function (e) {
            var t = f.prototype[e];
            f.prototype[e] = function () {
                var e = arguments;
                return "function" == typeof e[0] || "function" == typeof e[1] ? t.apply(this, [arguments[2]]).then(function (t) {
                    "function" == typeof e[0] && e[0].apply(null, [t])
                }, function (t) {
                    "function" == typeof e[1] && e[1].apply(null, [t])
                }) : t.apply(this, arguments)
            }
        }), p = ["setLocalDescription", "setRemoteDescription", "addIceCandidate"], p.forEach(function (e) {
            var t = f.prototype[e];
            f.prototype[e] = function () {
                var e = arguments;
                return "function" == typeof e[1] || "function" == typeof e[2] ? t.apply(this, arguments).then(function () {
                    "function" == typeof e[1] && e[1].apply(null)
                }, function (t) {
                    "function" == typeof e[2] && e[2].apply(null, [t])
                }) : t.apply(this, arguments)
            }
        }), ["getStats"].forEach(function (e) {
            var t = f.prototype[e];
            f.prototype[e] = function () {
                var e = arguments;
                return "function" == typeof e[1] ? t.apply(this, arguments).then(function () {
                    "function" == typeof e[1] && e[1].apply(null)
                }) : t.apply(this, arguments)
            }
        }), f
    }
}, function (e, t, n) {
    "use strict";
    e.exports = function (e) {
        var t = e && e.navigator,
            n = function (e) {
                return {
                    name: {
                        PermissionDeniedError: "NotAllowedError"
                    }[e.name] || e.name,
                    message: e.message,
                    constraint: e.constraint,
                    toString: function () {
                        return this.name
                    }
                }
            },
            r = t.mediaDevices.getUserMedia.bind(t.mediaDevices);
        t.mediaDevices.getUserMedia = function (e) {
            return r(e).catch(function (e) {
                return Promise.reject(n(e))
            })
        }
    }
}, function (e, t, n) {
    "use strict";
    var r = n(7),
        o = {
            shimOnTrack: function (e) {
                "object" != typeof e || !e.RTCPeerConnection || "ontrack" in e.RTCPeerConnection.prototype || Object.defineProperty(e.RTCPeerConnection.prototype, "ontrack", {
                    get: function () {
                        return this._ontrack
                    },
                    set: function (e) {
                        this._ontrack && (this.removeEventListener("track", this._ontrack), this.removeEventListener("addstream", this._ontrackpoly)), this.addEventListener("track", this._ontrack = e), this.addEventListener("addstream", this._ontrackpoly = function (e) {
                            e.stream.getTracks().forEach(function (t) {
                                var n = new Event("track");
                                n.track = t, n.receiver = {
                                    track: t
                                }, n.transceiver = {
                                    receiver: n.receiver
                                }, n.streams = [e.stream], this.dispatchEvent(n)
                            }.bind(this))
                        }.bind(this))
                    }
                }), "object" == typeof e && e.RTCTrackEvent && "receiver" in e.RTCTrackEvent.prototype && !("transceiver" in e.RTCTrackEvent.prototype) && Object.defineProperty(e.RTCTrackEvent.prototype, "transceiver", {
                    get: function () {
                        return {
                            receiver: this.receiver
                        }
                    }
                })
            },
            shimSourceObject: function (e) {
                "object" == typeof e && (!e.HTMLMediaElement || "srcObject" in e.HTMLMediaElement.prototype || Object.defineProperty(e.HTMLMediaElement.prototype, "srcObject", {
                    get: function () {
                        return this.mozSrcObject
                    },
                    set: function (e) {
                        this.mozSrcObject = e
                    }
                }))
            },
            shimPeerConnection: function (e) {
                var t = r.detectBrowser(e);
                if ("object" == typeof e && (e.RTCPeerConnection || e.mozRTCPeerConnection)) {
                    e.RTCPeerConnection || (e.RTCPeerConnection = function (n, r) {
                        if (t.version < 38 && n && n.iceServers) {
                            for (var o = [], i = 0; i < n.iceServers.length; i++) {
                                var a = n.iceServers[i];
                                if (a.hasOwnProperty("urls"))
                                    for (var s = 0; s < a.urls.length; s++) {
                                        var c = {
                                            url: a.urls[s]
                                        };
                                        0 === a.urls[s].indexOf("turn") && (c.username = a.username, c.credential = a.credential), o.push(c)
                                    } else o.push(n.iceServers[i])
                            }
                            n.iceServers = o
                        }
                        return new e.mozRTCPeerConnection(n, r)
                    }, e.RTCPeerConnection.prototype = e.mozRTCPeerConnection.prototype, e.mozRTCPeerConnection.generateCertificate && Object.defineProperty(e.RTCPeerConnection, "generateCertificate", {
                        get: function () {
                            return e.mozRTCPeerConnection.generateCertificate
                        }
                    }), e.RTCSessionDescription = e.mozRTCSessionDescription, e.RTCIceCandidate = e.mozRTCIceCandidate), ["setLocalDescription", "setRemoteDescription", "addIceCandidate"].forEach(function (t) {
                        var n = e.RTCPeerConnection.prototype[t];
                        e.RTCPeerConnection.prototype[t] = function () {
                            return arguments[0] = new ("addIceCandidate" === t ? e.RTCIceCandidate : e.RTCSessionDescription)(arguments[0]), n.apply(this, arguments)
                        }
                    });
                    var n = e.RTCPeerConnection.prototype.addIceCandidate;
                    e.RTCPeerConnection.prototype.addIceCandidate = function () {
                        return arguments[0] ? n.apply(this, arguments) : (arguments[1] && arguments[1].apply(null), Promise.resolve())
                    };
                    var o = function (e) {
                        var t = new Map;
                        return Object.keys(e).forEach(function (n) {
                            t.set(n, e[n]), t[n] = e[n]
                        }), t
                    },
                        i = {
                            inboundrtp: "inbound-rtp",
                            outboundrtp: "outbound-rtp",
                            candidatepair: "candidate-pair",
                            localcandidate: "local-candidate",
                            remotecandidate: "remote-candidate"
                        },
                        a = e.RTCPeerConnection.prototype.getStats;
                    e.RTCPeerConnection.prototype.getStats = function (e, n, r) {
                        return a.apply(this, [e || null]).then(function (e) {
                            if (t.version < 48 && (e = o(e)), t.version < 53 && !n) try {
                                e.forEach(function (e) {
                                    e.type = i[e.type] || e.type
                                })
                            } catch (t) {
                                if ("TypeError" !== t.name) throw t;
                                e.forEach(function (t, n) {
                                    e.set(n, Object.assign({}, t, {
                                        type: i[t.type] || t.type
                                    }))
                                })
                            }
                            return e
                        }).then(n, r)
                    }
                }
            },
            shimRemoveStream: function (e) {
                "removeStream" in e.RTCPeerConnection.prototype || (e.RTCPeerConnection.prototype.removeStream = function (e) {
                    var t = this;
                    r.deprecated("removeStream", "removeTrack"), this.getSenders().forEach(function (n) {
                        n.track && -1 !== e.getTracks().indexOf(n.track) && t.removeTrack(n)
                    })
                })
            }
        };
    e.exports = {
        shimOnTrack: o.shimOnTrack,
        shimSourceObject: o.shimSourceObject,
        shimPeerConnection: o.shimPeerConnection,
        shimRemoveStream: o.shimRemoveStream,
        shimGetUserMedia: n(68)
    }
}, function (e, t, n) {
    "use strict";
    var r = n(7),
        o = r.log;
    e.exports = function (e) {
        var t = r.detectBrowser(e),
            n = e && e.navigator,
            i = e && e.MediaStreamTrack,
            a = function (e) {
                return {
                    name: {
                        InternalError: "NotReadableError",
                        NotSupportedError: "TypeError",
                        PermissionDeniedError: "NotAllowedError",
                        SecurityError: "NotAllowedError"
                    }[e.name] || e.name,
                    message: {
                        "The operation is insecure.": "The request is not allowed by the user agent or the platform in the current context."
                    }[e.message] || e.message,
                    constraint: e.constraint,
                    toString: function () {
                        return this.name + (this.message && ": ") + this.message
                    }
                }
            },
            s = function (e, r, i) {
                var s = function (e) {
                    if ("object" != typeof e || e.require) return e;
                    var t = [];
                    return Object.keys(e).forEach(function (n) {
                        if ("require" !== n && "advanced" !== n && "mediaSource" !== n) {
                            var r = e[n] = "object" == typeof e[n] ? e[n] : {
                                ideal: e[n]
                            };
                            if (void 0 === r.min && void 0 === r.max && void 0 === r.exact || t.push(n), void 0 !== r.exact && ("number" == typeof r.exact ? r.min = r.max = r.exact : e[n] = r.exact, delete r.exact), void 0 !== r.ideal) {
                                e.advanced = e.advanced || [];
                                var o = {};
                                "number" == typeof r.ideal ? o[n] = {
                                    min: r.ideal,
                                    max: r.ideal
                                } : o[n] = r.ideal, e.advanced.push(o), delete r.ideal, Object.keys(r).length || delete e[n]
                            }
                        }
                    }), t.length && (e.require = t), e
                };
                return e = JSON.parse(JSON.stringify(e)), t.version < 38 && (o("spec: " + JSON.stringify(e)), e.audio && (e.audio = s(e.audio)), e.video && (e.video = s(e.video)), o("ff37: " + JSON.stringify(e))), n.mozGetUserMedia(e, r, function (e) {
                    i(a(e))
                })
            },
            c = function (e) {
                return new Promise(function (t, n) {
                    s(e, t, n)
                })
            };
        if (n.mediaDevices || (n.mediaDevices = {
            getUserMedia: c,
            addEventListener: function () { },
            removeEventListener: function () { }
        }), n.mediaDevices.enumerateDevices = n.mediaDevices.enumerateDevices || function () {
            return new Promise(function (e) {
                e([{
                    kind: "audioinput",
                    deviceId: "default",
                    label: "",
                    groupId: ""
                }, {
                    kind: "videoinput",
                    deviceId: "default",
                    label: "",
                    groupId: ""
                }])
            })
        }, t.version < 41) {
            var l = n.mediaDevices.enumerateDevices.bind(n.mediaDevices);
            n.mediaDevices.enumerateDevices = function () {
                return l().then(void 0, function (e) {
                    if ("NotFoundError" === e.name) return [];
                    throw e
                })
            }
        }
        if (t.version < 49) {
            var u = n.mediaDevices.getUserMedia.bind(n.mediaDevices);
            n.mediaDevices.getUserMedia = function (e) {
                return u(e).then(function (t) {
                    if (e.audio && !t.getAudioTracks().length || e.video && !t.getVideoTracks().length) throw t.getTracks().forEach(function (e) {
                        e.stop()
                    }), new DOMException("The object can not be found here.", "NotFoundError");
                    return t
                }, function (e) {
                    return Promise.reject(a(e))
                })
            }
        }
        if (!(t.version > 55 && "autoGainControl" in n.mediaDevices.getSupportedConstraints())) {
            var d = function (e, t, n) {
                t in e && !(n in e) && (e[n] = e[t], delete e[t])
            },
                g = n.mediaDevices.getUserMedia.bind(n.mediaDevices);
            if (n.mediaDevices.getUserMedia = function (e) {
                return "object" == typeof e && "object" == typeof e.audio && (e = JSON.parse(JSON.stringify(e)), d(e.audio, "autoGainControl", "mozAutoGainControl"), d(e.audio, "noiseSuppression", "mozNoiseSuppression")), g(e)
            }, i && i.prototype.getSettings) {
                var f = i.prototype.getSettings;
                i.prototype.getSettings = function () {
                    var e = f.apply(this, arguments);
                    return d(e, "mozAutoGainControl", "autoGainControl"), d(e, "mozNoiseSuppression", "noiseSuppression"), e
                }
            }
            if (i && i.prototype.applyConstraints) {
                var p = i.prototype.applyConstraints;
                i.prototype.applyConstraints = function (e) {
                    return "audio" === this.kind && "object" == typeof e && (e = JSON.parse(JSON.stringify(e)), d(e, "autoGainControl", "mozAutoGainControl"), d(e, "noiseSuppression", "mozNoiseSuppression")), p.apply(this, [e])
                }
            }
        }
        n.getUserMedia = function (e, o, i) {
            if (t.version < 44) return s(e, o, i);
            r.deprecated("navigator.getUserMedia", "navigator.mediaDevices.getUserMedia"), n.mediaDevices.getUserMedia(e).then(o, i)
        }
    }
}, function (e, t, n) {
    "use strict";
    var r = n(7),
        o = {
            shimLocalStreamsAPI: function (e) {
                if ("object" == typeof e && e.RTCPeerConnection) {
                    if ("getLocalStreams" in e.RTCPeerConnection.prototype || (e.RTCPeerConnection.prototype.getLocalStreams = function () {
                        return this._localStreams || (this._localStreams = []), this._localStreams
                    }), "getStreamById" in e.RTCPeerConnection.prototype || (e.RTCPeerConnection.prototype.getStreamById = function (e) {
                        var t = null;
                        return this._localStreams && this._localStreams.forEach(function (n) {
                            n.id === e && (t = n)
                        }), this._remoteStreams && this._remoteStreams.forEach(function (n) {
                            n.id === e && (t = n)
                        }), t
                    }), !("addStream" in e.RTCPeerConnection.prototype)) {
                        var t = e.RTCPeerConnection.prototype.addTrack;
                        e.RTCPeerConnection.prototype.addStream = function (e) {
                            this._localStreams || (this._localStreams = []), -1 === this._localStreams.indexOf(e) && this._localStreams.push(e);
                            var n = this;
                            e.getTracks().forEach(function (r) {
                                t.call(n, r, e)
                            })
                        }, e.RTCPeerConnection.prototype.addTrack = function (e, n) {
                            n && (this._localStreams ? -1 === this._localStreams.indexOf(n) && this._localStreams.push(n) : this._localStreams = [n]), t.call(this, e, n)
                        }
                    }
                    "removeStream" in e.RTCPeerConnection.prototype || (e.RTCPeerConnection.prototype.removeStream = function (e) {
                        this._localStreams || (this._localStreams = []);
                        var t = this._localStreams.indexOf(e);
                        if (-1 !== t) {
                            this._localStreams.splice(t, 1);
                            var n = this,
                                r = e.getTracks();
                            this.getSenders().forEach(function (e) {
                                -1 !== r.indexOf(e.track) && n.removeTrack(e)
                            })
                        }
                    })
                }
            },
            shimRemoteStreamsAPI: function (e) {
                "object" == typeof e && e.RTCPeerConnection && ("getRemoteStreams" in e.RTCPeerConnection.prototype || (e.RTCPeerConnection.prototype.getRemoteStreams = function () {
                    return this._remoteStreams ? this._remoteStreams : []
                }), "onaddstream" in e.RTCPeerConnection.prototype || Object.defineProperty(e.RTCPeerConnection.prototype, "onaddstream", {
                    get: function () {
                        return this._onaddstream
                    },
                    set: function (e) {
                        this._onaddstream && (this.removeEventListener("addstream", this._onaddstream), this.removeEventListener("track", this._onaddstreampoly)), this.addEventListener("addstream", this._onaddstream = e), this.addEventListener("track", this._onaddstreampoly = function (e) {
                            var t = e.streams[0];
                            if (this._remoteStreams || (this._remoteStreams = []), !(this._remoteStreams.indexOf(t) >= 0)) {
                                this._remoteStreams.push(t);
                                var n = new Event("addstream");
                                n.stream = e.streams[0], this.dispatchEvent(n)
                            }
                        }.bind(this))
                    }
                }))
            },
            shimCallbacksAPI: function (e) {
                if ("object" == typeof e && e.RTCPeerConnection) {
                    var t = e.RTCPeerConnection.prototype,
                        n = t.createOffer,
                        r = t.createAnswer,
                        o = t.setLocalDescription,
                        i = t.setRemoteDescription,
                        a = t.addIceCandidate;
                    t.createOffer = function (e, t) {
                        var r = arguments.length >= 2 ? arguments[2] : arguments[0],
                            o = n.apply(this, [r]);
                        return t ? (o.then(e, t), Promise.resolve()) : o
                    }, t.createAnswer = function (e, t) {
                        var n = arguments.length >= 2 ? arguments[2] : arguments[0],
                            o = r.apply(this, [n]);
                        return t ? (o.then(e, t), Promise.resolve()) : o
                    };
                    var s = function (e, t, n) {
                        var r = o.apply(this, [e]);
                        return n ? (r.then(t, n), Promise.resolve()) : r
                    };
                    t.setLocalDescription = s, s = function (e, t, n) {
                        var r = i.apply(this, [e]);
                        return n ? (r.then(t, n), Promise.resolve()) : r
                    }, t.setRemoteDescription = s, s = function (e, t, n) {
                        var r = a.apply(this, [e]);
                        return n ? (r.then(t, n), Promise.resolve()) : r
                    }, t.addIceCandidate = s
                }
            },
            shimGetUserMedia: function (e) {
                var t = e && e.navigator;
                t.getUserMedia || (t.webkitGetUserMedia ? t.getUserMedia = t.webkitGetUserMedia.bind(t) : t.mediaDevices && t.mediaDevices.getUserMedia && (t.getUserMedia = function (e, n, r) {
                    t.mediaDevices.getUserMedia(e).then(n, r)
                }.bind(t)))
            },
            shimRTCIceServerUrls: function (e) {
                var t = e.RTCPeerConnection;
                e.RTCPeerConnection = function (e, n) {
                    if (e && e.iceServers) {
                        for (var o = [], i = 0; i < e.iceServers.length; i++) {
                            var a = e.iceServers[i];
                            !a.hasOwnProperty("urls") && a.hasOwnProperty("url") ? (r.deprecated("RTCIceServer.url", "RTCIceServer.urls"), a = JSON.parse(JSON.stringify(a)), a.urls = a.url, delete a.url, o.push(a)) : o.push(e.iceServers[i])
                        }
                        e.iceServers = o
                    }
                    return new t(e, n)
                }, e.RTCPeerConnection.prototype = t.prototype, "generateCertificate" in e.RTCPeerConnection && Object.defineProperty(e.RTCPeerConnection, "generateCertificate", {
                    get: function () {
                        return t.generateCertificate
                    }
                })
            },
            shimTrackEventTransceiver: function (e) {
                "object" == typeof e && e.RTCPeerConnection && "receiver" in e.RTCTrackEvent.prototype && !e.RTCTransceiver && Object.defineProperty(e.RTCTrackEvent.prototype, "transceiver", {
                    get: function () {
                        return {
                            receiver: this.receiver
                        }
                    }
                })
            },
            shimCreateOfferLegacy: function (e) {
                var t = e.RTCPeerConnection.prototype.createOffer;
                e.RTCPeerConnection.prototype.createOffer = function (e) {
                    var n = this;
                    if (e) {
                        var r = n.getTransceivers().find(function (e) {
                            return e.sender.track && "audio" === e.sender.track.kind
                        });
                        !1 === e.offerToReceiveAudio && r ? "sendrecv" === r.direction ? r.setDirection("sendonly") : "recvonly" === r.direction && r.setDirection("inactive") : !0 !== e.offerToReceiveAudio || r || n.addTransceiver("audio");
                        var o = n.getTransceivers().find(function (e) {
                            return e.sender.track && "video" === e.sender.track.kind
                        });
                        !1 === e.offerToReceiveVideo && o ? "sendrecv" === o.direction ? o.setDirection("sendonly") : "recvonly" === o.direction && o.setDirection("inactive") : !0 !== e.offerToReceiveVideo || o || n.addTransceiver("video")
                    }
                    return t.apply(n, arguments)
                }
            }
        };
    e.exports = {
        shimCallbacksAPI: o.shimCallbacksAPI,
        shimLocalStreamsAPI: o.shimLocalStreamsAPI,
        shimRemoteStreamsAPI: o.shimRemoteStreamsAPI,
        shimGetUserMedia: o.shimGetUserMedia,
        shimRTCIceServerUrls: o.shimRTCIceServerUrls,
        shimTrackEventTransceiver: o.shimTrackEventTransceiver,
        shimCreateOfferLegacy: o.shimCreateOfferLegacy
    }
}, function (e, t, n) {
    "use strict";

    function r(e, t, n) {
        if (e.RTCPeerConnection) {
            var r = e.RTCPeerConnection.prototype,
                o = r.addEventListener;
            r.addEventListener = function (e, r) {
                if (e !== t) return o.apply(this, arguments);
                var i = function (e) {
                    r(n(e))
                };
                return this._eventMap = this._eventMap || {}, this._eventMap[r] = i, o.apply(this, [e, i])
            };
            var i = r.removeEventListener;
            r.removeEventListener = function (e, n) {
                if (e !== t || !this._eventMap || !this._eventMap[n]) return i.apply(this, arguments);
                var r = this._eventMap[n];
                return delete this._eventMap[n], i.apply(this, [e, r])
            }, Object.defineProperty(r, "on" + t, {
                get: function () {
                    return this["_on" + t]
                },
                set: function (e) {
                    this["_on" + t] && (this.removeEventListener(t, this["_on" + t]), delete this["_on" + t]), e && this.addEventListener(t, this["_on" + t] = e)
                }
            })
        }
    }
    var o = n(39),
        i = n(7);
    e.exports = {
        shimRTCIceCandidate: function (e) {
            if (!(e.RTCIceCandidate && "foundation" in e.RTCIceCandidate.prototype)) {
                var t = e.RTCIceCandidate;
                e.RTCIceCandidate = function (e) {
                    "object" == typeof e && e.candidate && 0 === e.candidate.indexOf("a=") && (e = JSON.parse(JSON.stringify(e)), e.candidate = e.candidate.substr(2));
                    var n = new t(e),
                        r = o.parseCandidate(e.candidate),
                        i = Object.assign(n, r);
                    return i.toJSON = function () {
                        return {
                            candidate: i.candidate,
                            sdpMid: i.sdpMid,
                            sdpMLineIndex: i.sdpMLineIndex,
                            usernameFragment: i.usernameFragment
                        }
                    }, i
                }, r(e, "icecandidate", function (t) {
                    return t.candidate && Object.defineProperty(t, "candidate", {
                        value: new e.RTCIceCandidate(t.candidate),
                        writable: "false"
                    }), t
                })
            }
        },
        shimCreateObjectURL: function (e) {
            var t = e && e.URL;
            if ("object" == typeof e && e.HTMLMediaElement && "srcObject" in e.HTMLMediaElement.prototype && t.createObjectURL && t.revokeObjectURL) {
                var n = t.createObjectURL.bind(t),
                    r = t.revokeObjectURL.bind(t),
                    o = new Map,
                    a = 0;
                t.createObjectURL = function (e) {
                    if ("getTracks" in e) {
                        var t = "polyblob:" + ++a;
                        //alert("shimCreateObjectURL");
                        return o.set(t, e), i.deprecated("URL.createObjectURL(stream)", "elem.srcObject = stream"), t
                    }
                    return n(e)
                }, t.revokeObjectURL = function (e) {
                    r(e), o.delete(e)
                };
                var s = Object.getOwnPropertyDescriptor(e.HTMLMediaElement.prototype, "src");
                Object.defineProperty(e.HTMLMediaElement.prototype, "src", {
                    get: function () {
                        return s.get.apply(this)
                    },
                    set: function (e) {
                        return this.srcObject = o.get(e) || null, s.set.apply(this, [e])
                    }
                });
                var c = e.HTMLMediaElement.prototype.setAttribute;
                e.HTMLMediaElement.prototype.setAttribute = function () {
                    return 2 === arguments.length && "src" === ("" + arguments[0]).toLowerCase() && (this.srcObject = o.get(arguments[1]) || null), c.apply(this, arguments)
                }
            }
        }
    }
}, function (e, t, n) {
    "use strict";
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var r = n(30);
    t.Conversation = r.Conversation;
    var o = n(23);
    t.Message = o.Message;
    var i = n(31);
    t.Messenger = i.Messenger
        }]);

 

   

 