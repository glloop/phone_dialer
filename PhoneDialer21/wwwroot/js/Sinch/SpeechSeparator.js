﻿class SpeechSeparator {

    mRecorder = null;
    sourceNode = null;
    fftBins = null;
    analyser = null;

    listenToStream(stream, options) {
        var audioContextType = window.webkitAudioContext ||
            window.AudioContext;

        var listener = this;
        //var this.mRecorder;
        //var this.sourceNode;
        //var this.fftBins, this.analyser;
        listener.events = {};

        listener.on = (event, callback) => {
            listener.events[event] = callback;
        };

        listener.emit = () => {
            if (listener.events[arguments[0]]) {
                listener.events[arguments[0]](arguments[1], arguments[2], arguments[3], arguments[4]);
            }
        };

        if (!audioContextType) return listener;

        options = options || {};

        var smoothing = (options.smoothing || 0.1),
            interval = (options.interval || 50),
            threshold = options.threshold,
            play = options.play,
            history = options.history || 10,
            running = true;


        if (!window.audioContext00) window.audioContext00 = new audioContextType();

        var gainNode = audioContext00.createGain();
        gainNode.connect(audioContext00.destination);
        gainNode.gain.value = options.gainValue || 0;
        this.analyser = audioContext00.createAnalyser();
        this.analyser.fftSize = options.fftSize || 512;
        this.analyser.smoothingTimeConstant = smoothing;
        this.fftBins = new Float32Array(this.analyser.fftSize);
        this.sourceNode = audioContext00.createMediaStreamSource(stream);
        threshold = threshold || -50;

        this.sourceNode.connect(this.analyser);

        if (play) this.analyser.connect(audioContext00.destination);

        listener.speaking = false;

        listener.setThreshold = (t) => {
            threshold = t;
        };

        listener.setInterval = (i) => {
            interval = i;
        };

        listener.stop = () => {
            running = false;
            listener.emit('volume_change', options.volumeСhange || -100, threshold);
            if (listener.speaking) {
                listener.speaking = false;
                listener.emit('stopped_speaking');
            }
        };

        listener.speakingHistory = [];

        for (var i = 0; i < history; i++) {
            listener.speakingHistory.push(0);
        }

        var looper = () => {
            setTimeout(() => {

                if (!running) return;

                var currentVolume = getMaxVolume(this.analyser, this.fftBins);
                listener.emit('volume_change', currentVolume, threshold);

                var history = 0;
                if (currentVolume > threshold && !listener.speaking) {
                    for (var i = listener.speakingHistory.length - 3; i < listener.speakingHistory.length; i++) {
                        history += listener.speakingHistory[i];
                    }
                    if (history >= 2) {
                        listener.speaking = true;
                        this.mRecorder = new Recorder(this.sourceNode, { numChannels: 1, type: "audio/wav" });
                        this.mRecorder && this.mRecorder.record();
                        listener.emit('speaking');
                    }
                } else if (currentVolume < threshold && listener.speaking) {

                    for (var j = 0; j < listener.speakingHistory.length; j++) {
                        history += listener.speakingHistory[j];
                    }

                    if (history === 0) {
                        listener.speaking = false;
                        var base64;
                        this.mRecorder && this.mRecorder.stop();
                        this.mRecorder && this.mRecorder.exportWAV((blob) => {
                            if (options.audioStream) {
                                if (options.audioStream.parse && options.audioStream.callback) {
                                    this._convertBlobToBase64(blob, options.audioStream.callback);
                                    if (options.audioStream.downloadAudioExcerpts) this._downloadAudioFileFromBlob(blob);
                                } else {
                                    console.log("Attention! The parsing parameter or callback method is not set. Check the configuration object.");
                                }
                            } else {
                                console.log("Attention! The audio stream configuration object is not set, if you are trying to parse the audio stream - add the configuration object for the audio stream.");
                            }

                            this.mRecorder.clear();
                            }, "audio/wav");
                     

                        listener.emit('stopped_speaking', base64);
                    }
                }
                listener.speakingHistory.shift();
                listener.speakingHistory.push(0 + (currentVolume > threshold));

                looper();
            }, interval);
        };
        looper();

        function getMaxVolume(analyser, fftBins) {
            var maxVolume = -Infinity;
            analyser.getFloatFrequencyData(fftBins);

            for (var i = 4, ii = fftBins.length; i < ii; i++) {
                if (fftBins[i] > maxVolume && fftBins[i] < 0) {
                    maxVolume = fftBins[i];
                }
            }

            return maxVolume;
        }

        return listener;
    }
    _downloadAudioFileFromBlob(blob) {
        var clipName = "audio_blob";
        var clipContainer = document.createElement('article');
        var clipLabel = document.createElement('p');
        var audio = document.createElement('audio');
        var deleteButton = document.createElement('button');
        clipContainer.classList.add('clip');
        audio.setAttribute('controls', '');
        deleteButton.innerHTML = "Delete";
        clipLabel.innerHTML = clipName;
        clipContainer.appendChild(audio);
        clipContainer.appendChild(clipLabel);
        clipContainer.appendChild(deleteButton);
        var audioURL = window.URL.createObjectURL(blob);
        audio.src = audioURL;
        var temporaryElement = document.createElement("a");
        document.body.appendChild(temporaryElement);
        temporaryElement.style = "display: none";
        var url = window.URL.createObjectURL(blob);
        temporaryElement.href = url;
        temporaryElement.download = clipName + new Date().toISOString() + '.wav';
        temporaryElement.click();
        window.URL.revokeObjectURL(url);
    }
    _convertBlobToBase64(blob, callback) {
        var reader = new FileReader();
        reader.onload = () => {
            var dataUrl = reader.result;
            var base64 = dataUrl.split(',')[1];
            callback(base64);
        };
        reader.readAsDataURL(blob);        
    }
}