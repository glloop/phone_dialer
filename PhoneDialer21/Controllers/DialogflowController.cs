﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Dialogflow.v2beta1;
using Google.Apis.Dialogflow.v2beta1.Data;
using Google.Apis.Services;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.AspNetCore.Mvc;
using NAudio.Wave;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SoxSharp;

namespace PhoneDialer21.Controllers
{
    public class DialogflowController : Controller
    {
        /// <summary>
        /// System.Net.Http variable to use HttpClient
        /// </summary>
        private static readonly HttpClient _Client = new HttpClient();

        /// <summary>
        /// Person's GUID for DF identity
        /// </summary>
        private static Guid personGuid = Guid.NewGuid();

        /// <summary>
        /// Class for parsing voice string (wav byte array from FRONT END)
        /// </summary>
        public class VoiceString
        {
            public string strByteArray { get; set; }
        }

        /// <summary>
        /// Reducing the frequency of the input byte array, it is necessary to prepare the incoming sound before sending it to the DF
        /// </summary>
        /// <param name="fileBytes">from JS FRONT incoming audioStream byte Array - wav, 1 chanel, 48000 sampleHertz</param>
        /// <returns>output Byte Array - </returns>
        private static async Task<byte[]> DecreasingFrequencyAsync(byte[] fileBytes)
        {
            byte[] outputByteArray = null;
            var target = Path.GetTempPath() + Guid.NewGuid().ToString() + "target.wav";
            var output = Path.GetTempPath() + Guid.NewGuid().ToString() + "output.wav";
            var secondOutput = Path.GetTempPath() + Guid.NewGuid().ToString() + "output2.mp3";

            try
            {
                var writeBytesTask = System.IO.File.WriteAllBytesAsync(target, fileBytes);
#if DEBUG
                var soxPath = $"{Environment.CurrentDirectory}\\wwwroot\\sox-14-4-2\\sox.exe";
#else
                var soxPath = Path.Combine(Environment.CurrentDirectory + "/wwwroot","sox-14-4-2/sox.exe");
#endif
                using (var sox = new Sox(soxPath))
                {
                    sox.Output.Type = FileType.MP3;
                    sox.Output.SampleRate = 16000;
                    await writeBytesTask;
                    sox.Process(new InputFile(target), secondOutput);

                    using (var mp3FileReader = new Mp3FileReader(secondOutput))
                    {
                        using (var converter = WaveFormatConversionStream.CreatePcmStream(mp3FileReader))
                        using (var upsampler = new WaveFormatConversionStream(new WaveFormat(16000, 16, 1), converter))
                        {
                            WaveFileWriter.CreateWaveFile(output, upsampler);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error into ConcatenateWAV method: " + ex.Message + " | " + ex.InnerException == null ? "" : ex.InnerException.Message);
            }
            finally
            {
                var outputByteArrayTask = System.IO.File.ReadAllBytesAsync(output);
                new FileInfo(target).Delete();               
                new FileInfo(secondOutput).Delete();
                outputByteArray = await outputByteArrayTask;
                new FileInfo(output).Delete();
            }

            return outputByteArray;
        }

        public async Task<ServiceAccountCredential> ServiceAccountCredentialCreateTest(string jsonPath)
        {
            var jsonKeyTask = System.IO.File.ReadAllTextAsync(jsonPath);
            var credentials = JsonConvert.DeserializeObject(await jsonKeyTask) as JObject;

            return new ServiceAccountCredential(new ServiceAccountCredential.Initializer(credentials.GetValue("client_email").ToString())
            {
                Scopes = new[] { "https://www.googleapis.com/auth/cloud-platform" }
            }.FromPrivateKey(credentials.GetValue("private_key").ToString()));
        }

        public async Task<string> SendAudio([FromQuery]string projectId, [FromQuery]string apiKey, [FromQuery]string jsonPath, [FromQuery]string langCode = "en")
        {
            try
            {
                var serviceAccountCredentialCreateTask = ServiceAccountCredentialCreateTest(jsonPath);

                byte[] audioBytes = null;
                var jsonVoiceByteArray = "";
                var _audioConfig = new { audioEncoding = "MP3", pitch = "0.40", speakingRate = "1.0" };
                var _voice = new { languageCode = "en-US" };
                var url = "https://texttospeech.googleapis.com/v1beta1/text:synthesize?key=" + "AIzaSyBO7qBrtGtcdGXEJj_mp7xP1Pz28zzJf8U";
                var userRequest = Request.HttpContext.Request;
                userRequest.EnableRewind();

                using (var reader = new StreamReader(userRequest.Body, Encoding.UTF8, true, 1024, true))
                {
                    jsonVoiceByteArray = await reader.ReadToEndAsync();
                }

                userRequest.Body.Position = 0;
                var voiceArray = JsonConvert.DeserializeObject<VoiceString>(jsonVoiceByteArray);
                var fileBytes = voiceArray != null ? Convert.FromBase64String(voiceArray.strByteArray) : null;
                audioBytes = await DecreasingFrequencyAsync(fileBytes);

                var service = new DialogflowService(new BaseClientService.Initializer()
                {
                    ApiKey = apiKey,
                    HttpClientInitializer = await serviceAccountCredentialCreateTask
                });

                var acc = service.Projects.Agent.Sessions.DetectIntent(new GoogleCloudDialogflowV2beta1DetectIntentRequest()
                {
                    QueryInput = new GoogleCloudDialogflowV2beta1QueryInput()
                    {
                        AudioConfig = new GoogleCloudDialogflowV2beta1InputAudioConfig()
                        {
                            LanguageCode = langCode,
                            SampleRateHertz = 16000,
                            AudioEncoding = "AUDIO_ENCODING_LINEAR_16",
                            Model = "phone_call"
                        }
                    },
                    InputAudio = Convert.ToBase64String(audioBytes)
                }, $"projects/{projectId}/agent/sessions/{personGuid}");


                var dfTextAnswerTask = acc.ExecuteAsync();
                var httpRequestMessage = new HttpRequestMessage();
                httpRequestMessage.Method = HttpMethod.Post;
                httpRequestMessage.RequestUri = new Uri(url);
                var httpContent =
                    new StringContent(
                        JsonConvert.SerializeObject(new
                        {
                            audioConfig = _audioConfig,
                            input = new { text = (await dfTextAnswerTask).QueryResult.FulfillmentText ?? "Sorry, I don't understand. Voice from back-end." },
                            voice = _voice
                        }),
                    Encoding.UTF8,
                    "application/json");

                httpRequestMessage.Content = httpContent;

                return await (await _Client.SendAsync(httpRequestMessage)).Content.ReadAsStringAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error while ExecuteAsync, error: " + ex.Message + " ||| " + ex.InnerException == null ? "" : ex.InnerException.Message);
                return "";
            }
        }

    }
}